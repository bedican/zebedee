<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\paypal\ipn;

use plugin\paypal\PaypalException;
use framework\net\Request;
use framework\exception\IllegalArgumentException;
use framework\db\DatabaseManager;
use framework\application\Application;

class PaypalIpnService
{
	private $ipnUrl;
	private $application;
	private $dbm;

	private $defaultHandler;
	private $handlers;
	
	public function __construct($application, $dbm)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(! $dbm instanceof DatabaseManager) {
			throw new IllegalArgumentException('$dbm is not of type DatabaseManager');
		}

		$this->ipnUrl = $application->getPlugin('paypal')->getConfigValue('ipn-url');
		$this->application = $application;
		$this->dbm = $dbm;

		$this->defaultHandler = null;
		$this->handlers = array();
		
		$this->init();
	}
	
	protected function init() {}
	
	protected function getApplication()
	{
		return $this->application;
	}
	
	protected function getDatabaseManager()
	{
		return $this->dbm;
	}
	
	protected function onValidateRequestFailure($request)
	{
		throw new PaypalException('Failed ipn request callback validation');
	}
	
	protected function onValidateRequestSuccess($request)
	{
	}
	
	public function setDefaultHandler($handler)
	{
		if(! $handler instanceof PaypalIpnHandler) {
			throw new IllegalArgumentException('$handler is not of type PaypalIpnHandler');
		}
		
		$this->defaultHandler = $handler;
	}
	
	public function addHandler($txType, $handler)
	{
		if(!is_string($txType)) {
			throw new IllegalArgumentException('$txType is not of type string');
		}
		if(! $handler instanceof PaypalIpnHandler) {
			throw new IllegalArgumentException('$handler is not of type PaypalIpnHandler');
		}
		
		$this->handlers[$txType] = $handler;
	}
	
	public function process($request)
	{
		if(! $request instanceof Request) {
			throw new IllegalArgumentException('$request is not of type Request');
		}
		
		if(!$this->validateRequest($request)) {
			$this->onValidateRequestFailure($request);
			return false;
		}
		
		$this->onValidateRequestSuccess($request);

		$txType = $request->getPostValue('txn_type');
		
		if(isset($this->handlers[$txType])) {
			$success = $this->handlers[$txType]->process($request);
		} else if($this->defaultHandler) {
			$success = $this->defaultHandler->process($request);
		}
		
		return (isset($success)) ? $success : true;
	}
	
	protected function validateRequest($request)
	{
		// Verify the notification was sent from paypal
		$data = $request->getPostValues();
		$web = parse_url($this->ipnUrl);

		$postdata = '';
		foreach($data as $i => $v) { 
			$postdata .= $i.'='.urlencode($v).'&';
		}
		
		$postdata .= 'cmd=_notify-validate';

		if($web['scheme'] == "https") {
			$web['port'] = "443"; 
			$ssl="ssl://"; 
		} else {
			$web['port']="80";
		} 
		
		//Create paypal connection
		if(!($fp = @fsockopen($ssl.$web['host'], $web['port'], $errnum, $errstr, 10))) {
			$this->error = $errnum.': '.$errstr;
			return false;
		}

		fputs($fp, "POST {$web['path']} HTTP/1.1\r\n"); 
		fputs($fp, "Host: {$web['host']}\r\n"); 
		fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n"); 
		fputs($fp, "Content-length: ".strlen($postdata)."\r\n"); 
		fputs($fp, "Connection: close\r\n\r\n"); 
		fputs($fp, $postdata . "\r\n\r\n");
		
		// loop through the response from the server 
		while(!feof($fp)) {
			$info[] = @fgets($fp, 1024); 
		}

		fclose($fp);

		// break up results into a string
		$info = implode(',', $info);
		
		// check result
		if(eregi('VERIFIED', $info)) {
			return true;
		}

		return false;
	}
}