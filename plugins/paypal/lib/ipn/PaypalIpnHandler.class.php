<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\paypal\ipn;

use framework\db\DatabaseManager;
use framework\application\Application;
use framework\net\Request;
use framework\exception\IllegalArgumentException;

abstract class PaypalIpnHandler
{
	private $application;
	private $dbm;
	
	public function __construct($application, $dbm)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(! $dbm instanceof DatabaseManager) {
			throw new IllegalArgumentException('$dbm is not of type DatabaseManager');
		}
		
		$this->application = $application;
		$this->dbm = $dbm;
		
		$this->init();
	}

	abstract protected function doProcess($request);

	protected function init() {}
	
	protected function getApplication()
	{
		return $this->application;
	}
	
	protected function getDatabaseManager()
	{
		return $this->dbm;
	}
	
	public function process($request)
	{
		if(! $request instanceof Request) {
			throw new IllegalArgumentException('$request is not of type Request');
		}
		
		$success = $this->doProcess($request);
		return (isset($success)) ? $success : true;
	}
}