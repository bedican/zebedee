<?php // (c) Copyright 2013 Bedican Solutions

namespace plugin\paypal\nvp;

use framework\exception\IllegalArgumentException;

class NvpResponse
{
	private $response;

	public function __construct($response)
	{
		if(!is_array($response)) {
			throw new IllegalArgumentException('$response is not of type array');
		}
		
		$this->response = $response;
	}
	
	public function getValue($name, $default = null)
	{
		if(!array_key_exists($name, $this->response)) {
			return $default;
		}
		
		return $this->response[$name];
	}
	
	public function isSuccess()
	{
		return ($this->getValue('ACK') == 'Success');
	}
	
	public function getError()
	{
		return $this->getValue('L_LONGMESSAGE0', '');
	}
}