<?php // (c) Copyright 2013 Bedican Solutions

namespace plugin\paypal\nvp;

class SetExpressCheckoutRequest extends NvpRequest
{
	protected function init()
	{
		// https://developer.paypal.com/webapps/developer/docs/classic/express-checkout/gs_expresscheckout/
		
		$this->setParam('METHOD', 'SetExpressCheckout');
		$this->setParam('PAYMENTREQUEST_0_PAYMENTACTION', 'Sale');
		$this->setParam('VERSION', '93');
		$this->setParam('ALLOWNOTE', '0');
	}
	
	public function setAmmount($amount)
	{
		$this->setParam('PAYMENTREQUEST_0_AMT', number_format($amount, 2));
	}
	
	public function setCurrencyCode($currency)
	{
		$this->setParam('PAYMENTREQUEST_0_CURRENCYCODE', $currency);
	}
	
	public function setReturnUrl($url)
	{
		$this->setParam('RETURNURL', $url);
	}

	public function setCancelUrl($url)
	{
		$this->setParam('CANCELURL', $url);
	}
	
	public function setNoShipping($noShipping = true)
	{
		$this->setParam('NOSHIPPING', $noShipping ? '1' : '0');
	}
	
	public function setAllowNote($allowNote = true)
	{
		$this->setParam('ALLOWNOTE', $allowNote ? '1' : '0');
	}
}