<?php // (c) Copyright 2013 Bedican Solutions

namespace plugin\paypal\nvp;

use plugin\paypal\PaypalException;
use framework\application\Application;

abstract class NvpRequest
{
	private $apiUrl;
	private $username;
	private $password;
	private $signature;
	
	private $application;
	
	private $parameters;

	public function __construct($application, $dbm)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		
		$this->application = $application;
		$plugin = $application->getPlugin('paypal');
		
		$config = $this->getApplication()->getConfig();

		$this->apiUrl = $plugin->getConfigValue('nvp-api-url');
		$this->username = $config->getValue('paypal.nvp-api.username');
		$this->password = $config->getValue('paypal.nvp-api.password');
		$this->signature = $config->getValue('paypal.nvp-api.signature');
		
		$this->parameters = array();

		$this->init();
	}
	
	// Default empty implementation
	protected function init() {}
	
	protected function getApplication()
	{
		return $this->application;
	}
	
	protected function getApiUrl()
	{
		return $this->apiUrl;
	}
	
	protected function getUsername()
	{
		return $this->username;
	}
	
	protected function getPassword()
	{
		return $this->password;
	}
	
	protected function getSignature()
	{
		return $this->signature;
	}
	
	protected function setParam($name, $value)
	{
		$this->parameters[$name] = $value;
	}
	
	protected function getParam($name, $default = null)
	{
		if(!array_key_exists($name, $this->parameters)) {
			return $default;
		}
		
		return $this->parameters[$name];
	}
	
	protected function getAuthParams()
	{
		return array(
			'USER' => $this->username,
			'PWD' => $this->password,
			'SIGNATURE' => $this->signature
		);
	}
	
	public function send()
	{
		$postData = array_merge($this->parameters, $this->getAuthParams());

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->apiUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,  http_build_query($postData));

		$result = curl_exec($ch);

		if(!$result) {
			$error = 'Failed to execute curl: ('.curl_errno($ch).') '.curl_error($ch);
		}
		
		curl_close($ch);

		if(!$result) {
			throw new PaypalException($error);
		}
		
		$response = array();
		parse_str($result, $response);
		
		$response = array_map('urldecode', $response);

		return new NvpResponse($response);
	}
}