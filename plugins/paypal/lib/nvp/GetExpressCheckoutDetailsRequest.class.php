<?php // (c) Copyright 2013 Bedican Solutions

namespace plugin\paypal\nvp;

class GetExpressCheckoutDetailsRequest extends NvpRequest
{
	protected function init()
	{
		$this->setParam('METHOD', 'GetExpressCheckoutDetails');
		$this->setParam('VERSION', '93');
	}
	
	public function setToken($token)
	{
		$this->setParam('TOKEN', $token);
	}
}