<?php // (c) Copyright 2013 Bedican Solutions

namespace plugin\paypal\nvp;

class DoExpressCheckoutPaymentRequest extends NvpRequest
{
	protected function init()
	{
		$this->setParam('METHOD', 'DoExpressCheckoutPayment');
		$this->setParam('PAYMENTREQUEST_0_PAYMENTACTION', 'Sale');
		$this->setParam('VERSION', '93');
	}
	
	public function setToken($token)
	{
		$this->setParam('TOKEN', $token);
	}
	
	public function setPayerId($payerId)
	{
		$this->setParam('PAYERID', $payerId);
	}
	
	public function setAmmount($amount)
	{
		$this->setParam('PAYMENTREQUEST_0_AMT', number_format($amount, 2));
	}
	
	public function setCurrencyCode($currency)
	{
		$this->setParam('PAYMENTREQUEST_0_CURRENCYCODE', $currency);
	}
}
