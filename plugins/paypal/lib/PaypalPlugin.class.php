<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\paypal;

use framework\plugin\Plugin;

class PaypalPlugin extends Plugin
{
	private $isSandbox;
	
	protected function configure()
	{
		$config = $this->getApplication()->getConfig();
		$this->isSandbox = ($config->getValue('paypal.sandbox') == 'true');	
	}
	
	public function isSandbox()
	{
		return $this->isSandbox;
	}
	
	public function getConfigValue($name, $default = null)
	{
		$configNs = $this->isSandbox ? 'sandbox' : 'prod';
		return $this->getConfig()->getValue($configNs.'.'.$name, $default);
	}
}