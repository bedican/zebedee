<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\dbTaskSchedule;

use framework\plugin\Plugin;

class DbTaskSchedulePlugin extends Plugin
{
	protected function getDependencies()
	{
		return array('mySql mongo');
	}
}