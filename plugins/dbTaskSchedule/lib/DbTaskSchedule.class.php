<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\dbTaskSchedule;

use framework\task\TaskSchedule;
use framework\task\ScheduledTask;
use framework\db\DatabaseManager;

abstract class DbTaskSchedule extends TaskSchedule
{
	private $taskAliases = array();
	
	abstract protected function initTaskAliases();
	
	protected function init()
	{
		$this->initTaskAliases();
		
		$dbm = new DatabaseManager($this->getApplication());
		$dao = $dbm->getDao('task-schedule');
		
		$schedules = $dao->getGroupedSchedules();
		
		foreach($schedules as $name => $schedule) {
			if(!array_key_exists($name, $this->taskAliases)) {
				$this->getApplication()->getLogger()->message('Unknown task alias: '.$name);
				continue;
			}

			$this->addTask($this->taskAliases[$name], $schedule);
		}
	}
	
	protected function addTaskAlias($name, $module, $action)
	{
		$this->taskAliases[$name] = new ScheduledTask($module, $action);
	}
}