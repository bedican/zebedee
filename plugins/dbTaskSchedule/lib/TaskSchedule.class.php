<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\dbTaskSchedule;

class TaskSchedule
{
	private $id;
	private $name;
	private $schedule;

	public function getId()
	{
		return $this->id;
	}
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function getName()
	{
		return $this->name;
	}
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function getSchedule()
	{
		return $this->schedule;
	}
	public function setSchedule($schedule)
	{
		$this->schedule = $schedule;
	}
}