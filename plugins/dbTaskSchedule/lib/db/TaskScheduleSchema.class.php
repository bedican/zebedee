<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\dbTaskSchedule\db;

use framework\db\schema\Schema;

class TaskScheduleSchema extends Schema
{
	protected function init()
	{
		$this->addPrimaryKeyField('id', true);
		$this->addField('name');
		$this->addField('schedule');
	}
}