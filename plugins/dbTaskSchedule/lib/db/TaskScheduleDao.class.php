<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\dbTaskSchedule\db;

use framework\db\dao\AbstractDao;

class TaskScheduleDao extends AbstractDao
{
	public function getGroupedSchedules()
	{
		$schedules = $this->retrieve()->execute();

		$grouped = array();
		foreach($schedules as $schedule) {
			
			$name = $schedule->getName();
			if(!array_key_exists($name, $grouped)) {
				$grouped[$name] = array();
			}

			$grouped[$name][] = $schedule->getSchedule();
		}

		return $grouped;
	}
}