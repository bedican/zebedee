<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\pager;

use framework\exception\IllegalArgumentException;

class Pager
{
	private $page;
	private $pagesize;
	private $count;
	
	public function __construct($page, $pagesize, $count)
	{
		if(!is_int($page)) {
			throw new IllegalArgumentException('$page is not of type integer');
		}
		if(!is_int($pagesize)) {
			throw new IllegalArgumentException('$pagesize is not of type integer');
		}
		if(!is_int($count)) {
			throw new IllegalArgumentException('$count is not of type integer');
		}

		$this->page = $page;
		$this->pagesize = $pagesize;
		$this->count = $count;
	}
	
	public function hasPages()
	{
		return ($this->getPages() > 1);
	}

	public function getPage()
	{
		return $this->page;
	}
	
	public function getPagesize()
	{
		return $this->pagesize;
	}
	
	public function getCount()
	{
		return $this->count;
	}
	
	public function getPages()
	{
		$pages = intval($this->count / $this->pagesize);
		if(($this->count % $this->pagesize) != 0) {
			$pages++;
		}

		return $pages; 
	}
}