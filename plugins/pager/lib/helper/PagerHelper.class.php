<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\pager\helper;

use plugin\pager\Pager;
use framework\view\helper\AbstractHelper;
use framework\exception\IllegalArgumentException;

class PagerHelper extends AbstractHelper
{
	private $routing;
	
	public function __construct($routing)
	{
		$this->routing = $routing;
	}
	
	protected function getMethods()
	{
		return array('pager');
	}
	
	protected function renderPager($renderer, $vars)
	{
		print($renderer->render(dirname(__FILE__).'/templates/pager.php', $vars));
	}
	
	protected function pager($renderer, $pager, $route, $urlParams = array())
	{
		if(!$pager instanceof Pager) {
			throw new IllegalArgumentException('$pager is not of type Pager');
		}
		
		if($pager->getPages() <= 1) {
			return;
		}
		
		$previousPage = $pager->getPage() - 1;
		$previousPage = ($previousPage < 1) ? 1 : $previousPage;
		
		$nextPage = $pager->getPage() + 1;
		$nextPage = ($nextPage > $pager->getPages()) ? $pager->getPages() : $nextPage;
		
		$pageStart = $pager->getPage() - 3;
		$pageStart = ($pageStart < 1) ? 1 : $pageStart;
		$pageStart = ($pageStart > 3) ? 3 : $pageStart;

		$pageEnd = $pager->getPage() + 3;
		$pageEnd = ($pageEnd > $pager->getPages()) ? $pager->getPages() : $pageEnd;

		$locationPrefix = $this->routing->getUrl($route);

		if(sizeof($urlParams)) {
			
			$query = array();
			foreach($urlParams as $key => $value) {
				$query[] = $key.'='.$value;
			}
			
			$locationPrefix .= '?'.implode('&', $query).'&page=';
			
		} else {
			$locationPrefix .= '?page=';
		}
		
		$this->renderPager($renderer, array(
			'locationPrefix' 	=> $locationPrefix,
			'currentPage' 		=> $pager->getPage(),
			'lastPage' 			=> $pager->getPages(),
			'previousPage' 		=> $previousPage,
			'nextPage'			=> $nextPage,
			'pageStart'			=> $pageStart,
			'pageEnd'			=> $pageEnd
		));
	}
}