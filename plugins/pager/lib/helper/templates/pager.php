<div class="pager">

  <a href="<?= $locationPrefix ?>1">
    <img src="/images/ui/first.png" alt="First page" title="First page" />
  </a>

  <a href="<?= $locationPrefix ?><?= $previousPage ?>">
  	<img src="/images/ui/previous.png" alt="Previous page" title="Previous page" />
  </a>
  
  <?php for($x = $pageStart; $x <= $pageEnd; $x++) { ?>
  	<?php if($currentPage == $x) { ?>
  		<span class="current"><?= $x ?></span>
  	<?php } else { ?>
  		<a href="<?= $locationPrefix ?><?= $x ?>"><?= $x ?></a>
  	<?php } ?>
  <?php } ?>

  <a href="<?= $locationPrefix ?><?= $nextPage ?>">
    <img src="/images/ui/next.png" alt="Next page" title="Next page" />
  </a>

  <a href="<?= $locationPrefix ?><?= $lastPage ?>">
    <img src="/images/ui/last.png" alt="Last page" title="Last page" />
  </a>

  <div class="page-of">(page <?= $currentPage ?>/<?= $lastPage ?>)</div>

</div>