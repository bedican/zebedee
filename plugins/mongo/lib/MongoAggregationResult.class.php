<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\mongo;

use framework\db\result\Result;
use framework\db\result\AggregatedResultRow;
use framework\exception\IllegalArgumentException;

class MongoAggregationResult implements Result
{
	private $result;
	private $criteria;

	public function __construct($result, $criteria)
	{
		if(!is_array($result)) {
			throw new IllegalArgumentException('$result is not of type array');
		}
		if(! $criteria instanceof MongoSelectCriteria) {
			throw new IllegalArgumentException('$criteria is not of type MongoSelectCriteria');
		}
		
		$this->result = $result;
		$this->criteria = $criteria;
	}
	
	public function getNextRow()
	{
		$document = array_shift($this->result);
		
		if(!is_array($document)) {
			return false;
		}

		$row = new AggregatedResultRow();
		
		if($this->criteria->hasGroup()) {
			$group = $this->criteria->getGroup();
			foreach($group as $field) {
				$row->addGroupBy($field, $document[$field]);
			}
		}

		if($this->criteria->hasSumFields()) {
			$fields = $this->criteria->getSumFields();
			foreach($fields as $alias => $field) {
				if(array_key_exists($alias, $document)) {
					$row->setSum($field, $document[$alias]);
				}
			}
		}

		if($this->criteria->hasMinFields()) {
			$fields = $this->criteria->getMinFields();
			foreach($fields as $alias => $field) {
				if(array_key_exists($alias, $document)) {
					$row->setMin($field, $document[$alias]);
				}
			}
		}

		if($this->criteria->hasMaxFields()) {
			$fields = $this->criteria->getMaxFields();
			foreach($fields as $alias => $field) {
				if(array_key_exists($alias, $document)) {
					$row->setMax($field, $document[$alias]);
				}
			}
		}

		if($this->criteria->hasAvgFields()) {
			$fields = $this->criteria->getAvgFields();
			foreach($fields as $alias => $field) {
				if(array_key_exists($alias, $document)) {
					$row->setAvg($field, $document[$alias]);
				}
			}
		}

		return $row;
	}
}