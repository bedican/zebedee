<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\mongo;

use framework\db\criteria\SelectCriteria;
use framework\db\result\AggregatedResultRow;
use framework\exception\IllegalArgumentException;
use MongoId;

class MongoMapReduceResult extends MongoResult
{
	private function processValue($value)
	{
		if($value instanceof MongoId) {
			return $value->__toString();
		}
		
		return $value;
	}
	
	protected function processDocument($document)
	{
		if(!array_key_exists('_id', $document)) {
			throw new MongoResultException('Missing _id from map reduce result');
		}
		if(!array_key_exists('value', $document)) {
			throw new MongoResultException('Missing value from map reduce result');
		}
		
		$keys = $document['_id'];
		$values = $document['value'];

		$row = new AggregatedResultRow();
		
		if(is_array($keys)) {
			foreach($keys as $key => $value) {
				$row->addGroupBy($key, $this->processValue($value));
			}	
		}
		
		if(array_key_exists('sum', $values)) {			
			$sum = $values['sum'];
			foreach($sum as $field => $value) {
				$row->setSum($field, $this->processValue($value));
			}
		}

		if(array_key_exists('min', $values)) {		
			$min = $values['min'];
			foreach($min as $field => $value) {
				$row->setMin($field, $this->processValue($value));
			}
		}
		
		if(array_key_exists('max', $values)) {			
			$max = $values['max'];
			foreach($max as $field => $value) {
				$row->setMax($field, $this->processValue($value));
			}
		}
		
		if(array_key_exists('avg', $values)) {	
			$avgs = $values['avg'];
			foreach($avg as $field => $value) {
				$row->setAvg($field, $this->processValue($value));
			}
		}

		return $row;		
	}
}