<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\mongo;

use framework\db\driver\AbstractDriver;
use framework\db\criteria\Criterion;
use framework\db\criteria\SelectCriteria;
use framework\db\criteria\DeleteCriteria;
use framework\db\criteria\UpdateCriteria;
use framework\exception\IllegalArgumentException;
use Mongo, MongoId, MongoRegex, MongoConnectionException, MongoCursorException, MongoCursorTimeoutException, MongoCode;

class MongoDriver extends AbstractDriver
{
	private $mongo = null;
	private $db = null;
	
	private $connectTimeout = 4000;
	private $selectTimeout = 4000;
	private $updateTimeout = 4000;
	private $enableUpsert = false;
	private $replicaSet = false;
	private $useMapReduce = false;
	private $keepMapReduceCollections = false;
	
	private $mapReduceCollections = array();

	protected function init()
	{
		$config = $this->getDriverConfig();

		if(ctype_digit($value = $config->getParameter('connect-timeout'))) {
			$this->connectTimeout = intval($value);
		}
		if(ctype_digit($value = $config->getParameter('select-timeout'))) {
			$this->selectTimeout = intval($value);
		}
		if(ctype_digit($value = $config->getParameter('update-timeout'))) {
			$this->updateTimeout = intval($value);
		}
		if(is_string($value = $config->getParameter('enable-upsert'))) {
			$this->enableUpsert = ($value == 'true');
		}
		if(is_string($value = $config->getParameter('replica-set'))) {
			$this->replicaSet = $value;
		}
		if(is_string($value = $config->getParameter('aggregate-with-map-reduce'))) {
			$this->useMapReduce = ($value == 'true');
		}
		if(is_string($value = $config->getParameter('keep-map-reduce-collections'))) {
			$this->keepMapReduceCollections = ($value == 'true');
		}
	}
	
	public function newSelectCriteria()
	{
		return new MongoSelectCriteria();
	}

	private function isMongoId($value)
	{
		return ((is_string($value)) && (preg_match('#[0-9a-f]{24}#i', $value) == 1));
	}
	
	private function toMongoId($id)
	{
		return ($this->isMongoId($id)) ? new MongoId($id) : $id;
	}

	public function shutdown()
	{
		if($this->mongo) {
			
			if(!$this->keepMapReduceCollections) {
				foreach($this->mapReduceCollections as $collection) {
					$this->getCollection($collection)->drop();
				}
			}

			$this->mongo->close();
		}
	}
	
	private function connect()
	{
		if($this->db != null) {
			return;
		}

		$dsn = $this->getDsn();
		
		if($dsn->getDriver() != 'mongodb') {
			throw new MongoDriverException('Invalid dsn, driver is not mongodb');
		}
		
		$server = 'mongodb://'.$dsn->getHost();
		$options = array('timeout' => $this->connectTimeout);

		if($username = $dsn->getUsername()) {
			$options['username'] = $username;
		}
		if($password = $dsn->getPassword()) {
			$options['password'] = $password;
		}
		if($this->replicaSet) {
			$options['replicaSet'] = $this->replicaSet;
		}

		$this->mongo = new Mongo($server, $options);
		$this->db = $this->mongo->selectDB($dsn->getDatabase());
	}

	private function getCollection($collection)
	{
		$this->connect();

		return $this->db->selectCollection($collection);
	}

	public function retrieve($criteria)
	{
		if(! $criteria instanceof SelectCriteria) {
			throw new IllegalArgumentException('$criteria is not of type SelectCriteria');
		}
		
		if(($criteria->isAggregated()) && (!$this->useMapReduce)) {
			return $this->retrieveAggregationResult($criteria);
		} else {
			return $this->retrieveWithCursor($criteria);
		}
	}
	
	private function retrieveWithCursor($criteria)
	{
		if(! $criteria instanceof SelectCriteria) {
			throw new IllegalArgumentException('$criteria is not of type SelectCriteria');
		}
		
		if($criteria->isAggregated()) {
			$cursor = $this->getCursorMapReduce($criteria);
		} else {
			$cursor = $this->getCursor($criteria);
		}
		
		if($criteria->hasOrder())
		{
			$sort = array();
			$order = $criteria->getOrder();

			foreach($order as $directionAndField) {
				
				list($direction, $field) = $directionAndField;
				
				if($direction == SelectCriteria::ORDER_ASC) {
					$sort[$field] = 1;
				} else if($direction == SelectCriteria::ORDER_DESC) {
					$sort[$field] = -1;
				}
			}

			if(sizeof($sort)) {
				$cursor->sort($sort);
			}
		}

		if($criteria->hasOffset()) {
			$cursor = $cursor->skip($criteria->getOffset());
		}
		if($criteria->hasLength()) {
			$cursor = $cursor->limit($criteria->getLength());
		}
		
		if($criteria->isAggregated()) {
			return new MongoMapReduceResult($cursor);
		} else {
			return new MongoResult($cursor);
		}
	}
	
	private function retrieveAggregationResult($criteria)
	{
		if(! $criteria instanceof SelectCriteria) {
			throw new IllegalArgumentException('$criteria is not of type SelectCriteria');
		}
		
		$pipeline = array();
		
		if($criterion = $criteria->getCriterion()) {
			$pipeline[] = array(
				'$match' => $this->convertCriterion($criterion) 
			);
		}
		
		if($criteria->hasGroup()) {
			$groupKeys = array();
			$groupFields = $criteria->getGroup();
			foreach($groupFields as $field) {
				$groupKeys[$field] = '$'.$field;
			}
		} else {
			$groupKeys = 1;
		}
		
		$group = array(
			'_id' => $groupKeys
		);
		
		if($criteria->hasSumFields()) {
			$fields = $criteria->getSumFields();
			foreach($fields as $alias => $field) {
				$group[$alias] = array('$sum' => '$'.$field);
			}
		}
		
		if($criteria->hasMinFields()) {
			$fields = $criteria->getMinFields();
			foreach($fields as $alias => $field) {
				$group[$alias] = array('$min' => '$'.$field);
			}
		}
		
		if($criteria->hasMaxFields()) {
			$fields = $criteria->getMaxFields();
			foreach($fields as $alias => $field) {
				$group[$alias] = array('$max' => '$'.$field);
			}
		}
		
		if($criteria->hasAvgFields()) {
			$fields = $criteria->getAvgFields();
			foreach($fields as $alias => $field) {
				$group[$alias] = array('$avg' => '$'.$field);
			}
		}
		
		$pipeline[] = array(
			'$group' => $group
		);	
		
		$havingQuery = array();
		if($criteria->hasHaving()) {
			if($havingCriterion = $criteria->getHaving()->getCriterion()) {
				$pipeline[] = array(
					'$match' => $this->convertCriterion($havingCriterion)
				);
			}
		}	

		if($criteria->hasOrder())
		{
			$sort = array();
			$order = $criteria->getOrder();
			foreach($order as $directionAndField) {

				list($direction, $field) = $directionAndField;
				
				if($direction == SelectCriteria::ORDER_ASC) {
					$sort[$field] = 1;
				} else if($direction == SelectCriteria::ORDER_DESC) {
					$sort[$field] = -1;
				}
			}
			
			if(sizeof($sort)) {
				$pipeline[] = array(
					'$sort' => $sort
				);
			}
		}
		
		if($criteria->hasOffset()) {
			$pipeline[] = array(
				'$skip' => $criteria->getOffset()
			);
		}
		
		if($criteria->hasLength()) {
			$pipeline[] = array(
				'$limit' => $criteria->getLength()
			);
		}

		$result = $this->getCollection($criteria->getCollection())->aggregate($pipeline);

		if($result['ok'] != 1) {
			throw new MongoDriverException('Failed to run aggregation: "'.$result['errmsg'].'"');
		}

		return new MongoAggregationResult($result['result'], $criteria);
	}

	public function count($criteria)
	{
		if(! $criteria instanceof SelectCriteria) {
			throw new IllegalArgumentException('$criteria is not of type SelectCriteria');
		}
		
		try {
			$collection = $this->getCollection($criteria->getCollection());
			
			$criterion = $criteria->getCriterion();
			$query = $criterion ? $this->convertCriterion($criterion) : array();

			return $collection->count($query);

		} catch(MongoConnectionException $mce) {
			throw new MongoDriverException('Could not connect to mongo', 1, $mce);
		}
	}

	private function getCursor($criteria)
	{
		if(! $criteria instanceof SelectCriteria) {
			throw new IllegalArgumentException('$criteria is not of type SelectCriteria');
		}
		
		try {
			$collection = $this->getCollection($criteria->getCollection());
			
			$criterion = $criteria->getCriterion();
			$query = $criterion ? $this->convertCriterion($criterion) : array();

			$fields = array_flip($criteria->getSelectedFields());
			foreach($fields as $key => $field) {
				$fields[$key] = true;
			}

			return $collection->find($query, $fields)->timeout($this->selectTimeout);

		} catch(MongoConnectionException $mce) {
			throw new MongoDriverException('Could not connect to mongo', 1, $mce);
		}
	}
	
	private function getCursorMapReduce($criteria)
	{
		if(! $criteria instanceof SelectCriteria) {
			throw new IllegalArgumentException('$criteria is not of type SelectCriteria');
		}
		
		// Consider generating a permanent collection using map-reduce within a background 
		// task and querying it in real time. It may yeild better performance in some instances.
		
		if($criteria->hasGroup()) {
			
			$mapGroupKeys = array();
			$groupFields = $criteria->getGroup();
			
			foreach($groupFields as $field) {
				$mapGroupKeys[] = 'this.'.$field;
			}

			$mapKeys = '{ '.implode(', ', $mapGroupKeys).' }';

		} else {
			$mapKeys = 1;
		}
		
		$mapFields = array(
			'count: 1'
		);
		
		if($criteria->hasSumFields()) {
			$mapSumFields = array();
			$sumFields = $criteria->getSumFields();
			
			foreach($sumFields as $alias => $field) {
				$mapSumFields[] = $field.': this.'.$field;
			}
			
			$mapFields[] = 'sum: { '.implode(', ', $mapSumFields).' }';
		}
		
		if($criteria->hasMaxFields()) {
			$mapMaxFields = array();
			$maxFields = $criteria->getMaxFields();
			
			foreach($maxFields as $alias => $field) {
				$mapMaxFields[] = $field.': this.'.$field;
			}
			
			$mapFields[] = 'max: { '.implode(', ', $mapMaxFields).' }';
		}
		
		if($criteria->hasMinFields()) {
			$mapMinFields = array();
			$minFields = $criteria->getMinFields();
			
			foreach($minFields as $alias => $field) {
				$mapMinFields[] = $field.': this.'.$field;
			}
			
			$mapFields[] = 'min: { '.implode(', ', $mapMinFields).' }';
		}
		
		if($criteria->hasAvgFields()) {
			$mapAvgFields = array();
			$avgFields = $criteria->getAvgFields();
			
			foreach($avgFields as $alias => $field) {
				$mapAvgFields[] = $field.': this.'.$field;
			}
			
			$mapFields[] = 'avg: { '.implode(', ', $mapAvgFields).' }';
		}

		// Create a map of all the selected sum fields
		$map = 'function() { emit( '.$mapKeys.', { '.implode(', ', $mapFields).' }); }';
		
		// Create the reduce function aggregation values
		$reduce = '
			function(key, values) {
	
				var result = values.shift();
				
				while(values.length > 0) {
					var value = values.shift();
					result.count += value.count;';
		
		if($criteria->hasSumFields()) {
			foreach($sumFields as $alias => $field) {
				$reduce .= '
					result.sum.'.$field.' += value.sum.'.$field.';';
			}
		}
		
		if($criteria->hasMaxFields()) {
			foreach($maxFields as $alias => $field) {
				$reduce .= '
					result.max.'.$field.' = Math.max(result.max.'.$field.', value.max.'.$field.');';
			}
		}

		if($criteria->hasMinFields()) {
			foreach($minFields as $alias => $field) {
				$reduce .= '
					result.min.'.$field.' = Math.min(result.min.'.$field.', value.min.'.$field.');';
			}
		}
		
		if($criteria->hasAvgFields()) {
			foreach($avgFields as $alias => $field) {
				$reduce .= '
					result.avg.'.$field.' += value.avg.'.$field.';';
			}
		}
		
		$reduce .= '
				}
	
				return result;
			}';
		
		// Create the finalize function to calculate the average
		$finalize = '
			function finalize(key, value) {';
		if($criteria->hasAvgFields()) {
			foreach($avgFields as $field) {
				$reduce .= '
					value.avg.'.$field.' = value.avg.'.$field.' / value.count;';
			}
		}
		$finalize .= '
				return value;
			}';

		// Create the query
		$criterion = $criteria->getCriterion();
		$query = $criterion ? $this->convertCriterion($criterion) : array();

		// Run map reduce, and store it in a quasi-temporary collection.
		$tempCollection = uniqid($criteria->getCollection().'_');
		$this->mapReduceCollections[] = $tempCollection;
		
		$result = $this->runCommand(array(
			'mapreduce' => $criteria->getCollection(),
			'map' => new MongoCode($map),
			'reduce' => new MongoCode($reduce),
			'finalize' => new MongoCode($finalize),
			'query' => $query,
			'out' => $tempCollection
		));
		
		if($result['ok'] != 1) {
			throw new MongoDriverException('Failed to run map reduce: "'.$result['errmsg'].'"');
		}
		
		// Add having
		$havingQuery = array();
		if($criteria->hasHaving()) {
			if($havingCriterion = $criteria->getHaving()->getCriterion()) {
				$havingQuery = $this->convertCriterion($havingCriterion);
				$havingQuery = $this->convertHavingValueForMapReduce($havingQuery, $criteria);
			}
		}
		
		return $this->getCollection($result['result'])->find($havingQuery)->timeout($this->selectTimeout);
	}
	
	private function convertHavingValueForMapReduce($value, $criteria)
	{
		if(!is_array($value)) {
			return $value;
		}
		
		$result = array();
		
		foreach($value as $field => $fieldValue) {
			$field = $this->convertHavingFieldForMapReduce($field, $criteria);
			$fieldValue = $this->convertHavingValueForMapReduce($fieldValue, $criteria);
			
			$result[$field] = $fieldValue;
		}
		
		return $result;
	}
	
	private function convertHavingFieldForMapReduce($field, $criteria)
	{
		// Convert the field to match the map reduce output.
		// We also need to substitute the alias for their real values.
				
		if(substr($field, 0, 1) == '$') {
			return $field;
		}
		
		if($criteria->hasSumFields()) {
			$sumFields = $criteria->getSumFields();
			if(array_key_exists($field, $sumFields)) {
				return 'value.sum.'.$sumFields[$field];
			}
		}

		if($criteria->hasMaxFields()) {
			$maxFields = $criteria->getMaxFields();
			if(array_key_exists($field, $maxFields)) {
				return 'value.max.'.$maxFields[$field];
			}
		}

		if($criteria->hasMinFields()) {
			$minFields = $criteria->getMinFields();
			if(array_key_exists($field, $minFields)) {
				return 'value.min.'.$minFields[$field];
			}
		}

		if($criteria->hasAvgFields()) {
			$avgFields = $criteria->getAvgFields();
			if(array_key_exists($field, $avgFields)) {
				return 'value.avg.'.$avgFields[$field];
			}
		}
		
		return $field;
	}

	private function convertCriterion($criterion)
	{
		$type = $criterion->getType();
		$value = $criterion->getValue();

		if($criterion->getField() == '_id') {
			$value = (is_array($value)) ? array_map(array($this, 'toMongoId'), $value) : $this->toMongoId($value);
		}

		if($type == Criterion::NOT_EQUALS) {
			$value = array('$ne' => $value);
		} else if($type == Criterion::IN) {
			$value = array('$in' => $value);
		} else if($type == Criterion::NOT_IN) {
			$value = array('$nin' => $value);
		} else if($type == Criterion::GREATER_THAN) {
			$value = array('$gt' => $value);
		} else if($type == Criterion::LESS_THAN) {
			$value = array('$lt' => $value);
		} else if($type == Criterion::GREATER_EQUALS) {
			$value = array('$gte' => $value);
		} else if($type == Criterion::LESS_EQUALS) {
			$value = array('$lte' => $value);
		} else if($type == Criterion::LIKE) {
			$value = (substr($value, 0, 1) == '%') ? substr($value, 1) : ('^'.$value);
			$value = (substr($value, -1) == '%') ? substr($value, 0, -1) : ($value.'$');
			//$value = array('$regex' => $value);
			$value = new MongoRegex('/'.$value.'/');
		} else if($type == Criterion::REGEX) {
			//$value = array('$regex' => $value);
			$value = new MongoRegex('/'.$value.'/');
		} else if($type != Criterion::EQUALS) {
			throw new MongoDriverException('Operation not supported '.$type);
		}

		$query = array($criterion->getField() => $value);
		$innerCriterion = $criterion->getCriterion();

		$orQuery = array();
		foreach($innerCriterion as $iterator)
		{
			list($op, $crit) = $iterator;
			$innerQuery = $this->convertCriterion($crit);

			if($op == Criterion::LOGIC_AND) {
				foreach($innerQuery as $field => $value) {
					if(array_key_exists($field, $query)) {						
						if(!is_array($query[$field])) {
							$query[$field] = array('$and' => array($query[$field]));
						}
						$query[$field]['$and'][] = $value;
					} else {
						$query[$field] = $value;
					}
				}
			} else if($op == Criterion::LOGIC_OR) {
				$orQuery[] = $innerQuery;
			} else {
				throw new MongoDriverException('Logic operator "'.$op.'" not supported');
			}

			if(sizeof($orQuery)) {
				array_unshift($orQuery, $query);
				$query = array('$or' => $orQuery);
			}
		}

		return $query;
	}

	public function insert($collection, $document)
	{
		if(!is_array($document)) {
			throw new IllegalArgumentException('$document is not an array');
		}

		if((array_key_exists('_id', $document)) && ($this->isMongoId($document['_id']))) {
			$document['_id'] = new MongoId($document['_id']);
		}
		
		try {
			$result = $this->getCollection($collection)->insert($document, array('safe' => true, 'timeout' => $this->updateTimeout));
		} catch(MongoCursorTimeoutException $mcte) {
			throw new MongoDriverException('Failed to insert document, timed out', 3, $mcte);
		} catch(MongoCursorException $mce) {
			throw new MongoDriverException('Failed to insert document', 4, $mce);
		}
		
		if($result['ok'] != 1) {
			return false;
		}
		
		// Return the auto generated primary key, if one was set.
		// Its auto generated, if the _id field is a MongoId
		
		$id = $document['_id'];
		
		if($id instanceof MongoId) {
			return $id->__toString();
		}
		
		return true;
	}

	public function update($document, $criteria)
	{
		if(!is_array($document)) {
			throw new IllegalArgumentException('$document is not an array');
		}
		if(! $criteria instanceof UpdateCriteria) {
			throw new IllegalArgumentException('$criteria is not of type UpdateCriteria');
		}
		if(!$criteria->hasCriterion()) {
			throw new MongoDriverException('Criteria does not contain usable criterion');
		}
		
		if((array_key_exists('_id', $document)) && ($this->isMongoId($document['_id']))) {
			$document['_id'] = new MongoId($document['_id']);
		}
		
		$query = $this->convertCriterion($criteria->getCriterion());
		
		try {
			$result = $this->getCollection($criteria->getCollection())->update($query, $document, array('safe' => true, 'timeout' => $this->updateTimeout, 'upsert' => $this->enableUpsert));
		} catch(MongoCursorTimeoutException $mcte) {
			throw new MongoDriverException('Failed to update document, timed out', 5, $mcte);
		} catch(MongoCursorException $mce) {
			throw new MongoDriverException('Failed to update document', 6, $mce);
		}

		if($result['ok'] != 1) {
			return false;
		}

		return true;
	}
	
	public function upsert($document, $criteria)
	{
		if(!is_array($document)) {
			throw new IllegalArgumentException('$document is not an array');
		}
		if(! $criteria instanceof UpdateCriteria) {
			throw new IllegalArgumentException('$criteria is not of type UpdateCriteria');
		}
		if(!$criteria->hasCriterion()) {
			throw new MongoDriverException('Criteria does not contain usable criterion');
		}
		
		if((array_key_exists('_id', $document)) && ($this->isMongoId($document['_id']))) {
			$document['_id'] = new MongoId($document['_id']);
		}
		
		$query = $this->convertCriterion($criteria->getCriterion());
		
		try {
			$result = $this->getCollection($criteria->getCollection())->update($query, $document, array('safe' => true, 'timeout' => $this->updateTimeout, 'upsert' => true));
		} catch(MongoCursorTimeoutException $mcte) {
			throw new MongoDriverException('Failed to update document, timed out', 5, $mcte);
		} catch(MongoCursorException $mce) {
			throw new MongoDriverException('Failed to update document', 6, $mce);
		}

		if($result['ok'] != 1) {
			return false;
		}

		return true;
	}

	public function delete($criteria)
	{
		if(! $criteria instanceof DeleteCriteria) {
			throw new IllegalArgumentException('$criteria is not of type UpdateCriteria');
		}
		if(!$criteria->hasCriterion()) {
			throw new MongoDriverException('Criteria does not contain usable criterion');
		}

		$query = $this->convertCriterion($criteria->getCriterion());
		
		try {
			$result = $this->getCollection($criteria->getCollection())->remove($query, array('safe' => true, "timeout" => $this->updateTimeout));
		} catch(MongoCursorTimeoutException $mcte) {
			throw new MongoDriverException('Failed to remove document, timed out', 7, $mcte);
		} catch(MongoCursorException $mce) {
			throw new MongoDriverException('Failed to remove document', 8, $mce);
		}

		if($result['ok'] != 1) {
			return false;
		}

		return true;
	}
	
	public function execute($collection, $query, $options = array(), $fields = array())
	{
		if(!is_array($query)) {
			throw new IllegalArgumentException('$query is not of type array');
		}
		if(!is_array($options)) {
			throw new IllegalArgumentException('$options is not of type array');
		}
		if(!is_array($fields)) {
			throw new IllegalArgumentException('$fields is not of type array');
		}

		if(!sizeof($query)) {
			throw new IllegalArgumentException('Empty query');
		}

		try {

			$cursor = $this->getCollection($collection)->find($query, $fields)->timeout($this->selectTimeout);
			
			if((array_key_exists('sort', $options)) && (is_array($options['sort']))) {
				if(sizeof($options['sort'])) {
					$cursor->sort($options['sort']);
				}
			}

			if((array_key_exists('skip', $options)) && (is_int($options['skip']))) {
				$cursor = $cursor->skip($options['skip']);
			}
			if((array_key_exists('limit', $options)) && (is_int($options['limit']))) {
				$cursor = $cursor->limit($options['limit']);
			}
			
			return new MongoResult($cursor);

		} catch(MongoConnectionException $mce) {
			throw new MongoDriverException('Could not connect to mongo', 9, $mce);
		}
	}
	
	public function runCommand($command)
	{
		if(!is_array($command)) {
			throw new IllegalArgumentException('$command is not of type array');
		}
		
		try {			
			return $this->db->command($command);
		} catch(MongoConnectionException $mce) {
			throw new MongoDriverException('Could not connect to mongo', 10, $mce);
		}
	}
}