<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\mongo;

use framework\db\criteria\SelectCriteria;

class MongoSelectCriteria extends SelectCriteria
{
	public function whereExpression($js)
	{
		return $this->where('$where')->equals($js);
	}
}
