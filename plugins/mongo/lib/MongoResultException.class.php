<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\mongo;

use framework\db\result\ResultException;

class MongoResultException extends ResultException
{
}