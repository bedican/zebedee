<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\mongo;

use framework\db\result\Result;
use framework\exception\IllegalArgumentException;
use MongoCursor, MongoId, MongoCursorTimeoutException, MongoCursorException;

class MongoResult implements Result
{
	private $cursor;
	
	public function __construct($cursor)
	{
		if(! $cursor instanceof MongoCursor) {
			throw new IllegalArgumentException('$cursor is not type MongoCursor');
		}
		
		$this->cursor = $cursor;
	}
	
	protected function processDocument($document)
	{
		if((array_key_exists('_id', $document)) && ($document['_id'] instanceof MongoId)) {
			$document['_id'] = $document['_id']->__toString();
		}
			
		return $document;
	}
	
	public function getNextRow()
	{
		try {
			if(!$this->cursor->hasNext()) {
				return false;
			}

			return $this->processDocument($this->cursor->getNext());

		} catch(MongoCursorTimeoutException $mcte) {
			throw new MongoResultException('Failed to get next document, timed out', 1, $mcte);
		} catch(MongoCursorException $mce) {
			throw new MongoResultException('Failed to get next document "'.$mce->getMessage().'"', 2, $mce);
		}
	}
}