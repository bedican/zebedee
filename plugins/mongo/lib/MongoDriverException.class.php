<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\mongo;

use framework\db\driver\DriverException;

class MongoDriverException extends DriverException
{
}