<?php // (c) Copyright 2011 Bedican Solutions

namespace module\error;

use framework\module\AbstractWebModule;

class ErrorModule extends AbstractWebModule
{
	protected function doError404()
	{
		$this->getResponse()->setStatus(404);
		$this->getView()->setTitle('Page Not Found');
		$this->getView()->setVar('uri', $this->getRouting()->getCanonicalPath($this->getRequest()->getUri()));
	}
	
	protected function doException()
	{
		$this->getView()->setLayout(false);
		$this->getView()->setVar('exception', $this->getContext()->getException());
	}
}