<?php // (c) Copyright 2013 Bedican Solutions

namespace module\task;

use plugin\common\modules\BaseTaskModule;

// Override this class within your own application or plugin module to add new functionality.

class TaskModule extends BaseTaskModule
{
}