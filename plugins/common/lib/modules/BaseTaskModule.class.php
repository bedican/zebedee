<?php // (c) Copyright 2013 Bedican Solutions

namespace plugin\common\modules;

use framework\cache\ViewCache;
use framework\cache\ConfigCache;
use framework\cache\SnippetCache;
use framework\module\AbstractTaskModule;

abstract class BaseTaskModule extends AbstractTaskModule
{
	protected function doClearCache()
	{
		$this->setView(false);
		
		$console = $this->getConsole();				
		$cacheType = $console->getOption('type');
		
		if((!$cacheType) || ($cacheType == 'config')) {
			$this->flushCache(new ConfigCache($this->getApplication()), 'Flushing full config cache... ');			
		}
		
		if((!$cacheType) || ($cacheType == 'view')) {
			if($module = $console->getOption('module')) {
				$this->flushCache(new ViewCache($this->getApplication(), $module), 'Flushing module '.$module.' view cache... ', false);
			} else {
				$this->flushCache(new ViewCache($this->getApplication(), $this->getContext()->getModule()), 'Flushing full view cache... ');
			}
		}
		
		if((!$cacheType) || ($cacheType == 'view')) {
			if($module = $console->getOption('module')) {
				$this->flushCache(new SnippetCache($this->getApplication(), $module), 'Flushing module '.$module.' snippet cache... ', false);
			} else {
				$this->flushCache(new SnippetCache($this->getApplication(), $this->getContext()->getModule()), 'Flushing full snippet cache... ');
			}
		}
	}
	
	private function flushCache($cache, $message, $all = true)
	{
		$console = $this->getConsole();
		$console->write($message);
		
		if($all) {
			$cache->flushAll();
		} else {
			$cache->flush();
		}
		
		$console->writeln('done');
	}
}