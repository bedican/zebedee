<?php // (c) Copyright 2011 Bedican Solutions

namespace module\auth;

use plugin\auth\modules\BaseAuthModule;

// Override this class within your own application or plugin module
// to add new functionality or replace view templates.

class AuthModule extends BaseAuthModule
{
}