<div id="login">
	<form action="<?= $routing->getUrl('auth.login'); ?>" method="post">
		<div class="input">
			<label for="username">Username:</label><input class="text" type="text" name="username" id="username" value="<?= $username ?>" />
			<label for="password">Password:</label><input class="text" type="password" name="password" id="password" />
		</div>
		<div class="submit">
			<input type="hidden" name="action" value="login" />
			<input class="submit" type="submit" value="Login" />
		</div>
	</form>
</div>