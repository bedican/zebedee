<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\auth;

use framework\context\WebContext;
use framework\context\ErrorWebContext;
use framework\exception\IllegalArgumentException;
use plugin\auth\db\AuthUserDao;

class WebAuthProvider
{
	private $context;
	private $user = null;
	
	public function __construct($context)
	{
		if((! $context instanceof WebContext) && (! $context instanceof ErrorWebContext)) {
			throw new IllegalArgumentException('$context is not of type WebContext');
		}

		$this->context = $context;
	}
	
	private function getSession()
	{
		return $this->context->getSession('auth');
	}
	
	private function getMetaSession()
	{
		return $this->context->getSession('auth.meta');
	}
	
	public function setMetaValue($key, $value)
	{
		$this->getMetaSession()->setValue($key, $value);
	}
	
	public function getMetaValue($key, $default = null)
	{
		return $this->getMetaSession()->getValue($key, $default);
	}
	
	public function isAuthenticated()
	{
		$session = $this->getSession();
		
		if(($session->isNew()) || ($session->isExpired())) {
			return false;
		}

		if($session->getValue('ip') != $this->context->getRequest()->getClientIp()) {
			$this->doLogout();
			return false;
		}

		return true;
	}
	
	public function isExpired()
	{
		return $this->getSession()->isExpired();
	}
	
	public function getLoggedInSince()
	{
		return $this->getSession()->getCreated();
	}
	
	public function doLogout()
	{
		$this->user = null;
		$this->getSession()->destroy();
	}
	
	public function doLogin($user)
	{
		if(! $user instanceof AuthUser) {
			throw new IllegalArgumentException('$user is not of type AuthUser');
		}

		$this->user = null;
		
		$session = $this->getSession();
		$request = $this->context->getRequest();
		
		$session->setValue('ip', $request->getClientIp());
		$session->setValue('proxy', $request->getClientProxy());
		$session->setValue('uid', $user->getId());
	}
	
	public function getUser($userDao)
	{
		if(! $userDao instanceof AuthUserDao) {
			throw new IllegalArgumentException('$userDao is not of type AuthUserDao');
		}
		
		if(!$this->isAuthenticated()) {
			return null;
		}
		
		if($this->user != null) {
			return $this->user;
		}

		if(!($uid = $this->getSession()->getValue('uid'))) {
			return null;
		}
		
		$this->user = $userDao->getById($uid);
		return $this->user;
	}
}