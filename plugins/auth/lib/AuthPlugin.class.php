<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\auth;

use framework\plugin\Plugin;

class AuthPlugin extends Plugin
{
	protected function getDependencies()
	{
		return array('mySql mongo');
	}
}