<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\auth\helper;

use plugin\auth\AuthUser;
use plugin\auth\RoleProvider;
use framework\view\helper\AbstractHelper;
use framework\exception\IllegalArgumentException;

class AuthHelper extends AbstractHelper
{
	private $user;
	private $roleProvider;
	
	public function __construct($user, $roleProvider)
	{
		if((! $user instanceof AuthUser) && ($user != null)) {
			throw new IllegalArgumentException('$user is not of type AuthUser or null');
		}
		if(! $roleProvider instanceof RoleProvider) {
			throw new IllegalArgumentException('$roleProvider is not of type RoleProvider');
		}
		
		$this->user = $user;
		$this->roleProvider = $roleProvider;
	}
	
	protected function isLoggedIn($renderer)
	{
		return ($this->user != null);
	}
	
	protected function isMe($renderer, $user)
	{
		if(! $user instanceof AuthUser) {
			throw new IllegalArgumentException('$user is not of type User');
		}

		return (($this->user != null) && ($this->user->getId() == $user->getId()));
	}
	
	protected function hasPermission($renderer, $permission)
	{
		if($this->user == null) {
			return false;
		}
		
		return $this->roleProvider->getRole($this->user)->hasPermission($permission);
	}
	
	protected function isAuthorityOf($renderer, $user)
	{
		if($this->user == null) {
			return false;
		}
		
		$thisRole = $this->roleProvider->getRole($this->user);
		$thatRole = $this->roleProvider->getRole($user);
		
		return $thisRole->isAuthorityOf($thatRole);
	}
	
	protected function getRole($renderer, $user = null)
	{
		return $this->roleProvider->getRole(($user) ? $user : $this->user);
	}
	
	protected function username($renderer)
	{
		return ($this->user != null) ? $this->user->getUsername() : '';
	}
}