<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\auth\config;

use framework\config\AbstractConfigBuilder;
use framework\config\ConfigBuilderException;

class AuthConfigBuilder extends AbstractConfigBuilder
{
	protected function doBuild($xpath)
	{
		$config = new AuthConfig();

		$this->parseRoles($config, $xpath);
		$this->parseGroups($config, $xpath);
		$this->parsePermissions($config, $xpath);

		return $config;
	}
	
	private function parseRoles($config, $xpath)
	{
		$nodeList = $xpath->query('/auth/roles/role');
		
		foreach($nodeList as $node) {
			if(!$node->hasAttribute('id')) {
				throw new ConfigBuilderException('Missing id on role');
			}
			
			$id = $node->getAttribute('id');
			$name = $node->firstChild->nodeValue;
			
			if($node->hasAttribute('title')) {
				$title = $node->getAttribute('title');
			} else {
				$title = $name;
			}
			
			$config->addRole($id, $name, $title);
		}		
	}
	
	private function parseGroups($config, $xpath)
	{
		$nodeList = $xpath->query('/auth/groups/group');
		
		foreach($nodeList as $node) {
			if(!$node->hasAttribute('name')) {
				throw new ConfigBuilderException('Missing name on group');
			}
			
			$roles = array();
			$name = $node->getAttribute('name');

			$roleNodeList = $xpath->query('role', $node);
			foreach($roleNodeList as $roleNode) {
				$roles[] = $roleNode->firstChild->nodeValue;
			}
			
			$config->addGroup($name, $roles);
		}		
	}
	
	private function parsePermissions($config, $xpath)
	{
		$nodeList = $xpath->query('/auth/permissions/permission');
		
		foreach($nodeList as $node) {
			if(!$node->hasAttribute('name')) {
				throw new ConfigBuilderException('Missing name on permission');
			}
			
			$name = $node->getAttribute('name');
			
			$roles = array();
			$groups = array();
			
			$roleNodeList = $xpath->query('role', $node);
			foreach($roleNodeList as $roleNode) {
				$roles[] = $roleNode->firstChild->nodeValue;
			}

			$groupNodeList = $xpath->query('group', $node);
			foreach($groupNodeList as $groupNode) {
				$groups[] = $groupNode->firstChild->nodeValue;
			}
			
			$config->addPermission($name, $roles, $groups);		
		}
	}
}