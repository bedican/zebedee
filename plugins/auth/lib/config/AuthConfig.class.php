<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\auth\config;

use framework\config\Cachable;
use framework\config\Mergeable;
use framework\exception\IllegalArgumentException;

class AuthConfig implements Cachable, Mergeable
{
	private $roles = array();
	private $rolesTitles = array();
	private $groups = array();
	private $permissions = array();
	
	public function __construct()
	{
	}
	
	public function addRole($id, $name, $title)
	{
		if(array_key_exists($id, $this->roles)) {
			throw new IllegalArgumentException('Role '.$id.' already exists');
		}
		
		$this->roles[$id] = $name;
		$this->rolesTitles[$id] = $title;
	}
	
	public function addGroup($name, $roles)
	{
		if(array_key_exists($name, $this->groups)) {
			throw new IllegalArgumentException('Group '.$name.' already exists');
		}
		if(!is_array($roles)) {
			throw new IllegalArgumentException('$roles is not of type array');
		}

		$this->groups[$name] = $roles;
	}
	
	public function addPermission($name, $roles, $groups)
	{
		if(array_key_exists($name, $this->permissions)) {
			throw new IllegalArgumentException('Permission '.$name.' already exists');
		}
		if(!is_array($roles)) {
			throw new IllegalArgumentException('$roles is not of type array');
		}
		if(!is_array($groups)) {
			throw new IllegalArgumentException('$groups is not of type array');
		}

		$this->permissions[$name] = array(
			'roles' => $roles,
			'groups' => $groups
		);
	}
	
	public function isRoleId($roleId)
	{
		return array_key_exists($roleId, $this->roles);
	}
	
	public function getRoleIds()
	{
		return array_keys($this->roles);
	}
	
	public function getRoleNames($roleId = null)
	{
		if(!$roleId) {
			return $this->roles;
		}
		
		if(!array_key_exists($roleId, $this->roles)) {
			throw new IllegalArgumentException('Role "'.$roleId.'" is unknown');
		}
		
		$roleIds = array_flip(array_keys($this->roles));
		$index = $roleIds[$roleId];
		
		return array_slice($this->roles, $index, sizeof($this->roles) - $index, true);
	}
	
	public function getGroupNames()
	{
		return array_keys($this->groups);
	}
	
	public function getGroupRoles($group)
	{
		if(!array_key_exists($group, $this->groups)) {
			throw new IllegalArgumentException('Unknown group');
		}
		
		return $this->groups[$group];
	}
	
	public function getRoleName($roleId)
	{
		if(!array_key_exists($roleId, $this->roles)) {
			throw new IllegalArgumentException('Unknown role id');
		}
		
		return $this->roles[$roleId];
	}
	
	public function getRoleTitle($roleId)
	{
		if(!array_key_exists($roleId, $this->rolesTitles)) {
			throw new IllegalArgumentException('Unknown role id');
		}
		
		return $this->rolesTitles[$roleId];
	}
	
	public function hasPermission($roleId, $permission)
	{
		if(!array_key_exists($roleId, $this->roles)) {
			throw new IllegalArgumentException('Unknown role id');
		}
		if(!array_key_exists($permission, $this->permissions)) {
			throw new IllegalArgumentException('Unknown permission');
		}
		
		if(in_array($roleId, $this->permissions[$permission]['roles'])) {
			return true;
		}

		foreach($this->groups as $name => $roles) {
			if((in_array($roleId, $roles)) && (in_array($name, $this->permissions[$permission]['groups']))) {
				return true;
			}
		}
		
		return false;
	}
	
	public function isAuthorityOf($roleId1, $roleId2)
	{
		if(!array_key_exists($roleId1, $this->roles)) {
			throw new IllegalArgumentException('Unknown role id: '.$roleId1);
		}
		if(!array_key_exists($roleId2, $this->roles)) {
			throw new IllegalArgumentException('Unknown role id: '.$roleId2);
		}
		
		$roleIds = array_flip(array_keys($this->roles));
		
		if($roleIds[$roleId1] > $roleIds[$roleId2]) {
			return false;
		}

		return true;
	}
	
	public function getPermissions()
	{
		return array_keys($this->permissions);
	}
	
	public function getRoleIdsWith($permission, $resolveGroups = true)
	{
		if(!array_key_exists($permission, $this->permissions)) {
			throw new IllegalArgumentException('Unknown permission');
		}
		
		$roles = $this->permissions[$permission]['roles'];
		
		if($resolveGroups)
		{
			$groups = $this->permissions[$permission]['groups'];
			foreach($groups as $group) {
				$roles = array_unique(array_merge($roles, $this->groups[$group]));
			}
		}
		
		return $roles;
	}
	
	public function getGroupsWith($permission)
	{
		if(!array_key_exists($permission, $this->permissions)) {
			throw new IllegalArgumentException('Unknown permission');
		}
		
		return $this->permissions[$permission]['groups'];
	}
	
	public function inGroup($roleId, $group)
	{
		if(!array_key_exists($roleId, $this->roles)) {
			throw new IllegalArgumentException('Unknown role id');
		}
		if(!array_key_exists($group, $this->groups)) {
			throw new IllegalArgumentException('Unknown group');
		}
		
		return in_array($roleId, $this->groups[$group]);
	}
	
	public function merge($config, $overwrite = false)
	{
		if(! $config instanceof AuthConfig) {
			throw new IllegalArgumentException('$config is not of type AuthConfig');
		}
		
		// Merge roles
		$roleNames = $config->getRoleNames();
		foreach($roleNames as $roleId => $roleName) {
			if((!in_array($roleId, $this->roles)) || ($overwrite)) {		
				$this->roles[$roleId] = $roleName;
				$this->rolesTitles[$roleId] = $config->getRoleTitle($roleId);
			}
		}
		
		// Merge groups
		$groups = $config->getGroupNames();
		foreach($groups as $group) {
			$roles = $config->getGroupRoles($group);
			if((!in_array($group, $this->groups)) || ($overwrite)) {
				$this->groups[$group] = $roles;
			}
		}

		// Merge permissions
		$permissions = $config->getPermissions();
		foreach($permissions as $permission) {
			
			if(($overwrite) && (!isset($this->permissions[$permission]))) {
				$this->permissions[$permission] = array('roles' => array(), 'groups' => array());
			}

			$roles = $config->getRoleIdsWith($permission, false);
			if($overwrite) {
				$this->permissions[$permission]['roles'] = array_unique(array_merge($this->permissions[$permission]['roles'], $roles));
			}
			
			$groups = $config->getGroupsWith($permission);
			if($overwrite) {
				$this->permissions[$permission]['groups'] = array_unique(array_merge($this->permissions[$permission]['groups'], $groups));
			}
		}
	}
}