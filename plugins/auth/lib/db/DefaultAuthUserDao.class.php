<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\auth\db;

use framework\db\dao\AbstractDao;

class DefaultAuthUserDao extends AbstractDao implements AuthUserDao
{		
	public function getByUsername($username)
	{
		return $this->retrieveOne()->where('username')->equals($username)->execute();
	}
	public function getById($id)
	{
		return $this->getByPk($id);
	}
}