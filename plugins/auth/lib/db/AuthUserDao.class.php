<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\auth\db;

interface AuthUserDao
{
	public function getById($id);
	public function getByUsername($username);
}