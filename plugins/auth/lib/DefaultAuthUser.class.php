<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\auth;

use framework\exception\IllegalStateException;

class DefaultAuthUser implements AuthUser
{
	private $id;
	private $username;
	private $password;
	private $roleId;
	
	private $role = null;
	
	public function getId()
	{
		return $this->id;
	}
	public function setId($id)
	{
		$this->id = $id;
	}

	public function getUsername()
	{
		return $this->username;
	}
	public function setUsername($username)
	{
		$this->username = $username;
	}
	
	public function getPassword()
	{
		return $this->password;
	}
	public function setPassword($password, $doHash = false)
	{
		if($doHash) {
			$password = crypt($password);
		}
		
		$this->password = $password;
	}
	
	public function getRoleId()
	{
		return $this->roleId;
	}
	public function setRoleId($roleId)
	{
		$this->roleId = $roleId;
	}

	public function validateLogin($password)
	{
		return (crypt($password, $this->getPassword()) == $this->getPassword());
	}
}