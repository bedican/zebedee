<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\auth\modules;

use plugin\auth\db\AuthUserDao;
use framework\exception\RuntimeException;

/**
 * This class provides the entry point of the authentication mechanism.
 * The provided auth module can be used, or alternatively this class can 
 * be extended within the application.
 */
abstract class BaseAuthModule extends AbstractAuthModule
{
	// Default empty implementation
	protected function onLogin($user) {}
	
	protected function returnRedirect($perm = false)
	{
		$defaultAuthPath = $this->getConfig()->getValue('auth.default-path', 'default');
		$location = $this->getAuthProvider()->getMetaValue('return-uri', $this->getRouting()->getUrl($defaultAuthPath));
		$this->getResponse()->redirect($location, $perm);
	}
	
	protected function doLogin()
	{
		if($this->isAuthenticated()) {
			$this->returnRedirect();
			return;
		}

		$authDao = $this->getAuthDao();
		if(! $authDao instanceof AuthUserDao) {
			throw new RuntimeException(get_class($authDao).' does not implement AuthUserDao');
		}
		
		if($message = $this->getAuthProvider()->getMetaValue('message')) {
			$this->getView()->setVar('message', $message);
			$this->getAuthProvider()->setMetaValue('message', null);
		}

		$request = $this->getRequest();

		if($request->getPostValue('action') == 'login') {
			
			$username = $request->getPostValue('username');
			$password = $request->getPostValue('password');

			if((strlen($username)) && (strlen($password))) {

				$user = $authDao->getByUsername($username);

				if(($user) && ($user->validateLogin($password))) {
					
					$this->getAuthProvider()->doLogin($user);
					$this->onLogin($user);
					$this->returnRedirect(true);

				} else {
					
					$this->getView()->setVar('username', $username);
					$this->getView()->setVar('message', 'Incorrect username or password, please try again.');
				}
			}
		}
	}
	
	protected function doLogout()
	{
		// Should we log out to the login page, or the page we were currently viewing. TODO: make configurable
		
		$this->getAuthProvider()->doLogout();
		$this->getResponse()->redirect($this->getRouting()->getUrl('auth.login'));
	}
}