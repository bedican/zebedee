<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\auth\modules;

/**
 * This class provides functionality to ensure that any module extending it 
 * cannot be accessed until the user has been authenticated.
 */
abstract class SecureWebModule extends AbstractAuthModule
{	
	protected function preAction()
	{
		parent::preAction();
		
		if(!$this->getRequest()->isXmlHttpRequest()) {
			$this->getAuthProvider()->setMetaValue('return-uri', $this->getRequest()->getAbsoluteUri());
		}

		if($this->getAuthProvider()->isAuthenticated()) {
			return;
		}
		
		if($this->getAuthProvider()->isExpired()) {
			$this->getAuthProvider()->setMetaValue('message', 'Your session has expired, please log in again.');
		}

		$this->getResponse()->redirect($this->getRouting()->getUrl('auth.login'));
	}
}