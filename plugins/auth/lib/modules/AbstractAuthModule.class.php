<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\auth\modules;

use plugin\auth\helper\AuthHelper;
use plugin\auth\RoleProvider;
use plugin\auth\WebAuthProvider;
use framework\module\AbstractWebModule;

abstract class AbstractAuthModule extends AbstractWebModule
{
	private $authProvider;
	private $roleProvider;

	protected function init()
	{
		$this->authProvider = new WebAuthProvider($this->getContext());
		$this->roleProvider = new RoleProvider($this->getApplication());
	}
	
	protected function preAction()
	{
		parent::preAction();
		$this->getView()->addHelper(new AuthHelper($this->getUser(), $this->roleProvider));
	}
	
	protected function getAuthProvider()
	{
		return $this->authProvider;
	}
	
	protected function getRoleProvider()
	{
		return $this->roleProvider;
	}
	
	protected function isAuthenticated()
	{
		return $this->authProvider->isAuthenticated();
	}
	
	protected function getLoggedInSince()
	{
		return $this->authProvider->getLoggedInSince();
	}
	
	protected function getAuthDao()
	{
		return $this->getDatabaseManager()->getDao('auth');
	}
	
	protected function getUser()
	{
		return $this->authProvider->getUser($this->getAuthDao());
	}
	
	protected function getRole($user = null)
	{
		if(($user == null) && (!$this->isAuthenticated())) {
			return null;
		}
		
		return $this->roleProvider->getRole(($user) ? $user : $this->getUser());
	}
	
	protected function hasPermission($permission)
	{
		return (($this->isAuthenticated()) && ($this->getRole()->hasPermission($permission)));
	}
}