<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\auth;

interface AuthUser
{
	public function getId();
	public function getUsername();
	public function getRoleId();

	public function validateLogin($password);
}