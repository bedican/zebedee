<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\auth;

use plugin\auth\config\AuthConfig;
use framework\exception\IllegalArgumentException;

class Role
{
	const ID_SYSADMIN = 'sysadmin';

	private $roleId;
	private $config;
	
	public function __construct($roleId, $config)
	{
		if(! $config instanceof AuthConfig) {
			throw new IllegalArgumentException('$config is not of type AuthConfig');
		}
		
		$this->roleId = $roleId;
		$this->config = $config;
	}
	
	public function isSysadmin()
	{
		return ($this->roleId == self::ID_SYSADMIN);
	}
	
	public function getRoleId()
	{
		return $this->roleId;
	}
	
	public function getName()
	{
		return $this->config->getRoleName($this->roleId);
	}
	
	public function getTitle()
	{
		return $this->config->getRoleTitle($this->roleId);
	}
	
	public function __toString()
	{
		return $this->getName();
	}
	
	public function isAuthorityOf($role)
	{
		if(! $role instanceof Role) {
			throw new IllegalArgumentException('$role is not of type Role');
		}
		if($this->isSysadmin()) {
			return true;
		}
		
		return $this->config->isAuthorityOf($this->roleId, $role->getRoleId());
	}
	
	public function hasPermission($permission)
	{
		return $this->config->hasPermission($this->roleId, $permission);
	}
	
	public function inGroup($group)
	{
		return $this->config->inGroup($this->roleId, $group);
	}
}