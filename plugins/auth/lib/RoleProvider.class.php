<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\auth;

use plugin\auth\config\AuthConfigBuilder;
use plugin\auth\config\AuthConfig;
use framework\cache\ConfigCache;
use framework\application\Application;
use framework\exception\IllegalArgumentException;

class RoleProvider
{
	private $application;
	private $config;

	private $roles = array();

	public function __construct($application)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		
		$this->application = $application;
		$this->initConfig();
	}
	
	private function initConfig()
	{		
		$cache = new ConfigCache($this->application);
		$this->config = $cache->get('auth.xml');
		
		if($this->config instanceof AuthConfig) {
			return;
		}
		
		$builder = new AuthConfigBuilder();
		
		// Plugin auth.xml should always be present
		$this->config = $builder->build(dirname(__FILE__).'/../config/auth.xml');
		
		// Override with application auth.xml if present
		$filename = $this->application->getApplicationDir().'/config/auth.xml';
		if(is_file($filename)) {
			$this->config->merge($builder->build($filename), true);
		}
		
		$cache->put('auth.xml', $this->config);
	}
	
	public function getRole($user)
	{
		if(! $user instanceof AuthUser) {
			throw new IllegalArgumentException('$user is not of type AuthUser');
		}
		
		return $this->getRoleById($user->getRoleId());
	}
	
	public function getRoleById($roleId)
	{
		if(!array_key_exists($roleId, $this->roles)) {
			if(!$this->config->isRoleId($roleId)) {
				throw new IllegalArgumentException('Unknown role: '.$roleId);
			}

			$this->roles[$roleId] = new Role($roleId, $this->config);
		}
		
		return $this->roles[$roleId];
	}
	
	public function getRoleIdsWith($permission)
	{
		return $this->config->getRoleIdsWith($permission);
	}
	
	public function getRoleNames($roleId = null)
	{
		return $this->config->getRoleNames($roleId);
	}
}