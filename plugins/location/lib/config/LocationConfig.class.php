<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\location\config;

use framework\config\Cachable;
use framework\config\Mergeable;
use framework\exception\IllegalArgumentException;

class LocationConfig implements Cachable, Mergeable
{
	private $locations = array();
	
	public function __construct()
	{
	}
	
	public function addLocation($name, $code)
	{
		if(array_key_exists($code, $this->locations)) {
			throw new IllegalArgumentException('Location '.$code.' already exists');
		}
		
		$this->locations[$code] = $name;
	}
	
	public function getLocations()
	{
		return $this->locations;
	}
	
	public function getName($code)
	{
		return (array_key_exists($code, $this->locations)) ? $this->locations[$code] : '';
	}
	
	public function merge($config, $overwrite = false)
	{
		if(! $config instanceof LocationConfig) {
			throw new IllegalArgumentException('$config is not of type LocationConfig');
		}
		
		$locations = $config->getLocations();
		
		foreach($locations as $code => $name) {
			if((!array_key_exists($code, $this->locations)) || ($overwrite)) {
				$this->locations[$code] = $name;
			}
		}
	}
}