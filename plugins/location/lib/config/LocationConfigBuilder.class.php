<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\location\config;

use framework\config\AbstractConfigBuilder;
use framework\config\ConfigBuilderException;

class LocationConfigBuilder extends AbstractConfigBuilder
{
	protected function doBuild($xpath)
	{
		$config = new LocationConfig();

		$this->parseLocations($config, $xpath);

		return $config;
	}
	
	private function parseLocations($config, $xpath)
	{
		$nodeList = $xpath->query('/locations/location');
		
		foreach($nodeList as $node) {
			
			$nameNodeList = $xpath->query('name/text()', $node);
			$codeNodeList = $xpath->query('code/text()', $node);
			
			if((!$nameNodeList->length) || (!$codeNodeList->length)) {
				continue;
			}

			$name = $nameNodeList->item(0)->nodeValue;
			$code = $codeNodeList->item(0)->nodeValue;
			
			if((!strlen($name)) || (!strlen($code))) {
				continue;
			}

			$config->addLocation($name, $code);
		}		
	}
}