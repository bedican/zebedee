<?php // (c) Copyright 2013 Bedican Solutions

namespace plugin\location;

use plugin\location\config\LocationConfig;
use plugin\location\config\LocationConfigBuilder;
use framework\cache\ConfigCache;
use framework\application\Application;
use framework\exception\IllegalArgumentException;

class LocationProvider
{
	private $application;
	private $config;

	private $roles = array();

	public function __construct($application)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		
		$this->application = $application;
		$this->initConfig();
	}
	
	private function initConfig()
	{		
		$cache = new ConfigCache($this->application);
		$this->config = $cache->get('locations.xml');
		
		if($this->config instanceof LocationConfig) {
			return;
		}
		
		$builder = new LocationConfigBuilder();
		
		// Plugin locations.xml should always be present
		$this->config = $builder->build(dirname(__FILE__).'/../config/locations.xml');
		
		// Override with application locations.xml if present
		$filename = $this->application->getApplicationDir().'/config/locations.xml';
		if(is_file($filename)) {
			$this->config->merge($builder->build($filename), true);
		}
		
		$cache->put('locations.xml', $this->config);
	}
	
	public function getLocations()
	{
		return $this->config->getLocations();
	}
	
	public function getName($code)
	{
		return $this->config->getName($code);
	}
}