<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\menu;

class MenuBar
{
	private $menus;
	private $routing;
	private $currentRoute;
	
	public function __construct($routing, $currentRoute)
	{		
		$this->menus = array();

		$this->routing = $routing;
		$this->currentRoute = strtolower($currentRoute->getPath());
	}
	
	public function addMenu($name, $route)
	{
		$menu = new Menu($name, $route, $this->routing, $this->currentRoute);

		$this->menus[] = $menu;
		return $menu;
	}
	
	public function getMenus()
	{
		return $this->menus;
	}
	
	public function getActiveMenu()
	{
		foreach($this->menus as $menu) {
			if($menu->isActive()) {
				return $menu;
			}
		}
		
		return null;
	}
}