<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\menu;

class MenuItem
{
	private $name;
	private $route;
	private $routing;
	private $currentRoute;
	private $activeRoutes;
	
	public function __construct($name, $route, $routing, $currentRoute, $activeRoutes = array())
	{
		$this->name = $name;
		$this->route = $route;
		$this->routing = $routing;
		$this->currentRoute = $currentRoute;
		$this->activeRoutes = $activeRoutes;
	}

	public function isActive()
	{
		if($this->isExternal()) {
			return false;
		}
		
		if($this->routing->getCanonicalRoute($this->route) == $this->currentRoute) {
			return true;
		}
		
		foreach($this->activeRoutes as $route) {
			if($this->routing->getCanonicalRoute($route) == $this->currentRoute) {
				return true;
			}
		}
		
		return false;
	}
	
	public function isExternal()
	{
		return (preg_match('#^https?://#i', $this->route) == 1);
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getLocation()
	{
		if($this->isExternal()) {
			return $this->route;
		} else {
			return $this->routing->getUrl($this->route);
		}
	}
}