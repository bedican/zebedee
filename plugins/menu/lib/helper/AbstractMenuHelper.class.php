<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\menu\helper;

use plugin\auth\Role;
use plugin\menu\MenuBar;
use framework\application\Application;
use framework\routing\Route;
use framework\routing\Routing;
use framework\view\helper\AbstractHelper;
use framework\exception\IllegalArgumentException;

abstract class AbstractMenuHelper extends AbstractHelper
{
	private $application;
	private $routing;
	private $currentRoute;
	private $role;
	
	public function __construct($application, $routing, $currentRoute, $role = null)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(! $routing instanceof Routing) {
			throw new IllegalArgumentException('$routing is not of type Routing');
		}
		if(! $currentRoute instanceof Route) {
			throw new IllegalArgumentException('$currentRoute is not of type Route');
		}
		if((! $role instanceof Role) && ($role != null)) {
			throw new IllegalArgumentException('$role is not of type Role');
		}

		$this->application = $application;
		$this->routing = $routing;
		$this->currentRoute = $currentRoute;
		$this->role = $role;
	}
	
	protected function getMethods()
	{
		return array('menu');
	}

	abstract protected function buildMenu($menuBar);
	
	protected function buildNoAuthMenu($menuBar)
	{
		$menuBar->addMenu('Login', 'auth.login');
	}
	
	protected function hasPermission($permission)
	{
		if($this->role == null) {
			return false;
		}

		return $this->role->hasPermission($permission);
	}
	
	protected function getApplication()
	{
		return $this->application;
	}
	
	protected function menu($renderer)
	{
		$menuBar = new MenuBar($this->routing, $this->currentRoute);
		
		if($this->role == null) {
			$this->buildNoAuthMenu($menuBar);
		} else {
			$this->buildMenu($menuBar);
		}

		print($renderer->render(dirname(__FILE__).'/templates/menu.php', array('menuBar' => $menuBar)));
	}
}