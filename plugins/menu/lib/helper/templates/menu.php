<div id="menu">
<?php if(sizeof($menus = $menuBar->getMenus())) { ?>
	<ul>
	<?php foreach($menus as $menu) { ?>
		<li<?php if($menu->isActive()) { ?> class="active"<?php } ?>><a href="<?= $menu->getLocation(); ?>"<?php if($menu->isExternal()) { ?> target="_blank"<?php } ?>><?= $menu->getName(); ?></a>
	<?php } ?>
 	</ul>
<?php } ?>
</div>

<?php if($submenu = $menuBar->getActiveMenu()) { if($size = sizeof($items = $submenu->getItems())) { ?>
	<div id="submenu">
	<?php foreach($items as $index => $item) { ?>
		<a href="<?= $item->getLocation(); ?>"<?php if($item->isActive()) { ?> class="active"<?php } ?><?php if($item->isExternal()) { ?> target="_blank"<?php } ?>><?= $item->getName(); ?></a>
		<?php if($index < ($size - 1)) { print('|'); } ?>
	<?php } ?>
	</div>
<?php }} ?>