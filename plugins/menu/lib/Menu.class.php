<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\menu;

class Menu
{
	private $name;
	private $route;
	private $routing;
	private $currentRoute;

	private $items;
	
	public function __construct($name, $route, $routing, $currentRoute)
	{
		$this->items = array();

		$this->name = $name;
		$this->route = $route;
		$this->routing = $routing;
		$this->currentRoute = $currentRoute;
	}
	
	public function isActive()
	{
		if((!$this->isExternal()) && ($this->routing->getCanonicalRoute($this->route) == $this->currentRoute)) {
			return true;
		}
		
		foreach($this->items as $item) {
			if($item->isActive()) {
				return true;
			}
		}
		
		return false;
	}
	
	public function isExternal()
	{
		return (preg_match('#^https?://#i', $this->route) == 1);
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getLocation()
	{
		if($this->isExternal()) {
			return $this->route;
		} else {
			return $this->routing->getUrl($this->route);
		}
	}
	
	public function addItem($name, $route, $activeRoutes = array())
	{
		$this->items[] = new MenuItem($name, $route, $this->routing, $this->currentRoute, $activeRoutes);
		return $this;
	}
	
	public function getItems()
	{
		return $this->items;
	}
}