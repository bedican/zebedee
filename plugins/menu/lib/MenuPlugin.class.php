<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\menu;

use framework\plugin\Plugin;

class MenuPlugin extends Plugin
{
	protected function getDependencies()
	{
		return array('auth');
	}
}