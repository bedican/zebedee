<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\mySql;

use framework\db\result\Result;

class MySqlResult implements Result
{
	private $result;

   	public function __construct($result)
	{
		$this->result = $result;
	}

	public function __destruct()
	{
		$this->free();
	}

	public function getNextRow()
	{
		if(!$this->result) {
			return false;
		}
		
		return mysqli_fetch_assoc($this->result);
	}

	public function free()
	{
		if(!$this->result) {
			return false;
		}

		return mysqli_free_result($this->result);
	}
}