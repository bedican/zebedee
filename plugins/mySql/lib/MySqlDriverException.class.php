<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\mySql;

use framework\db\driver\DriverException;

class MySqlDriverException extends DriverException
{
}