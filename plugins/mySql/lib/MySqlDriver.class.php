<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\mySql;

use framework\db\driver\AbstractDriver;
use framework\db\criteria\Criterion;
use framework\db\criteria\SelectCriteria;
use framework\db\criteria\DeleteCriteria;
use framework\db\criteria\UpdateCriteria;
use framework\exception\IllegalArgumentException;

class MySqlDriver extends AbstractDriver
{
	const LOCK_READ = 'READ';
	const LOCK_WRITE = 'WRITE';
	const LOCK_LOW_PRIORITY_WRITE = 'LOW_PRIORITY WRITE';
	
	private static $lockTypes = array(
		self::LOCK_READ,
		self::LOCK_WRITE,
		self::LOCK_LOW_PRIORITY_WRITE
	);
	
	private static $criterionTypeConversionMap = array(
		Criterion::EQUALS => '=',
		Criterion::NOT_EQUALS => '!=',
		Criterion::GREATER_THAN => '>',
		Criterion::LESS_THAN => '<',
		Criterion::GREATER_EQUALS => '>=',
		Criterion::LESS_EQUALS => '<=',
		Criterion::IN => 'IN',
		Criterion::NOT_IN => 'NOT IN',
		Criterion::LIKE => 'LIKE',
		Criterion::NOT_LIKE => 'NOT LIKE',
		Criterion::REGEX => 'REGEXP'
	);
	
	private $db = null;
	
	protected function init()
	{
	}
	
	public function newSelectCriteria()
	{
		return new MySqlSelectCriteria();
	}
	
	protected function shutdown()
	{
		if($this->db) {
			mysqli_close($this->db);
		}
	}
	
	private function connect()
	{		
		if($this->db != null) {
			return;
		}
		
		$dsn = $this->getDsn();
		
		$host = $dsn->getHost();
		$username = ($dsn->getUsername()) ? $dsn->getUsername() : ini_get("mysql.default_user");
		$password = ($dsn->getPassword()) ? $dsn->getPassword() : ini_get("mysql.default_password");

		$this->db = mysqli_connect($host, $username, $password, $dsn->getDatabase());

		if(!$this->db) {
			throw new MySqlDriverException('Could not connect to '.$host);
		}
	}
	
	private function buildQuery($criteria)
	{
		$query = 'SELECT ';

		if(sizeof($fields = $criteria->getSelectedFields())) {
			if($criteria->hasGroup()) {
				$fields = array_unique(array_merge($criteria->getGroup(), $fields));
			}

			$query .= implode(', ', $fields);
		} else {
			// We specify table.* instead of just * so that by default we select all from the
			// main table and not all of the joins unless explicitly stated. This avoids abiguity of field names.
			$query .= ' '.$criteria->getCollection().'.*';
		}
		
		if(sizeof($sumFields = $criteria->getSumFields())) {
			foreach($sumFields as $alias => $field) {
				$query .= ', SUM('.$field.') AS '.$alias;
			}
		}
		
		if(sizeof($maxFields = $criteria->getMaxFields())) {
			foreach($maxFields as $alias => $field) {
				$query .= ', MAX('.$field.') AS '.$alias;
			}
		}
		
		if(sizeof($minFields = $criteria->getMinFields())) {
			foreach($minFields as $alias => $field) {
				$query .= ', MIN('.$field.') AS '.$alias;
			}
		}
		
		if(sizeof($avgFields = $criteria->getAvgFields())) {
			foreach($avgFields as $alias => $field) {
				$query .= ', AVG('.$field.') AS '.$alias;
			}
		}
		
		$query .= ' FROM '.$criteria->getCollection();
		
		if($joins = $criteria->getJoins()) {
			foreach($joins as $type => $tableAndOn) {
				foreach($tableAndOn as $table => $on) {
					$query .= ' '.$type.' JOIN '.$table.' ON '.$on;
				}
			}
		}

		if($criterion = $criteria->getCriterion()) {
			$query .= ' WHERE '.$this->convertCriterion($criterion);
		}
		
		if($criteria->hasGroup())
		{
			$query .= ' GROUP BY '.implode(', ', $criteria->getGroup());
		}
		
		if($criteria->hasHaving()) {
			if($criterion = $criteria->getHaving()->getCriterion()) {
				$query .= ' HAVING '.$this->convertCriterion($criterion);
			}
		}
		
		if($criteria->hasOrder())
		{
			$query .= ' ORDER BY';

			$order = $criteria->getOrder();
			$orderSize = sizeof($order) - 1;
			
			foreach($order as $index => $directionAndField) {
				list($direction, $field) = $directionAndField;
				$query .= ' '.$field.($direction == SelectCriteria::ORDER_DESC ? ' DESC' : ' ASC');
				$query .= ($index < $orderSize) ? ', ' : '';
			}
		}
		
		if(($criteria->hasOffset()) && ($criteria->hasLength())) {
			$query .= ' LIMIT '.$criteria->getOffset().', '.$criteria->getLength();
		} else if($criteria->hasLength()) {
			$query .= ' LIMIT '.$criteria->getLength();
		}
		
		return $query;
	}
	
	private function buildCountQuery($criteria)
	{
		$query = 'SELECT COUNT(*) FROM '.$criteria->getCollection();
		
		if($joins = $criteria->getJoins()) {
			foreach($joins as $type => $tableAndOn) {
				foreach($tableAndOn as $table => $on) {
					$query .= ' '.$type.' JOIN '.$table.' ON '.$on;
				}
			}
		}

		if($criterion = $criteria->getCriterion()) {
			$query .= ' WHERE '.$this->convertCriterion($criterion);
		}
		
		if($criteria->hasGroup())
		{
			$query .= ' GROUP BY '.implode(', ', $criteria->getGroup());
		}
		
		if($criteria->hasHaving()) {
			if($criterion = $criteria->getHaving()->getCriterion()) {
				$query .= ' HAVING '.$this->convertCriterion($criterion);
			}
		}

		return $query;
	}
	
	public function escape($value)
	{
		if(is_array($value)) {
			return array_map(array($this, 'escape'), $value);
		} else if(is_string($value)) {
			return '"'.mysqli_real_escape_string($this->db, $value).'"';
		} else if(is_null($value)) {
			return 'NULL';
		}
		
		return $value;
	}
	
	private function isNestedArray($arr)
	{
		if(!is_array($arr)) {
			return;
		}
		
		foreach($arr as $value) {
			if(is_array($value)) {
				return true;
			}
		}
		
		return false;
	}
	
	private function convertCriterion($criterion)
	{		
		$type = $criterion->getType();
		if(!array_key_exists($type, self::$criterionTypeConversionMap)) {
			throw new MySqlDriverException('Operation not supported '.$type);
		}
		
		$value = $criterion->getValue();
		if($this->isNestedArray($value)) {
			throw new MySqlDriverException('Driver does not support nested array values');
		}
		
		$condition = self::$criterionTypeConversionMap[$type];		
		$value = $this->escape($value);

		if(is_array($value)) {
			if(!in_array($type, array(Criterion::IN, Criterion::NOT_IN))) {
				throw new MySqlDriverException('Operation '.$type.' not supported for array value');
			}
			
			$value = '('.implode(',', $value).')';
			
		} else if($value === 'NULL') {
			if(!in_array($type, array(Criterion::EQUALS, Criterion::NOT_EQUALS))) {
				throw new MySqlDriverException('Operation '.$type.' not supported for null value');
			}
			
			$condition = ($type == Criterion::EQUALS) ? 'IS' : 'IS NOT';
		}
		
		$query = '('.$criterion->getField().' '.$condition.' '.$value.')';	
		
		foreach($criterion->getCriterion() as $iterator) {
			list($iteratorOp, $iteratorCriterion) = $iterator;
			
			if($iteratorOp == Criterion::LOGIC_AND) {
				$query .= ' AND ';
			} else if($iteratorOp == Criterion::LOGIC_OR) {
				$query .= ' OR ';
			} else {
				throw new MySqlDriverException('Logic operator "'.$iteratorOp.'" not supported');
			}

			$query .= $this->convertCriterion($iteratorCriterion);
		}
		
		return '('.$query.')';
	}
	
	public function retrieve($criteria)
	{
		if(! $criteria instanceof MySqlSelectCriteria) {
			throw new IllegalArgumentException('$criteria is not of type MySqlSelectCriteria');
		}

		$this->connect();

		$query = $this->buildQuery($criteria);
		$result = mysqli_query($this->db, $query);

		if(!$result) {
			throw new MySqlDriverException('Failed to run query "'.$query.'" with error "'.mysqli_error($this->db).'"');
		}

		if($criteria->isAggregated()) {
			return new MySqlAggregationResult($result, $criteria);
		} else {
			return new MySqlResult($result);	
		}
	}
	
	public function count($criteria)
	{
		if(! $criteria instanceof MySqlSelectCriteria) {
			throw new IllegalArgumentException('$criteria is not of type Criteria');
		}
		
		$this->connect();

		$query = $this->buildCountQuery($criteria);
		$result = mysqli_query($this->db, $query);
		
		if(!$result) {
			throw new MySqlDriverException('Failed to run query "'.$query.'" with error "'.mysqli_error($this->db).'"');
		}

		$result = new MySqlResult($result);
		
		if($row = $result->getNextRow()) {
			return intval(array_shift($row));
		} else {
			return 0;
		}
	}

	public function insert($collection, $document)
	{
		if(!is_array($document)) {
			throw new IllegalArgumentException('$document is not an array');
		}

		$this->connect();	
		
		$fields = array_keys($document);
		$values = array_values($document);
		
		if($this->isNestedArray($values)) {
			throw new MySqlDriverException('Driver does not support nested array values');
		}
		
		$values = $this->escape($values);
		
		$query = 'INSERT INTO '.$collection.' ('.implode(',', $fields).') VALUES ('.implode(',', $values).')';
		$result = mysqli_query($this->db, $query);
		
		if(!$result) {
			return false;
		}
		
		$id = mysqli_insert_id($this->db);
		
		if($id === false) { // Connection problem
			return false;
		}

		if($id == 0) { // Successful, but no auto-increment id was generated
			return true;
		}
		
		return $id;
	}
	
	public function update($document, $criteria)
	{
		if(!is_array($document)) {
			throw new IllegalArgumentException('$document is not an array');
		}
		if(! $criteria instanceof UpdateCriteria) {
			throw new IllegalArgumentException('$criteria is not of type UpdateCriteria');
		}
		if(!$criteria->hasCriterion()) {
			throw new MySqlDriverException('Criteria does not contain usable criterion');
		}
		
		$this->connect();
		
		$update = array();
		foreach($document as $field => $value) {
			if((!is_scalar($value)) && ($value != null)) {
				throw new MySqlDriverException('Object contains non scalar value for '.$field);
			}
			
			$update[] = $field.'='.$this->escape($value);
		}
		
		$query = 'UPDATE '.$criteria->getCollection().' SET '.implode(', ', $update).' WHERE '.$this->convertCriterion($criteria->getCriterion());
		$result = mysqli_query($this->db, $query);
		
		return $result;
	}
	
	public function upsert($document, $criteria)
	{
		// TODO: insert on duplicate key update ...
		throw new MySqlDriverException('MySqlDriver does not currently support upsert');
	}

	public function delete($criteria)
	{
		if(! $criteria instanceof DeleteCriteria) {
			throw new IllegalArgumentException('$criteria is not of type UpdateCriteria');
		}
		if(!$criteria->hasCriterion()) {
			throw new MongoDriverException('Criteria does not contain usable criterion');
		}
		
		$this->connect();
		
		$query = 'DELETE FROM '.$criteria->getCollection().' WHERE '.$this->convertCriterion($criteria->getCriterion());
		$result = mysqli_query($this->db, $query);
		
		return $result;
	}
	
	public function begin()
	{
		return $this->execute('BEGIN');
	}
	
	public function commit()
	{
		return $this->execute('COMMIT');
	}
	
	public function rollback()
	{
		return $this->execute('ROLLBACK');
	}
	
	public function lock($tables)
	{
		if(!is_array($tables)) {
			throw new IllegalArgumentException('$tables is not of type array');
		}
		
		$lockTables = array();
		foreach($tables as $table => $type) {
			if(!in_array($type, self::$lockTypes)) {
				throw new IllegalArgumentException('Lock tables type "'.$type.'" is not valid');
			}
			
			$lockTables[] = $table.' '.$type;
		}		
		
		return $this->execute('LOCK TABLES '.implode(',', $lockTables));
	}

	public function unlock()
	{
		return $this->execute('UNLOCK TABLES');
	}
	
	public function execute($query)
	{
		if(!is_string($query)) {
			throw new IllegalArgumentException('$query is not of type string');
		}
		
		$query = trim($query);
		if(!strlen($query)) {
			throw new IllegalArgumentException('Empty query');
		}
		
		$this->connect();

		$result = mysqli_query($this->db, $query);
		
		if(!$result) {
			throw new MySqlDriverException('Failed to run query "'.$query.'" with error "'.mysqli_error($this->db).'"');
		}
		
		if($result === true) {
			return true;
		}

		return new MySqlResult($result);
	}
}