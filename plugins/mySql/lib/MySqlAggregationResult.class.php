<?php // (c) Copyright 2012 Bedican Solutions

namespace plugin\mySql;

use framework\db\result\Result;
use framework\db\result\AggregatedResultRow;
use framework\exception\IllegalArgumentException;

class MySqlAggregationResult implements Result
{
	private $result;
	private $criteria;

   	public function __construct($result, $criteria)
	{
		if(!is_array($result)) {
			throw new IllegalArgumentException('$result is not of type array');
		}
		if(! $criteria instanceof MySqlSelectCriteria) {
			throw new IllegalArgumentException('$criteria is not of type MySqlSelectCriteria');
		}
		
		$this->result = $result;
		$this->criteria = $criteria;
	}

	public function __destruct()
	{
		$this->free();
	}

	public function getNextRow()
	{
		if(!$this->result) {
			return false;
		}
		
		$row = mysqli_fetch_assoc($this->result);

		if(!is_array($row)) {
			return false;
		}

		$aggregatedRow = new AggregatedResultRow();
		
		if($this->criteria->hasGroup()) {
			$group = $this->criteria->getGroup();
			foreach($group as $field) {
				$aggregatedRow->addGroupBy($field, $row[$field]);
			}
		}

		if($this->criteria->hasSumFields()) {
			$fields = $this->criteria->getSumFields();
			foreach($fields as $alias => $field) {
				if(array_key_exists($alias, $row)) {
					$aggregatedRow->setSum($field, $row[$alias]);
				}
			}
		}
		
		if($this->criteria->hasMinFields()) {
			$fields = $this->criteria->getMinFields();
			foreach($fields as $alias => $field) {
				if(array_key_exists($alias, $row)) {
					$aggregatedRow->setMin($field, $row[$alias]);
				}
			}
		}
		
		if($this->criteria->hasMaxFields()) {
			$fields = $this->criteria->getMaxFields();
			foreach($fields as $alias => $field) {
				if(array_key_exists($alias, $row)) {
					$aggregatedRow->setMax($field, $row[$alias]);
				}
			}
		}
		
		if($this->criteria->hasAvgFields()) {
			$fields = $this->criteria->getAvgFields();
			foreach($fields as $alias => $field) {
				if(array_key_exists($alias, $row)) {
					$aggregatedRow->setAvg($field, $row[$alias]);
				}
			}
		}

		return $aggregatedRow;
	}

	public function free()
	{
		if(!$this->result) {
			return false;
		}

		return mysqli_free_result($this->result);
	}
}