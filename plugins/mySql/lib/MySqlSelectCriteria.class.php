<?php // (c) Copyright 2011 Bedican Solutions

namespace plugin\mySql;

use framework\db\criteria\SelectCriteria;
use framework\exception\IllegalArgumentException;

class MySqlSelectCriteria extends SelectCriteria
{
	const JOIN_INNER = 'INNER';
	const JOIN_LEFT = 'LEFT';
	const JOIN_RIGHT = 'RIGHT';
	
	private $joins = array();
	
	public function join($table, $on, $type = self::JOIN_INNER)
	{
		if(!is_string($table)) {
			throw new IllegalArgumentException('$table is not of type string');
		}
		if(!is_string($on)) {
			throw new IllegalArgumentException('$on is not of type string');
		}
		if(!is_string($type)) {
			throw new IllegalArgumentException('$type is not of type string');
		}
		
		$this->joins[$type][$table] = $on;
		
		return $this;
	}
	
	public function getJoins()
	{
		return $this->joins;
	}
	
	public function toString()
	{
		$output = parent::toString();
		
		if(!sizeof($this->joins)) {
			return $output;
		}
		
		$output .= ' using joins ';
		
		foreach($this->joins as $type => $tableAndOn) {
			foreach($tableAndOn as $table => $on) {
				$output .= ' '.strtolower($type).'('.$table.' on '.$on.')';
			}
		}
		
		return $output;
	}
}