<?php

include_once(dirname(__FILE__).'/../config/config.inc.php');

use framework\controller\WebController;
use app\TemplateApplication;

$controller = new WebController(new TemplateApplication(dirname(__FILE__).'/..'));
$controller->run();
