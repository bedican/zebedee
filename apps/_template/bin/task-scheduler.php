<?php

chdir(dirname(__FILE__));
include_once(dirname(__FILE__).'/../config/config.inc.php');

use framework\controller\TaskSchedulerController;
use app\TemplateApplication;

$controller = new TaskSchedulerController(new TemplateApplication(dirname(__FILE__).'/..'));
$controller->run();
