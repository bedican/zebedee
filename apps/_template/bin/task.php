<?php

include_once(dirname(__FILE__).'/../config/config.inc.php');

use framework\controller\TaskController;
use app\TemplateApplication;

$controller = new TaskController(new TemplateApplication(dirname(__FILE__).'/..'));
$controller->run();