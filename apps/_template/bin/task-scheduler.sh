#!/bin/bash

BINDIR=$(/usr/bin/dirname $0)
DIR=$(/bin/bash -c "cd $BINDIR/..; /bin/pwd")
PIDFILE=$DIR/tmp/task-scheduler.pid

if [ -f $PIDFILE ]; then
	PID=$(/bin/cat $PIDFILE)
	RUNNING=$(/bin/ps -p $PID -o pid= | /bin/grep -c $PID)
else
	RUNNING=0
fi

if [ $RUNNING -eq 0 ]; then
	/usr/bin/php $BINDIR/task-scheduler.php & /bin/echo $! > $PIDFILE
fi
