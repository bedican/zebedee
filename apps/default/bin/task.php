<?php // (c) Copyright 2011 Bedican Solutions

include_once(dirname(__FILE__).'/../config/config.inc.php');

use framework\controller\TaskController;
use app\DefaultApplication;

$controller = new TaskController(new DefaultApplication(dirname(__FILE__).'/..'));
$controller->run();