<?php // (c) Copyright 2011 Bedican Solutions

ini_set('display_errors', '1');

use framework\lang\ClassLoader;

include_once(dirname(__FILE__).'/../../../config/config.inc.php');
ClassLoader::getInstance()->bind(dirname(__FILE__).'/..');