<?php // (c) Copyright 2011 Bedican Solutions

namespace module\task;

use framework\module\AbstractTaskModule;

class TaskModule extends AbstractTaskModule
{
	protected function doCreateApplication()
	{		
		$this->setView(false);
		
		$console = $this->getConsole();				
		$applicationName = $console->getOption('name');
		
		if(!$applicationName) {
			$console->writeln('No application name specified, use --name=<name>');
			return;
		}
		if(!preg_match('#[a-z][a-z0-9]*#i', $applicationName)) {
			$console->writeln('Invalid application name, must be alphanumeric');
			return;
		}

		$frameworkDir = realpath($this->getApplication()->getApplicationDir().'/../..');
		$applicationClassName = ucfirst($applicationName).'Application';
		$applicationDir = getcwd().'/'.$applicationName;

		$console->writeln('Creating application '.$applicationName);

		// Copy _template directory
		$console->write('Copying files... ');		
		$this->rcopy($frameworkDir.'/apps/_template', $applicationDir);
		rename($applicationDir.'/lib/TemplateApplication.class.php', $applicationDir.'/lib/'.$applicationClassName.'.class.php');
		$console->writeln('done.');
		
		// Fix permissions
		$console->write('Fixing permissions... ');
		chmod($applicationDir.'/cache', 0777);
		chmod($applicationDir.'/log', 0777);
		chmod($applicationDir.'/bin/task-scheduler.sh', 0755);
		$console->writeln('done.');
		
		// Rewrite project name in some files
		$console->write('Rewriting project name... ');
		$this->rewriteText($applicationDir.'/bin/task-scheduler.php', 'TemplateApplication', $applicationClassName);
		$this->rewriteText($applicationDir.'/bin/task.php', 'TemplateApplication', $applicationClassName);
		$this->rewriteText($applicationDir.'/web/index.php', 'TemplateApplication', $applicationClassName);
		$this->rewriteText($applicationDir.'/lib/'.$applicationClassName.'.class.php', 'TemplateApplication', $applicationClassName);
		$console->writeln('done.');
		
		// Rewrite path to framework in ./config/config.inc.php
		$console->write('Linking framework... ');
		$this->rewriteText($applicationDir.'/config/config.inc.php', 'framework_path', $frameworkDir.'/config/config.inc.php');
		$console->writeln('done.');
	}
	
	private function rcopy($srcDir, $destDir)
	{
		if(!is_dir($destDir)) {
			mkdir($destDir);
		}
		
		$files = scandir($srcDir);
		foreach($files as $file) {
			if(($file == '.') || ($file == '..')) {
				continue;
			}

			$file = $srcDir.'/'.$file;

			if(is_file($file)) {
				copy($file, $destDir.'/'.basename($file));
			} else {
				$this->rcopy($file, $destDir.'/'.basename($file));
			}
		}
	}

	private function rewriteText($filename, $fromText, $toText)
	{
		$contents = file_get_contents($filename);
		$contents = str_replace($fromText, $toText, $contents);
		file_put_contents($filename, $contents);
	}
}