<?php // (c) Copyright 2011 Bedican Solutions

namespace app;

use plugin\dbTaskSchedule\DbTaskSchedule;

class TestTaskSchedule extends DbTaskSchedule
{
	protected function initTaskAliases()
	{
		$this->addTaskAlias('test', 'scheduler', 'helloWorld');
	}
}