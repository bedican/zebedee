<?php

include_once(dirname(__FILE__).'/../config/config.inc.php');

use framework\controller\WebController;
use app\TestApplication;

$controller = new WebController(new TestApplication(dirname(__FILE__).'/..'));
$controller->run();
