<?php

chdir(dirname(__FILE__));
include_once(dirname(__FILE__).'/../config/config.inc.php');

use framework\controller\TaskSchedulerController;
use app\TestApplication;

$controller = new TaskSchedulerController(new TestApplication(dirname(__FILE__).'/..'));
$controller->run();
