<?php // (c) Copyright 2011 Bedican Solutions

include_once(dirname(__FILE__).'/../config/config.inc.php');

use framework\controller\TaskController;
use app\TestApplication;

$controller = new TaskController(new TestApplication(dirname(__FILE__).'/..'));
$controller->run();