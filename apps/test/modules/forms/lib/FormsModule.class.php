<?php // (c) Copyright 2011 Bedican Solutions

namespace module\forms;

use framework\module\AbstractWebModule;

class FormsModule extends AbstractWebModule
{
	protected function doDefault()
	{
		$request = $this->getRequest();
		$form = new TestForm($this->getApplication(), $this->getContext());

		$this->getView()->setVar('form', $form, false);

		if(($request->isPost()) && ($request->isPostValue('action', 'do'))) {
			$form->process($request);
		}
	}
}