<?php // (c) Copyright 2011 Bedican Solutions

namespace module\forms;

use framework\form\AbstractForm;
use framework\form\components\InputFieldComponent;
use framework\form\components\TextareaFieldComponent;
use framework\form\components\RadioInputFieldComponent;
use framework\form\components\SelectFieldComponent;
use framework\form\validators\StringFieldValidator;
use framework\form\validators\FileInputFieldValidator;
use framework\form\validators\ListFieldValidator;

class TestForm extends AbstractForm
{
	protected function init()
	{
		$this->addFieldComponent(new InputFieldComponent('field1', array('type' => 'text', 'label' => 'Field 1')));
		$this->addFieldComponent(new InputFieldComponent('field2', array('type' => 'text', 'label' => 'Field 2')));
		$this->addFieldComponent(new InputFieldComponent('field3', array('type' => 'text', 'label' => 'Field 3', 'required' => true)));
		$this->addFieldComponent(new InputFieldComponent('field4', array('type' => 'checkbox', 'label' => 'Field 4')));
		$this->addFieldComponent(new TextareaFieldComponent('field5', array('label' => 'Field 5')));
		$this->addFieldComponent(new RadioInputFieldComponent('field6', array('label' => 'Field 6', 'default-value' => 'val2', 'values' => array('val1' => 'Value 1', 'val2' => 'Value 2', 'val3' => 'value 3'))));
		$this->addFieldComponent(new SelectFieldComponent('field7', array('label' => 'Field 7', 'default-value' => 'val3', 'values' => array('val1' => 'Value 1', 'val2' => 'Value 2', 'val3' => 'value 3'))));
		$this->addFieldComponent(new InputFieldComponent('password', array('type' => 'password', 'label' => 'Password', 'required' => true)));
		$this->addFieldComponent(new InputFieldComponent('file1', array('type' => 'file', 'label' => 'Photo')));
		$this->addFieldComponent(new InputFieldComponent('action', array('type' => 'hidden', 'value' => 'do')));
		$this->addFieldComponent(new InputFieldComponent('submit', array('type' => 'submit', 'value' => 'Submit')));		

		$this->addFieldValidator(new StringFieldValidator('field1', array('min-length' => 4, 'max-length' => 8, 'message' => 'Field 1 must be less than 4 characters')));
		$this->addFieldValidator(new StringFieldValidator('field2', array('min-length' => 2, 'message' => 'Field 2 must be less than 2 characters')));
		$this->addFieldValidator(new StringFieldValidator('field3', array('required' => true)));
		$this->addFieldValidator(new StringFieldValidator('password', array('required' => true)));
		$this->addFieldValidator(new FileInputFieldValidator('file1', array('types' => array('jpg', 'jpeg', 'gif', 'png'))));
		$this->addFieldValidator(new ListFieldValidator('field6', array('values' => array('val1', 'val2', 'val3'))));
		$this->addFieldValidator(new ListFieldValidator('field7', array('values' => array('val1', 'val2', 'val3'))));
	}
}