<?php // (c) Copyright 2011 Bedican Solutions

namespace module\scheduler;

use framework\module\AbstractTaskSchedulerModule;

class SchedulerModule extends AbstractTaskSchedulerModule
{
	protected function doHelloWorld()
	{
		$this->setView(false);
		$this->getConsole()->writeln('Hello World');
	}
}