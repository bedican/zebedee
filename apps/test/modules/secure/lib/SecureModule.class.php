<?php // (c) Copyright 2011 Bedican Solutions

namespace module\secure;

use plugin\auth\SecureWebModule;

class SecureModule extends SecureWebModule
{
	protected function doDefault()
	{
		// Access to this action should be behind a login because we have extended SecureWebModule
	}
}