<?php // (c) Copyright 2013 Bedican Solutions

namespace framework\form;

use framework\net\Request;
use framework\context\WebContext;
use framework\application\Application;
use framework\form\validators\FieldValidator;
use framework\exception\IllegalArgumentException;
use framework\exception\IllegalStateException;
use framework\exception\IOException;
use framework\exception\FileNotFoundException;

class FormProcessor
{
	private $application;
	private $context;
	
	private $handlers;
	private $validators;
	
	private $errors;
	private $processed;
	private $values;
	
	private $filePaths;
	private $fileUploadDir;
	
	public function __construct($application, $context)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(! $context instanceof WebContext) {
			throw new IllegalArgumentException('$context is not of type WebContext');
		}
		
		$this->application = $application;
		$this->context = $context;

		$this->handlers = array();
		$this->validators = array();
		$this->values = array();
		
		$this->filePaths = array();
		
		$this->processed = false;
		$this->errors = false;

		$this->fileUploadDir = null;
		
		$this->init();
	}
	
	protected function init() {}
	
	protected function getApplication()
	{
		return $this->application;
	}
	protected function getContext()
	{
		return $this->context;
	}
	
	public function addEventHandler($handler)
	{
		if(! $handler instanceof FormProcessorEventHandler) {
			throw new IllegalArgumentException('$handler is not of type FormProcessorEventHandler');
		}
		
		$this->handlers[] = $handler;
	}
	
	private function fireEvent($type, $data)
	{
		$event = new FormProcessorEvent($type, array_merge($data, array('processor' => $this)));
		
		foreach($this->handlers as $handler) {
			$handler->onProcessorEvent($event);
		}
	}
	
	public function addValidator($validator)
	{
		if(! $validator instanceof FieldValidator) {
			throw new IllegalArgumentException('$validator is not of type FieldValidator');
		}
		
		$fieldName = $validator->getFieldName();
		
		if(!array_key_exists($fieldName, $this->validators)) {
			$this->validators[$fieldName] = array();
		}
		
		$this->validators[$fieldName][] = $validator;
	}
	
	public function hasErrors()
	{
		return $this->errors;
	}
	
	public function isProcessed()
	{
		return $this->processed;
	}
	
	public function hasValue($fieldName)
	{
		return array_key_exists($fieldName, $this->values);
	}
	
	public function getValue($fieldName, $default = null)
	{
		if(!$this->processed) {
			throw new IllegalStateException('Form has not been processed');
		}
		if(!is_string($fieldName)) {
			throw new IllegalArgumentException('$fieldName is not of type string');
		}
		
		if(!array_key_exists($fieldName, $this->values)) {
			return $default;
		}
		
		return $this->values[$fieldName];
	}
	
	public function process($request)
	{
		if(! $request instanceof Request) {
			throw new IllegalArgumentException('$request is not of type Request');
		}

		$this->errors = false;
		
		$this->fireEvent(FormProcessorEvent::FORM_PROCESSING, array('request' => $request));
		
		$this->processFields($request);
		
		if(!$this->errors) {
			$this->processFiles($request);
		}
		
		$this->processed = true;
		
		if($this->errors) {
			$this->fireEvent(FormProcessorEvent::FORM_ERROR, array('request' => $request));
			return false;
		}

		$this->fireEvent(FormProcessorEvent::FORM_SUCCESS, array('request' => $request));
		return true;
	}
	
	private function processFields($request)
	{
		$fieldNames = array_unique(array_merge(
			$request->isPost() ? $request->getPostNames() : $request->getGetNames(),
			array_keys($this->validators)
		));

		foreach($fieldNames as $fieldName) {
			
			if($request->hasFile($fieldName)) {
				continue;
			}

			$value = $request->isPost() ? $request->getPostValue($fieldName) : $request->getGetValue($fieldName);
			
			$baseEventData = array(
				'request' => $request,
				'field' => $fieldName,
				'value' => $value
			);
			
			if(is_array($value)) {
				
				$this->values[$fieldName] = array();
				
				for($index = 0; $index < sizeof($value); $index++) {
					
					$eventData = array_merge($baseEventData, array('index' => $index));
					$this->fireEvent(FormProcessorEvent::FIELD_PROCESSING, $eventData);
					
					$this->values[$fieldName][] = $value[$index];
					$errors = $this->validate($fieldName, $value[$index]);

					if(sizeof($errors)) {						
						$this->errors = true;
						$this->fireEvent(FormProcessorEvent::FIELD_ERROR, array_merge($eventData, array('errors' => $errors)));			
						continue;
					}
					
					$this->fireEvent(FormProcessorEvent::FIELD_SUCCESS, $eventData);
				}
				
			} else {

				$this->fireEvent(FormProcessorEvent::FIELD_PROCESSING, $baseEventData);
					
				$this->values[$fieldName] = $value;
				$errors = $this->validate($fieldName, $value);

				if(sizeof($errors)) {
					$this->errors = true;
					$this->fireEvent(FormProcessorEvent::FIELD_ERROR, array_merge($baseEventData, array('errors' => $errors)));
					continue;
				}
				
				$this->fireEvent(FormProcessorEvent::FIELD_SUCCESS, $baseEventData);
			}
		}
	}
	
	private function processFiles($request)
	{
		if(!$request->hasFiles()) {
			return;
		}
		
		$filePaths = array();
		
		$fieldNames = $request->getFileFieldNames();		
		foreach($fieldNames as $fieldName) {
			
			$files = array();
			
			$file = $request->getFile($fieldName);
			$isArrayValue = is_array($file['name']);
			
			if($isArrayValue) {
				for($index = 0; $index < sizeof($file['name']); $index++) {
					$files[] = array(
						'name' 		=> $file['name'][$index],
						'type' 		=> $file['type'][$index],
						'tmp_name' 	=> $file['tmp_name'][$index],
						'error' 	=> $file['error'][$index],
						'size' 		=> $file['size'][$index],
					);
				}
			} else {
				$files[] = $file;
			}

			$baseEventData = array(
				'request' => $request,
				'field' => $fieldName,
				'file' => ($isArrayValue) ? $files : $file
			);
			
			foreach($files as $index => $file) {
				
				$eventData = $baseEventData;
				if($isArrayValue) {
					$eventData['index'] = $index;
				}

				$this->fireEvent(FormProcessorEvent::FILE_PROCESSING, $eventData);

				$errors = $this->validate($fieldName, $file);
			
				if(sizeof($errors)) {
					$eventData['errors'] = $errors;
					$this->fireEvent(FormProcessorEvent::FILE_ERROR, $eventData);
					continue;
				}
				
				$this->fireEvent(FormProcessorEvent::FILE_SUCCESS, $eventData);
				
				$destination = $this->processFile($fieldName, $file);
				
				if(is_string($destination)) {
					if($isArrayValue) {
						if(!array_key_exists($fieldName, $filePaths)) {
							$filePaths[$fieldName] = array();
						}
	
						$filePaths[$fieldName][] = $destination;
					} else {
						$filePaths[$fieldName] = $destination;
					}	
				} else {
					$eventData['errors'] = array('Failed to upload file');
					$this->fireEvent(FormProcessorEvent::FILE_ERROR, $eventData);
				}
			}
		}

		$this->filePaths = $filePaths;
	}
	
	private function validate($fieldName, $value)
	{
		$errors = array();
		if(!array_key_exists($fieldName, $this->validators)) {
			return $errors;
		}
			
		$valid = true;
		foreach($this->validators[$fieldName] as $validator) {

			$result = $validator->validate($value);
			if((is_bool($result)) && ($result)) {
				continue;
			}
			
			$valid = false;
			if(!is_string($result)) {
				continue;
			}
			
			$errors[] = $result;
		}
		
		if((!$valid) && (!sizeof($errors))) {
			$errors[] = 'Invalid value';
		}
		
		return $errors;
	}
	
	protected function processFile($fieldName, $file)
	{
		$destination = $this->generateFileDestination($file);
		
		if((!is_string($destination)) || (!@move_uploaded_file($file['tmp_name'], $destination))) {
			
			// Failed to move file to the destination. Clean up the placeholder.
			@unlink($destination);
			return false;
		}
		
		return $destination;
	}
	
	protected function generateFileDestination($file)
	{
		$hash = sha1(time());
		$origExt = pathinfo($file['name'], PATHINFO_EXTENSION);

		$baseDir = $this->getFileUploadDir();
		$subDir = substr($hash, 0, 2).'/'.substr($hash, 2, 2);
		$filename = $hash.'.'.$origExt;

		return $baseDir.'/'.$subDir.'/'.$filename;
	}
	
	public function getFileUploadDir()
	{
		if($this->fileUploadDir != null) {
			return $this->fileUploadDir;
		}

		$uploadDir = $this->context->getConfig()->getUploadDir();

		if(!strlen($uploadDir)) {
			$uploadDir = $this->application->getApplicationDir().'/web/uploads';
		}
		
		$uploadDir = rtrim($uploadDir, '/');
		
		if(!is_dir($uploadDir)) {
			throw new FileNotFoundException('Upload dir not found "'.$uploadDir.'"');
		}
		if(!is_writable($uploadDir)) {
			throw new IOException('Upload dir not writable "'.$uploadDir.'"');
		}
		
		$this->fileUploadDir = $uploadDir;
		return $this->fileUploadDir;
	}
}