<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\validators;

use framework\exception\IllegalArgumentException;

class StringFieldValidator extends AbstractFieldValidator
{
	protected function configure()
	{
		$this->setOptionalParams(array('value', 'max-length', 'min-length'), true);
	}
	
	protected function init()
	{
		if($this->hasParam('value')) {
			if(!is_string($this->getParam('value'))) {
				throw new IllegalArgumentException('value parameter is not of type string');
			}
		}
		if($this->hasParam('max-length')) {
			if(!is_int($this->getParam('max-length'))) {
				throw new IllegalArgumentException('max-length parameter is not of type int');
			}
		}
		if($this->hasParam('min-length')) {
			if(!is_int($this->getParam('min-length'))) {
				throw new IllegalArgumentException('min-length parameter is not of type int');
			}
		}
		if(($this->hasParam('min-length')) && ($this->hasParam('max-length'))) {
			if($this->getParam('min-length') > $this->hasParam('max-length')) {
				throw new IllegalArgumentException('min-length is more than max-length');
			}
		}
	}
	
	protected function doValidate($value)
	{
		if(!is_string($value)) {
			return false;
		}
		
		if(!strlen(trim($value))) {
			return $this->getRequired() ? 'Required' : true;
		}

		if($this->hasParam('value')) {
			if($value != $this->getParam('value')) {
				return 'Value is not '.$this->getParam('value');
			}
		}
		if($this->hasParam('max-length')) {
			if(strlen($value) > $this->getParam('max-length')) {
				return 'Must be less than '.($this->getParam('max-length') + 1).' characters';
			}
		}
		if($this->hasParam('min-length')) {
			if(strlen($value) < $this->getParam('min-length')) {
				return 'Must be more than '.($this->getParam('min-length') - 1).' characters';
			}
		}

		return true;
	}
}