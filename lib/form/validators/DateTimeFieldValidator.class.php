<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\validators;

class DateTimeFieldValidator extends AbstractFieldValidator
{
	protected function configure()
	{
		$this->setDefaultRequired();
		$this->setOptionalParams(array('with-date'));
	}
	
	protected function init()
	{
		if($this->hasParam('with-date')) {
			if(!is_bool($this->getParam('with-date'))) {
				throw new IllegalArgumentException('with-date parameter is not of type bool');
			}
		}
	}
	
	protected function doValidate($value)
	{
		if(!is_string($value)) {
			return false;
		}
		
		if(!strlen(trim($value))) {
			return $this->getRequired() ? 'Required' : true;
		}
			
		$regex = '(1?[0-9]|2[0-4]):([0-5][0-9])'; // HH:MM
		if($this->getParam('with-date', false)) {
			$regex .= ' [0-9]{4}-(0[1-9]?|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])'; // YYYY-MM-DD
		}

		if(!preg_match($regex, $value)) {
			return 'Invalid date/time format';
		}
		
		return true;
	}
}