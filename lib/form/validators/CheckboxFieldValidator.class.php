<?php // (c) Copyright 2013 Bedican Solutions

namespace framework\form\validators;

class CheckboxFieldValidator extends AbstractFieldValidator
{	
	protected function doValidate($value)
	{
		return in_array($value, array(null, '1'));
	}
}