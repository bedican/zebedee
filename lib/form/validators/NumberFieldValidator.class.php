<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\validators;

use framework\exception\IllegalArgumentException;

class NumberFieldValidator extends AbstractFieldValidator
{
	protected function configure()
	{
		$this->setDefaultRequired();		
		$this->setOptionalParams(array('value', 'as-int', 'max', 'min'));
	}
	
	protected function init()
	{
		if($this->hasParam('value')) {
			if(!is_numeric($this->getParam('value'))) {
				throw new IllegalArgumentException('value parameter is not numeric');
			}
		}
		if($this->hasParam('as-int')) {
			if(!is_bool($this->getParam('as-int'))) {
				throw new IllegalArgumentException('as-int parameter is not of type bool');
			}
		}
		if($this->hasParam('max')) {
			if(!is_int($this->getParam('max'))) {
				throw new IllegalArgumentException('max parameter is not of type int');
			}
		}
		if($this->hasParam('min')) {
			if(!is_int($this->getParam('min'))) {
				throw new IllegalArgumentException('min parameter is not of type int');
			}
		}
	}
	
	protected function doValidate($value)
	{
		if(!is_string($value)) {
			return false;
		}
		
		if(!strlen(trim($value))) {
			return $this->getRequired() ? 'Required' : true;
		}

		if($this->getParam('as-int', false)) {
			$valid = ((ctype_digit($value) || (is_int($value))));	
		} else {
			$valid = is_numeric($value);
		}
		
		if($this->hasParam('max')) {
			$valid = $valid && ($value <= $this->getParam('max'));
		}
		if($this->hasParam('min')) {
			$valid = $valid && ($value >= $this->getParam('min'));
		}
		if($this->hasParam('value')) {
			$valid = $valid && ($value == $this->getParam('value'));
		}
		
		return $valid;
	}
}