<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\validators;

interface FieldValidator
{
	public function getFieldName();
	public function getRequired();
	public function validate($request);
}