<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\validators;

use framework\exception\IllegalArgumentException;

class RegexFieldValidator extends AbstractFieldValidator
{
	protected function configure()
	{
		$this->setRequiredParams(array('pattern'));
	}
	
	protected function init()
	{
		if($this->hasParam('pattern')) {
			if(!is_string($this->getParam('pattern'))) {
				throw new IllegalArgumentException('pattern parameter is not of type string');
			}
		}
	}
	
	protected function doValidate($value)
	{
		if(!is_string($value)) {
			return false;
		}
		
		if(!strlen(trim($value))) {
			return $this->getRequired() ? 'Required' : true;
		}

		return (preg_match($this->getParam('pattern'), $value) == 1);
	}	
}