<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\validators;

use framework\exception\IllegalArgumentException;

class ListFieldValidator extends AbstractFieldValidator
{
	protected function configure()
	{
		$this->setDefaultRequired();		
		$this->setRequiredParams(array('values'));
	}
	
	protected function init()
	{
		if(!is_array($this->getParam('values'))) {
			throw new IllegalArgumentException('values parameter is not of type array');
		}
	}
	
	protected function doValidate($value)
	{
		if(($value === null) || ((is_array($value)) && (!sizeof($value)))) {
			return $this->getRequired() ? 'Required' : true;
		}

		$value = is_array($value) ? $value : array($value);
		
		// Ensure all the values in $value are valid by 
		// ensuring that they all exist within the values parameter.
		
		return (sizeof(array_diff($value, $this->getParam('values'))) == 0);
	}
}