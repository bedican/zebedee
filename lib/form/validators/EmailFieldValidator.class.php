<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\validators;

class EmailFieldValidator extends AbstractFieldValidator
{
	protected function configure()
	{
		$this->setDefaultRequired();
	}
	
	protected function doValidate($value)
	{
		if(!is_string($value)) {
			return false;
		}
		
		if(!strlen(trim($value))) {
			return $this->getRequired() ? 'Required' : true;
		}

		if(!preg_match('#^[^@]+@[^.]+(\.[^.]+)+$#i', $value)) {
			return 'Invalid email format';
		}
		
		return true;
	}
}