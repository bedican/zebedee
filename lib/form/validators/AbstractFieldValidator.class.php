<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\validators;

use framework\net\Request;
use framework\exception\IllegalArgumentException;

abstract class AbstractFieldValidator implements FieldValidator
{
	private $fieldName;
	private $params;
	private $required;

	private $requiredParams = array();
	private $optionalParams = array();
	private $optionalMustHave = false;
	
	public final function __construct($fieldName, $params = array())
	{
		if(!is_string($fieldName)) {
			throw new IllegalArgumentException('$fieldName is not of type string');
		}
		
		$this->fieldName = $fieldName;
		$this->params = is_array($params) ? $params : array();
		$this->required = false;
		
		$this->configure();
		
		if(sizeof($missingParams = array_keys(array_diff_key(array_flip($this->requiredParams), $this->params)))) {
			throw new IllegalArgumentException('Missing params: '.implode(', ', $missingParams));
		}
		$optional = array_merge($this->optionalParams, array('message', 'required'));
		if(sizeof($unknownParams = array_keys(array_diff_key($this->params, array_flip(array_merge($this->requiredParams, $optional)))))) {
			throw new IllegalArgumentException('Unknown params: '.implode(', ', $unknownParams));
		}
		if($this->optionalMustHave) {
			if(!sizeof(array_intersect_key($this->params, array_flip($optional)))) {
				throw new IllegalArgumentException('Must provide one of the following params: '.implode(', ', $optional));
			}
		}
		
		if(array_key_exists('required', $this->params)) {
			if(!is_bool($this->params['required'])) {
				throw new IllegalArgumentException('Parameter "required" is not of type bool');
			}
			$this->required = $this->params['required'];
		}
		
		$this->init();
	}
	
	abstract protected function doValidate($request);
	protected function configure() {}
	protected function init() {}

	public function getFieldName()
	{
		return $this->fieldName;
	}
	
	public function getRequired()
	{
		return $this->required;
	}
	
	protected function setDefaultRequired()
	{
		if(!$this->hasParam('required')) {
			$this->setParam('required', true);
		}
	}
	
	protected function getParams($params = array())
	{
		if((is_array($params)) && (sizeof($params))) {
			return array_intersect_key($this->params, array_flip($params));
		}
		
		return $this->params;
	}
	
	protected function hasParam($name)
	{
		return array_key_exists($name, $this->params);
	}
	
	protected function getParam($name, $default = null)
	{
		if(!array_key_exists($name, $this->params)) {
			return $default;
		}
		
		return $this->params[$name];
	}
	
	protected function setParam($name, $value)
	{
		if(!is_string($name)) {
			throw new IllegalArgumentException('$name is not of type string');
		}
		
		$this->params[$name] = $value;
	}
	
	protected function setRequiredParams($params)
	{
		if(!is_array($params)) {
			throw new IllegalArgumentException('$params is not of type array');
		}
		
		$this->requiredParams = $params;
	}
	
	public function setOptionalParams($params, $mustHave = false)
	{
		if(!is_array($params)) {
			throw new IllegalArgumentException('$params is not of type array');
		}
		
		$this->optionalParams = $params;
		$this->optionalMustHave = $mustHave;
	}
	
	public function validate($value)
	{
		if((!is_string($value)) && (!is_array($value)) && ($value !== null)) {
			throw new IllegalArgumentException('$value is not of type string or array');
		}
		
		$valid = $this->doValidate($value);

		if(($valid === false) && (array_key_exists('message', $this->params))) {
			return $this->params['message'];
		}
		
		return $valid;
	}
}