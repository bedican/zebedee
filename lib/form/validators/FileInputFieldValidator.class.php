<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\validators;

use framework\exception\IllegalArgumentException;

class FileInputFieldValidator extends AbstractFieldValidator
{
	protected function configure()
	{
		$this->setOptionalParams(array('types', 'filesize', 'filename-pattern'));
	}
	
	protected function init()
	{
		if($this->hasParam('types')) {
			if(!is_array($this->getParam('types'))) {
				throw new IllegalArgumentException('types parameter is not of type array');
			}
		}
		if($this->hasParam('filesize')) {
			if(!is_int($this->getParam('filesize'))) {
				throw new IllegalArgumentException('filesize parameter is not of type int');
			}
		}
		if($this->hasParam('filename-pattern')) {
			if(!is_string($this->getParam('filename-pattern'))) {
				throw new IllegalArgumentException('filename-pattern parameter is not of type string');
			}
		}
	}
	
	protected function doValidate($file)
	{
		if(!is_array($file)) {
			return false;
		}
		
		switch($file['error']) {
			case UPLOAD_ERR_INI_SIZE:
			case UPLOAD_ERR_FORM_SIZE:
				return 'File exceeds max upload file size';
			case UPLOAD_ERR_PARTIAL:
				return 'File failed to upload, (partial upload)';
			case UPLOAD_ERR_NO_FILE:
				if($this->getRequired()) {
					return 'No file was uploaded';	
				}
				return true;
			case UPLOAD_ERR_NO_TMP_DIR:
				return 'Failed to upload, (missing tmp dir)';
			case UPLOAD_ERR_CANT_WRITE:
				return 'Failed to upload, (could not write file)';
			case UPLOAD_ERR_EXTENSION:
				return 'Failed to upload, (caused by PHP extension)';
			case UPLOAD_ERR_OK:
				break;
        }
        
		if($this->hasParam('filesize')) {
			if($file['size'] > $this->getParam('filesize')) {
				return 'File size exceeds '.number_format($this->getParam('filesize'));
			}
		}
		
		if($this->hasParam('filename-pattern')) {
			if(!preg_match($this->getParam('filename-pattern'), $file['name'])) {
				return 'Filename is not the correct format';
			}
		}
		
		// We go for file extension, rather than mime type for security.
		if($this->hasParam('types')) {
			$valid = false;
			$types = $this->getParam('types');
			foreach($types as $type) {
				$ext = '.'.$type;
				$valid = $valid || (substr($file['name'], (0 - strlen($ext))) == $ext);
			}
			if(!$valid) {
				return 'Incorrect file type';
			}
		}
		
		return true;
	}
}