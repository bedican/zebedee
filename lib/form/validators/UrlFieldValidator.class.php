<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\validators;

class UrlFieldValidator extends AbstractFieldValidator
{
	protected function configure()
	{
		$this->setDefaultRequired();
	}
	
	protected function doValidate($value)
	{
		if(!is_string($value)) {
			return false;
		}
		
		if(!strlen(trim($value))) {
			return $this->getRequired() ? 'Required' : true;
		}

		if(!preg_match('#^https?://[^.]+(\.[^.]+)+((/|(/[^/]+)+)(\?.*)?)?$#i', $value)) {
			return 'Invalid url format';
		}
		
		return true;
	}
}