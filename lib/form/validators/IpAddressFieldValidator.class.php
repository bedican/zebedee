<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\form\validators;

class IpAddressFieldValidator extends AbstractFieldValidator
{
	protected function configure()
	{
		$this->setDefaultRequired();
	}
	
	protected function doValidate($value)
	{
		if(!is_string($value)) {
			return false;
		}
		
		if(!strlen(trim($value))) {
			return $this->getRequired() ? 'Required' : true;
		}

		if(!preg_match('#^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$#i', $value)) {
			return 'Invalid ip address format';
		}
		
		return true;
	}
}