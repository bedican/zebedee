<?php // (c) Copyright 2013 Bedican Solutions

namespace framework\form;

use framework\application\Application;
use framework\context\WebContext;
use framework\net\Request;
use framework\exception\IllegalArgumentException;

abstract class AbstractForm implements Form
{
	private $application;
	private $context;
	private $params;

	private $renderer;
	private $processor;
	
	public function __construct($application, $context, $params = array())
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(! $context instanceof WebContext) {
			throw new IllegalArgumentException('$context is not of type WebContext');
		}
		if(!is_array($params)) {
			throw new IllegalArgumentException('$params is not of type array');
		}
		
		$this->application = $application;
		$this->context = $context;
		$this->params = $params;
		
		$this->renderer = new FormRenderer();

		$this->processor = new FormProcessor($this->application, $this->context);
		$this->processor->addEventHandler($this->renderer);
		
		$this->init();
	}
	
	protected function getApplication()
	{
		return $this->application;
	}
	
	protected function getContext()
	{
		return $this->context;
	}
	
	protected function getParam($key, $default = null)
	{
		if(!array_key_exists($key, $this->params)) {
			return $default;
		}
		
		return $this->params[$key];
	}
	
	public function render()
	{
		return $this->renderer->render();
	}
	
	public function process($request)
	{
		if(! $request instanceof Request) {
			throw new IllegalArgumentException('$request is not of type Request');
		}
		
		return $this->processor->process($request);
	}
	
	abstract protected function init();
	
	protected function getRenderer()
	{
		return $this->renderer;
	}
	
	protected function getProcessor()
	{
		return $this->processor;
	}
}