<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form;

use framework\net\Request;
use framework\exception\IllegalArgumentException;
use framework\exception\IllegalStateException;

class CompositeForm implements Form
{
	private $forms;
	
	public function __construct()
	{
		$this->forms = array();
	}
	
	public function addForm($form)
	{
		if(! $form instanceof Form) {
			throw new IllegalArgumentException('$form is not of type Form');
		}

		$this->forms[] = $form;
	}
	
	public function getForms()
	{
		return $this->forms;
	}

	public function render()
	{
		$content = '';
		foreach($this->forms as $form) {
			$content .= $form->render();
		}
		
		return $content;
	}

	public function process($request)
	{
		if(! $request instanceof Request) {
			throw new IllegalArgumentException('$request is not of type Request');
		}
		
		$success = true;
		foreach($this->forms as $form) {
			$success = $form->process($request) && $success;
		}
		
		return $success;
	}
}