<?php // (c) Copyright 2013 Bedican Solutions

namespace framework\form;

class FormProcessorEvent
{
	const FORM_PROCESSING = 'form-processing';
	const FORM_ERROR = 'form-error';
	const FORM_SUCCESS = 'form-success';
	const FIELD_PROCESSING = 'field-processing';
	const FIELD_ERROR = 'field-error';
	const FIELD_SUCCESS = 'field-success';
	const FILE_PROCESSING = 'file-processing';
	const FILE_ERROR = 'file-error';
	const FILE_SUCCESS = 'file-success';
	
	private $type;
	private $data;
	
	public function __construct($type, $data = array())
	{
		$this->type = $type;
		$this->data = $data;
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function getData()
	{
		return $this->data;
	}
}