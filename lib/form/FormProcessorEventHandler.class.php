<?php // (c) Copyright 2013 Bedican Solutions

namespace framework\form;

interface FormProcessorEventHandler
{
	public function onProcessorEvent($event);
}