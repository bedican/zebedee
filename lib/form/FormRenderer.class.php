<?php // (c) Copyright 2013 Bedican Solutions

namespace framework\form;

use framework\form\components\FieldComponent;
use framework\exception\IllegalArgumentException;

class FormRenderer implements FormProcessorEventHandler
{
	const STACK_COMPONENT = 'component';
	const STACK_COMPONENT_GROUP = 'component-group';
	const STACK_PLACEHOLDER = 'placeholder';
	
	private $components;
	private $placeholders;
	
	private $errors;
	private $renderErrors;
	
	private $renderStack;
	
	public function __construct()
	{
		$this->components = array();
		$this->placeholders = array();
		
		$this->errors = array();
		$this->renderErrors = true;
		
		$this->renderStack = array();
	}
	
	public function onProcessorEvent($event)
	{
		if(! $event instanceof FormProcessorEvent) {
			throw new IllegalArgumentException('$event is not of type FormProcessorEvent');
		}
		
		switch($event->getType())
		{
			case FormProcessorEvent::FIELD_PROCESSING:
				$eventData = $event->getData();
				
				$fieldName = $eventData['field'];
				$index = array_key_exists('index', $eventData) ? $eventData['index'] : 0;
				$value = array_key_exists('index', $eventData) ? $eventData['value'][$index] : $eventData['value'];
				
				if(!array_key_exists($fieldName, $this->components)) {
					return;
				}
				if(!array_key_exists($index, $this->components[$fieldName])) {
					return;
				}
				
				$this->components[$fieldName][$index]->setValue($value);
				
			break;

			case FormProcessorEvent::FIELD_ERROR:
			case FormProcessorEvent::FILE_ERROR:
				
				$eventData = $event->getData();
				
				$errors = $eventData['errors'];
				$fieldName = $eventData['field'];
				$index = array_key_exists('index', $eventData) ? $eventData['index'] : 0;
				
				if(!array_key_exists($fieldName, $this->errors)) {
					$this->errors[$fieldName] = array();
				}
				
				$this->errors[$fieldName][$index] = $errors;
			break;
		}
	}
	
	public function hideErrors()
	{
		$this->renderErrors = false;
	}
	
	public function addComponent($component)
	{
		if(! $component instanceof FieldComponent) {
			throw new IllegalArgumentException('$component is not of type FieldComponent');
		}
		
		$index = $this->doAddComponent($component);
		$this->renderStack[] = array(self::STACK_COMPONENT, $component->getName(), $index);
	}
	
	public function addGroupedComponents($group, $components)
	{
		if(!is_string($group)) {
			throw new IllegalArgumentException('$group is not of type string');
		}
		if(!is_array($components)) {
			throw new IllegalArgumentException('$components is not of type array');
		}
		
		$grouped = array();
		foreach($components as $component) {
			$index = $this->doAddComponent($component);
			$grouped[] = array($component->getName(), $index);
		}
		
		$this->renderStack[] = array(self::STACK_COMPONENT_GROUP, $group, $grouped);
	}
	
	public function addPlaceholder($placeholder)
	{
		if(!is_string($placeholder)) {
			throw new IllegalArgumentException('$placeholder is not of type string');
		}
		
		$this->renderStack[] = array(self::STACK_PLACEHOLDER, $placeholder);
	}
	
	public function injectComponentToPlaceholder($placeholder, $component)
	{
		if(!is_string($placeholder)) {
			throw new IllegalArgumentException('$placeholder is not of type string');
		}
		if(! $component instanceof FieldComponent) {
			throw new IllegalArgumentException('$component is not of type FieldComponent');
		}
		
		if(!array_key_exists($placeholder, $this->placeholders)) {
			$this->placeholders[$placeholder] = array();
		}

		$index = $this->doAddComponent($component);
		$this->placeholders[$placeholder][] = array(self::STACK_COMPONENT, $component->getName(), $index);
	}
	
	public function injectGroupedComponentsToPlaceholder($placeholder, $group, $components)
	{
		if(!is_string($group)) {
			throw new IllegalArgumentException('$group is not of type string');
		}
		if(!is_array($components)) {
			throw new IllegalArgumentException('$components is not of type array');
		}
		
		$grouped = array();
		foreach($components as $component) {
			$index = $this->doAddComponent($component);
			$grouped[] = array($component->getName(), $index);
		}
		
		$this->placeholders[$placeholder][] = array(self::STACK_COMPONENT_GROUP, $group, $grouped);
	}
	
	private function doAddComponent($component)
	{
		if(! $component instanceof FieldComponent) {
			throw new IllegalArgumentException('$component is not of type FieldComponent');
		}
		
		if(array_key_exists($component->getName(), $this->components)) {
			if($component->isArrayValue() != $this->components[$component->getName()][0]->isArrayValue()) {
				throw new IllegalArgumentException('Mismatch in array value type');
			}
			if($component->isResizable() != $this->components[$component->getName()][0]->isResizable()) {
				throw new IllegalArgumentException('Mismatch in array resizable option');
			}
			if(!$this->components[$component->getName()][0]->isArrayValue()) {
				throw new IllegalArgumentException('Component already exists');
			}
			if(get_class($component) != get_class($this->components[$component->getName()][0])) {
				throw new IllegalArgumentException('Can not add component of a different type');
			}
		}
		
		if(!array_key_exists($component->getName(), $this->components)) {
			$this->components[$component->getName()] = array();
		}
		
		$this->components[$component->getName()][] = $component;
		
		return (sizeof($this->components[$component->getName()]) - 1);
	}
	
	protected function isComponentArrayValue($name)
	{
		if(!array_key_exists($name, $this->components)) {
			throw new IllegalArgumentException('Unknown component '.$name);
		}
		
		return $this->components[$name][0]->isArrayValue();
	}
	
	protected function isComponentResizable($name)
	{
		if(!array_key_exists($name, $this->components)) {
			throw new IllegalArgumentException('Unknown component '.$name);
		}
		
		return $this->components[$name][0]->isResizable();
	}
	
	public function hasComponent($name)
	{
		return array_key_exists($name, $this->components);
	}
	
	public function getComponent($name)
	{
		if(!array_key_exists($name, $this->components)) {
			throw new IllegalArgumentException('Unknown component '.$name);
		}
		
		if($this->isComponentArrayValue($name)) {
			return $this->components[$name];
		}
		
		return $this->components[$name][0];
	}
	
	public function getComponentNames()
	{
		return array_keys($this->components);
	}
	
	public function render()
	{
		$content = '';
		foreach($this->renderStack as $params) {
			
			$type = array_shift($params);
			
			switch($type) {
				case self::STACK_PLACEHOLDER:
					$content .= $this->renderPlaceholder($params);
				break;
				case self::STACK_COMPONENT:
					$content .= $this->renderComponent($params);
				break;
				case self::STACK_COMPONENT_GROUP:
					$content .= $this->renderComponentGroup($params);
				break;
			}
		}

		return $content;
	}
	
	private function renderPlaceholder($params)
	{
		$content = '';
		
		$placeholder = array_shift($params);
		
		if(!array_key_exists($placeholder, $this->placeholders)) {
			return $content;
		}
		
		foreach($this->placeholders[$placeholder] as $params) {
			
			$type = array_shift($params);
			
			switch($type) {
				case self::STACK_COMPONENT:
					$content .= $this->renderComponent($params);
				break;
				case self::STACK_COMPONENT_GROUP:
					$content .= $this->renderComponentGroup($params);
				break;
			}
		}

		return $content;
	}
	
	private function renderComponent($params)
	{
		list($fieldName, $index) = $params;		
		$component = $this->components[$fieldName][$index];	

		$content = '<div class="form-field field-'.str_replace(' ', '-', $component->getName());
		if($component->getRequired()) {
			$content .= ' form-field-required';
		}

		if(array_key_exists($component->getName(), $this->errors)) {
			if(array_key_exists($index, $this->errors[$component->getName()])) {
				$content .= ' form-field-has-error';
			}
		}
		$content .= '">';
		
		if($this->renderErrors) {
			if(array_key_exists($component->getName(), $this->errors)) {
				if(array_key_exists($index, $this->errors[$component->getName()])) {
					$errors = array_unique($this->errors[$component->getName()][$index]);
					foreach($errors as $error) {
						$content .= '<div class="form-field-error">'.$error.'</div>';
					}
				}
			}
		}
		
		$content .= $component->render();
		$content .= '</div>' . "\n";
		
		return $content;
	}
	
	private function renderComponentGroup($params)
	{
		$group = array_shift($params);
		$components = array_shift($params);
		
		$content = '<div class="form-field-group group-'.str_replace(' ', '-', $group).'">';
		
		foreach($components as $component) {
			$content .= $this->renderComponent($component);
		}
		
		$content .= '</div>';
		
		return $content;
	}
}