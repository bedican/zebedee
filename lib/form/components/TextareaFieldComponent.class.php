<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\components;

use framework\exception\IllegalArgumentException;

class TextareaFieldComponent extends AbstractFieldComponent
{
	protected function configure()
	{
		$this->setOptionalParams(array('id', 'class', 'label'));
	}
	
	protected function init()
	{
		if($this->hasParam('id')) {
			if(!is_string($this->getParam('id'))) {
				throw new IllegalArgumentException('id parameter is not a string');
			}
		}
		if($this->hasParam('class')) {
			if(!is_string($this->getParam('class'))) {
				throw new IllegalArgumentException('class parameter is not a string');
			}
		}
		if($this->hasParam('label')) {
			if(!is_string($this->getParam('label'))) {
				throw new IllegalArgumentException('label parameter is not a string');
			}
		}
	}
	
	protected function doRender()
	{
		$content = '<div class="form-field-type-textarea">';

		if($label = $this->getParam('label', false)) {
			$content .= '<label';
			if($id = $this->getParam('id', false)) {
				$content .= ' for="'.$id.'"';
			}
			$content .= '>'.htmlspecialchars($label, ENT_QUOTES);
			if($this->getRequired()) {
				$content .= '<span>*</span>';
			}
			$content .= '</label>';
		}
		
		$name = $this->getName();
		$name .= $this->isArrayValue() ? '[]' : ''; 
		
		$attributes = array_merge($this->getParams(array('id', 'class')), array('name' => $name));

		$content .= $this->renderTag('textarea', $attributes, htmlspecialchars($this->getValue(), ENT_QUOTES));
		$content .= '</div>';
		
		return $content;
	}
}