<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\components;

use framework\exception\IllegalArgumentException;

abstract class AbstractFieldComponent implements FieldComponent
{
	private $name;
	private $value;
	private $params;
	private $required;
	private $isArrayValue;
	private $isResizable;

	private $requiredParams = array();
	private $optionalParams = array();
	private $optionalMustHave = false;
	
	public final function __construct($name, $params = array())
	{
		if(!is_string($name)) {
			throw new IllegalArgumentException('$name is not of type string');
		}

		$this->value = '';
		$this->name = $name;
		$this->required = false;
		$this->isArrayValue = false;
		$this->isResizable = false;
		$this->params = is_array($params) ? $params : array();
	
		$this->configure();

		if(sizeof($missingParams = array_keys(array_diff_key(array_flip($this->requiredParams), $this->params)))) {
			throw new IllegalArgumentException('Missing params: '.implode(', ', $missingParams));
		}
		$optional = array_merge($this->optionalParams, array('value', 'required', 'is-array', 'is-array-resizable'));
		if(sizeof($unknownParams = array_keys(array_diff_key($this->params, array_flip(array_merge($this->requiredParams, $optional)))))) {
			throw new IllegalArgumentException('Unknown params: '.implode(', ', $unknownParams));
		}
		if($this->optionalMustHave) {
			if(!sizeof(array_intersect_key($this->params, array_flip($this->optionalParams)))) {
				throw new IllegalArgumentException('Must provide one of the following params: '.implode(', ', $this->optionalParams));
			}
		}
		
		if(array_key_exists('value', $this->params)) {
			$this->setValue($this->params['value']);
		}
		if(array_key_exists('required', $this->params)) {
			if(!is_bool($this->params['required'])) {
				throw new IllegalArgumentException('Parameter "required" is not of type bool');
			}
			$this->required = $this->params['required'];
		}
		if(array_key_exists('is-array', $this->params)) {
			if(!is_bool($this->params['is-array'])) {
				throw new IllegalArgumentException('Parameter "is-array" is not of type bool');
			}
			$this->isArrayValue = $this->params['is-array'];
		}
		if(array_key_exists('is-array-resizable', $this->params)) {
			if(!is_bool($this->params['is-array-resizable'])) {
				throw new IllegalArgumentException('Parameter "is-array-resizable" is not of type bool');
			}
			$this->isResizable = $this->params['is-array-resizable'];
			
			if($this->isResizable) {
				$this->isArrayValue = true;
			}
		}

		$this->init();
	}

	abstract protected function doRender();
	protected function configure() {}
	protected function init() {}
	
	public function getValue()
	{
		return $this->value;
	}

	public function getName()
	{
		return $this->name;
	}
	
	public function getRequired()
	{
		return $this->required;
	}
	
	public function isArrayValue()
	{
		return $this->isArrayValue;
	}
	
	public function isResizable()
	{
		return $this->isResizable;
	}
	
	public function setValue($value)
	{
		$this->value = $value;
	}
	
	protected function setDefaultRequired()
	{
		if(!$this->hasParam('required')) {
			$this->setParam('required', true);
		}
	}
	
	protected function setIsArrayValue($value = true)
	{
		$this->setParam('is-array', $value);
	}

	protected function getParams($params = array())
	{
		if((is_array($params)) && (sizeof($params))) {
			return array_intersect_key($this->params, array_flip($params));
		}
		
		return $this->params;
	}
	
	protected function hasParam($name)
	{
		return array_key_exists($name, $this->params);
	}
	
	protected function getParam($name, $default = null)
	{
		if(!array_key_exists($name, $this->params)) {
			return $default;
		}
		
		return $this->params[$name];
	}
	
	protected function setParam($name, $value)
	{
		if(!is_string($name)) {
			throw new IllegalArgumentException('$name is not of type string');
		}
		
		$this->params[$name] = $value;
	}
	
	protected function setRequiredParams($params)
	{
		if(!is_array($params)) {
			throw new IllegalArgumentException('$params is not of type array');
		}
		
		$this->requiredParams = $params;
	}
	
	public function setOptionalParams($params, $mustHave = false)
	{
		if(!is_array($params)) {
			throw new IllegalArgumentException('$params is not of type array');
		}
		
		$this->optionalParams = $params;
		$this->optionalMustHave = $mustHave;
	}
	
	public function render()
	{
		return $this->doRender();
	}
	
	protected function renderTag($name, $attributes, $contents = false)
	{
		if(!strlen($name)) {
			throw new IllegalArgumentException('$name is empty string');
		}
		if(!is_array($attributes)) {
			throw new IllegalArgumentException('$attributes is not of type array');
		}
		
		$out = '<'.$name;
		foreach($attributes as $attName => $attValue) {
			$out .= ' '.$attName.'="'.htmlspecialchars($attValue, ENT_QUOTES).'"';
		}
		
		if(is_string($contents)) {
			$out .= '>'.$contents.'</'.$name.'>';
		} else {		
			$out .= ' />';
		}
		
		return $out;
	}
}