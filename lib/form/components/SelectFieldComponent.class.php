<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\components;

use framework\exception\IllegalArgumentException;

class SelectFieldComponent extends AbstractFieldComponent
{
	protected function configure()
	{
		$this->setDefaultRequired();
		$this->setRequiredParams(array('values'));
		$this->setOptionalParams(array('id', 'class', 'label', 'default-value'));
	}
	
	protected function init()
	{
		if(!is_array($this->getParam('values'))) {
			throw new IllegalArgumentException('values parameter is not an array');
		}
		if($this->hasParam('default-value')) {
			if($this->getParam('default-value') != null) {
				if(!is_string($this->getParam('default-value'))) {
					throw new IllegalArgumentException('default-value parameter is not a string');
				}
				if(!in_array($this->getParam('default-value'), array_keys($this->getParam('values')))) {
					throw new IllegalArgumentException('default-value parameter is not within values parameter');
				}
			}
		}
		if($this->hasParam('id')) {
			if(!is_string($this->getParam('id'))) {
				throw new IllegalArgumentException('id parameter is not a string');
			}
		}
		if($this->hasParam('class')) {
			if(!is_string($this->getParam('class'))) {
				throw new IllegalArgumentException('class parameter is not a string');
			}
		}
		if($this->hasParam('label')) {
			if(!is_string($this->getParam('label'))) {
				throw new IllegalArgumentException('label parameter is not a string');
			}
		}
	}
	
	protected function doRender()
	{
		$content = '<div class="form-field-type-select">';
		
		if($label = $this->getParam('label', false)) {
			$content .= '<label';
			if($id = $this->getParam('id', false)) {
				$content .= ' for="'.$id.'"';
			}
			$content .= '>'.htmlspecialchars($label, ENT_QUOTES).'</label>';
		}

		$values = $this->getParam('values');
		$currentValue = $this->getValue();
		
		if(!array_key_exists($currentValue, $values)) {
			$keys = array_keys($values);
			$currentValue = $this->getParam('default-value', array_shift($keys));
			$currentValue = ($currentValue !== null) ? strval($currentValue) : null;
		}

		$optionsContent = '';
		foreach($values as $value => $display) {
			
			$attributes = array('value' => $value);
			if($currentValue == $value) {
				$attributes['selected'] = 'selected';
			}
			
			$optionsContent .= $this->renderTag('option', $attributes, htmlspecialchars($display, ENT_QUOTES));
		}

		$name = $this->getName();
		$name .= $this->isArrayValue() ? '[]' : ''; 
		
		$attributes = array_merge(
			$this->getParams(array('class', 'id')), array('name' => $name)
		);
		
		$content .= $this->renderTag('select', $attributes, $optionsContent);	
		$content .= '</div>';
		
		return $content;
	}
}