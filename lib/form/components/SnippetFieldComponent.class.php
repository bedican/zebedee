<?php // (c) Copyright 2013 Bedican Solutions

namespace framework\form\components;

use framework\application\Application;
use framework\context\WebContext;
use framework\view\renderer\PhpTemplateRenderer;
use framework\exception\IllegalArgumentException;

class SnippetFieldComponent extends AbstractFieldComponent
{
	protected function configure()
	{
		$this->setRequiredParams(array('view', 'application', 'context'));
		$this->setOptionalParams(array('vars'));
	}
	
	protected function init()
	{
		if($this->hasParam('vars')) {
			if(!is_array($this->getParam('vars'))) {
				throw new IllegalArgumentException('vars parameter is not an array');
			}
		}
		
		$application = $this->getParam('application');
		$context = $this->getParam('context');
		
		if(!$application instanceof Application) {
			throw new IllegalArgumentException('application parameter is not of type Application');
		}
		if(!$context instanceof WebContext) {
			throw new IllegalArgumentException('context parameter is not of type WebContext');
		}
	}
	
	public function doRender()
	{
		// If the view name is prefixed with "app." then the template exists at the application level. 
		// This allows forms to exist at the application level and be reusable across modules
		// or have application level templates.
		
		$view = $this->getParam('view');
		$application = $this->getParam('application');
		$context = $this->getParam('context');

		if(substr($view, 0, 4) == 'app.') {
			$view = substr($view, 4);
			$basePath = $application->getApplicationDir();
		} else {
			$basePath = $application->getClassLoader()->getModulePath($context->getModule());
		}
		
		$renderer = new PhpTemplateRenderer();
		
		$template = $basePath . '/views/forms/'.$view.'.php';		

		$content = '<div class="form-field-type-snippet">';
		$content .= $renderer->render($template, $this->getParam('vars', array()));
		$content .= '</div>';
		
		return $content;
	}
}