<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\components;

use framework\exception\IllegalArgumentException;

class InputFieldComponent extends AbstractFieldComponent
{
	protected function configure()
	{
		$this->setRequiredParams(array('type'));
		$this->setOptionalParams(array('id', 'class', 'label'));
	}
	
	protected function init()
	{
		if(!in_array($this->getParam('type'), array('hidden', 'password', 'text', 'checkbox', 'submit', 'file'))) {
			throw new IllegalArgumentException('Invalid type: '.$this->getParam('type'));
		}
		if($this->hasParam('id')) {
			if(!is_string($this->getParam('id'))) {
				throw new IllegalArgumentException('id parameter is not a string');
			}
		}
		if($this->hasParam('class')) {
			if(!is_string($this->getParam('class'))) {
				throw new IllegalArgumentException('class parameter is not a string');
			}
		}
		if($this->hasParam('label')) {
			if(!is_string($this->getParam('label'))) {
				throw new IllegalArgumentException('label parameter is not a string');
			}
		}
	}
	
	private function isTypePassword()
	{
		return ($this->getParam('type') == 'password');
	}
	private function isTypeFile()
	{
		return ($this->getParam('type') == 'file');
	}
	private function isTypeCheckbox()
	{
		return ($this->getParam('type') == 'checkbox');
	}
	
	public function setValue($value)
	{
		if(($this->isTypePassword()) || ($this->isTypeFile())) {
			return;
		}
		
		return parent::setValue($value);
	}
	
	protected function doRender()
	{
		$content = '<div class="form-field-type-input-'.$this->getParam('type').'">';

		if(!in_array($this->getParam('type'), array('hidden', 'submit'))) {
			if($label = $this->getParam('label', false)) {
				$content .= '<label';
				if($id = $this->getParam('id', false)) {
					$content .= ' for="'.$id.'"';
				}
				$content .= '>'.htmlspecialchars($label, ENT_QUOTES);
				if((!$this->isTypePassword()) && ($this->getRequired())) {
					$content .= '<span>*</span>';
				}
				$content .= '</label>';
			}
		}
		
		$name = $this->getName();
		$name .= $this->isArrayValue() ? '[]' : '';

		$attributes = array_merge($this->getParams(array('id', 'class', 'type')), array('name' => $name));

		if($this->isTypeCheckbox()) {
			$attributes['value'] = '1';
			if($this->getValue() == '1') {
				$attributes['checked'] = 'checked';
			}
		} else {
			$attributes['value'] = $this->getValue();
		}

		$content .= $this->renderTag('input', $attributes);
		$content .= '</div>';
		
		return $content;
	}
}