<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\components;

use framework\exception\IllegalArgumentException;

class RadioInputFieldComponent extends AbstractFieldComponent
{
	protected function configure()
	{
		$this->setDefaultRequired();
		$this->setRequiredParams(array('values', 'default-value'));
		$this->setOptionalParams(array('id-prefix', 'class', 'label'));
	}
	
	protected function init()
	{
		if(!is_array($this->getParam('values'))) {
			throw new IllegalArgumentException('values parameter is not an array');
		}
		if(!is_string($this->getParam('default-value'))) {
			throw new IllegalArgumentException('default-value parameter is not a string');
		}
		if(!in_array($this->getParam('default-value'), array_keys($this->getParam('values')))) {
			throw new IllegalArgumentException('default-value parameter is not within values parameter');
		}
		if($this->hasParam('id-prefix')) {
			if(!is_string($this->getParam('id-prefix'))) {
				throw new IllegalArgumentException('id-prefix parameter is not a string');
			}
			if(!strlen($this->getParam('id-prefix'))) {
				throw new IllegalArgumentException('id-prefix parameter is empty');
			}
		}
		if($this->hasParam('class')) {
			if(!is_string($this->getParam('class'))) {
				throw new IllegalArgumentException('class parameter is not a string');
			}
		}
		if($this->hasParam('label')) {
			if(!is_string($this->getParam('label'))) {
				throw new IllegalArgumentException('label parameter is not a string');
			}
		}
	}
	
	protected function doRender()
	{
		$content = '<div class="form-field-type-input-radio form-field-type-multi"><fieldset>';
		
		if($label = $this->getParam('label', false)) {
			$content .= '<legend>'.htmlspecialchars($label, ENT_QUOTES);
			if($this->getRequired()) {
				$content .= '<span>*</span>';
			}
			$content .= '</legend>';
		}
		
		$name = $this->getName();
		$name .= $this->isArrayValue() ? '[]' : ''; 

		$idPrefix = $this->getParam('id-prefix', false);
		$values = $this->getParam('values');
		
		$currentValue = $this->getValue();
		if(!array_key_exists($currentValue, $values)) {
			$currentValue = $this->getParam('default-value');
		}

		foreach($values as $value => $label) {

			$attributes = array_merge(
				$this->getParams(array('class')), 
				array('name' => $name, 'type' => 'radio', 'value' => $value)
			);
			
			if($idPrefix) {
				$attributes['id'] = $idPrefix.'-'.$value;
			}

			if($currentValue == $value) {
				$attributes['checked'] = 'checked';
			}
			
			$content .= '<div><label';
			if(array_key_exists('id', $attributes)) {
				$content .= ' for="'.$attributes['id'].'"';
			}
			$content .= '>'.htmlspecialchars($label, ENT_QUOTES).'</label>'.$this->renderTag('input', $attributes).'</div>';
		}
		
		$content .= '</fieldset></div>';
		
		return $content;
	}
}