<?php // (c) Copyright 2013 Bedican Solutions

namespace framework\form\components;

use framework\view\escaper\Escaper;
use framework\exception\IllegalArgumentException;

class HeadingFieldComponent extends AbstractFieldComponent
{
	protected function configure()
	{
		$this->setRequiredParams(array('content'));
	}
	
	protected function init()
	{
		if(!is_string($this->getParam('content'))) {
			throw new IllegalArgumentException('content parameter is not of type string');
		}
	}
	
	public function doRender()
	{
		$escaper = new Escaper();
		$content = $escaper->escape($this->getParam('content'));
		
		return '<div class="form-heading">'.$content.'</div>' . "\n";
	}
}