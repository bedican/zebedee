<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\components;

interface FieldComponent
{
	public function getName();
	public function getValue();
	
	public function getRequired();
	public function setValue($value);
	
	public function isArrayValue();
	public function isResizable();
	
	public function render();
}