<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\form\components;

use framework\exception\IllegalArgumentException;

class MultiCheckboxInputFieldComponent extends AbstractFieldComponent
{
	protected function configure()
	{
		// We call setIsArrayValue with false to enforce this component handling all the values.
		
		$this->setIsArrayValue(false);
		$this->setRequiredParams(array('values'));
		$this->setOptionalParams(array('default-values', 'id-prefix', 'class', 'label'));
	}
	
	protected function init()
	{
		if(!is_array($this->getParam('values'))) {
			throw new IllegalArgumentException('values parameter is not an array');
		}
		if($this->hasParam('default-values')) {
			if(!is_array($this->getParam('default-values'))) {
				throw new IllegalArgumentException('default-values parameter is not an array');
			}
			if(sizeof($diff = array_diff_key(array_flip($this->getParam('default-values')), $this->getParam('values')))) {
				throw new IllegalArgumentException('default-values parameter contains invalid values: '.implode(', ', array_flip($diff)));
			}
		}
		if($this->hasParam('id-prefix')) {
			if(!is_string($this->getParam('id-prefix'))) {
				throw new IllegalArgumentException('id-prefix parameter is not a string');
			}
			if(!strlen($this->getParam('id-prefix'))) {
				throw new IllegalArgumentException('id-prefix parameter is empty');
			}
		}
		if($this->hasParam('class')) {
			if(!is_string($this->getParam('class'))) {
				throw new IllegalArgumentException('class parameter is not a string');
			}
		}
		if($this->hasParam('label')) {
			if(!is_string($this->getParam('label'))) {
				throw new IllegalArgumentException('label parameter is not a string');
			}
		}
	}
	
	protected function doRender()
	{
		$content = '<div class="form-field-type-input-checkbox-multi form-field-type-multi"><fieldset>';
		
		if($label = $this->getParam('label', false)) {
			$content .= '<legend>'.htmlspecialchars($label, ENT_QUOTES);
			if($this->getRequired()) {
				$content .= '<span>*</span>';
			}
			$content .= '</legend>';
		}

		$name = $this->getName().'[]';
		$values = $this->getParam('values');
		$idPrefix = $this->getParam('id-prefix', false);
		
		$currentValues = $this->getValue();
		$currentValues = is_array($currentValues) ? $currentValues : array();
		
		foreach($values as $value => $label) {

			$attributes = array_merge(
				$this->getParams(array('class')), 
				array('name' => $name, 'type' => 'checkbox', 'value' => $value)
			);
			
			if($idPrefix) {
				$attributes['id'] = $idPrefix.'-'.$value;
			}
			
			if(in_array($value, $currentValues)) {
				$attributes['checked'] = 'checked';
			}
			
			$content .= '<div><label';
			if(array_key_exists('id', $attributes)) {
				$content .= ' for="'.$attributes['id'].'"';
			}
			$content .= '>'.htmlspecialchars($label, ENT_QUOTES).'</label>'.$this->renderTag('input', $attributes).'</div>';
		}
		
		$content .= '</fieldset></div>';
		
		return $content;
	}
}