<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\view\helper;

use framework\exception\IllegalArgumentException;

class DateTimeHelper extends AbstractHelper
{
	public function __construct()
	{
	}
	
	public function formatTs($renderer, $ts)
	{
		if((!ctype_digit($ts)) && (!is_int($ts)) && (!is_float($ts))) {
			throw new IllegalArgumentException('$ts is not a timestamp');
		}

		return strftime('%e %b %G (%r)', intval($ts));
	}
	
	public function formatSeconds($renderer, $seconds)
	{
		if((!ctype_digit($seconds)) && (!is_int($seconds)) && (!is_float($seconds))) {
			throw new IllegalArgumentException('$seconds is not an int');
		}
		
		$minutes = intval(floor($seconds / 60));
		$seconds = $seconds % 60;
		
		$hours = intval(floor($minutes / 60));
		$minutes = $minutes % 60;
		
		$days = intval(floor($hours / 24));
		$hours = $hours % 24;
		
		$weeks = intval(floor($days / 7));
		$days = $days % 7;
		
		return $weeks.'w '.$days.'d '.$hours.'h '.$minutes.'m '.$seconds.'s';
	}
}