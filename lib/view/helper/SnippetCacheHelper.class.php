<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\view\helper;

use framework\application\Application;
use framework\context\Context;
use framework\cache\SnippetCache;
use framework\view\ViewException;
use framework\exception\IllegalArgumentException;

class SnippetCacheHelper extends AbstractHelper
{
	private $context;
	private $lifetime;
	private $snippetStack;
	private $snippetCache;
	
	public function __construct($application, $context, $lifetime)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(! $context instanceof Context) {
			throw new IllegalArgumentException('$context is not of type Context');
		}
		if((!is_int($lifetime)) && ($lifetime !== false)) {
			throw new IllegalArgumentException('$lifetime is not of type integer or false');
		}
		
		$this->context = $context;
		$this->lifetime = ((is_int($lifetime)) && ($lifetime > 0)) ? $lifetime : false;
		
		$this->snippetStack = array();
		$this->snippetCache = new SnippetCache($application, $this->context->getModule());
	}
	
	protected function cache($renderer, $key, $lifetime = false)
	{
		if(!is_int($this->lifetime)) {
			return false;
		}
		if(!is_int($lifetime)) {
			$lifetime = $this->lifetime;
		}
		
		$key = $this->context->getAction().'-'.$key;
		$content = $this->snippetCache->get($key, $lifetime);
		
		if($content !== null) {
			print($content);
			return false;
		}
		
		array_unshift($this->snippetStack, $key);
		ob_start();
		
		return true;
	}
	
	protected function save($renderer)
	{
		if(!is_int($this->lifetime)) {
			return;
		}
		if(!sizeof($this->snippetStack)) {
			throw new ViewException('Can not save cache snippet, empty stack');
		}

		print($content = ob_get_clean());

		$key = array_shift($this->snippetStack);
		$this->snippetCache->put($key, $content);
	}
}