<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\view\helper;

use framework\application\Application;
use framework\context\Context;
use framework\exception\IllegalArgumentException;

class IncludeHelper extends AbstractHelper
{
	private $application;
	private $context;
	
	public function __construct($application, $context)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(! $context instanceof Context) {
			throw new IllegalArgumentException('$context is not of type Context');
		}
		
		$this->application = $application;
		$this->context = $context;
	}
	
	protected function template($renderer, $name, $vars = array(), $rawVars = array())
	{
		if((!strlen($name)) || (!preg_match('#^[^/]+(/[^/]+)?$#i', $name))) {
			throw new IllegalArgumentException('Invalid template name');
		}
		if(!is_array($vars)) {
			throw new IllegalArgumentException('$vars is not an array');
		}
		if(!is_array($rawVars)) {
			throw new IllegalArgumentException('$rawVars is not an array');
		}
		
		if(is_int(strpos($name, '/'))) {
			list($module, $name) = explode('/', $name);
		} else {
			$module = $this->context->getModule();
		}
		
		$moduleDir = $this->application->getClassLoader()->getModulePath($this->context->getModule());	
		$filename = $moduleDir.'/views/includes/'.$name.'.php';
		
		$escapeKeys = array_keys($vars);
		$vars = array_merge($vars, $rawVars);
		
		print($renderer->render($filename, $vars, $escapeKeys));
	}
}