<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\view\helper;

use framework\view\renderer\PhpTemplateRenderer;
use framework\exception\IllegalArgumentException;

class AbstractHelper implements Helper
{
	public function hasHelper($method)
	{
		$methods = $this->getHelpers();
		
		if((is_array($methods)) && (!in_array($method, $methods))) {
			return false;
		}
		if(in_array($method, array('hasHelper', 'getHelpers', 'doHelper'))) {
			return false;
		}
		
		return is_callable(array($this, $method));
	}
	
	/**
	 * Overriding this method to return an array of helper method names
	 * allows us to define other methods we may not wish to expose as helpers.
	 */
	protected function getHelpers()
	{
		return true;
	}

	public function doHelper($method, $args, $renderer)
	{
		if(!$this->hasHelper($method)) {
			throw new HelperException('Unknown helper "'.$method.'"');
		}
		if(!is_array($args)) {
			throw new IllegalArgumentException('$args is not of type array');
		}
		if(! $renderer instanceof PhpTemplateRenderer) {
			throw new IllegalArgumentException('$renderer is not of type PhpTemplateRenderer');
		}
		
		// By placing the always present renderer var to the first arg, it allows 
		// us to default any args to the right of the method signature.
		array_unshift($args, $renderer);

		return call_user_func_array(array($this, $method), $args);
	}
}