<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\view\helper;

interface Helper
{
	public function hasHelper($method);
	public function doHelper($method, $args, $renderer);
}