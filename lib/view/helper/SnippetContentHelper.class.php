<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\view\helper;

use framework\view\ViewException;

class SnippetContentHelper extends AbstractHelper
{
	private $snippetStack;
	private $snippetContent;
	
	public function __construct()
	{
		$this->snippetStack = array();
		$this->snippetContent = array();
	}
	
	protected function has($renderer, $name)
	{
		return array_key_exists($name, $this->snippetContent);
	}
	
	protected function start($renderer, $name)
	{
		if(array_key_exists($name, $this->snippetStack)) {
			throw new ViewException('Already started snippet '.$name);
		}
		
		$this->snippetContent[$name] = '';

		array_unshift($this->snippetStack, $name);
		ob_start();
	}
	
	protected function append($renderer, $name)
	{
		if(array_key_exists($name, $this->snippetStack)) {
			throw new ViewException('Already started snippet '.$name);
		}
		
		if(!array_key_exists($name, $this->snippetContent)) {
			$this->snippetContent[$name] = '';
		}

		array_unshift($this->snippetStack, $name);
		ob_start();
	}
	
	protected function end($renderer)
	{
		if(!sizeof($this->snippetStack)) {
			throw new ViewException('Can not store snippet, empty stack');
		}
		
		$content = ob_get_clean();
		$name = array_shift($this->snippetStack);
		
		$this->snippetContent[$name] .= $content;
	}
	
	protected function show($renderer, $name)
	{
		if(array_key_exists($name, $this->snippetStack)) {
			throw new ViewException('Can not show a currently started snippet '.$name);
		}
		if(!array_key_exists($name, $this->snippetContent)) {
			return;
		}
		
		print($this->snippetContent[$name]);
	}
}