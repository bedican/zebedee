<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\view\helper;

use framework\routing\Route;
use framework\routing\Routing;
use framework\exception\IllegalArgumentException;

class RoutingHelper extends AbstractHelper
{
	private $routing;
	private $currentRoute;

	public function __construct($routing, $currentRoute)
	{
		if(! $routing instanceof Routing) {
			throw new IllegalArgumentException('$routing is not of type Routing');
		}
		if(! $currentRoute instanceof Route) {
			throw new IllegalArgumentException('$currentRoute is not of type Route');
		}
		
		$this->routing = $routing;
		$this->currentRoute = $currentRoute;
	}
	
	protected function getUrl($renderer, $path)
	{
		return $this->routing->getUrl($path);
	}
	
	protected function isCurrentRoute($renderer, $path)
	{
		return ($this->currentRoute->getPath() == $this->routing->getCanonicalRoute($path));
	}
	
	protected function safeUrl($renderer, $text)
	{
		return preg_replace('#[^0-9a-z_-]#i', '-', $text);
	}
}