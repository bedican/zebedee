<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\view;

use framework\writer\ResponseWriter;
use framework\context\ErrorWebContext;
use framework\context\WebContext;
use framework\exception\IllegalArgumentException;

class JsonView extends AbstractView
{
	private $data = null;
	
	protected function init()
	{
		$context = $this->getContext();
		if((! $context instanceof WebContext) && (! $context instanceof ErrorWebContext)) {
			throw new IllegalArgumentException('$context is not of type WebContext or ErrorWebContext');
		}
		
		$response = $context->getResponse();
		$response->setHeader('Content-Type', 'text/json');
		
		$this->setWriter(new ResponseWriter($response));
	}
	
	public function setData($data)
	{
		if(!is_array($data)) {
			throw new IllegalArgumentException('$data is not an array');
		}
		
		$this->data = $data;
	}
	
	protected function getContent()
	{
		if(!is_array($this->data)) {
			throw new ViewException('No data to render');
		}
		
		return json_encode($this->data);
	}
}