<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\view;

use framework\context\WebContext;
use framework\context\ErrorWebContext;
use framework\view\renderer\PhpTemplateRenderer;
use framework\view\helper\Helper;
use framework\view\helper\SnippetCacheHelper;
use framework\view\helper\SnippetContentHelper;
use framework\view\helper\IncludeHelper;
use framework\view\helper\DateTimeHelper;
use framework\exception\IllegalArgumentException;
use framework\exception\FileNotFoundException;

class PhpTemplateView extends AbstractView
{
	private $layoutFile;
	private $templateFile;
	
	private $moduleBaseDir;
	
	private $vars = array();
	private $escapeVars = array();
	private $helpers = array();
	
	protected function init()
	{
		$context = $this->getContext();
		$application = $this->getApplication();
		
		$module = $context->getModule();
		$action = $context->getAction();
		
		$this->moduleBaseDir = $application->getClassLoader()->getModulePath($module);
		
		$this->setTemplate($this->moduleBaseDir.'/views/'.$action.'.php');
		
		$this->layoutFile = false;
		if(($context instanceof WebContext) || ($context instanceof ErrorWebContext)) {
			$this->setLayout($context->getLayout());
		}
	}
	
	public function setTemplate($templateFile)
	{
		if(!is_file($templateFile)) {
			throw new FileNotFoundException('Template file not found "'.$templateFile.'"');
		}
		
		$this->templateFile = $templateFile;
	}
	
	public function setLayout($layout)
	{
		if($layout !== false) {

			$layoutFile = $this->moduleBaseDir.'/layouts/'.$layout.'.php';
	
			if(!is_file($layoutFile)) {
				$layoutFile = $this->getApplication()->getApplicationDir().'/layouts/'.$layout.'.php';
			}
			if(!is_file($layoutFile)) {
				throw new FileNotFoundException('Layout file not found "'.$layoutFile.'"');
			}

			$layout = $layoutFile;
		}

		$this->layoutFile = $layout;
	}
	
	public function setModuleView($viewName)
	{
		$baseDir = $this->getApplication()->getClassLoader()->getModulePath($this->getContext()->getModule());
		$this->setTemplate($baseDir.'/views/'.$viewName.'.php');
	}
	
	public function setVar($name, $value, $escape = true)
	{
		$this->vars[$name] = $value;

		if($escape) {
			if(!in_array($name, $this->escapeVars)) {
				$this->escapeVars[] = $name;
			}
		} else {
			$this->escapeVars = array_diff($this->escapeVars, array($name));
		}
	}
	
	public function getVar($name, $default = null)
	{
		if(!array_key_exists($name, $this->vars)) {
			return $default;
		}
		
		return $this->vars[$name];
	}
	
	public function addHelper($helper)
	{
		if(! $helper instanceof Helper) {
			throw new IllegalArgumentException('$helper is not of type Helper');
		}
		
		$this->helpers[] = $helper;
	}
	
	protected function setCacheEnabled($value)
	{
		parent::setCacheEnabled($value);
		$this->setSnippetCacheEnabled($value);
	}
	
	protected function setSnippetCacheEnabled($value)
	{
		// Snippet cache is enabled by default (it doesnt follow the page cache setting as that would not make sense).
		// It can be disabled with a cache configuration use-snippet-cache = false (default true)
		$this->setCacheConfigValue('use-snippet-cache', ($value === true) ? 'true' : 'false');
	}

	protected function getSnippetCacheLifetime()
	{
		$lifetime = $this->getCacheLifetime();
		
		if($this->getCacheConfigValue('use-snippet-cache') == 'false') {
			$lifetime = false;
		}
		
		return $lifetime;
	}
	
	protected function getContent()
	{
		// Add standard helpers to the top of the stack, to stop overriding.

		$helpers = array(
			new IncludeHelper($this->getApplication(), $this->getContext()),
			new SnippetCacheHelper($this->getApplication(), $this->getContext(), $this->getSnippetCacheLifetime()),
			new SnippetContentHelper(),
			new DateTimeHelper()
		);
		
		$helpers = array_merge($helpers, $this->helpers);
		$escapeKeys = array_intersect(array_keys($this->vars), $this->escapeVars);

		// A separate class is used to avoid methods on this view being exposed in the template itself.
		// Methods in the template are made available in the form of helpers.
		$renderer = new PhpTemplateRenderer($helpers);
		$content = $renderer->render($this->templateFile, $this->vars, $escapeKeys);

		if($this->layoutFile !== false) {
			$content = $renderer->render($this->layoutFile, array_merge($this->vars, array('content' => $content)), false);
		}
		
		return $content;
	}
}