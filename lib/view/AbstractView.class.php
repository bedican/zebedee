<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\view;

use framework\application\Application;
use framework\view\config\ViewCacheConfig;
use framework\view\config\ViewCacheConfigBuilder;
use framework\cache\ConfigCache;
use framework\cache\ViewCache;
use framework\context\Context;
use framework\writer\Writer;
use framework\writer\PrintWriter;
use framework\exception\IllegalArgumentException;

abstract class AbstractView implements View
{
	private $application;
	private $context;
	private $writer;
	
	private $cache;
	private $cacheConfig;
	private $cacheContent;
	
	public final function __construct($application, $context)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(! $context instanceof Context) {
			throw new IllegalArgumentException('$context is not of type Context');
		}
		
		$this->application = $application;
		$this->context = $context;
		$this->writer = new PrintWriter();

		$this->cache = new ViewCache($application, $this->context->getModule());
		
		$this->initCacheConfig();
		$this->initCacheContent();
		$this->init();
	}
	
	// Empty implementation
	protected function init() {}

	abstract protected function getContent();
	
	private function initCacheConfig()
	{
		$cache = new ConfigCache($this->application, $this->context->getModule());
		$this->cacheConfig = $cache->get('cache.xml');
		
		if($this->cacheConfig instanceof ViewCacheConfig) {
			return;
		}
		
		$builder = new ViewCacheConfigBuilder();
		
		// Framework always be present
		$this->cacheConfig = $builder->build(dirname(__FILE__).'/../../config/cache.xml');
		
		/*
		// Merge with each enabled plugin cache.xml if present
		$plugins = $this->application->getPlugins();
		foreach($plugins as $plugin) {
			$filename = $plugin->getPluginDir().'/config/cache.xml';
			if(is_file($filename)) {
				$this->cacheConfig->merge($builder->build($filename), true);
			}
		}
		*/
		
		// Override with application cache.xml if present
		$filename = $this->application->getApplicationDir().'/config/cache.xml';
		if(is_file($filename)) {
			$this->cacheConfig->merge($builder->build($filename), true);
		}
		
		// Merge environment configuration at application level
		if(is_string($environment = $this->application->getEnvironment())) {
			if(is_file($filename = $this->application->getApplicationDir().'/config/cache.'.$environment.'.xml')) {
				$this->cacheConfig->merge($builder->build($filename), true);
			}
		}
		
		$modulePath = $this->application->getClassLoader()->getModulePath($this->context->getModule());
		
		// Merge with module cache.xml if present
		$filename = $modulePath.'/config/cache.xml';
		if(is_file($filename)) {
			$this->cacheConfig->merge($builder->build($filename), true);	
		}
		
		// Merge environment configuration at module level
		if(is_string($environment = $this->application->getEnvironment())) {
			if(is_file($modulePath.'/config/cache.'.$environment.'.xml')) {
				$this->cacheConfig->merge($builder->build($filename), true);
			}
		}

		$cache->put('cache.xml', $this->cacheConfig);
	}
	
	private function initCacheContent()
	{
		$this->cacheContent = null;
		if($this->getCacheEnabled()) {
			$this->cacheContent = $this->cache->get($this->getCacheKey(), $this->getCacheLifetime());
		}
	}
	
	protected function getCacheKey()
	{
		return $this->context->getAction();
	}
	
	protected function getContext()
	{
		return $this->context;
	}
	
	protected function getApplication()
	{
		return $this->application;
	}

	protected function getCacheConfigValue($name, $default = null)
	{
		return $this->cacheConfig->getActionConfig($this->context->getAction(), $name, $default);
	}
	
	protected function setCacheConfigValue($name, $value)
	{
		$this->cacheConfig->setActionConfig($this->context->getAction(), $name, $value);
	}
	
	protected function getCacheEnabled()
	{
		return $this->cacheConfig->getActionEnabled($this->context->getAction());
	}
	
	protected function getCacheLifetime()
	{
		return $this->cacheConfig->getActionLifetime($this->context->getAction());
	}
	
	protected function setCacheEnabled($value)
	{
		$this->cacheConfig->setActionEnabled($this->context->getAction(), $value);
	}
	
	protected function setCacheLifetime($value)
	{
		$this->cacheConfig->setActionLifetime($this->context->getAction(), $value);
	}
	
	public function isCached()
	{
		return is_string($this->cacheContent);
	}
	
	public function render()
	{
		if(($this->getCacheEnabled()) && ($this->isCached())) {
			$content = $this->cacheContent;
		} else {
			$content = $this->getContent();
			if($this->getCacheEnabled()) {
				$this->cacheContent = $content;
				$this->cache->put($this->getCacheKey(), $content);	
			}
		}
		
		$this->writer->write($content);
	}
	
	public function setWriter($writer) {
		if(! $writer instanceof Writer) {
			throw new IllegalArgumentException('$writer is not of type Writer');
		}
		
		$this->writer = $writer;
	}
}