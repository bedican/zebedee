<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\view;

interface View
{
	public function isCached();
	public function render();
}