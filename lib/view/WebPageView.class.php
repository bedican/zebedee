<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\view;

use framework\cache\ConfigCache;
use framework\context\WebContext;
use framework\context\ErrorWebContext;
use framework\view\config\WebPageViewAssetsConfigBuilder;
use framework\view\config\WebPageViewAssetsConfig;
use framework\view\helper\RoutingHelper;
use framework\writer\ResponseWriter;
use framework\exception\IllegalArgumentException;

class WebPageView extends PhpTemplateView
{	
	private $vars;
	private $javascripts;
	private $stylesheets;
	private $assetsConfig;
	
	protected function init()
	{
		$context = $this->getContext();
		if((! $context instanceof WebContext) && (! $context instanceof ErrorWebContext)) {
			throw new IllegalArgumentException('$context is not of type WebContext or ErrorWebContext');
		}

		parent::init();
		
		$this->setWriter(new ResponseWriter($context->getResponse()));
		
		if($this->isCached()) {
			return;
		}
		
		$this->vars = array();
		$this->javascripts = array();
		$this->stylesheets = array();
		
		$this->initAssetsConfig();
		$this->initAssets();
	}
	
	private function initAssetsConfig()
	{
		$cache = new ConfigCache($this->getApplication(), $this->getContext()->getModule());
		$this->assetsConfig = $cache->get('assets.xml');
		
		if($this->assetsConfig instanceof WebPageViewAssetsConfig) {
			return;
		}

		$builder = new WebPageViewAssetsConfigBuilder();
		
		// Framework always be present
		$this->assetsConfig = $builder->build(dirname(__FILE__).'/../../config/assets.xml');
		
		/*
		// Merge with each enabled plugin assets.xml if present
		$plugins = $this->getApplication()->getPlugins();
		foreach($plugins as $plugin) {
			$filename = $plugin->getPluginDir().'/config/assets.xml';
			if(is_file($filename)) {
				$this->assetsConfig->merge($builder->build($filename), true);
			}
		}
		*/
		
		// Override with application assets.xml if present
		$filename = $this->getApplication()->getApplicationDir().'/config/assets.xml';
		if(is_file($filename)) {
			$this->assetsConfig->merge($builder->build($filename), true);
		}
		
		// Merge environment configuration at application level
		if(is_string($environment = $this->getApplication()->getEnvironment())) {
			if(is_file($filename = $this->getApplication()->getApplicationDir().'/config/assets.'.$environment.'.xml')) {
				$this->assetsConfig->merge($builder->build($filename), true);
			}
		}
		
		$modulePath = $this->getApplication()->getClassLoader()->getModulePath($this->getContext()->getModule());
		
		// Merge with module cache.xml if present
		$filename = $modulePath.'/config/assets.xml';
		if(is_file($filename)) {
			$this->assetsConfig->merge($builder->build($filename), true);	
		}
		
		// Merge environment configuration at module level
		if(is_string($environment = $this->getApplication()->getEnvironment())) {
			if(is_file($modulePath.'/config/assets.'.$environment.'.xml')) {
				$this->assetsConfig->merge($builder->build($filename), true);
			}
		}
		
		$cache->put('assets.xml', $this->assetsConfig);
	}
	
	private function initAssets()
	{
		$action = $this->getContext()->getAction();

		$javascripts = $this->assetsConfig->getJavascripts($action);
		foreach($javascripts as $javascript) {			
			$this->addJavascript($javascript);
		}
		
		$stylesheets = $this->assetsConfig->getStylesheets($action);
		foreach($stylesheets as $stylesheet) {			
			$this->addStylesheet($stylesheet);
		}
	}
	
	protected function getCacheKey()
	{
		return $this->getContext()->getRequest()->getUri();
	}
	
	public function render()
	{
		$request = $this->getContext()->getRequest();
		$response = $this->getContext()->getResponse();
			
		if((!$this->isCached()) || ($request->isPost())) {
			$this->setVars();
			// If the response status is not 200, disable the cache.
			if($response->getStatus() != 200) {
				$this->setCacheEnabled(false);
			}
		}
		
		// Set cache headers if cache is enabled
		if($this->getCacheEnabled()) {
			$response->setCacheLifetime($this->getCacheLifetime());
		}
		
		parent::render();
	}
	
	private function setVars()
	{
		foreach($this->vars as $name => $value) {
			$this->setVar($name, $value);
		}
		
		$context = $this->getContext();
		$module = $context->getModule();
		$action = $context->getAction();

		$this->setVar('routePath', $module.'.'.$action);
		$this->setVar('route', $context->getCurrentRoute());
		$this->setVar('requestUri', $context->getRequest()->getUri());
		$this->setVar('routing', $context->getRouting());		
		$this->setVar('javascripts', $this->javascripts);
		$this->setVar('stylesheets', $this->stylesheets);
		
		// Gives access to routing in sub-templates
		$this->addHelper(new RoutingHelper($context->getRouting(), $context->getCurrentRoute()));
	}
	
	public function setTitle($title)
	{
		$this->vars['meta_title'] = $title;
	}
	
	public function addJavascript($filename)
	{
		if((substr($filename, 0, 7) != 'http://') && (substr($filename, 0, 8) != 'https://')) {
			if(substr($filename, 0, 1) != '/') {
				$filename = '/js/'.$filename;
			}
		}

		$this->javascripts[] = $filename;
	}
	
	public function addStylesheet($filename)
	{
		if((substr($filename, 0, 7) != 'http://') && (substr($filename, 0, 8) != 'https://')) {
			if(substr($filename, 0, 1) != '/') {
				$filename = '/css/'.$filename;
			}
		}
		
		$this->stylesheets[] = $filename;
	}
}