<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\view\escaper;

use framework\exception\IllegalArgumentException;

class ObjectEscaper
{
	private $object;
	private $escaper;

	public function __construct($object, $escaper)
	{
		if(!is_object($object)) {
			throw new IllegalArgumentException('$object is not an Object');
		}
		if(! $escaper instanceof Escaper) {
			throw new IllegalArgumentException('$escaper is not of type Escaper');
		}
		
		$this->object = $object;
		$this->escaper = $escaper;
	}
	
	public function getObject()
	{
		return $this->object;
	}
	
	public function __call($method, $args)
	{
		$result = call_user_func_array(array($this->object, $method), $args);

		if(!isset($result)) {
			return;
		}
		
		return $this->escaper->escape($result);
	}
}