<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\view\escaper;

class Escaper
{
	public function __construct()
	{
	}
	
	public function escape($value) {
        
		if(is_string($value)) {
			$value = htmlspecialchars($value, ENT_QUOTES);
		} else if(is_array($value)) {
			$value = array_map(array($this, 'escape'), $value);
		} else if((is_object($value)) && (! $value instanceof ObjectEscaper)) {
			$value = new ObjectEscaper($value, $this);
		}
        
		return $value;
	}	
}