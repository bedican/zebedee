<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\view\config;

use framework\config\AbstractConfigBuilder;

class WebPageViewAssetsConfigBuilder extends AbstractConfigBuilder
{
	protected function doBuild($xpath)
	{
		$config = new WebPageViewAssetsConfig();

		$this->parseAssets($config, $xpath);

		return $config;
	}
	
	private function parseAssets($config, $xpath)
	{
		$nodeList = $xpath->query('/assets/action[@name]');

		for($x = 0; $x < $nodeList->length; $x++) {
			
			$node = $nodeList->item($x);

			$actionName = $node->getAttribute('name');
			$actionConfig = $this->getConfigFromNode($node);
			
			// Action name could be a comma separated list of actions.
			$actionNames = preg_split('#[,\s]+#', $actionName);
			
			foreach($actionNames as $actionName) {	
				if((array_key_exists('javascripts', $actionConfig)) && (array_key_exists('javascript', $actionConfig['javascripts']))) {
					$javascripts = $actionConfig['javascripts']['javascript'];
					$javascripts = is_array($javascripts) ? $javascripts : array($javascripts);
					
					foreach($javascripts as $javascript) {
						$config->addJavascript($actionName, $javascript);
					}
				}
				if((array_key_exists('stylesheets', $actionConfig)) && (array_key_exists('stylesheet', $actionConfig['stylesheets']))) {
					$stylesheets = $actionConfig['stylesheets']['stylesheet'];
					$stylesheets = is_array($stylesheets) ? $stylesheets : array($stylesheets);
					
					foreach($stylesheets as $stylesheet) {
						$config->addStylesheet($actionName, $stylesheet);
					}
				}
			}
		}
	}
}