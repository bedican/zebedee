<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\view\config;

use framework\config\Cachable;
use framework\config\Mergeable;
use framework\exception\IllegalArgumentException;

class ViewCacheConfig implements Cachable, Mergeable
{
	private $enabled;
	private $lifetime;
	private $config;

	private $actionEnabled;
	private $actionLifetime;
	private $actionConfig;

	public function __construct()
	{
		$this->enabled = null;
		$this->lifetime = null;
		$this->config = array();
		$this->actionEnabled = array();
		$this->actionLifetime = array();
		$this->actionConfig = array();
	}
	
	public function setEnabled($enabled)
	{
		if(!is_bool($enabled)) {
			throw new IllegalArgumentException('$enabled is not a boolean');
		}

		$this->enabled = $enabled;
	}
	
	public function setLifetime($lifetime)
	{
		if(!is_int($lifetime)) {
			throw new IllegalArgumentException('$lifetime is not an integer');
		}
		
		$this->lifetime = $lifetime;
	}
	
	public function setConfig($name, $value)
	{
		$this->config[$name] = $value;
	}
	
	public function setActionEnabled($action, $enabled)
	{
		if(!is_bool($enabled)) {
			throw new IllegalArgumentException('$enabled is not a boolean');
		}
		
		$this->actionEnabled[$action] = $enabled;
	}
	
	public function setActionLifetime($action, $lifetime)
	{
		if(!is_int($lifetime)) {
			throw new IllegalArgumentException('$lifetime is not an integer');
		}
		
		$this->actionLifetime[$action] = $lifetime;
	}
	
	public function setActionConfig($action, $name, $value)
	{
		if(!array_key_exists($action, $this->actionConfig)) {
			$this->actionConfig[$action] = array();
		}
		
		$this->actionConfig[$action][$name] = $value;
	}
	
	public function hasEnabled()
	{
		return is_bool($this->enabled);
	}
	
	public function hasLifetime()
	{
		return is_int($this->lifetime);
	}
	
	public function hasConfig($name)
	{
		return array_key_exists($name, $this->config);
	}
	
	public function hasActionEnabled($action)
	{
		return array_key_exists($action, $this->actionEnabled);
	}
	
	public function hasActionLifetime($action)
	{
		return array_key_exists($action, $this->actionLifetime);
	}
	
	public function hasActionConfig($action, $name)
	{
		return ((array_key_exists($action, $this->actionConfig)) && (array_key_exists($name, $this->actionConfig[$action])));
	}
	
	public function getEnabled($default = false)
	{
		return $this->hasEnabled() ? $this->enabled : $default;
	}
	
	public function getLifetime($default = 3600)
	{
		return $this->hasLifetime() ? $this->lifetime : $default;
	}
	
	public function getConfig($name, $default = null)
	{
		if(!array_key_exists($name, $this->config)) {
			return $default;
		}
		
		return $this->config[$name];
	}
	
	public function getConfigNames()
	{
		return array_keys($this->config);
	}
	
	public function getActionEnabled($action, $default = false)
	{
		if(!array_key_exists($action, $this->actionEnabled)) {
			return $this->getEnabled($default);
		}
		
		return $this->actionEnabled[$action];
	}
	
	public function getActionLifetime($action, $default = 3600)
	{
		if(!array_key_exists($action, $this->actionLifetime)) {
			return $this->getLifetime($default);
		}
		
		return $this->actionLifetime[$action];
	}
	
	public function getActionConfig($action, $name, $default = null)
	{
		if((!array_key_exists($action, $this->actionConfig)) || (!array_key_exists($name, $this->actionConfig[$action]))) {
			return $this->getConfig($name, $default);
		}
		
		return $this->actionConfig[$action][$name];
	}
	
	public function getActionConfigNames($action, $useDefaultConfig = true)
	{
		$names = array();
			
		if(array_key_exists($action, $this->actionConfig)) {
			$names = array_merge($names, array_keys($this->actionConfig));
		}
		if($useDefaultConfig) {
			$names = array_merge($names, array_keys($this->getConfigNames()));
		}

		return array_unique($names);
	}
	
	public function getActionEnabledActionNames()
	{
		return array_keys($this->actionEnabled);
	}
	
	public function getActionLifetimeActionNames()
	{
		return array_keys($this->actionLifetime);
	}
	
	public function getActionConfigActionNames()
	{
		return array_keys($this->actionConfig);
	}
	
	public function merge($config, $overwrite = false)
	{
		if(! $config instanceof ViewCacheConfig) {
			throw new IllegalArgumentException('$config is not of type ViewCacheConfig');
		}
		
		// Merge default values

		if(($this->enabled == null) || ($overwrite)) {
			if($config->hasEnabled()) {
				$this->enabled = $config->getEnabled();
			}
		}

		if(($this->lifetime == null) || ($overwrite)) {
			if($config->hasLifetime()) {
				$this->lifetime = $config->getLifetime();
			}
		}
		
		$configNames = $config->getConfigNames();
		foreach($configNames as $configName) {
			if((!array_key_exists($configName, $this->config)) || ($overwrite)) {
				$this->setConfig($configName, $config->getConfig($configName));
			}
		}
		
		// Merge action values
		
		$actionNames = $config->getActionEnabledActionNames();
		foreach($actionNames as $actionName) {
			if((!array_key_exists($actionName, $this->actionEnabled)) || ($overwrite)) {
				// Will not default to the global value as we are requesting actons that we know have a value set.
				$this->setActionEnabled($actionName, $config->getActionEnabled($actionName));
			}
		}
		
		$actionNames = $config->getActionLifetimeActionNames();
		foreach($actionNames as $actionName) {
			if((!array_key_exists($actionName, $this->actionLifetime)) || ($overwrite)) {
				// Will not default to the global value as we are requesting actons that we know have a value set.
				$this->setActionLifetime($actionName, $config->getActionLifetime($actionName));
			}
		}
		
		$actionNames = $config->getActionConfigActionNames();
		foreach($actionNames as $actionName) {
			$configNames = $config->getActionConfigNames($actionName, false);
			foreach($configNames as $configName) {
				if((!array_key_exists($actionName, $this->actionConfig)) || (array_key_exist($configName, $this->actionConfig[$actionName])) || ($overwrite)) {
					$this->setActionConfig($actionName, $configName, $config->getActionConfig($actionName, $configName));
				}
			}
		}
	}
}
