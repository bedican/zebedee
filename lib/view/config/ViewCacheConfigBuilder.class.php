<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\view\config;

use framework\config\AbstractConfigBuilder;

class ViewCacheConfigBuilder extends AbstractConfigBuilder
{
	protected function doBuild($xpath)
	{
		$config = new ViewCacheConfig();

		$this->parseCache($config, $xpath);

		return $config;
	}
	
	private function parseCache($config, $xpath)
	{
		$nodeList = $xpath->query('/cache/action[@name]');
		for($x = 0; $x < $nodeList->length; $x++) {
			
			$node = $nodeList->item($x);

			$actionName = $node->getAttribute('name');
			$actionConfig = $this->getConfigFromNode($node);

			// Action name could be * for all, or a comma separated list of actions.
			$actionNames = preg_split('#[,\s]+#', $actionName);
			
			foreach($actionNames as $actionName) {				
				if($actionName == '*') {
					if(array_key_exists('enabled', $actionConfig)) {
						$config->setEnabled($actionConfig['enabled'] == 'true');
					}
					if((array_key_exists('lifetime', $actionConfig)) && (ctype_digit($actionConfig['lifetime']))) {
						$config->setLifetime(intval($actionConfig['lifetime']));
					}
					if((array_key_exists('config', $actionConfig)) && (is_array($actionConfig['config']))) {
						foreach($actionConfig['config'] as $name => $value) {
							$config->setConfig($name, $value);
						}
					}
				} else {
					if(array_key_exists('enabled', $actionConfig)) {
						$config->setActionEnabled($actionName, $actionConfig['enabled'] == 'true');
					}
					if((array_key_exists('lifetime', $actionConfig)) && (ctype_digit($actionConfig['lifetime']))) {
						$config->setActionLifetime($actionName, intval($actionConfig['lifetime']));
					}
					if((array_key_exists('config', $actionConfig)) && (is_array($actionConfig['config']))) {
						foreach($actionConfig['config'] as $name => $value) {
							$config->setActionConfig($actionName, $name, $value);
						}
					}
				}
			}
		}
	}
}