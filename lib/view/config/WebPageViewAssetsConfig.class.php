<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\view\config;

use framework\config\Cachable;
use framework\config\Mergeable;

class WebPageViewAssetsConfig implements Cachable, Mergeable
{
	private $javascripts;
	private $stylesheets;

	public function __construct()
	{
		$this->javascripts = array();
		$this->stylesheets = array();
	}
	
	public function addJavascript($action, $filename)
	{
		if(!array_key_exists($action, $this->javascripts)) {
			$this->javascripts[$action] = array();
		}
		
		$this->javascripts[$action][] = $filename;
	}
	
	public function addStylesheet($action, $filename)
	{
		if(!array_key_exists($action, $this->stylesheets)) {
			$this->stylesheets[$action] = array();
		}
		
		$this->stylesheets[$action][] = $filename;
	}
	
	public function getJavascriptActions()
	{
		return array_keys($this->javascripts);
	}
	
	public function getStylesheetActions()
	{
		return array_keys($this->stylesheets);
	}
	
	public function getJavascripts($action)
	{
		return array_key_exists($action, $this->javascripts) ? $this->javascripts[$action] : array();	
	}
	
	public function getStylesheets($action)
	{
		return array_key_exists($action, $this->stylesheets) ? $this->stylesheets[$action] : array();	
	}
	
	public function merge($config, $overwrite = false)
	{
		if(! $config instanceof WebPageViewAssetsConfig) {
			throw new IllegalArgumentException('$config is not of type WebPageViewAssetsConfig');
		}
		
		// Merge javascripts
		$actions = $config->getJavascriptActions();
		foreach($actions as $action) {
			$javascripts = $config->getJavascripts($action);
			foreach($javascripts as $javascript) {			
				$this->addJavascript($action, $javascript);
			}
		}
		
		// Merge stylesheets
		$actions = $config->getStylesheetActions();
		foreach($actions as $action) {
			$stylesheets = $config->getStylesheets($action);
			foreach($stylesheets as $stylesheet) {			
				$this->addStylesheet($action, $stylesheet);
			}
		}
	}
}
