<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\view\renderer;

use framework\view\escaper\Escaper;
use framework\view\escaper\ObjectEscaper;
use framework\view\ViewException;
use framework\exception\IllegalArgumentException;
use framework\exception\FileNotFoundException;
use framework\exception\IllegalStateException;
use Exception;

class PhpTemplateRenderer
{
	private $helpers;
	private $escaper;
	
	public function __construct($helpers = array())
	{
		$value = strtolower(ini_get('output_buffering'));
		if(!in_array($value, array('1', 'on', 'true'))) {
			throw new IllegalStateException('ini_get("output_buffering") is not set to on, currently set to '.$value);
		}
		
		$this->helpers = is_array($helpers) ? $helpers : array();
		$this->escaper = new Escaper();
	}
	
	public function escape($value)
	{
		return $this->escaper->escape($value);
	}
	
	public function render($filename, $vars = array(), $autoEscape = true)
	{
		if(!is_file($filename)) {
			throw new FileNotFoundException('File not found "'.$filename.'"');
		}
		if(!is_array($vars)) {
			throw new IllegalArgumentException('$vars is not of type array');
		}
		if((!is_bool($autoEscape)) && (!is_array($autoEscape))) {
			throw new IllegalArgumentException('$autoEscape is not of type bool or array');
		}
		
		if(is_array($autoEscape)) {
			$autoEscape = array_flip($autoEscape);
			
			$escapeVars = array_intersect_key($vars, $autoEscape);
			$rawVars = array_diff_key($vars, $autoEscape);
			
			$vars = array_merge($this->escape($escapeVars), $rawVars);

		} else if($autoEscape) {
			$vars = $this->escape($vars);
		}

		extract(array_merge($vars, array('this' => $this))); // Ensure we cannot override $this

		ob_start();

		try {
			include($filename);
		} catch(Exception $e) {
			ob_clean(); throw $e;
		}

		return ob_get_clean();
	}
	
	public function __call($method, $args)
	{
		foreach($this->helpers as $helper) {
			if(!$helper->hasHelper($method)) {
				continue;
			}
			
			foreach($args as $index => $arg) {
				if($arg instanceof ObjectEscaper) {
					$args[$index] = $arg->getObject();
				}
			}

			return $helper->doHelper($method, $args, $this);
		}

		throw new ViewException('Unknown helper "'.$method.'"');
	}
}