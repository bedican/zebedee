<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\view;

class NullView implements View
{
	public function isCached()
	{
		return false;
	}
	
	public function render()
	{
	}
	
	public function __call($method, $args)
	{
	}
}