<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\application\config;

use framework\config\AbstractConfigBuilder;

class ApplicationConfigBuilder extends AbstractConfigBuilder
{
	protected function doBuild($xpath)
	{
		$config = new ApplicationConfig();
		
		$this->parsePlugins($config, $xpath);
		$this->parsePluginModules($config, $xpath);
		$this->parseLogLevels($config, $xpath);
		$this->parseUseConfigCache($config, $xpath);
		$this->parseUserValues($config, $xpath);
		
		return $config;
	}
	
	private function parsePlugins($config, $xpath)
	{
		$nodeList = $xpath->query('/application/enabled-plugins/plugin/text()');
		foreach($nodeList as $node) {
			$config->addEnabledPlugin(trim($node->nodeValue));
		}
	}
	
	private function parsePluginModules($config, $xpath)
	{
		$nodeList = $xpath->query('/application/plugin-modules/plugin-module');
		foreach($nodeList as $node) {
			$nodeConfig = $this->getConfigFromNode($node);
			if((array_key_exists('plugin', $nodeConfig)) && (array_key_exists('module', $nodeConfig))) {
				$config->addPluginModule($nodeConfig['plugin'], $nodeConfig['module']);
			}
		}
	}
	
	private function parseLogLevels($config, $xpath)
	{
		$nodeList = $xpath->query('/application/logging/level/text()');
		foreach($nodeList as $node) {
			$config->addLogLevel(trim($node->nodeValue));
		}
	}
	
	private function parseUseConfigCache($config, $xpath)
	{
		$nodeList = $xpath->query('/application/use-config-cache/text()');	
		if(!$nodeList->length) {
			return;
		}
		
		$config->setUseConfigCache(trim($nodeList->item(0)->nodeValue) != 'false');
	}
	
	private function parseUserValues($config, $xpath)
	{
		$nodeList = $xpath->query('/application');
		
		if($nodeList->length) {
			if(is_array($nodeConfig = $this->getConfigFromNode($nodeList->item(0)))) {	
				foreach($nodeConfig as $key => $value) {
					if(!in_array(strtolower($key), array('use-config-cache', 'enabled-plugins', 'plugin-modules', 'logging'))) {
						$config->setValue($key, $value);
					}
				}
			}
		}
	}
}