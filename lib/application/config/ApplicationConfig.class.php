<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\application\config;

use framework\config\Cachable;
use framework\config\Mergeable;
use framework\config\MapConfig;
use framework\logger\Logger;
use framework\exception\IllegalArgumentException;

class ApplicationConfig extends MapConfig implements Cachable, Mergeable
{
	private $plugins;
	private $logLevels;
	private $useConfigCache;
	
	public function __construct()
	{
		$this->plugins = array();
		$this->logLevels = array();
		$this->useConfigCache = null;
	}
	
	public function addEnabledPlugin($plugin)
	{
		if(!is_string($plugin)) {
			throw new IllegalArgumentException('$plugin is not a string');
		}
		if(array_key_exists($plugin, $this->plugins)) {
			return;
		}
		
		$this->plugins[$plugin] = array();
	}
	
	public function getEnabledPlugins()
	{
		return array_keys($this->plugins);
	}
	
	public function addPluginModule($plugin, $module)
	{
		if(!array_key_exists($plugin, $this->plugins)) {
			throw new IllegalArgumentException('Can not add module '.$module.' for plugin '.$plugin.'. Plugin is not enabled.');
		}
		if(in_array($module, $this->plugins[$plugin])) {
			return;
		}
		
		$this->plugins[$plugin][] = $module;
	}
	
	public function getPluginModules($plugin)
	{
		return array_key_exists($plugin, $this->plugins) ? $this->plugins[$plugin] : array();
	}
	
	public function addLogLevel($level)
	{
		if(!in_array($level, array(Logger::LEVEL_ALL, Logger::LEVEL_DEBUG, Logger::LEVEL_ERROR, Logger::LEVEL_NOTICE))) {
			throw new IllegalArgumentException('Unknown log level '.$level);
		}
		if(in_array($level, $this->logLevels)) {
			return;
		}

		$this->logLevels[] = $level;
	}
	
	public function getLogLevels()
	{
		return $this->logLevels;
	}
	
	public function setUseConfigCache($value)
	{
		if(!is_bool($value)) {
			throw new IllegalArgumentException('$value is not of type bool');
		}
		
		$this->useConfigCache = $value;
	}
	
	public function getUseConfigCache()
	{
		return is_bool($this->useConfigCache) ? $this->useConfigCache : true;
	}
	
	public function merge($config, $overwrite = false)
	{
		if(! $config instanceof ApplicationConfig) {
			throw new IllegalArgumentException('$config is not of type ApplicatonConfig');
		}

		// We do not allow merging of enabled plugins / plugin modules here as it is unsafe.
		// i.e. a plugin configuration should not be able to enable another plugin.
		
		// Merge log levels
		if($overwrite) {
			$this->logLevels = array();
			$levels = $config->getLogLevels();
			foreach($levels as $level) {
				$this->addLogLevel($level);
			}
		}
		
		// Merge enable config cache
		if((!is_bool($this->useConfigCache)) || ($overwrite)) {
			$this->useConfigCache = $config->getUseConfigCache();	
		}
		
		// Merge generic config values
		parent::merge($config, $overwrite);
	}
}