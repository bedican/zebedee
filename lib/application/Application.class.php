<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\application;

use framework\lang\ClassLoader;
use framework\cache\ConfigCache;
use framework\exception\IllegalArgumentException;
use framework\application\config\ApplicationConfigBuilder;
use framework\application\config\ApplicationConfig;
use framework\logger\Logger;
use framework\logger\CompositeLogger;
use framework\logger\FileLogger;

class Application
{
	private $appDir;
	private $plugins;
	private $config;
	private $logger;
	private $classLoader;
	
	public final function __construct($appDir)
	{
		if(!is_dir($appDir)) {
			throw new IllegalArgumentException('Directory does not exist "'.$appDir.'"');
		}
		
		$this->appDir = $appDir;
		$this->plugins = array();
		$this->config = new ApplicationConfig();
		$this->logger = null;
		$this->classLoader = ClassLoader::getInstance();

		$this->initConfigAndPlugins();
		$this->initLoggers();
		$this->configure();
	}
	
	// Default empty implementation
	protected function configure() {}
	
	// Override to implement an environment detection method
	public function getEnvironment()
	{
		return false;
	}
	
	private function initConfigAndPlugins()
	{
		$cache = new ConfigCache($this);
		$config = $cache->get('app.xml');
		
		if($config instanceof ApplicationConfig) {
			$this->config = $config;
			$this->initPlugins();
			return;
		}
		
		$builder = new ApplicationConfigBuilder();

		// Both files should always be present.
		$this->config = $builder->build(dirname(__FILE__).'/../../config/app.xml');
		$appConfig = $builder->build($this->appDir.'/config/app.xml');

		// Merge application configuration
		$this->config->merge($appConfig, true);
		
		// Merge plugins and plugin modules seperately (its not done in the merge method).
		$plugins = $appConfig->getEnabledPlugins();
		foreach($plugins as $plugin) {
			$this->config->addEnabledPlugin($plugin);
			$modules = $appConfig->getPluginModules($plugin);
			foreach($modules as $module) {
				$this->config->addPluginModule($plugin, $module);
			}
		}
		
		// Merge environment configuration
		if(is_string($environment = $this->getEnvironment())) {
			if(is_file($filename = $this->appDir.'/config/app.'.$environment.'.xml')) {
				$this->config->merge($builder->build($filename), true);
			}
		}

		// Load plugins
		$this->initPlugins();

		// Merge plugin configuration
		$plugins = $this->getPlugins();
		foreach($plugins as $plugin) {
			if(is_file($filename = $plugin->getPluginDir().'/config/app.xml')) {
				$this->config->merge($builder->build($filename));
			}
		}

		$cache->put('app.xml', $this->config);
	}
	
	private function initPlugins()
	{
		$plugins = $this->config->getEnabledPlugins();
		foreach($plugins as $plugin) {

			$this->classLoader->addPlugin($plugin);	

			$modules = $this->config->getPluginModules($plugin);
			foreach($modules as $module) {
				$this->classLoader->addPluginModule($plugin, $module);
			}

			$this->addPlugin($plugin);
		}
	}
	
	private function addPlugin($plugins)
	{
		$plugins = is_array($plugins) ? $plugins : array($plugins);
		
		foreach($plugins as $plugin) {
			if(array_key_exists($plugin, $this->plugins)) {
				throw new IllegalArgumentException('Plugin '.$plugin.' has already been added.');
			}

			$pluginClassName = '\\plugin\\'.$plugin.'\\'.ucfirst($plugin).'Plugin';
		
			if(!is_subclass_of($pluginClassName, '\\framework\\plugin\\Plugin')) {
				throw new ApplicationException('Plugin class '.$pluginClassName.' does not extend Plugin.');
			}

			$pluginPath = $this->classLoader->getPluginPath($plugin);
			$this->plugins[$plugin] = new $pluginClassName($this, $pluginPath);
		}
	}
	
	private function initLoggers()
	{
		$this->logger = new CompositeLogger();
		$this->addLogger(new FileLogger($this));
		
		$levels = $this->config->getLogLevels();
		foreach($levels as $level) {
			$this->logger->addLevel($level);
		}
	}
	
	public function addLogger($logger)
	{
		$this->logger->addLogger($logger);
	}
	
	public function getApplicationDir()
	{
		return $this->appDir;
	}
	
	public function getClassLoader()
	{
		return $this->classLoader;
	}
	
	public function getConfig()
	{
		return $this->config;
	}
	
	public function getPlugins()
	{
		return $this->plugins;
	}
	
	public function hasPlugin($plugin)
	{
		return array_key_exists($plugin, $this->plugins);
	}
	
	public function getPlugin($plugin)
	{
		if(!array_key_exists($plugin, $this->plugins)) {
			return null;
		}
		
		return $this->plugins[$plugin];
	}
	
	public function getLogger()
	{
		return $this->logger;
	}
}
