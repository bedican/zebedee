<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\application;

use Exception;

class ApplicationException extends Exception
{
}