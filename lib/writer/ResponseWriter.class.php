<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\writer;

use framework\net\Response;
use framework\exception\IllegalArgumentException;

class ResponseWriter implements Writer
{
	private $response;

	public function __construct($response)
	{
		if(! $response instanceof Response) {
			throw new IllegalArgumentException('$response is not of type Response');
		}

		$this->response = $response;
	}
	
	public function write($content)
	{
		$this->response->setContent($content);
	}
}