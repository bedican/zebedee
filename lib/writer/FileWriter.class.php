<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\writer;

use framework\exception\IOException;

class FileWriter implements Writer
{
	private $fh = null;

	private $filename;
	private $mode;

	public function __construct($filename, $append = false)
	{
		$this->filename = $filename;
		$this->mode = $append ? 'a' : 'w';
	}
	
	public function __destruct()
	{
		$this->close();
	}
	
	public function write($content)
	{
		if($this->fh == null) {
			$this->fh = fopen($this->filename, $this->mode);
			if(!$this->fh) {
				throw new IOException('Could not create file handle for '.$this->filename);
			}
		}
		
		fwrite($this->fh, $content);
	}
	
	public function close()
	{
		if($this->fh) {
			fclose($this->fh);
		}
	}
}