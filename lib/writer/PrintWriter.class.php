<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\writer;

class PrintWriter implements Writer
{
	public function write($content)
	{
		print($content);
	}
}