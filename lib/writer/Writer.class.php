<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\writer;

interface Writer
{
	public function write($content);
}