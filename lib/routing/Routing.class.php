<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\routing;

use framework\application\Application;
use framework\routing\config\RoutingConfig;
use framework\routing\config\RoutingConfigBuilder;
use framework\cache\ConfigCache;
use framework\exception\IllegalArgumentException;

class Routing
{
	const DEFAULT_LAYOUT = 'default';
	const DEFAULT_MODULE = 'web';
	const DEFAULT_ACTION = 'default';
	
	private $config;
	private $application;
	
	public function __construct($application)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		
		$this->application = $application;
		
		$this->initConfig();
	}
	
	public function getConfig()
	{
		return $this->config;
	}
	
	private function initConfig()
	{
		$cache = new ConfigCache($this->application);
		$this->config = $cache->get('routing.xml');
		
		if($this->config instanceof RoutingConfig) {
			return;
		}
		
		$builder = new RoutingConfigBuilder();
		
		// Framework routing.xml should always be present
		$this->config = $builder->build(dirname(__FILE__).'/../../config/routing.xml');
	
		// Merge with each enabled plugin routing.xml if present
		$plugins = $this->application->getPlugins();
		foreach($plugins as $plugin) {
			$filename = $plugin->getPluginDir().'/config/routing.xml';
			if(is_file($filename)) {
				$this->config->merge($builder->build($filename), true);
			}
		}

		// Override with application routing.xml if present
		$filename = $this->application->getApplicationDir().'/config/routing.xml';
		if(is_file($filename)) {		
			$this->config->merge($builder->build($filename), true);
		}
		
		// Merge environment configuration
		if(is_string($environment = $this->application->getEnvironment())) {
			if(is_file($filename = $this->application->getApplicationDir().'/config/routing.'.$environment.'.xml')) {
				$this->config->merge($builder->build($filename), true);
			}
		}

		$cache->put('routing.xml', $this->config);
	}
	
	public function getCanonicalPath($url)
	{
		$parsed = parse_url($url);	
		$path = $parsed['path'];

		if(substr($path, -1) == '/') {
			$path .= $this->config->getDefaultActionWithSuffix();
		}

		return $path;
	}
	
	public function getCanonicalRoute($path)
	{
		if(!is_int(strpos($path, '.'))) {
			return self::DEFAULT_MODULE.'.'.$path;
		} else if(substr_count($path, '.') == 1) {
			return $path;
		}
		
		throw new RoutingException('Incorrect format for route path, should be "module.action"');
	}
	
	/**
	 * Returns an url for a given internal path.
	 * The internal path is of the format <module>.<action>.
	 *
	 * @param String $path The path.
	 * @return String the resulting url.
	 */
	public function getUrl($path)
	{
		$params = array();
		if(is_int($pos = strpos($path, '?'))) {
			parse_str(substr($path, $pos + 1), $params);
			$path = substr($path, 0, $pos);
		}

		list($module, $action) = explode('.', $this->getCanonicalRoute($path));
		
		$params = is_array($params) ? $params : array();
		$routeNames = $this->config->getRouteConfigNames();

		foreach($routeNames as $routeName) {
			
			$routeConfig = $this->config->getRouteConfig($routeName);
			
			if((!$routeConfig->allowReverseLookup()) || ($routeConfig->hasParameter('location'))) {
				continue;
			}
			if($action == self::DEFAULT_ACTION) { 
				$action = $this->config->getDefaultAction();
			}
			
			if($routeConfig->hasParameter('module')) {
				$routeModule = $routeConfig->getParameter('module');
			} else if(!is_int(strpos($routeConfig->getPath(), '{module}'))) {
				$routeModule = self::DEFAULT_MODULE;
			} else if($module != self::DEFAULT_MODULE) {
				$routeModule = $module;
			} else {
				continue;
			}
			
			if($routeConfig->hasParameter('action')) {
				$routeAction = $routeConfig->getParameter('action');
			} else if(!is_int(strpos($routeConfig->getPath(), '{action}'))) {
				$routeAction = self::DEFAULT_ACTION;
			} else if($action != self::DEFAULT_ACTION) {
				$routeAction = $action;
			} else {
				continue;
			}

			if(($routeModule != $module) || ($routeAction != $action)) {
				continue;
			}
			
			$search = array('{module}', '{action}'); $replace = array($module, $action);

			if(preg_match_all('#\{([0-9a-z]+)\}#i', $routeConfig->getPath(), $matches)) {
				$pathParamNames = array_diff($matches[1], array('module', 'action'));
				if($pathParamNames != array_keys($params)) {
					continue;
				}
				foreach($pathParamNames as $paramName) {
					$search[] = '{'.$paramName.'}';
					$replace[] = $params[$paramName];
				}
			}
			
			return str_replace($search, $replace, $routeConfig->getPath());
		}
		
		return '/'.$this->config->getDefaultActionWithSuffix();
	}
	
	/**
	 * Returns a route from a given url.
	 *
	 * @param String $url The url.
	 * @return Route The route from the given $url.
	 */
	public function getRoute($url)
	{
		$path = $this->getCanonicalPath($url);
		
		$routeNames = $this->config->getRouteConfigNames();
		foreach($routeNames as $routeName) {
			
			$routeConfig = $this->config->getRouteConfig($routeName);
			
			if(preg_match('#^'.$routeConfig->getRegex().'$#i', $path, $matches)) {

				// Discard numeric indexes on $matches
				$keys = array_filter(array_keys($matches), 'is_string');
				$matches = array_intersect_key($matches, array_flip($keys));
				
				$params = array();
				$routing = array(
					'layout' => self::DEFAULT_LAYOUT, 
					'module' => self::DEFAULT_MODULE, 
					'action' => self::DEFAULT_ACTION
				);
				
				// The route path cannot specify default values
				foreach($routing as $name => $defaultValue) {
					if((array_key_exists($name, $matches)) && ($matches[$name] == $defaultValue)) {
						continue 2;
					}
				}				
				
				// Separate out placeholder values obtained from the url in to route values and params 
				foreach($matches as $key => $value) {
					if(array_key_exists($key, $routing)) {
						$routing[$key] = $value;
					} else {
						$params[$key] = $value;
					}
				}
				
				// Separate config params in to route values and params
				$paramNames = $routeConfig->getParameterNames();
				foreach($paramNames as $paramName) {
					$paramValue = $routeConfig->getParameter($paramName);
					if(array_key_exists($paramName, $routing)) {
						$routing[$paramName] = $paramValue;
					} else {
						$params[$paramName] = $paramValue;
					}
				}
				
				// If the location parameter is set, we want to replace placeholders with param values.
				if($routeConfig->hasParameter('location')) {
					$keys = array_map(create_function('$v', 'return "{".$v."}";'), array_keys($matches));
					$values = array_values($matches);
					$params['location'] = str_replace($keys, $values, $routeConfig->getParameter('location'));
				}
				
				// Correct typing.
				foreach($params as $key => $value) {
					if(ctype_digit($value)) {
						$params[$key] = intval($value);
					} else if(in_array($value, array('true', 'false'))) {
						$params[$key] = ($value == 'true');
					}
				}
				
				// Treat index as a default page.
				if($routing['action'] == $this->config->getDefaultAction()) {
					$routing['action'] = self::DEFAULT_ACTION;
				}
				
				// Layout should be false if no layout is to be used.
				if($routing['layout'] == 'false') {
					$routing['layout'] = false;
				}
				
				$route = new Route($routeConfig->getName());
				$route->setLayout($routing['layout']);
				$route->setModule($routing['module']);
				$route->setAction($routing['action']);

				foreach($params as $key => $value) {
					$route->setParameter($key, $value);
				}

				return $route;
			}
		}
		
		$route = new Route('error404');

		$route->setModule($this->config->getError404Module());
		$route->setAction($this->config->getError404Action());

		if(strlen($layout = $this->config->getError404Layout())) {
			$route->setLayout(($layout == 'false') ? false : $layout);		
		}

		return $route;
	}
}