<?php // (c) Copyright 2010 Bedican Solutions

namespace framework\routing;

class Route
{
	private $name;
	private $layout;
	private $module;
	private $action;
	private $params;

	public function __construct($name)
	{
		$this->name = $name;

		$this->layout = Routing::DEFAULT_LAYOUT;
		$this->module = Routing::DEFAULT_MODULE;
		$this->action = Routing::DEFAULT_ACTION;

		$this->params = array();
	}

	public function getName()
	{
		return $this->name;
	}
	
	public function getLayout()
	{
		return $this->layout;
	}
	
	public function getModule()
	{
		return $this->module;
	}
	
	public function getAction()
	{
		return $this->action;
	}
	
	public function getPath()
	{
		return $this->module.'.'.$this->action;
	}
	
	public function setAction($action)
	{
		$this->action = $action;
	}

	public function setLayout($layout)
	{
		$this->layout = $layout;
	}

	public function setModule($module)
	{
		$this->module = $module;
	}
	
	public function getParameters()
	{
		return $this->params;
	}

	public function setParameter($name, $value)
	{
		$this->params[$name] = $value;
	}
	
	public function getParameter($name, $default = null)
	{
		if(!$this->hasParameter($name)) {
			return $default;
		}

		return $this->params[$name];
	}
	
	public function hasParameter($name)
	{
		return array_key_exists($name, $this->params);
	}
}