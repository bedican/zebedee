<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\routing\config;

use framework\exception\IllegalArgumentException;

class RouteConfig
{
	private $name;
	private $path;
	private $regex;
	private $allowReverseLookup;
	private $params;
	
	public function __construct($config)
	{
		if(!is_array($config)) {
			throw new IllegalArgumentException('$config is not an array');
		}
		
		$requiredFields = array('name', 'path', 'regex');
		if(sizeof($missingFields = array_keys(array_diff_key(array_flip($requiredFields), $config)))) {
			throw new IllegalArgumentException('Missing fields: '.implode(',', $missingFields));
		}
		
		$optionalFields = array('reverse-lookup', 'params');
		if(sizeof($unknownFields = array_keys(array_diff_key($config, array_flip(array_merge($requiredFields, $optionalFields)))))) {
			throw new IllegalArgumentException('Unknown fields: '.implode(',', $unknownFields));
		}

		$this->name = $config['name'];
		$this->path = $config['path'];
		$this->regex = $config['regex'];
		$this->allowReverseLookup = array_key_exists('reverse-lookup', $config) ? ($config['reverse-lookup'] === true) : true;
		$this->params = ((array_key_exists('params', $config)) && (is_array($config['params']))) ? $config['params'] : array();
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getPath()
	{
		return $this->path;
	}
	
	public function getRegex()
	{
		return $this->regex;
	}
	
	public function allowReverseLookup()
	{
		return $this->allowReverseLookup;
	}
	
	public function getParameterNames()
	{
		return array_keys($this->params);
	}
	
	public function hasParameter($name)
	{
		return array_key_exists($name, $this->params);
	}
	
	public function getParameter($name, $default = null)
	{
		if(!$this->hasParameter($name)) {
			return $default;
		}
		
		return $this->params[$name];
	}
}