<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\routing\config;

use framework\config\Cachable;
use framework\config\Mergeable;
use framework\exception\IllegalArgumentException;

class RoutingConfig implements Cachable, Mergeable
{
	private $defaultAction;
	private $defaultActionSuffix;
	
	private $error404Module;
	private $error404Action;
	private $error404Layout;

	private $routeConfigs;

	public function __construct()
	{
		$this->suffix = null;
		$this->error404Module = null;
		$this->error404Action = null;
		$this->error404Layout = null;
		$this->routeConfigs = array();
	}
	
	public function getDefaultAction()
	{
		return is_string($this->defaultAction) ? $this->defaultAction : '';
	}
	
	public function getDefaultActionSuffix()
	{
		return is_string($this->defaultActionSuffix) ? $this->defaultActionSuffix : '';
	}
	
	public function getDefaultActionWithSuffix()
	{
		return $this->getDefaultAction().$this->getDefaultActionSuffix();
	}
	
	public function getError404Module()
	{
		return is_string($this->error404Module) ? $this->error404Module : '';
	}
	
	public function getError404Action()
	{
		return is_string($this->error404Action) ? $this->error404Action : '';
	}
	
	public function getError404Layout()
	{
		return is_string($this->error404Layout) ? $this->error404Layout : '';
	}
	
	public function setDefaultAction($action)
	{
		if(!is_string($action)) {
			throw new IllegalArgumentException('$action is not a string');
		}
		
		$this->defaultAction = $action;
	}
	
	public function setDefaultActionSuffix($suffix)
	{
		if(!is_string($suffix)) {
			throw new IllegalArgumentException('$suffix is not a string');
		}
		
		$this->defaultActionSuffix = $suffix;
	}
	
	public function setError404Module($module)
	{
		if(!is_string($module)) {
			throw new IllegalArgumentException('$module is not a string');
		}
		
		$this->error404Module = $module;
	}
	
	public function setError404Action($action)
	{
		if(!is_string($action)) {
			throw new IllegalArgumentException('$action is not a string');
		}
		
		$this->error404Action = $action;
	}
	
	public function setError404Layout($layout)
	{
		if(!is_string($layout)) {
			throw new IllegalArgumentException('$layout is not a string');
		}
		
		$this->error404Layout = $layout;
	}
	
	public function getRouteConfigNames()
	{
		return array_keys($this->routeConfigs);
	}
	
	public function hasRouteConfig($name)
	{
		return array_key_exists($name, $this->routeConfigs);
	}
	
	public function getRouteConfig($name)
	{
		if(!$this->hasRouteConfig($name)) {
			return null;
		}
		
		return $this->routeConfigs[$name];
	}
	
	public function addRouteConfig($config)
	{
		if(! $config instanceof RouteConfig) {
			throw new IllegalArgumentException('$config is not of type RouteConfig');
		}
		
		$this->routeConfigs[$config->getName()] = $config;
	}
	
	public function merge($config, $overwrite = false)
	{
		if(! $config instanceof RoutingConfig) {
			throw new IllegalArgumentException('$config is not of type RoutingConfig');
		}
		
		// Merge default action
		if(strlen($action = $config->getDefaultAction())) {
			if(!is_string($this->defaultAction) || ($overwrite)) {
				$this->defaultAction = $action;
			}
		}
		
		// Merge default action suffix
		if(strlen($suffix = $config->getDefaultActionSuffix())) {
			if(!is_string($this->defaultActionSuffix) || ($overwrite)) {
				$this->defaultActionSuffix = $suffix;
			}
		}

		// Merge error404 module
		if(strlen($module = $config->getError404Module())) {
			if(!is_string($this->error404Module) || ($overwrite)) {
				$this->error404Module = $module;
			}
		}
		
		// Merge error404 action
		if(strlen($action = $config->getError404Action())) {
			if(!is_string($this->error404Action) || ($overwrite)) {
				$this->error404Action = $action;
			}
		}
		
		// Merge error404 layout
		if(strlen($layout = $config->getError404Layout())) {
			if(!is_string($this->error404Layout) || ($overwrite)) {
				$this->error404Layout = $layout;
			}
		}
		
		// Merge routes
		$routeNames = $config->getRouteConfigNames();
		foreach($routeNames as $name) {
			if((!array_key_exists($name, $this->routeConfigs)) || ($overwrite)) {
				$routeConfig = $config->getRouteConfig($name);
				$this->routeConfigs[$name] = $routeConfig;
			}
		}
	}
}