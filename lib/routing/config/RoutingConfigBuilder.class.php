<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\routing\config;

use framework\config\AbstractConfigBuilder;
use framework\routing\RoutingException;

class RoutingConfigBuilder extends AbstractConfigBuilder
{
	protected function doBuild($xpath)
	{
		$config = new RoutingConfig();
		
		$this->parseConfig($config, $xpath);
		$this->parseRoutes($config, $xpath);
		
		return $config;
	}
	
	private function parseConfig($config, $xpath)
	{
		$nodeList = $xpath->query('/routing/config');
		foreach($nodeList as $node) {
			$nodeConfig = $this->getConfigFromNode($node);
			
			if(array_key_exists('default-action', $nodeConfig)) {
				$config->setDefaultAction($nodeConfig['default-action']);
			}
			if(array_key_exists('default-action-suffix', $nodeConfig)) {
				$config->setDefaultActionSuffix($nodeConfig['default-action-suffix']);
			}
			if(array_key_exists('error404-module', $nodeConfig)) {
				$config->setError404Module($nodeConfig['error404-module']);
			}
			if(array_key_exists('error404-action', $nodeConfig)) {
				$config->setError404Action($nodeConfig['error404-action']);
			}
			if(array_key_exists('error404-layout', $nodeConfig)) {
				$config->setError404Layout($nodeConfig['error404-layout']);
			}
		}
	}
	
	private function parseRoutes($config, $xpath)
	{
		$routeNodeList = $xpath->query('/routing/routes/route');

		foreach($routeNodeList as $routeNode)
		{
			$routeName = $routeNode->getAttribute('name');
			if(!strlen($routeName)) {
				continue;
			}
			
			if($config->hasRouteConfig($routeName)) {
				throw new RoutingException('Duplicate route found "'.$routeName.'"');
			}

			$lookup = ($routeNode->getAttribute('lookup') != 'false');

			$pathNodeList = $xpath->query('path/text()', $routeNode);
			if(!$pathNodeList->length) {
				continue;
			}

			$path = $pathNodeList->item(0)->nodeValue;
			if(!strlen($path)) {
				continue;
			}

			if(preg_match_all('#\{([0-9a-z]+)\}#i', $path, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE))
			{
				$regex = ''; $offset = 0;

				foreach($matches as $match)
				{
					$matchFull = $match[0][0];
					$matchOffset = $match[0][1];
					$matchGroup = $match[1][0];

					$regex .= preg_quote(substr($path, $offset, ($matchOffset - $offset)), '#');
					$regex .= '(?P<'.$matchGroup.'>[0-9a-z_-]+)';

					$offset = ($matchOffset + strlen($matchFull));
				}

				$regex .= preg_quote(substr($path, $offset), '#');
			}
			else
			{
				$regex = preg_quote($path, '#');
			}

			$params = array();
			$paramsNodeList = $xpath->query('params/param', $routeNode);

			foreach($paramsNodeList as $paramNode) {
				if(strlen($paramName = $paramNode->getAttribute('name'))) {
					$params[$paramName] = $paramNode->nodeValue;
				}
			}

			$config->addRouteConfig(new RouteConfig(array(
				'name' => $routeName,
				'path' => $path, 
				'regex' => $regex, 
				'params' => $params,
				'reverse-lookup' => $lookup
			)));
		}
	}
}