<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\routing;

use Exception;

class RoutingException extends Exception
{
}