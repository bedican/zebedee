<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\plugin;

use framework\cache\ConfigCache;
use framework\plugin\config\PluginConfigBuilder;
use framework\plugin\config\PluginConfig;
use framework\application\Application;
use framework\exception\IllegalArgumentException;

abstract class Plugin
{
	private $application;
	private $pluginDir;
	
	private $pluginName;

	private $config;
	private $appConfig;
	
	public final function __construct($application, $pluginDir)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(!is_dir($pluginDir)) {
			throw new IllegalArgumentException('Directory does not exist "'.$pluginDir.'"');
		}
		
		$this->application = $application;
		$this->pluginDir = $pluginDir;
		
		$this->pluginName = basename($this->pluginDir);

		$this->checkDependencies();
		
		$this->initConfig();
		$this->configure();
	}
	
	// Default empty implementation
	protected function configure() {}
	
	private function initConfig()
	{
		$pluginName = basename($this->pluginDir);

		$cache = new ConfigCache($this->application, 'plugin-'.$this->pluginName);
		$this->config = $cache->get('plugin.xml');
		
		if($this->config instanceof PluginConfig) {
			return;
		}

		$filename = $this->pluginDir.'/config/plugin.xml';

		if(is_file($filename)) {
			$builder = new PluginConfigBuilder();
			$this->config = $builder->build($filename);
		} else {
			$this->config = new PluginConfig();
		}
		
		$cache->put('plugin.xml', $this->config);
	}
	
	private function checkDependencies()
	{
		// TODO: The dependancy could also note a type of plugin, instead of a specific plugin.
		// $this->application->hasPluginType('database');
		// Types could be denoted in the dependancy list with a prefix like ":database"
		
		$dependencies = $this->getDependencies();
		if((!is_array($dependencies)) || (!sizeof($dependencies))) {
			return;
		}
		
		// Each $dependency can be a space separated list, at least one must be satisfied.
		foreach($dependencies as $dependency) {	
			
			$found = false;
			$dependency = preg_split('#\s+#', $dependency);

			foreach($dependency as $dep) {
				if($this->application->hasPlugin($dep)) {
					$found = true; break;
				}
			}
			
			if(!$found) {
				throw new PluginException('Missing dependency: '.implode(', ', $dependency));
			}
		}
	}
	
	protected function getDependencies()
	{
		return array();
	}
	
	public function getApplication()
	{
		return $this->application;
	}
	
	public function getConfig()
	{
		return $this->config;
	}
	
	public function getPluginName()
	{
		return $this->pluginName;
	}
	
	public function getPluginDir()
	{
		return $this->pluginDir;
	}
}