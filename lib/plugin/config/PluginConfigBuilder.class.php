<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\plugin\config;

use framework\config\AbstractConfigBuilder;

class PluginConfigBuilder extends AbstractConfigBuilder
{
	protected function doBuild($xpath)
	{
		$config = new PluginConfig();

		$this->parseUserValues($config, $xpath);
		
		return $config;
	}
	
	private function parseUserValues($config, $xpath)
	{
		$nodeList = $xpath->query('/plugin');
		
		if($nodeList->length) {
			if(is_array($nodeConfig = $this->getConfigFromNode($nodeList->item(0)))) {	
				foreach($nodeConfig as $key => $value) {
					$config->setValue($key, $value);
				}
			}
		}
	}
}