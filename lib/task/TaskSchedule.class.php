<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\task;

use framework\application\Application;
use framework\exception\IllegalArgumentException;

abstract class TaskSchedule
{
	private static $dayAliases = array(
		'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'
	);
	
	private static $monthAliases = array(
		'jan', 'feb', 'mar', 'apr', 'may', 'jun',
		'jul', 'aug', 'sep', 'oct', 'nov', 'dec'
	);
	
	private $application;

	private $aliases = array();
	private $tasks = array();
	private $currentTime;
	
	public final function __construct($application)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		
		$this->application = $application;
		$this->currentTime = $this->normalizeTime(time());

		$this->addAlias('now', 		'*');
		$this->addAlias('hourly', 	'M00');
		$this->addAlias('cow', 		'M00');
		$this->addAlias('daily',	'H0');
		$this->addAlias('weekly', 	'DW1 H0 M0');
		$this->addAlias('monthly', 	'DM1 H0 M0');
		$this->addAlias('yearly', 	'MY1 DM1 H0 M0');

		$this->init();
	}
	
	// Override me
	protected function init() {}
	
	public function getApplication()
	{
		return $this->application;
	}
	
	public function getCurrentTime()
	{
		return $this->currentTime;
	}
	
	private function normalizeTime($timestamp)
	{
		return $timestamp - date('s', $timestamp);
	}

	public function addTask($task, $schedules = '*')
	{
		if(! $task instanceof ScheduledTask) {
			throw new IllegalArgumentException('$task is not of type ScheduledTask');
		}
		
		$schedules = is_array($schedules) ? $schedules : array($schedules);
		$schedules = array_map(array($this, 'parseSchedule'), $schedules);

		$this->tasks[] = array(
			'schedules' => $schedules,
			'task' => $task
		);
	}
	
	public function addAlias($alias, $schedule)
	{
		$this->aliases[$alias] = $schedule;
	}
	
	/**
	 * Parses schedule string in to a normalized array
	 * 
	 * Format values:
	 * 		DM1	- Day of month
	 * 		DW1	- Day of week (1=Mon, 7=Sun)
	 * 		MY1	- Month (1=Jan, 12=Dec)
	 * 		H0	- Hour 24hour format
	 * 		M0	- Minute
	 */
	private function parseSchedule($schedule)
	{
		foreach($this->aliases as $alias => $replacement) {
			$schedule = preg_replace('#\b'.$alias.'\b#i', $replacement, $schedule);
		}
		foreach(self::$dayAliases as $index => $day) {
			$schedule = preg_replace('#\b'.$day.'\b#i', 'DW'.($index + 1), $schedule);
		}
		foreach(self::$monthAliases as $index => $month) {
			$schedule = preg_replace('#\b'.$month.'\b#i', 'MY'.($index + 1), $schedule);
		}
		
		$parsed = array();

		if(preg_match('#\bDM(0?[1-9]|[1-2][0-9]|3[0-1])\b#', $schedule, $matches)) {
			$parsed['day-of-month'] = intval($matches[1]);
		}
		
		if(preg_match('#\bDW(0?[1-7])\b#', $schedule, $matches)) {
			$parsed['day-of-week'] = intval($matches[1]);
		}
		
		if(preg_match('#\bMY(0?[1-9]|1[0-2])\b#', $schedule, $matches)) {
			$parsed['month'] = intval($matches[1]);
		}
		
		if(preg_match('#\bH([0-1]?[0-9]|2[0-3])\b#', $schedule, $matches)) {
			$parsed['hour'] = intval($matches[1]);
		}
		
		if(preg_match('#\bM([0-5]?[0-9])\b#', $schedule, $matches)) {
			$parsed['minute'] = intval($matches[1]);
		}
		
		// The only schedule that should leave the $parsed array empty is '*' (every minute)
		if((!sizeof($parsed)) && ($schedule != '*')) {
			throw new IllegalArgumentException('Invalid schedule format');
		}

		return $parsed;
	}
	
	/**
	 * Return tasks scheduled since a given timestamp.
	 * Tasks greater than $then and less than or equal than the current time.
	 */
	public function getTasksSince($then)
	{
		if(!is_int($then)) {
			throw new IllegalArgumentException('$then is not an integer');
		}

		$now = $this->getCurrentTime();
		$then = $this->normalizeTime($then);
		
		if($then >= $now) {
			return array();
		}
		
		$tasks = array();
		foreach($this->tasks as $task) {
						
			$schedules = $task['schedules'];
			foreach($schedules as $schedule) {

				$year = date('Y', $now);
				$month = (array_key_exists('month', $schedule)) ? $schedule['month'] : date('n', $now);
				$dayOfMonth = (array_key_exists('day-of-month', $schedule)) ? $schedule['day-of-month'] : date('j', $now);
				$hour = (array_key_exists('hour', $schedule)) ? $schedule['hour'] : date('H', $now);
				$minute = (array_key_exists('minute', $schedule)) ? $schedule['minute'] : date('i', $now);
				
				$scheduledTs = mktime($hour, $minute, 0, $month, $dayOfMonth, $year);
				
				if(($scheduledTs > $now) || ($scheduledTs <= $then)) {
					continue;
				}
				
				// If there has been a day-of-week between then and now, we are scheduled to run.
				if(array_key_exists('day-of-week', $schedule)) {
					$currentDayOfWeek = date('N', $now);
					if($schedule['day-of-week'] != $currentDayOfWeek) {
						$lastDayOfWeek = strtotime('last '.self::$dayAliases[$schedule['day-of-week']], $now);
						if($lastDayOfWeek <= $then) {
							continue;
						}
					}
				}
	
				$tasks[] = $task['task'];
				break;
			}
		}

		return $tasks;
	}
}