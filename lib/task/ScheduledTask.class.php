<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\task;

class ScheduledTask
{
	private $module;
	private $action;

	public function __construct($module, $action)
	{
		$this->module = $module;
		$this->action = $action;
	}
	
	public function getModule()
	{
		return $this->module;
	}
	
	public function getAction()
	{
		return $this->action;
	}
}