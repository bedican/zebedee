<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\exception;

use Exception;

class IllegalStateException extends Exception
{
}