<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\context;

use framework\console\Console;

class TaskContext implements Context
{
	private $console;
	private $action;
	private $module;
	
	public function __construct()
	{
		$this->console = new Console();
		
		$this->module = $this->hyphenToCamelCase($this->console->shiftArg());		
		$this->action = $this->hyphenToCamelCase($this->console->shiftArg());

		if(!$this->module) {
			$this->module = 'task';
			$this->action = 'default';
		} else if(!$this->action) {
			$this->action = $this->module;
			$this->module = 'task';
		}
	}
	
	private function hyphenToCamelCase($value)
	{
		return lcfirst(implode('', array_map('ucfirst', explode('-', $value))));
	}
	
	public function getConsole()
	{
		return $this->console;
	}
	
	public function getModule()
	{
		return $this->module;
	}
	
	public function getAction()
	{
		return $this->action;
	}
}