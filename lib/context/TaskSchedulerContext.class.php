<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\context;

use framework\task\ScheduledTask;
use framework\console\Console;
use framework\exception\IllegalArgumentException;

class TaskSchedulerContext implements Context
{
	private $console;
	private $task;
	
	public function __construct($console, $task)
	{
		if(! $console instanceof Console) {
			throw new IllegalArgumentException('$console is not of type Console');
		}
		if(! $task instanceof ScheduledTask) {
			throw new IllegalArgumentException('$task is not of type ScheduledTask');
		}
		
		$this->console = $console;
		$this->task = $task;
	}
	
	public function getConsole()
	{
		return $this->console;
	}

	public function getModule()
	{
		return $this->task->getModule();
	}
	
	public function getAction()
	{
		return $this->task->getAction();
	}
}