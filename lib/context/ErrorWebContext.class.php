<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\context;

use framework\module\ActionNotFoundException;
use framework\controller\config\WebControllerConfig;
use framework\exception\IllegalArgumentException;
use Exception;

class ErrorWebContext implements Context
{
	private $context;
	private $config;
	private $exception;
	
	private $module;
	private $action;

	public function __construct($context, $config, $exception)
	{
		if(! $context instanceof WebContext) {
			throw new IllegalArgumentException('$context is not of type WebContext');
		}
		if(! $config instanceof WebControllerConfig) {
			throw new IllegalArgumentException('$config is not of type WebControllerConfig');
		}
		if(! $exception instanceof Exception) {
			throw new IllegalArgumentException('$exception is not of type Exception');
		}

		$this->context = $context;
		$this->config = $config;
		$this->exception = $exception;
		
		if($this->exception instanceof ActionNotFoundException) {
			$this->module = $this->context->getRouting()->getConfig()->getError404Module();
			$this->action = $this->context->getRouting()->getConfig()->getError404Action();
		} else {
			$this->module = $this->config->getErrorModule();
			$this->action = $this->config->getErrorAction();
		}
	}
	
	public function getException()
	{
		return $this->exception;
	}
	
	public function getModule()
	{
		return $this->module;
	}
	
	public function getAction()
	{
		return $this->action;
	}
	
	public function __call($method, $args)
	{
		return call_user_func_array(array($this->context, $method), $args);
	}
}