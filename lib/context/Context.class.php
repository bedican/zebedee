<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\context;

interface Context
{
	public function getModule();
	public function getAction();
}