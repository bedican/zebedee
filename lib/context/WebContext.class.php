<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\context;

use framework\application\Application;
use framework\routing\Routing;
use framework\net\Response;
use framework\net\Request;
use framework\net\Session;
use framework\exception\IllegalArgumentException;
use framework\controller\config\WebControllerConfig;

class WebContext implements Context
{
	private $routing;
	private $currentRoute;
	private $request;
	private $response;
	private $config;
	
	public function __construct($application, $config)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(! $config instanceof WebControllerConfig) {
			throw new IllegalArgumentException('$config is not of type WebControllerConfig');
		}

		$this->request = new Request();
		$this->response = new Response();		
		$this->routing = new Routing($application);
		$this->currentRoute = $this->routing->getRoute($this->request->getUri());
		$this->config = $config;
	}
	
	public function getConfig()
	{
		return $this->config;
	}
	
	public function getRouting()
	{
		return $this->routing;
	}
	
	public function getCurrentRoute()
	{
		return $this->currentRoute;
	}
	
	public function getRequest()
	{
		return $this->request;
	}
	
	public function getResponse()
	{
		return $this->response;
	}
	
	public function getSession($name = 'default')
	{
		// Update PHP session garbage collection.
		// Needs to be done before session_start is called
		// as that is where the garbage collector is invoked.
		$phpMaxLifetime = intval(ini_get('session.gc_maxlifetime'));
		$maxInactivity = $this->config->getSessionMaxInactivity();
		if($phpMaxLifetime < $maxInactivity) {
			ini_set('session.gc_maxlifetime', $maxInactivity);
		}

		$session = Session::getInstance($name);
		$session->setMaxInactivity($maxInactivity);

		return $session;
	}
	
	public function getModule()
	{
		return $this->currentRoute->getModule();
	}
	
	public function getAction()
	{
		return $this->currentRoute->getAction();
	}
	
	public function getLayout()
	{
		return $this->currentRoute->getLayout();
	}
	
	public function getRoutePath()
	{
		return $this->currentRoute->getPath();
	}
}