<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\lang;

use framework\exception\IllegalArgumentException;
use framework\exception\ClassNotFoundException;
use framework\exception\FileNotFoundException;

class ClassLoader
{
	private static $instance = null;
	
	private $bootstrap;
	private $plugins;
	private $appDir;
	private $frameworkDir;
	
	private function __construct()
	{
		$this->bootstrap = false;
		$this->plugins = array();
		$this->appDir = null;
		$this->frameworkDir = dirname(__FILE__).'/../..';
	}
	
	public static function getInstance()
	{
		if(self::$instance == null) {
			self::$instance = new ClassLoader();
		}

		return self::$instance;
	}
	
	public function bootstrap()
	{
		if(!$this->bootstrap) {
			$this->bootstrap = spl_autoload_register(array($this, 'autoload'));
		}
	}
	
	public function bind($appDir)
	{
		if(!is_dir($appDir)) {
			throw new IllegalArgumentException('Unknown directory "'.$appDir.'"');
		}
		
		$this->appDir = $appDir;
	}
	
	public function autoload($class)
	{
		$classPath = array_filter(explode('\\', $class), 'strlen');
		if(sizeof($classPath) < 2) {
			throw new ClassNotFoundException('No class specified (using '.$class.')');
		}
		
		$rootNs = strtolower(array_shift($classPath));

		if((!$this->appDir) && ($rootNs == 'app')) {
			return false; // Trying to use app namespace when an application has not been bound
		}
		
		try {
			if($rootNs == 'framework') {
				$basePath = $this->frameworkDir;
			} else if($rootNs == 'app') {
				$basePath = $this->appDir;
			} else if($rootNs == 'module') {
				$basePath = $this->getModulePath(array_shift($classPath));
			} else if($rootNs == 'plugin') {
				$basePath = $this->getPluginPath(array_shift($classPath));
			} else {
				return false; // Unknown root namespace
			}
		} catch(FileNotFoundException $fnfe) {
			return false;
		}
		
		$filePath = $basePath.'/lib/'.implode('/', $classPath).'.class.php';
		
		if(is_file($filePath)) {
			include_once($filePath);
			if((class_exists($class, false)) || (interface_exists($class, false))) {
				return;
			}

			// While the autoloader should not really throw exceptions, this is .. exceptional
			throw new ClassNotFoundException($class.' is not defined within '.$filePath);
		}
	}
	
	public function getModulePath($module)
	{
		if(!is_string($module)) {
			throw new IllegalArgumentException('$module is not of type string');
		}

		// Check application modules if bound.
		if($this->appDir) {
			$modulePath = $this->appDir.'/modules/'.$module;
			if(is_dir($modulePath)) {
				return $modulePath;
			}
		}

		// Check enabled plugin modules
		foreach($this->plugins as $plugin => $modules) {
			if(!in_array($module, $modules)) {
				continue;
			}
			
			$pluginPath = $this->getPluginPath($plugin);
			$modulePath = $pluginPath.'/modules/'.$module;

			if(is_dir($modulePath)) {
				return $modulePath;
			}
		}
		
		throw new FileNotFoundException('Module path not found');
	}

	public function getPluginPath($plugin)
	{
		if(!is_string($plugin)) {
			throw new IllegalArgumentException('$plugin is not of type string');
		}
		
		if(!array_key_exists($plugin, $this->plugins)) {
			throw new ClassNotFoundException('Plugin '.$plugin.' is unknown by class loader');
		}
		
		$basePaths = array($this->frameworkDir.'/plugins');
		if($this->appDir) {
			array_unshift($basePaths, $this->appDir.'/plugins');
		}

		foreach($basePaths as $basePath) {
			$pluginPath = $basePath.'/'.$plugin;
			if(is_dir($pluginPath)) {
				return $pluginPath;
			}
		}
		
		throw new FileNotFoundException('Plugin path not found');
	}
	
	public function addPlugin($plugin)
	{
		if(!is_string($plugin)) {
			throw new IllegalArgumentException('$plugin is not a string');
		}
		if(array_key_exists($plugin, $this->plugins)) {
			return;
		}
		
		$this->plugins[$plugin] = array();
		return $this->getPluginPath($plugin);
	}
	
	public function addPluginModule($plugin, $module)
	{
		if(!array_key_exists($plugin, $this->plugins)) {
			throw new IllegalArgumentException('Can not add module '.$module.' for plugin '.$plugin.', unknown plugin.');
		}
		if(in_array($module, $this->plugins[$plugin])) {
			return;
		}
		
		$this->plugins[$plugin][] = $module;
		return $this->getModulePath($module);
	}
}
