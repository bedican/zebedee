<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\lang;

use framework\exception\EnumNotFoundException;

abstract class Enum
{
	private static $registered = false;
	private static $values = array();

	private final function __construct($args)
	{
		$this->init($args);
	}
	
	protected abstract function init($args);
	
	protected static function addValues()
	{
	}
	
	private static function register()
	{
		if(!self::$registered) {
			self::$registered = true;
			static::addValues();
		}
	}
	
	protected static function addValue($name, $args = array())
	{
		$className = get_called_class();
		self::$values[$name] = new $className($args);
	}
	
	public static function getValues()
	{
		self::register();
		return array_values(self::$values);
	}
	
	public static function __callStatic($name, $args)
	{
		self::register();
		if(!array_key_exists($name, self::$values)) {
			throw new EnumNotFoundException('Unknown enum value '.get_called_class().'::'.$name);
		}
		
		return self::$values[$name];
	}
}