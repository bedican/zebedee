<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\controller;

use Exception;

class ControllerException extends Exception
{
}