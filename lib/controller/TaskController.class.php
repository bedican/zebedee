<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\controller;

use framework\context\TaskContext;
use framework\logger\ConsoleLogger;
use Exception;

class TaskController extends AbstractController
{
	public function run()
	{
		$context = new TaskContext();
		$console = $context->getConsole();
		
		if($console->getOption('log-console', false)) {
			$this->getApplication()->addLogger(new ConsoleLogger($console));
		}

		try {
			$this->getModule($context, 'TaskModule')->runAction();
		} catch(Exception $e) {
			$console->writeln($e->getMessage());
			$this->getApplication()->getLogger()->exception($e);
		}
	}
}