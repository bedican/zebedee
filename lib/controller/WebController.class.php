<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\controller;

use framework\view\WebPageView;
use framework\view\NullView;
use framework\controller\config\WebControllerConfigBuilder;
use framework\controller\config\WebControllerConfig;
use framework\cache\ConfigCache;
use framework\context\WebContext;
use framework\context\ErrorWebContext;
use framework\exception\IllegalArgumentException;
use framework\exception\FileNotFoundException;
use Exception;

class WebController extends AbstractController
{
	private $config;
	private $context;
	
	protected function init()
	{
		$this->initConfig();
		$this->context = new WebContext($this->getApplication(), $this->config);
	}
	
	private function initConfig()
	{
		$cache = new ConfigCache($this->getApplication());
		$this->config = $cache->get('web.xml');
		
		if($this->config instanceof WebControllerConfig) {
			return;
		}

		$builder = new WebControllerConfigBuilder();
		
		// Framework web.xml should always be present
		$this->config = $builder->build(dirname(__FILE__).'/../../config/web.xml');

		// Merge with each enabled plugin web.xml if present
		$plugins = $this->getApplication()->getPlugins();
		foreach($plugins as $plugin) {
			$filename = $plugin->getPluginDir().'/config/web.xml';
			if(is_file($filename)) {
				$this->config->merge($builder->build($filename), true);
			}
		}
		
		// Override with application web.xml if present
		$filename = $this->getApplication()->getApplicationDir().'/config/web.xml';
		if(is_file($filename)) {
			$this->config->merge($builder->build($filename), true);
		}
		
		// Merge environment configuration
		if(is_string($environment = $this->getApplication()->getEnvironment())) {
			if(is_file($filename = $this->getApplication()->getApplicationDir().'/config/web.'.$environment.'.xml')) {
				$this->config->merge($builder->build($filename), true);
			}
		}
		
		$cache->put('web.xml', $this->config);
	}

	private function redirectIfRequired()
	{
		// Redirect if the routing says so.
		
		$currentRoute = $this->context->getCurrentRoute();

		if($currentRoute->hasParameter('location')) {
			$this->context->getResponse()->redirect($currentRoute->getParameter('location'), true);
			return true;
		}
		
		return false;
	}
	
	private function fixRequest()
	{
		// If the current route is a 404, the uri does not end with / or the default action suffix
		// then redirect to the same url with / appended.
		
		$routingConfig = $this->context->getRouting()->getConfig();
		$currentRouteName = $this->context->getCurrentRoute()->getName();
		$request = $this->context->getRequest();
		
		$requestUri = $request->getUri();
		$suffix = $routingConfig->getDefaultActionSuffix();

		if(($currentRouteName == 'error404') && (substr($requestUri, -1) != '/') && (substr($requestUri, 0 - strlen($suffix)) != $suffix)) {
			$this->context->getResponse()->redirect($request->getScheme().$request->getHost().$requestUri.'/');
			return true;
		}
		
		return false;
	}
	
	protected function getView($context)
	{
		try {
			return new WebPageView($this->getApplication(), $context);
		} catch(FileNotFoundException $fnfe) {
			return new NullView();
		}
	}
	
	public function run()
	{
		if($this->redirectIfRequired()) {
			return;
		}
		if($this->fixRequest()) {
			return;
		}
		
		try {
			$this->getModule($this->context, 'WebModule')->runAction();
		} catch(Exception $e) {
			$this->getApplication()->getLogger()->exception($e);
			$this->context->getResponse()->clear();
			$this->getModule(new ErrorWebContext($this->context, $this->config, $e), 'WebModule')->runAction();
		}

		$this->context->getResponse()->flush();
	}
}
