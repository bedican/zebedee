<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\controller;

interface Controller
{
	public function run();
}