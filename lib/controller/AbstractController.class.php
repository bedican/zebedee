<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\controller;

use framework\context\Context;
use framework\application\Application;
use framework\exception\IllegalArgumentException;
use framework\module\ActionNotFoundException;
use framework\view\NullView;

abstract class AbstractController implements Controller
{
	private $application;
	
	public final function __construct($application)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		
		$this->application = $application;
		$this->init();
	}
	
	// Default implementation
	protected function init() {}
	
	public function getApplication()
	{
		return $this->application;
	}
	
	protected function getModule($context, $moduleInterfaceName)
	{
		$moduleInterfaceName = '\\framework\\module\\'.$moduleInterfaceName;
		
		if(! $context instanceof Context) {
			throw new IllegalArgumentException('$context is not of type Context');
		}
		if(!interface_exists($moduleInterfaceName, true)) {
			throw new IllegalArgumentException('Undefined interface: '.$moduleInterfaceName);
		}
		
		$moduleClassName = '\\module\\'.$context->getModule().'\\'.ucfirst($context->getModule()).'Module';
		
		if(!class_exists($moduleClassName, true)) {
			throw new ActionNotFoundException('Module '.$context->getModule().' not found');
		}

		// We should implement the specified interface. This ensures we get modules for the specified controller.
		// e.g. a WebController will only wish to obtain and use a module implementing the WebModule interface.
		// This means to tag a module to be runnable by one or more specific controllers, the interfaces 
		// WebModule, TaskModule, TaskSchedulerModule can be used.

		if((!is_subclass_of($moduleClassName, $moduleInterfaceName)) && (!is_subclass_of($moduleClassName, '\\framework\\module\\Module'))) {
			throw new ControllerException($context->getModule().' is not a valid '.$moduleInterfaceName);
		}

		$module = new $moduleClassName($this->getApplication(), $context);
		$module->setView($this->getView($context));

		return $module;
	}
	
	protected function getView($context)
	{
		return new NullView();
	}
}