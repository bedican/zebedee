<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\controller;

use framework\console\Console;
use framework\context\TaskSchedulerContext;
use Exception;

class TaskSchedulerController extends AbstractController
{
	private $console;
	
	protected function init()
	{
		$this->console = new Console();
	}
	
	private function getTaskSchedule()
	{
		$taskSchedule = $this->console->shiftArg();
		
		if($taskSchedule) {
			$taskScheduleClassName = implode('', array_map('ucfirst', explode('-', $task)));
		} else {
			$taskScheduleClassName = $this->getApplication()->getConfig()->getValue('task-schedule-class');
			if(!$taskScheduleClassName) {
				throw new ControllerException('No task schedule found.');
				// TODO: Maybe default to using DefaultTaskSchedule which reads in from a configuration file.
			}
		}
		
		if(!class_exists($taskScheduleClassName, true)) {
			throw new ControllerException('Unknown task schedule');
		}
		
		if(!is_subclass_of($taskScheduleClassName, '\\framework\\task\\TaskSchedule')) {
			throw new ControllerException($task.' is not a valid task schedule');
		}
		
		return new $taskScheduleClassName($this->getApplication());
	}
	
	public function run()
	{
		if($this->console->getOption('log-console', false)) {
			$this->getApplication()->addLogger(new ConsoleLogger($this->console));
		}

		$logger = $this->getApplication()->getLogger();
		$taskSchedule = $this->getTaskSchedule();
		
		$lastRunFilePath = $this->getApplication()->getApplicationDir().'/tmp/cron.lastrun';

		$then = @filemtime($lastRunFilePath);
		$touched = touch($lastRunFilePath, $taskSchedule->getCurrentTime());
		
		if((!is_int($then)) || (!$touched)) {
			$logger->error('Failed to obtain last run time');
			return;
		}
		
		$tasks = $taskSchedule->getTasksSince($then);

		foreach($tasks as $task) {			
			switch ($pid = pcntl_fork()) {

				case -1:
					$this->getApplication()->getLogger()->error('Failed to fork task: '.$task->getModule().'/'.$task->getAction());
				break;
				
				case 0: // Child process
					try {
						$this->getModule(new TaskSchedulerContext($this->console, $task), 'TaskSchedulerModule')->runAction();
					} catch(Exception $e) {
						$this->getApplication()->getLogger()->exception($e);
					}
				break;
				
				default: // Parent process
					pcntl_waitpid($pid, $status);
				break;
			}
		}
	}
}