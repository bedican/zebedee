<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\controller\config;

use framework\config\AbstractConfigBuilder;

class WebControllerConfigBuilder extends AbstractConfigBuilder
{
	protected function doBuild($xpath)
	{
		$config = new WebControllerConfig();

		$this->parseErrorModuleAction($config, $xpath);
		$this->parseUploadDir($config, $xpath);
		$this->parseSessionMaxInactivity($config, $xpath);
		$this->parseUserValues($config, $xpath);

		return $config;
	}
	
	private function parseErrorModuleAction($config, $xpath)
	{
		$nodeList = $xpath->query('/web/error-module/text()');
		if(!$nodeList->length) {
			return;
		}
		
		$errorModule = trim($nodeList->item(0)->nodeValue);
		
		$nodeList = $xpath->query('/web/error-action/text()');
		if(!$nodeList->length) {
			return;
		}
		
		$errorAction = trim($nodeList->item(0)->nodeValue);

		$config->setErrorModuleAction($errorModule, $errorAction);
	}
	
	private function parseUploadDir($config, $xpath)
	{
		$nodeList = $xpath->query('/web/upload-dir/text()');		
		if(!$nodeList->length) {
			return;
		}
		
		$uploadDir = trim($nodeList->item(0)->nodeValue);
		
		$config->setUploadDir($uploadDir);
	}
	
	private function parseSessionMaxInactivity($config, $xpath)
	{
		$nodeList = $xpath->query('/web/session-max-inactivity/text()');
		if(!$nodeList->length) {
			return;
		}
		
		$sessionMaxInactivity = trim($nodeList->item(0)->nodeValue);
		
		if(!ctype_digit($sessionMaxInactivity)) {
			return;
		}
		
		$config->setSessionMaxInactivity(intval($sessionMaxInactivity));
	}
	
	private function parseUserValues($config, $xpath)
	{
		$nodeList = $xpath->query('/web');
		
		if($nodeList->length) {
			if(is_array($nodeConfig = $this->getConfigFromNode($nodeList->item(0)))) {	
				foreach($nodeConfig as $key => $value) {
					if(!in_array(strtolower($key), array('error-module', 'error-action', 'upload-dir', 'session-max-inactivity'))) {
						$config->setValue($key, $value);
					}
				}
			}
		}
	}
}