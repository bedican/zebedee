<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\controller\config;

use framework\config\Cachable;
use framework\config\Mergeable;
use framework\config\MapConfig;
use framework\exception\IllegalArgumentException;

class WebControllerConfig extends MapConfig implements Cachable, Mergeable
{
	private $errorModule;
	private $errorAction;
	private $uploadDir;
	private $sessionMaxInactivity;
	
	public function __construct()
	{
		$this->errorModule = null;
		$this->errorAction = null;
		$this->uploadDir = null;
		$this->sessionMaxInactivity = null;
	}
	
	public function setErrorModuleAction($module, $action)
	{
		if(!is_string($module)) {
			throw new IllegalArgumentException('$module is not a string');
		}
		if(!is_string($action)) {
			throw new IllegalArgumentException('$action is not a string');
		}
		
		$this->errorModule = $module;
		$this->errorAction = $action;
	}
	
	public function setUploadDir($dir)
	{
		if(!is_string($dir)) {
			throw new IllegalArgumentException('$dir is not a string');
		}
		
		$this->uploadDir = $dir;
	}
	
	public function setSessionMaxInactivity($sessionMaxInactivity)
	{
		if(!is_int($sessionMaxInactivity)) {
			throw new IllegalArgumentException('$sessionMaxInactivity is not an integer');
		}
		
		$this->sessionMaxInactivity = $sessionMaxInactivity;
	}

	public function getErrorModule()
	{
		return is_string($this->errorModule) ? $this->errorModule : '';
	}
	
	public function getErrorAction()
	{
		return is_string($this->errorAction) ? $this->errorAction : '';
	}
	
	public function getUploadDir()
	{
		return is_string($this->uploadDir) ? $this->uploadDir : '';
	}
	
	public function getSessionMaxInactivity()
	{
		return is_int($this->sessionMaxInactivity) ? $this->sessionMaxInactivity : 1800;
	}
	
	public function merge($config, $overwrite = false)
	{
		if(! $config instanceof WebControllerConfig) {
			throw new IllegalArgumentException('$config is not of type WebControllerConfig');
		}
		
		// Merge error module
		if(strlen($module = $config->getErrorModule())) {
			if(!is_string($this->errorModule) || ($overwrite)) {
				$this->errorModule = $module;
			}
		}
		
		// Merge error action
		if(strlen($action = $config->getErrorAction())) {
			if(!is_string($this->errorAction) || ($overwrite)) {
				$this->errorAction = $action;
			}
		}
		
		// Merge upload dir
		if(strlen($uploadDir = $config->getUploadDir())) {
			if(!is_string($this->uploadDir) || ($overwrite)) {
				$this->uploadDir = $uploadDir;
			}
		}
		
		// Merge session max inactivity
		if((!is_int($this->sessionMaxInactivity)) || ($overwrite)) {
			$this->sessionMaxInactivity = $config->getSessionMaxInactivity();
		}
		
		// Merge generic config values
		parent::merge($config, $overwrite);
	}
}
