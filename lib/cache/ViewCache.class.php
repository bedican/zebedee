<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\cache;

use framework\application\Application;
use framework\context\Context;
use framework\exception\IllegalArgumentException;

class ViewCache implements Flushable
{
	private $fileCache;

	public function __construct($application, $module)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(!is_string($module)) {
			throw new IllegalArgumentException('$module is not a string');
		}
		if(!strlen($module)) {
			throw new IllegalArgumentException('Empty $module');
		}
		
		$this->fileCache = new FileCache($application, 'view.'.$module);
	}
	
	public function put($key, $content)
	{
		$this->fileCache->put($key, $content);
	}

	public function get($key, $lifetime = 0)
	{	
		return $this->fileCache->get($key, $lifetime);
	}
	
	public function delete($key)
	{
		$this->fileCache->delete($key);
	}
	
	public function flush()
	{
		$this->fileCache->flush();
	}
	
	public function flushAll()
	{
		$this->fileCache->flushAll();
	}
}