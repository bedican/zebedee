<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\cache;

use framework\config\Cachable;
use framework\application\Application;
use framework\context\Context;
use framework\exception\IllegalArgumentException;

class ConfigCache implements Flushable
{
	private $fileCache;
	private $application;
	
	public function __construct($application, $module = null)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(($module != null) && (!is_string($module))) {
			throw new IllegalArgumentException('$module is not a string');
		}
		
		$fileCacheNamespace = 'config';
		if($module != null) {
			$fileCacheNamespace.= '.'.$module;
		}
		
		$this->application = $application;
		$this->fileCache = new FileCache($application, $fileCacheNamespace);
	}
	
	private function isEnabled()
	{
		return $this->application->getConfig()->getUseConfigCache();
	}
	
	public function put($key, $content)
	{
		if(! $content instanceof Cachable) {
			throw new IllegalArgumentException('$content is not of type Cachable');
		}
		if(!$this->isEnabled()) {
			return;
		}
		
		$this->fileCache->put($key, serialize($content));
	}

	public function get($key, $lifetime = 0)
	{
		if(!$this->isEnabled()) {
			return null;
		}
		
		$content = $this->fileCache->get($key, $lifetime);
		
		if(!$content) {
			return null;
		}
		
		return unserialize($content);
	}
	
	public function delete($key)
	{
		$this->fileCache->delete($key);
	}
	
	public function flush()
	{
		$this->fileCache->flush();
	}
	
	public function flushAll()
	{
		$this->fileCache->flushAll();
	}
}