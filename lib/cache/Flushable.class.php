<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\cache;

interface Flushable
{
	public function flush();
	public function flushAll();
}