<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\cache;

use framework\context\Context;
use framework\application\Application;
use framework\exception\IllegalArgumentException;
use framework\exception\IOException;

class FileCache implements Flushable
{
	private $cacheBaseDir;

	private $cacheDir;
	private $cacheDirRoot;

	public function __construct($application, $namespace)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if((!strlen($namespace)) || (preg_match('#[^.0-9a-z-]#i', $namespace))) {
			throw new IllegalArgumentException('Invalid $namespace');
		}
		
		$namespace = explode('.', $namespace);
				
		$this->cacheBaseDir = $application->getApplicationDir().'/cache';

		$this->cacheDir = $this->cacheBaseDir.'/'.implode('/', $namespace);
		$this->cacheDirRoot = $this->cacheBaseDir.'/'.array_shift($namespace);
	}
	
	private function getFilename($key)
	{
		return $this->cacheDir.'/'.sha1($key).'.cache';
	}
	
	public function put($key, $content)
	{
		$this->mkdirs();

		$filename = $this->getFilename($key);
		if(!is_int(file_put_contents($filename, $content))) {
			throw new IOException('Could not write "'.$filename.'"');
		}
		
		if(!chmod($filename, 0777)) {
			throw new IOException('Could not chmod "'.$filename.'"');
		}
	}
	
	public function get($key, $lifetime = 0)
	{
		if(!is_int($lifetime)) {
			throw new IllegalArgumentException('$lifetime is not an integer');
		}
		
		$filename = $this->getFilename($key);
		if(!is_file($filename)) {
			return null;
		}

		if(($lifetime) && (time() > (filemtime($filename) + $lifetime))) {
			$this->remove($filename);
			return null;
		}

		return file_get_contents($filename);
	}
	
	public function delete($key)
	{
		$filename = $this->getFilename($key);
		if(!is_file($filename)) {
			return;
		}
		
		$this->remove($filename);
	}
	
	public function flush()
	{
		$this->rrmdir($this->cacheDir);
	}
	
	public function flushAll()
	{
		$this->rrmdir($this->cacheDirRoot);
	}
	
	private function mkdirs()
	{
		if(is_dir($this->cacheDir)) {
			return;
		}

		$dir = $this->cacheBaseDir;
		$subDirs = explode('/', substr($this->cacheDir, strlen($this->cacheBaseDir) + 1));
		
		while(($subDir = array_shift($subDirs)) !== null) {
			$dir .= '/'.$subDir;

			if(is_dir($dir)) {
				continue;
			}
	
			if(!mkdir($dir, 0777)) {
				throw new IOException('Failed to create directory "'.$dir.'"');
			}
			if(!chmod($dir, 0777)) {
				throw new IOException('Could not chmod "'.$dir.'"');
			}
		}
	}
	
	private function rrmdir($dir, $rmDir = true)
	{
		$dir = (substr($dir, -1) == '/') ? substr($dir, 0, -1) : $dir;
		if(!is_dir($dir)) {
			return;
		}

		$files = glob($dir.'/*', GLOB_MARK);

		foreach($files as $file) {
			if(substr($file, -1) == '/') {
				$this->rrmdir($file);
			} else {
				$this->remove($file);
			}
		} 

		if(($rmDir) && (is_dir($dir))) {
			$this->remove($dir);
		}
	}
	
	private function remove($file)
	{
		if(!is_writable($file)) {
			throw new IOException('Not writable "'.$file.'"');
		}
		
		if(is_dir($file)) {
			rmdir($file);
		} else {
			unlink($file);
		}
	}
}