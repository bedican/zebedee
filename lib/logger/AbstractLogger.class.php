<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\logger;

use framework\exception\IllegalArgumentException;

abstract class AbstractLogger implements Logger
{
	private $levels = array();
	
	abstract protected function write($message, $level);

	public function addLevel($level)
	{
		if(!$this->isLevel($level)) {
			throw new IllegalArgumentException('Unknown log level '.$level);
		}
		if(in_array($level, $this->levels)) {
			return;
		}

		$this->levels[] = $level;
	}
	
	protected function getLevels()
	{
		return $this->levels;
	}
	
	protected function hasLevel($level)
	{
		return (in_array($level, $this->levels) || (in_array(Logger::LEVEL_ALL, $this->levels)));
	}
	
	protected function isLevel($level)
	{
		return in_array($level, array(
			Logger::LEVEL_ALL,
			Logger::LEVEL_DEBUG, 
			Logger::LEVEL_ERROR, 
			Logger::LEVEL_NOTICE
		));
	}
	
	public function __destruct()
	{
		$this->close();
	}
	
	public function close()
	{
	}
	
	public function debug($message)
	{
		if($this->hasLevel(Logger::LEVEL_DEBUG)) {
			$this->write($message, Logger::LEVEL_DEBUG);
		}
	}

	public function message($message)
	{
		if($this->hasLevel(Logger::LEVEL_NOTICE)) {
			$this->write($message, Logger::LEVEL_NOTICE);
		}		
	}

	public function error($message)
	{
		if($this->hasLevel(Logger::LEVEL_ERROR)) {
			$this->write($message, Logger::LEVEL_ERROR);
		}	
	}
	
	public function exception($exception)
	{
		if($this->hasLevel(Logger::LEVEL_ERROR)) {
			$this->write($exception->getMessage()."\n".$exception->getTraceAsString(), Logger::LEVEL_ERROR);
		}
	}
}