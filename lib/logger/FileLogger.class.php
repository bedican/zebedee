<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\logger;

use framework\application\Application;
use framework\exception\IOException;

class FileLogger extends AbstractLogger
{
	private static $levelToFilenameMap = array(
		Logger::LEVEL_DEBUG => 'debug.log',
		Logger::LEVEL_NOTICE => 'notice.log',
		Logger::LEVEL_ERROR => 'error.log'
	);

	private $handles;
	private $baseDir;

	public function __construct($application)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}

		$this->handles = array();
		$this->baseDir = $application->getApplicationDir().'/log';
	}
	
	public function close()
	{
		foreach($this->handles as $handle) {
			fclose($handle);
		}
		
		$this->handles = array();
	}
	
	private function getFileHandle($filename)
	{
		if(array_key_exists($filename, $this->handles)) {
			return $this->handles[$filename];
		}
		
		if(!is_dir($this->baseDir)) {
			if(!mkdir($this->baseDir, 0777)) {
				throw new IOException('Failed to create directory "'.$this->baseDir.'"');
			}
			if(!chmod($this->baseDir, 0777)) {
				throw new IOException('Could not chmod "'.$this->baseDir.'"');
			}
		}
		
		$filepath = $this->baseDir.'/'.$filename;
		
		if(!is_file($filepath)) {		
			if(!touch($filepath)) {
				throw new IOException('Could not touch "'.$filepath.'"');
			}
			if(!chmod($filepath, 0777)) {
				throw new IOException('Could not chmod "'.$filepath.'"');
			}
		}

		$handle = fopen($filepath, 'a+');
		
		if(!$handle) {
			throw new IOException('Could not open file pointer to '.$filename);
		}
		
		$this->handles[$filename] = $handle;
		return $handle;
	}

	protected function write($message, $level)
	{
		if(!array_key_exists($level, self::$levelToFilenameMap)) {
			$message = 'Failed to log to level "'.$level.'" with the following message: '.$message;
			$level = Logger::LEVEL_ERROR;
		}

		fwrite($this->getFileHandle(self::$levelToFilenameMap[$level]), '['.strftime('%d-%m-%Y %H:%M:%S', time()).'] '.$message . "\n");
	}
}