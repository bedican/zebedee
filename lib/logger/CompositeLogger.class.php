<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\logger;

use framework\exception\IllegalArgumentException;

class CompositeLogger extends AbstractLogger
{
	private $loggers;
	
	public function __construct()
	{
		$this->loggers = array();
	}
	
	public function addLogger($logger)
	{
		if(! $logger instanceof Logger) {
			throw new IllegalArgumentException('$logger is not of type Logger');
		}
		
		$this->loggers[] = $logger;

		foreach($this->getLevels() as $level) {
			$logger->addLevel($level);
		}
	}
	
	public function addLevel($level)
	{
		parent::addLevel($level);

		foreach($this->loggers as $logger) {
			$logger->addLevel($level);
		}
	}
	
	public function close()
	{
		foreach($this->loggers as $logger) {
			$logger->close();
		}
	}
	
	protected function write($message, $level)
	{
		throw new LoggerException('Logger does not support logging to level: '.$level);
	}
	
	public function debug($message)
	{
		foreach($this->loggers as $logger) {
			$logger->debug($message);
		}
	}

	public function message($message)
	{
		foreach($this->loggers as $logger) {
			$logger->message($message);
		}
	}

	public function error($message)
	{
		foreach($this->loggers as $logger) {
			$logger->error($message);
		}
	}

	public function exception($exception)
	{
		foreach($this->loggers as $logger) {
			$logger->exception($exception);
		}
	}
}