<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\logger;

use framework\console\Console;

class ConsoleLogger extends AbstractLogger
{
	private static $levelToStreamMap = array(
		Logger::LEVEL_DEBUG => Console::STDOUT,
		Logger::LEVEL_NOTICE => Console::STDOUT,
		Logger::LEVEL_ERROR => Console::STDERR
	);

	private $console;

	public function __construct($console)
	{
		if(! $console instanceof Console) {
			throw new IllegalArgumentException('$console is not of type Console');
		}
		
		$this->console = $console;
	}
	
	protected function write($message, $level)
	{
		if(!array_key_exists($level, self::$levelToStreamMap)) {
			$message = 'Failed to log to level "'.$level.'" with the following message: '.$message;
			$level = Logger::LEVEL_ERROR;
		}

		$this->console->writeln('['.strftime('%d-%m-%Y %H:%M:%S', time()).'] '.$message . "\n", self::$levelToStreamMap[$level]);
	}
}