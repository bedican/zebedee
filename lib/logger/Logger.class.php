<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\logger;

interface Logger
{
	const LEVEL_ALL = 'all';
	const LEVEL_DEBUG = 'debug';
	const LEVEL_NOTICE = 'notice';
	const LEVEL_ERROR = 'error';
	
	public function debug($message);
	public function message($message);
	public function error($message);
	public function exception($exception);
	public function addLevel($level);
	public function close();
}