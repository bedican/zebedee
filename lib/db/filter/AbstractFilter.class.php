<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\db\filter;

use framework\db\criteria\builder\SelectCriteriaBuilder;
use framework\exception\IllegalArgumentException;

abstract class AbstractFilter implements Filter
{
	abstract protected function doFilter($criteria);
	abstract protected function doLimit($criteria);
	abstract protected function doOrderBy($criteria);

	public function filter($criteria)
	{
		if(! $criteria instanceof SelectCriteriaBuilder) {
			throw new IllegalArgumentException('$criteria is not of type SelectCriteriaBuilder');
		}
		
		$this->doFilter($criteria);
	}
	
	public function limit($criteria)
	{
		if(! $criteria instanceof SelectCriteriaBuilder) {
			throw new IllegalArgumentException('$criteria is not of type SelectCriteriaBuilder');
		}

		$this->doLimit($criteria);
	}
	
	public function orderBy($criteria)
	{
		if(! $criteria instanceof SelectCriteriaBuilder) {
			throw new IllegalArgumentException('$criteria is not of type SelectCriteriaBuilder');
		}
		
		$this->doOrderBy($criteria);
	}
}