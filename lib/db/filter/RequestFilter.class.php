<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\db\filter;

use framework\net\Request;
use framework\exception\IllegalArgumentException;

abstract class RequestFilter extends AbstractFilter
{
	private $request;
	private $params;

	private $page;
	private $pagesize;

	final public function __construct($request, $params = array())
	{
		if(! $request instanceof Request) {
			throw new IllegalArgumentException('$request is not of type Request');
		}
		if(!is_array($params)) {
			throw new IllegalArgumentException('$params is not of type array');
		}
		
		$this->request = $request;
		$this->params = $params;
		
		$this->pagesize = 25;
		
		$this->page = $request->getGetValue('page');
		$this->page = ctype_digit($this->page) ? intval($this->page) : 1;
		
		$this->init();
	}
	
	abstract protected function init();
	
	protected function doLimit($criteria)
	{
		$criteria->limit((($this->page - 1) * $this->pagesize), $this->pagesize);
	}
	
	protected function getRequest()
	{
		return $this->request;
	}
	
	protected function getParam($name, $default = null)
	{
		if(!array_key_exists($name, $this->params)) {
			return $default;
		}
		
		return $this->params[$name];
	}
	
	public function getPage()
	{
		return $this->page;
	}
	
	public function getPagesize()
	{
		return $this->pagesize;
	}
	
	protected function setPagesize($pagesize)
	{
		if(!is_int($pagesize)) {
			throw new IllegalArgumentException('$pagesize is not of type int');
		}
		
		$this->pagesize = $pagesize;
	}
}