<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\db\filter;

interface Filter
{
	public function filter($criteria);
	public function limit($criteria);
	public function orderBy($criteria);
}