<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\config;

use framework\config\Cachable;
use framework\config\Mergeable;
use framework\exception\IllegalArgumentException;

class DatabaseConfig implements Cachable, Mergeable
{
	private $drivers;
	private $dataSources;
	private $dao;
	private $daoDataSources;
	
	public function __construct()
	{
		$this->drivers = array();
		$this->dataSources = array();
		$this->dao = array();
		$this->daoDataSources = array();
	}
	
	public function getDriverNames()
	{
		return array_keys($this->drivers);
	}
	
	public function getDriverConfig($name)
	{
		if(!array_key_exists($name, $this->drivers)) {
			return null;
		}
		
		return $this->drivers[$name];
	}
	
	public function addDriverConfig($config)
	{
		if(! $config instanceof DriverConfig) {
			throw new IllegalArgumentException('$config is not of type DriverConfig');
		}
		
		$this->drivers[$config->getName()] = $config;
	}
	
	public function getDataSourceNames()
	{
		return array_keys($this->dataSources);
	}
	
	public function getDataSourceConfig($name)
	{
		if(!array_key_exists($name, $this->dataSources)) {
			return null;
		}
		
		return $this->dataSources[$name];
	}
	
	public function addDataSourceConfig($config)
	{
		if(! $config instanceof DataSourceConfig) {
			throw new IllegalArgumentException('$config is not of type DataSourceConfig');
		}
		
		$this->dataSources[$config->getName()] = $config;
	}
	
	public function getDaoNames()
	{
		return array_keys($this->dao);
	}
	
	public function getDaoConfig($name)
	{
		if(!array_key_exists($name, $this->dao)) {
			return null;
		}
		
		return $this->dao[$name];
	}
	
	public function addDaoConfig($config)
	{
		if(! $config instanceof DaoConfig) {
			throw new IllegalArgumentException('$config is not of type DaoConfig');
		}
		
		$this->dao[$config->getName()] = $config;
	}
	
	public function getDaoDataSourceNames()
	{
		return array_keys($this->daoDataSources);
	}
	
	public function getDaoDataSource($dao)
	{
		if(!array_key_exists($dao, $this->daoDataSources)) {
			return null;
		}
		
		return $this->daoDataSources[$dao];
	}
	
	public function getDaoDataSourceConfig($dao)
	{
		if(!array_key_exists($dao, $this->daoDataSources)) {
			return null;
		}
		
		return $this->getDataSourceConfig($this->daoDataSources[$dao]);
	}
	
	public function addDaoDataSource($dao, $dataSource)
	{
		$this->daoDataSources[$dao] = $dataSource;
	}
	
	public function merge($config, $overwrite = false)
	{
		if(! $config instanceof DatabaseConfig) {
			throw new IllegalArgumentException('$config is not of type DatabaseConfig');
		}

		// Merge drivers
		$driverNames = $config->getDriverNames();
		foreach($driverNames as $name) {
			if((!array_key_exists($name, $this->drivers)) || ($overwrite)) {
				$driverConfig = $config->getDriverConfig($name);
				$this->drivers[$name] = $driverConfig;
			}
		}

		// Merge data sources
		$datasourceNames = $config->getDataSourceNames();
		foreach($datasourceNames as $name) {
			if((!array_key_exists($name, $this->dataSources)) || ($overwrite)) {
				$datasourceConfig = $config->getDataSourceConfig($name);
				$this->dataSources[$name] = $datasourceConfig;
			}
		}

		// Merge dao
		$daoNames = $config->getDaoNames();
		foreach($daoNames as $name) {
			if((!array_key_exists($name, $this->dao)) || ($overwrite)) {
				$daoConfig = $config->getDaoConfig($name);
				$this->dao[$name] = $daoConfig;
			}
		}

		// Merge dao data sources
		$daoNames = $config->getDaoDataSourceNames();
		foreach($daoNames as $name) {
			if((!array_key_exists($name, $this->daoDataSources)) || ($overwrite)) {
				$dataSource = $config->getDaoDataSource($name);
				$this->daoDataSources[$name] = $dataSource;
			}
		}
	}
}