<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\db\config;

use framework\db\schema\Schema;
use framework\config\AbstractConfigBuilder;
use framework\config\ConfigBuilderException;

class SchemaConfigBuilder extends AbstractConfigBuilder
{
	protected function doBuild($xpath)
	{
		$config = new SchemaConfig();
		
		$this->parseSchemas($config, $xpath);
		
		return $config;
	}
	
	private function parseSchemas($config, $xpath)
	{
		$nodeList = $xpath->query('/schemas/schema');
		foreach($nodeList as $node) {
			if(!$node->hasAttribute('name')) {
				throw new ConfigBuilderException('Missing name on schema');
			}
			
			$name = $node->getAttribute('name');
			$schema = new Schema($name);
			
			if($node->hasAttribute('extend')) {
				$extend = $node->getAttribute('extend');
				if(!$config->hasSchema($extend)) {
					throw new ConfigBuilderException('Undefined base schema '.$extend);
				}
				
				$this->extendSchema($schema, $config->getSchema($extend));
			}

			$fieldNodeList = $xpath->query('field', $node);
			foreach($fieldNodeList as $fieldNode) {
				
				$field = $fieldNode->firstChild->nodeValue;
				
				if(!strlen($field)) {
					continue;
				}
				
				$primary = (($fieldNode->hasAttribute('primary')) && ($fieldNode->getAttribute('primary') == 'true'));
				$auto = (($fieldNode->hasAttribute('auto')) && ($fieldNode->getAttribute('auto') == 'true'));
				$embed = ($fieldNode->hasAttribute('embed')) ? trim($fieldNode->getAttribute('embed')) : false;
				$collection = (($fieldNode->hasAttribute('collection')) && ($fieldNode->getAttribute('collection') == 'true'));
				
				if($primary) {
					$schema->addPrimaryKeyField($field, $auto);
				} else if($embed) {					
					if(!$config->hasSchema($embed)) {
						// Embedded schemas should be defined first. This forces the avoidance of cyclic embedding.
						throw new ConfigBuilderException('Undefined embedded schema '.$embed);
					}

					$schema->addEmbeddedField($field, $embed, $collection);
				} else {
					$schema->addField($field);
				}
			}
			
			$config->addSchema($schema);
		}
	}
	
	private function extendSchema($schema, $base)
	{
		$fields = $base->getFieldNames();
		
		foreach($fields as $field) {
			if($base->isPrimaryKey($field)) {
				$auto = (($base->hasAutoPrimaryKey()) && ($base->getAutoPrimaryKeyFieldName() == $field));
				$schema->addPrimaryKeyField($field, $auto);
			} else if($base->isEmbedded($field)) {
				$embed = $base->getEmbeddedSchemaName($field);
				$isCollection = $base->isEmbeddedCollection($field);
				$schema->addEmbeddedField($field, $embed, $isCollection);
			} else {
				$default = $base->getDefaultValue($field);
				$schema->addField($field, $default);
			}
		}
	}
}