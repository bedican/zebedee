<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\db\config;

use framework\config\Cachable;
use framework\config\Mergeable;
use framework\db\schema\Schema;
use framework\exception\IllegalArgumentException;

class SchemaConfig implements Cachable, Mergeable
{
	private $schemas;
	
	public function __construct()
	{
		$this->schemas = array();
	}
	
	public function addSchema($schema)
	{
		if(! $schema instanceof Schema) {
			throw new IllegalArgumentException('$schema is not of type Schema');
		}
		
		$this->schemas[$schema->getName()] = $schema;
	}
	
	public function hasSchema($name)
	{
		if(!is_string($name)) {
			throw new IllegalArgumentException('$name is not of type string');
		}

		return array_key_exists($name, $this->schemas);
	}
	
	public function getSchema($name)
	{
		if(!$this->hasSchema($name)) {
			throw new IllegalArgumentException('Unknown schema '.$name);
		}
		
		return $this->schemas[$name];
	}
	
	public function getSchemaNames()
	{
		return array_keys($this->schemas);
	}
	
	public function merge($config, $overwrite = false)
	{
		if(! $config instanceof SchemaConfig) {
			throw new IllegalArgumentException('$config is not of type SchemaConfig');
		}
		
		// Merge schemas
		$schemaNames = $config->getSchemaNames();
		foreach($schemaNames as $name) {
			if((!array_key_exists($name, $this->schemas)) || ($overwrite)) {
				$schema = $config->getSchema($name);
				$this->schemas[$name] = $schema;
			}
		}
	}
}