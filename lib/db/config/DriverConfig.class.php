<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\config;

use framework\exception\IllegalArgumentException;

class DriverConfig
{
	private $name;
	private $driverClass;
	private $params;
	
	public function __construct($config)
	{
		if(!is_array($config)) {
			throw new IllegalArgumentException('$config is not an array');
		}
		
		$requiredFields = array('name', 'driver-class');
		if(sizeof($missingFields = array_keys(array_diff_key(array_flip($requiredFields), $config)))) {
			throw new IllegalArgumentException('Missing fields: '.implode(',', $missingFields));
		}

		$optionalFields = array('params');
		if(sizeof($unknownFields = array_keys(array_diff_key($config, array_flip(array_merge($requiredFields, $optionalFields)))))) {
			throw new IllegalArgumentException('Unknown fields: '.implode(',', $unknownFields));
		}
		
		// Validate the driver classe is instance of Driver
		if(!in_array('framework\\db\\driver\\Driver', @class_implements($config['driver-class']))) {
			throw new IllegalArgumentException('driver-class does not implement framework.db.driver.Driver');
		}	
		
		$this->name = $config['name'];
		$this->driverClass = $config['driver-class'];
		$this->params = ((array_key_exists('params', $config)) && (is_array($config['params']))) ? $config['params'] : array();
	}
	
	public function getName()
	{
		return $this->name;
	}
	public function getDriverClass()
	{
		return $this->driverClass;
	}
	public function getParameterNames()
	{
		return array_keys($this->params);
	}
	public function hasParameter($name)
	{
		return array_key_exists($name, $this->params);
	}
	public function getParameter($name, $default = null)
	{
		if(!$this->hasParameter($name)) {
			return $default;
		}
		
		return $this->params[$name];
	}
}