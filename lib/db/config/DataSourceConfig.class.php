<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\config;

use framework\exception\IllegalArgumentException;

class DataSourceConfig
{
	private $name;
	private $dsn;
	
	public function __construct($config)
	{
		if(!is_array($config)) {
			throw new IllegalArgumentException('$config is not an array');
		}
		
		$requiredFields = array('name', 'dsn');
		if(sizeof($missingFields = array_keys(array_diff_key(array_flip($requiredFields), $config)))) {
			throw new IllegalArgumentException('Missing fields: '.implode(',', $missingFields));
		}
		if(sizeof($unknownFields = array_keys(array_diff_key($config, array_flip($requiredFields))))) {
			throw new IllegalArgumentException('Unknown fields: '.implode(',', $unknownFields));
		}
		
		$this->name = $config['name'];
		$this->dsn = $config['dsn'];
	}
	
	public function getName()
	{
		return $this->name;
	}
	public function getDsn()
	{
		return $this->dsn;
	}
}