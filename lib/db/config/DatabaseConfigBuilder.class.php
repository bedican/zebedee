<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\config;

use framework\config\AbstractConfigBuilder;

class DatabaseConfigBuilder extends AbstractConfigBuilder
{
	protected function doBuild($xpath)
	{
		$config = new DatabaseConfig();

		$this->parseDrivers($config, $xpath);
		$this->parseDataSources($config, $xpath);
		$this->parseDao($config, $xpath);
		$this->parseDaoDataSources($config, $xpath);

		return $config;
	}
	
	private function parseDrivers($config, $xpath)
	{
		$nodeList = $xpath->query('/config/drivers/driver');
		foreach($nodeList as $node) {
			$config->addDriverConfig(new DriverConfig($this->getConfigFromNode($node)));
		}
	}
	
	private function parseDataSources($config, $xpath)
	{
		$nodeList = $xpath->query('/config/data-sources/data-source');
		foreach($nodeList as $node) {
			$config->addDataSourceConfig(new DataSourceConfig($this->getConfigFromNode($node)));
		}
	}
	
	private function parseDao($config, $xpath)
	{
		$nodeList = $xpath->query('/config/daos/dao');
		foreach($nodeList as $node) {
			$config->addDaoConfig(new DaoConfig($this->getConfigFromNode($node)));
		}
	}
	
	private function parseDaoDataSources($config, $xpath)
	{
		$nodeList = $xpath->query('/config/dao-data-sources/dao-data-source');
		foreach($nodeList as $node) {
			$nodeConfig = $this->getConfigFromNode($node);
			if((array_key_exists('dao', $nodeConfig)) && (array_key_exists('data-source', $nodeConfig))) {
				$config->addDaoDataSource($nodeConfig['dao'], $nodeConfig['data-source']);
			}
		}
	}
}