<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\config;

use framework\config\MapConfig;
use framework\exception\IllegalArgumentException;

class DaoConfig
{
	private $name;
	private $collection;
	private $daoClass;
	private $mapperClass;
	private $mapperConfig;
	private $dataSource;
	
	public function __construct($config)
	{
		if(!is_array($config)) {
			throw new IllegalArgumentException('$config is not an array');
		}

		$requiredFields = array('name', 'collection');
		if(sizeof($missingFields = array_keys(array_diff_key(array_flip($requiredFields), $config)))) {
			throw new IllegalArgumentException('Missing fields: '.implode(',', $missingFields));
		}
		$optionalFields = array('dao-class', 'mapper-class', 'mapper-config', 'data-source');
		if(sizeof($unknownFields = array_keys(array_diff_key($config, array_flip(array_merge($requiredFields, $optionalFields)))))) {
			throw new IllegalArgumentException('Unknown fields: '.implode(',', $unknownFields));
		}
		
		$this->name = $config['name'];
		$this->collection = $config['collection'];
		
		if(array_key_exists('dao-class', $config)) {
			if(!in_array('framework\\db\\dao\\Dao', @class_implements($config['dao-class']))) {
				throw new IllegalArgumentException('dao-class does not implement framework.db.dao.Dao');
			}
			$this->daoClass = $config['dao-class'];
		} else {
			$this->daoClass = '\\framework\\db\\dao\\DefaultDao';
		}

		if(array_key_exists('mapper-class', $config)) {
			if(!in_array('framework\\db\\mapper\\Mapper', @class_implements($config['mapper-class']))) {
				throw new IllegalArgumentException('mapper-class does not implement framework.db.mapper.Mapper');
			}
			$this->mapperClass = $config['mapper-class'];
		} else {
			$this->mapperClass = '\\framework\\db\\mapper\\DefaultMapper';
		}

		// TODO: Could create DefaultMapperConfig when a DefaultMapper is used for convinience. 
		// However, this seems a little overkill right now.
		if(array_key_exists('mapper-config', $config)) {
			$this->mapperConfig = new MapConfig($config['mapper-config']);
		} else {
			$this->mapperConfig = new MapConfig();
		}

		$this->dataSource = (array_key_exists('data-source', $config)) ? $config['data-source'] : false;
	}
	
	public function getName()
	{
		return $this->name;
	}
	public function getCollection()
	{
		return $this->collection;
	}
	public function getDaoClass()
	{
		return $this->daoClass;
	}
	public function getMapperClass()
	{
		return $this->mapperClass;
	}
	public function getMapperConfig()
	{
		return $this->mapperConfig;
	}
	public function getDataSource()
	{
		return $this->dataSource;
	}
}