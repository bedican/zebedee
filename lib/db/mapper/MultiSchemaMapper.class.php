<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\mapper;

use framework\exception\IllegalArgumentException;

class MultiSchemaMapper extends AbstractSchemaMapper
{	
	private $typeField;
	private $firstSchema = null;

	private $schemaNames = array();
	private $modelClassNames = array();
	private $mappableModelClassNames = array();
	
	protected function init()
	{
		parent::init();
		
		$config = $this->getConfig();

		$this->typeField = $config->getValue('type-field', 'type');
		$types = $config->getValue('types', array());
		
		if(!sizeof($types)) {
			throw new MapperException('Missing type list within mapper-config');
		}
		
		foreach($types as $pattern => $schemas) {
			if(!is_array($schemas)) {
				throw new MapperException('Missing schema for type "'.$pattern.'" within mapper-config');
			}
			
			$embedded = array_key_exists('embedded', $schemas) ? $schemas['embedded'] : array();
			$schemas = array_diff_key($schemas, array_flip(array('embedded')));
			
			if(!sizeof($schemas)) {
				throw new MapperException('No schema defined for type "'.$pattern.'" within mapper-config');
			}
			
			// Take the first defined schema, if there is more than one.
			$schemaName = array_shift(array_keys($schemas));
			$modelClassName = array_shift(array_values($schemas));
			$modelClassName = ltrim($modelClassName, '\\');
			
			// Capture the first schema defined, so all futher (non-embedded) schemas can be validated against
			if($this->firstSchema == null) {
				$this->firstSchema = $this->getSchema($schemaName);
			}

			// Turn the name pattern in to a regex
			$regex = "\\Q" . $pattern . "\\E";
			$regex = str_replace('*', '\\E.+?\\Q', $regex);
			$regex = str_replace('\\Q\\E', '', $regex);
			
			// Capture all the models that this mapper can map
			$this->mappableModelClassNames[$regex] = $modelClassName;
			
			// Capture the top level schemas, and the mapping for all schemas to models
			$this->schemaNames[$regex] = $schemaName;
			$this->modelClassNames[$regex] = array(
				$schemaName => $modelClassName
			);
			
			foreach($embedded as $schemaName => $modelClassName) {
				if(array_key_exists($schemaName, $this->modelClassNames[$regex])) {
					throw new MapperException('Duplicate schema "'.$schemaName.'" for type "'.$pattern.'" found within mapper-config');
				}

				$this->modelClassNames[$regex][$schemaName] = ltrim($modelClassName, '\\');
			}

			$schemaNames = $this->getAllSchemaNames($schemaName);
			if(sizeof($missingSchemaNames = array_diff($schemaNames, array_keys($this->modelClassNames[$regex])))) {
				throw new MapperException('Missing model class for type "'.$pattern.'" for the following nested schemas '.implode(',', $missingSchemaNames));
			}
		}
	}
	
	private function usToCc($field)
	{
		// Underscore fields in the schema should translate to camel case method names
		return implode('', array_map('ucfirst', explode('_', $field)));
	}
	
	protected function canMap($model)
	{
		return (in_array(get_class($model), array_values($this->mappableModelClassNames)));
	}
	
	private function isSchemaValid($schema)
	{
		if($schema->getAutoPrimaryKeyFieldName() != $this->firstSchema->getAutoPrimaryKeyFieldName()) {
			return false;
		}
		if(sizeof(array_diff($schema->getPrimaryKeyFieldNames(), $this->firstSchema->getPrimaryKeyFieldNames()))) {
			return false;
		}
		if(sizeof(array_diff($this->firstSchema->getPrimaryKeyFieldNames(), $schema->getPrimaryKeyFieldNames()))) {
			return false;
		}

		return true;
	}
	
	private function getMatchingKey($type, $array)
	{
		$patterns = array_keys($array);
		foreach($patterns as $pattern) {
			if(preg_match('#'.$pattern.'#', $type)) {
				return $pattern;
			}
		}
		
		return null;
	}
	
	private function getSchemaByModel($model)
	{
		$modelClassName = get_class($model);
		
		$key = array_search($modelClassName, $this->mappableModelClassNames);
		if($key === false) {
			throw new MapperException('Can not find schema for model '.$modelClassName);
		}
		if(!array_key_exists($key, $this->schemaNames)) {
			throw new MapperException('Can not find schema for model '.$modelClassName);
		}
		
		$schemaName = $this->schemaNames[$key];
		$schema = $this->getSchema($schemaName);
		
		if(!$this->isSchemaValid($schema)) {
			throw new MapperException('Invalid schema for model '.$modelClassName);
		}
		
		return $schema;
	}
	
	protected function arrayToModel($arr)
	{
		if(!array_key_exists($this->typeField, $arr)) {
			throw new MapperException('Can not find model, missing field: '.$this->typeField);
		}
		
		$type = $arr[$this->typeField];
		
		$typeKey = $this->getMatchingKey($type, $this->schemaNames);
		if($typeKey == null) {
			throw new MapperException('Can not find schema, unknown type: '.$type);
		}
		
		if(!array_key_exists($typeKey, $this->schemaNames)) {
			throw new MapperException('Can not find model, unknown schema '.$schema->getName().' for type '.$type);
		}
		
		$schemaName = $this->schemaNames[$typeKey];
		$schema = $this->getSchema($schemaName);
		
		if(!$this->isSchemaValid($schema)) {
			throw new MapperException('Invalid schema for type '.$type);
		}

		return $this->doArrayToModel($arr, $typeKey, $schema);
	}
	
	private function doArrayToModel($arr, $typeKey, $schema)
	{
		if(!array_key_exists($schema->getName(), $this->modelClassNames[$typeKey])) {
			throw new MapperException('No model defined for schema '.$schema->getName());
		}

		$modelClassName = $this->modelClassNames[$typeKey][$schema->getName()];
		$model = new $modelClassName();
		
		if(!is_array($arr)) {
			return $model;
		}
	
		foreach($arr as $field => $value) {
			if(!$schema->hasField($field)) {
				continue;
			}
			
			if($schema->isEmbedded($field)) {

				$embeddedSchema = $this->getSchema($schema->getEmbeddedSchemaName($field));
				
				if($schema->isEmbeddedCollection($field)) {
					$collection = $value; $value = array();
					foreach($collection as $item) {
						$value[] = $this->doArrayToModel($item, $typeKey, $embeddedSchema);
					}
				} else {
					$value = $this->doArrayToModel($value, $typeKey, $embeddedSchema);
				}
			}

			$setter = 'set'.ucfirst($this->usToCc($field));
			if(!method_exists($model, $setter)) {
				continue;
			}

			$model->$setter($value);
		}
		
		return $model;
	}
	
	protected function modelToArray($model)
	{
		return $this->doModelToArray($model, $this->getSchemaByModel($model));
	}

	private function doModelToArray($model, $schema)
	{
		$arr = array();
		
		// Check is performed for recursion
		if(!is_object($model)) {
			return $arr;
		}

		$fields = $schema->getFieldNames();

		if($schema->hasAutoPrimaryKey()) {
			$fields = array_diff($fields, array($schema->getAutoPrimaryKeyFieldName()));
		}
		
		foreach($fields as $field) {
			$getter = 'get'.ucfirst($this->usToCc($field));
			if(!method_exists($model, $getter)) {
				continue;
			}
			
			$value = $model->$getter();
			
			if($schema->isEmbedded($field)) {
				
				$embeddedSchema = $this->getSchema($schema->getEmbeddedSchemaName($field));
				
				if($schema->isEmbeddedCollection($field)) {
					$collection = $value; $value = array();
					foreach($collection as $item) {
						$value[] = $this->doModelToArray($item, $embeddedSchema);
					}
				} else {
					$value = $this->doModelToArray($value, $embeddedSchema);
				}
			} else if((!is_array($value)) && (!is_scalar($value))) {
				continue;
			}

			$arr[$field] = $value;
		}
		
		return $arr;
	}
	
	protected function hasAutoField()
	{
		return $this->firstSchema->hasAutoPrimaryKey();
	}

	protected function setAutoField($model, $value)
	{
		$fieldName = $this->getSchemaByModel($model)->getAutoPrimaryKeyFieldName();
		
		$setter = 'set'.ucfirst($this->usToCc($fieldName));
		if(!method_exists($model, $setter)) {
			return;
		}
		
		$model->$setter($value);
	}
	
	protected function getPkValues($model)
	{
		$pkFields = $this->getSchemaByModel($model)->getPrimaryKeyFieldNames();
		
		$values = array();
		foreach($pkFields as $pkField) {
			$getter = 'get'.ucfirst($this->usToCc($pkField));
			if(!method_exists($model, $getter)) {
				continue;
			}
			
			$values[] = $model->$getter();
		}
		
		return $values;
	}
	
	protected function getPkFieldNames()
	{
		return $this->firstSchema->getPrimaryKeyFieldNames();
	}
}