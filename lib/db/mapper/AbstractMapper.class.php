<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\mapper;

use framework\application\Application;
use framework\config\MapConfig;
use framework\exception\RuntimeException;
use framework\exception\IllegalArgumentException;

abstract class AbstractMapper implements Mapper
{
	private $application;
	private $config;
	
	public final function __construct($application, $config)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(! $config instanceof MapConfig) {
			throw new IllegalArgumentException('$config is not of type MapConfig');
		}
		
		$this->application = $application;
		$this->config = $config;

		$this->init();
	}
	
	protected function init() {}
	
	abstract protected function canMap($model);
	
	abstract protected function arrayToModel($arr);
	abstract protected function modelToArray($model);
	
	abstract protected function hasAutoField();
	abstract protected function setAutoField($model, $value);
	
	abstract protected function getPkValues($model);
	abstract protected function getPkFieldNames();

	protected function getApplication()
	{
		return $this->application;
	}
	protected function getConfig()
	{
		return $this->config;
	}
	
	public function toModel($arr)
	{
		if(!is_array($arr)) {
			throw new IllegalArgumentException('$arr is not an array');
		}
		
		return $this->arrayToModel($arr);
	}
	
	public function toArray($model)
	{
		if(!$this->canMap($model)) {
			throw new IllegalArgumentException('Can not map $model, incorrect type '.get_class($model));
		}

		return $this->modelToArray($model);
	}
	
	public function setAutoPrimaryKey($model, $value)
	{
		if(!$this->hasAutoPrimaryKey()) {
			throw new RuntimeException('Model does not support auto generated primary key');
		}
		if(!$this->canMap($model)) {
			throw new IllegalArgumentException('Can not map $model, incorrect type '.get_class($model));
		}
		
		$this->setAutoField($model, $value);
	}
	
	public function getPrimaryKeyValues($model)
	{
		if(!$this->canMap($model)) {
			throw new IllegalArgumentException('Can not map $model, incorrect type '.get_class($model));
		}
		
		return $this->getPkValues($model);
	}
	
	public function getPrimaryKeyFieldNames()
	{
		return $this->getPkFieldNames();
	}
	
	public function hasAutoPrimaryKey()
	{
		return $this->hasAutoField();
	}
	
	public function isMappable($model)
	{
		return $this->canMap($model);
	}
}