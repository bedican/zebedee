<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\mapper;

use framework\db\model\DefaultModel;

class DefaultMapper extends AbstractSchemaMapper
{
	private $schema = null;
	
	protected function init()
	{
		parent::init();
		
		$config = $this->getConfig();
		$schemas = $config->getValue('schema');
		
		if(!$schema) {
			throw new MapperException('Missing schema config within mapper-config');
		}
		if(!is_string($schema)) {
			throw new MapperException('schema config within mapper-config is not of type string');
		}
		
		$this->schema = $this->getSchema($schema);
	}
	
	protected function canMap($model)
	{
		return ($model instanceof DefaultModel);
	}
	
	protected function arrayToModel($arr)
	{
		return $this->doArrayToModel($arr, $this->schema);
	}
	
	private function doArrayToModel($arr, $schema)
	{
		$model = new DefaultModel($schema);
		
		foreach($arr as $field => $value) {
			if(!$schema->hasField($field)) {
				continue;
			}
			
			if($schema->isEmbedded($field)) {

				$embeddedSchema = $this->getSchema($schema->getEmbeddedSchemaName($field));
				
				if($schema->isEmbeddedCollection($field)) {
					$collection = $value; $value = array();
					foreach($collection as $item) {
						$value[] = $this->doArrayToModel($item, $embeddedSchema);
					}
				} else {
					$value = $this->doArrayToModel($value, $embeddedSchema);
				}
			}
			
			$model->set($field, $value);
		}
		
		return $model;
	}
	
	protected function modelToArray($model)
	{
		return $this->doModelToArray($model, $this->schema);
	}
	
	protected function doModelToArray($model, $schema)
	{
		$arr = array();
		
		// Check is performed for recursion
		if(! $model instanceof DefaultModel) {
			return $arr;
		}

		$fields = $schema->getFieldNames();

		if($schema->hasAutoPrimaryKey()) {
			$fields = array_diff($fields, array($schema->getAutoPrimaryKeyFieldName()));
		}
		
		foreach($fields as $field) {
			
			$value = $model->get($field);
			
			if($schema->isEmbedded($field)) {
				
				$embeddedSchema = $this->getSchema($schema->getEmbeddedSchemaName($field));
				
				if($schema->isEmbeddedCollection($field)) {
					$collection = $value; $value = array();
					foreach($collection as $item) {
						$value[] = $this->doModelToArray($item, $embeddedSchema);
					}
				} else {
					$value = $this->doModelToArray($value, $embeddedSchema);
				}
			} else if((!is_array($value)) && (!is_scalar($value))) {
				continue;
			}

			$arr[$field] = $value;
		}
		
		return $arr;
	}

	protected function hasAutoField()
	{
		return $this->schema->hasAutoPrimaryKey();
	}
	
	protected function setAutoField($model, $value)
	{
		$model->set($this->schema->getAutoPrimaryKeyFieldName(), $value);
	}
	
	protected function getPkValues($model)
	{
		$pkFields = $this->schema->getPrimaryKeyFieldNames();
		
		$values = array();
		foreach($pkFields as $pkField) {
			$values[] = $model->get($pkField);
		}
		
		return $values;
	}
	
	protected function getPkFieldNames()
	{
		return $this->schema->getPrimaryKeyFieldNames();
	}
}