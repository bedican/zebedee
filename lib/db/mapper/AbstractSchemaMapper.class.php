<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\db\mapper;

use framework\db\config\SchemaConfig;
use framework\db\config\SchemaConfigBuilder;
use framework\cache\ConfigCache;

abstract class AbstractSchemaMapper extends AbstractMapper
{
	private $config;

	protected function init()
	{
		$this->initSchemaConfig();
	}
	
	private function initSchemaConfig()
	{
		$application = $this->getApplication();
		
		$cache = new ConfigCache($application);
		$this->config = $cache->get('schema.xml');
		
		if($this->config instanceof SchemaConfig) {
			return;
		}
		
		$builder = new SchemaConfigBuilder();
		
		// Framework schema.xml should always be present
		$this->config = $builder->build(dirname(__FILE__).'/../../../config/schema.xml');
		
		// Merge with each enabled plugin schema.xml if present
		$plugins = $application->getPlugins();
		foreach($plugins as $plugin) {
			$filename = $plugin->getPluginDir().'/config/schema.xml';
			if(is_file($filename)) {
				$this->config->merge($builder->build($filename), true);
			}
		}
		
		// Override with application shema.xml if present
		$filename = $application->getApplicationDir().'/config/schema.xml';
		if(is_file($filename)) {
			$this->config->merge($builder->build($filename), true);
		}
		
		// Merge environment configuration
		if(is_string($environment = $application->getEnvironment())) {
			if(is_file($filename = $application->getApplicationDir().'/config/schema.'.$environment.'.xml')) {
				$this->config->merge($builder->build($filename), true);
			}
		}
		
		$cache->put('schema.xml', $this->config);
	}
	
	protected function hasSchema($name)
	{
		return $this->config->hasSchema($name);
	}

	protected function getSchema($name)
	{
		return $this->config->getSchema($name);
	}

	protected function getAllSchemaNames($name)
	{
		$schemaNames = $this->config->getSchema($name)->getEmbeddedSchemaNames();
		
		$names = array($name);
		foreach($schemaNames as $schemaName) {
			$names = array_merge($names, $this->getAllSchemaNames($schemaName));
		}
		
		return $names;
	}
}