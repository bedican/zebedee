<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\mapper;

interface Mapper
{
	public function isMappable($model);

	public function toModel($arr);
	public function toArray($model);

	public function hasAutoPrimaryKey();
	public function setAutoPrimaryKey($model, $value);

	public function getPrimaryKeyFieldNames();
	public function getPrimaryKeyValues($model);
}