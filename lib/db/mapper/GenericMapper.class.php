<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\mapper;

use framework\exception\IllegalArgumentException;

class GenericMapper extends AbstractSchemaMapper
{
	private $schema = null;
	private $modelClassName = null;
	private $modelClassNames = array();
	
	protected function init()
	{
		parent::init();
		
		$config = $this->getConfig();
		$schemas = $config->getValue('schemas');
		
		$embedded = array_key_exists('embedded', $schemas) ? $schemas['embedded'] : array();
		$schemas = array_diff_key($schemas, array_flip(array('embedded')));
		
		if(!sizeof($schemas)) {
			throw new MapperException('No schemas defined within mapper-config');
		}

		// Take the first defined schema, if there is more than one.
		$schemaKeys = array_keys($schemas);
		$schemaValues = array_values($schemas);
		$schemaName = array_shift($schemaKeys);
		$modelClassName = array_shift($schemaValues);
		
		$this->schema = $this->getSchema($schemaName);
		$this->modelClassName = ltrim($modelClassName, '\\');
		
		$this->modelClassNames[$schemaName] = $this->modelClassName;
		foreach($embedded as $schemaName => $modelClassName) {
			if(array_key_exists($schemaName, $this->modelClassNames)) {
				throw new MapperException('Duplicate schema "'.$schemaName.'" found within mapper-config');
			}

			$this->modelClassNames[$schemaName] = ltrim($modelClassName, '\\');
		}

		$schemaNames = $this->getAllSchemaNames($this->schema->getName());
		if(sizeof($missingSchemaNames = array_diff($schemaNames, array_keys($this->modelClassNames)))) {
			throw new MapperException('Missing model class for the following nested schemas '.implode(',', $missingSchemaNames));
		}
	}
	
	private function usToCc($field)
	{
		// Underscore fields in the schema should translate to camel case method names
		return implode('', array_map('ucfirst', explode('_', $field)));
	}
	
	protected function canMap($model)
	{
		return is_a($model, $this->modelClassName);
	}
	
	protected function arrayToModel($arr)
	{
		return $this->doArrayToModel($arr, $this->schema);
	}
	
	private function doArrayToModel($arr, $schema)
	{
		if(!array_key_exists($schema->getName(), $this->modelClassNames)) {
			throw new MapperException('No model defined for schema '.$schema->getName());
		}
		
		$className = $this->modelClassNames[$schema->getName()];
		$model = new $className();
		
		if(!is_array($arr)) {
			return $model;
		}
	
		foreach($arr as $field => $value) {
			if(!$schema->hasField($field)) {
				continue;
			}
			
			if($schema->isEmbedded($field)) {

				$embeddedSchema = $this->getSchema($schema->getEmbeddedSchemaName($field));
				
				if($schema->isEmbeddedCollection($field)) {
					$collection = $value; $value = array();
					foreach($collection as $item) {
						$value[] = $this->doArrayToModel($item, $embeddedSchema);
					}
				} else {
					$value = $this->doArrayToModel($value, $embeddedSchema);
				}
			}

			$setter = 'set'.ucfirst($this->usToCc($field));
			if(!method_exists($model, $setter)) {
				continue;
			}

			$model->$setter($value);
		}
		
		return $model;
	}
	
	protected function modelToArray($model)
	{
		return $this->doModelToArray($model, $this->schema);
	}
	
	private function doModelToArray($model, $schema)
	{
		$arr = array();
		
		// Check is performed for recursion
		if(!is_object($model)) {
			return $arr;
		}

		$fields = $schema->getFieldNames();

		if($schema->hasAutoPrimaryKey()) {
			$fields = array_diff($fields, array($schema->getAutoPrimaryKeyFieldName()));
		}
		
		foreach($fields as $field) {
			$getter = 'get'.ucfirst($this->usToCc($field));
			if(!method_exists($model, $getter)) {
				continue;
			}
			
			$value = $model->$getter();
			
			if($schema->isEmbedded($field)) {
				
				$embeddedSchema = $this->getSchema($schema->getEmbeddedSchemaName($field));

				if($schema->isEmbeddedCollection($field)) {
					$collection = $value; $value = array();
					foreach($collection as $item) {
						$value[] = $this->doModelToArray($item, $embeddedSchema);
					}
				} else {
					$value = $this->doModelToArray($value, $embeddedSchema);
				}
			} else if((!is_array($value)) && (!is_scalar($value))) {
				continue;
			}

			$arr[$field] = $value;
		}

		return $arr;
	}
	
	protected function hasAutoField()
	{
		return $this->schema->hasAutoPrimaryKey();
	}

	protected function setAutoField($model, $value)
	{
		$fieldName = $this->schema->getAutoPrimaryKeyFieldName();
		
		$setter = 'set'.ucfirst($this->usToCc($fieldName));
		if(!method_exists($model, $setter)) {
			return;
		}
		
		$model->$setter($value);
	}
	
	protected function getPkValues($model)
	{
		$pkFields = $this->schema->getPrimaryKeyFieldNames();
		
		$values = array();
		foreach($pkFields as $pkField) {
			$getter = 'get'.ucfirst($this->usToCc($pkField));
			if(!method_exists($model, $getter)) {
				continue;
			}
			
			$values[] = $model->$getter();
		}
		
		return $values;
	}
	
	protected function getPkFieldNames()
	{
		return $this->schema->getPrimaryKeyFieldNames();
	}
}