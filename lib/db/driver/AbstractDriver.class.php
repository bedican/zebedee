<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\driver;

use framework\db\config\DriverConfig;
use framework\db\criteria\DeleteCriteria;
use framework\db\criteria\UpdateCriteria;
use framework\db\criteria\SelectCriteria;
use framework\exception\IllegalArgumentException;

abstract class AbstractDriver implements Driver
{
	private $dsn;
	private $driverConfig;

	public final function __construct($dsn, $driverConfig)
	{
		if(! $dsn instanceof Dsn) {
			throw new IllegalArgumentException('$dsn is not of type Dsn');
		}
		if(! $driverConfig instanceof DriverConfig) {
			throw new IllegalArgumentException('$driverConfig is not of type DriverConfig');
		}
		
		$this->dsn = $dsn;
		$this->driverConfig = $driverConfig;

		$this->init();
	}
	
	public final function __destruct()
	{
		$this->shutdown();
	}
	
	public function newSelectCriteria()
	{
		return new SelectCriteria();
	}
	public function newUpdateCriteria()
	{
		return new UpdateCriteria();
	}
	public function newDeleteCriteria()
	{
		return new DeleteCriteria();
	}

	// Default empty implementations
	protected function init() {}
	protected function shutdown() {}

	protected function getDsn()
	{
		return $this->dsn;
	}
	protected function getDriverConfig()
	{
		return $this->driverConfig;
	}
}