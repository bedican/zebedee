<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\driver;

interface Driver
{	
	public function newSelectCriteria();
	public function newUpdateCriteria();
	public function newDeleteCriteria();

	public function retrieve($criteria);
	public function count($criteria);

	public function insert($collection, $document);
	public function update($document, $criteria);
	public function upsert($document, $criteria);

	public function delete($criteria);
}