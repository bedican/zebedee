<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\driver;

use framework\exception\IllegalArgumentException;

class Dsn
{
	private $dsn;

	private $driver;
	private $username;
	private $password;
	private $host;
	private $port;
	private $database;

	public function __construct($dsn)
	{
		// <driver>://<username>:<password>@<host>:<port>/<database>

//		if(!preg_match('#^(?P<driver>[a-z0-9_-]+)(?:\://(?:(?P<username>[a-z0-9_-]+)(?:\:(?P<password>[a-z0-9_-]+))?@)?(?P<host>[a-z0-9_-]+)(?:\:(?P<port>[0-9]+))?)?/(?P<database>[a-z0-9_-]+)$#i', $dsn, $matches)) {
//			throw new IllegalArgumentException('Invalid DSN');
//		}

		if(!preg_match('#^(?P<driver>[a-z0-9_-]+)(?:\://(?:(?P<username>[a-z0-9_-]+)(?:\:(?P<password>[a-z0-9_-]+))?@)?(?P<host>([a-z0-9_-]+)(\:[0-9]+)?(,([a-z0-9_-]+)(\:[0-9]+)?)*))?/(?P<database>[a-z0-9_-]+)$#i', $dsn, $matches)) {
			throw new IllegalArgumentException('Invalid DSN');
		}

		$this->dsn = $dsn;

		$this->driver = $matches['driver'];
		$this->username = $matches['username'];
		$this->password = $matches['password'];
		$this->host = $matches['host'];
		$this->database = $matches['database'];
	}

	public function getDriver()
	{
		return $this->driver;
	}
	public function getUsername()
	{
		return $this->username;
	}
	public function getPassword()
	{
		return $this->password;
	}
	public function getHost()
	{
		return $this->host;
	}
	public function getDatabase()
	{
		return $this->database;
	}

	public function __toString()
	{
		return $this->toString();
	}
	public function toString()
	{
		return $this->dsn;
	}
}