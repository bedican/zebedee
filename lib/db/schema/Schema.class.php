<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\schema;

use framework\exception\IllegalArgumentException;

class Schema
{
	private $name;
	private $fields;
	private $autoPrimaryKey;

	public final function __construct($name)
	{
		$this->name = $name;
		$this->fields = array();
		$this->autoPrimaryKey = null;
		$this->init();
	}
	
	// Default empty implementation
	protected function init() {}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function addField($name, $default = null)
	{
		if(!preg_match('#^[0-9a-z_-]+$#i', $name)) {
			throw new IllegalArgumentException('Invalid field name');
		}

		$this->fields[$name] = array(
			'default' => $default
		);
	}
	
	public function addPrimaryKeyField($name, $auto = false)
	{
		if(!preg_match('#^[0-9a-z_-]+$#i', $name)) {
			throw new IllegalArgumentException('Invalid field name');
		}
		if(!is_bool($auto)) {
			throw new IllegalArgumentException('$auto is not of type bool');
		}
		
		$this->fields[$name] = array(
			'primary' => true
		);
		
		if($auto) {
			$this->autoPrimaryKey = $name;
		}
	}
	
	public function addEmbeddedField($name, $schemaName, $isCollection = false)
	{
		if(!preg_match('#^[0-9a-z_-]+$#i', $name)) {
			throw new IllegalArgumentException('Invalid field name');
		}
		if(!preg_match('#^[0-9a-z_-]+$#i', $schemaName)) {
			throw new IllegalArgumentException('Invalid schema name');
		}
		if(!is_bool($isCollection)) {
			throw new IllegalArgumentException('$isCollection is not of type bool');
		}
		
		$this->fields[$name] = array(
			'schema' => $schemaName,
			'is-collection' => $isCollection
		);
	}
	
	public function getFieldNames()
	{
		return array_keys($this->fields);
	}
	
	public function getPrimaryKeyFieldNames()
	{
		$keys = array();
		foreach($this->fields as $name => $field) {
			if((array_key_exists('primary', $field)) && ($field['primary'])) {
				$keys[] = $name;
			}
		}

		return $keys;
	}
	
	public function hasAutoPrimaryKey()
	{
		return ($this->autoPrimaryKey != null);
	}
	
	public function getAutoPrimaryKeyFieldName()
	{
		return $this->autoPrimaryKey;
	}
	
	public function hasField($field)
	{
		return in_array($field, $this->getFieldNames());
	}
	
	public function isPrimaryKey($field)
	{
		if(!$this->hasField($field)) {
			throw new IllegalArgumentException('Invalid field');
		}
		if(!array_key_exists('primary', $this->fields[$field])) {
			return false;
		}
		
		return $this->fields[$field]['primary'];
	}
	
	public function isEmbedded($field)
	{
		if(!$this->hasField($field)) {
			throw new IllegalArgumentException('Invalid field');
		}

		return array_key_exists('schema', $this->fields[$field]);
	}
	
	public function getEmbeddedSchemaName($field)
	{
		if(!$this->isEmbedded($field)) {
			throw new IllegalArgumentException('Field is not embedded');
		}

		return $this->fields[$field]['schema'];
	}
	
	public function getEmbeddedSchemaNames()
	{
		$names = array();
		foreach($this->fields as $field => $config) {
			if(array_key_exists('schema', $config)) {
				$names[] = $config['schema'];
			}
		}
		
		return $names;
	}
	
	public function isEmbeddedCollection($field)
	{
		if(!$this->isEmbedded($field)) {
			throw new IllegalArgumentException('Field is not embedded');
		}

		return $this->fields[$field]['is-collection'];
	}
	
	public function getDefaultValue($field)
	{
		if(!$this->hasField($field)) {
			throw new IllegalArgumentException('Unknown field');
		}
		if(!array_key_exists('default', $this->fields[$field])) {
			return null;
		}
		
		return $this->fields[$field]['default'];
	}
}