<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\criteria;

use framework\exception\IllegalArgumentException;

class Criteria
{
	private $parent = null;
	private $criterion = null;

	private $currentAndCriterion = null;
	private $currentOrCriterion = null;

	private $currentAndCriteria = null;
	private $currentOrCriteria = null;

	private $andCriteria = array();
	private $orCriteria = array();

	private static $currentCriterionMethodMap = array(
		'equals' => Criterion::EQUALS,
		'notEquals' => Criterion::NOT_EQUALS,
		'in' => Criterion::IN,
		'notIn' => Criterion::NOT_IN,
		'greaterThan' => Criterion::GREATER_THAN,
		'lessThan' => Criterion::LESS_THAN,
		'greaterThanOrEquals' => Criterion::GREATER_EQUALS,
		'lessThanOrEquals' => Criterion::LESS_EQUALS,
		'like' => Criterion::LIKE,
		'notLike' => Criterion::NOT_LIKE,
		'regex' => Criterion::REGEX,
		'notRegex' => Criterion::NOT_REGEX
	);

	private static $newCriterionMethodMap = array(
		'orEquals' => Criterion::EQUALS,
		'andNotEquals' => Criterion::NOT_EQUALS,
		'andIn' => Criterion::IN,
		'orIn' => Criterion::IN,
		'andNotIn' => Criterion::NOT_IN,
		'orNotIn' => Criterion::NOT_IN,
		'andGreaterThan' => Criterion::GREATER_THAN,
		'orGreaterThan' => Criterion::GREATER_THAN,
		'andLessThan' => Criterion::LESS_THAN,
		'orLessThan' => Criterion::LESS_THAN,
		'andGreaterThanOrEquals' => Criterion::GREATER_EQUALS,
		'orGreaterThanOrEquals' => Criterion::GREATER_EQUALS,
		'andLessThanOrEquals' => Criterion::LESS_EQUALS,
		'orLessThanOrEquals' => Criterion::LESS_EQUALS,
		'andLike' => Criterion::LIKE,
		'orLike' => Criterion::LIKE,
		'andNotLike' => Criterion::NOT_LIKE,
		'orNotLike' => Criterion::NOT_LIKE,
		'andRegex' => Criterion::REGEX,
		'orRegex' => Criterion::REGEX,
		'andNotRegex' => Criterion::NOT_REGEX,
		'orNotRegex' => Criterion::NOT_REGEX
	);
	
	public function __construct($parent = null)
	{
		if(($parent != null) && (! $parent instanceof Criteria)) {
			throw new IllegalArgumentException('$parent is not of type Criteria');
		}

		$this->parent = $parent;
	}

	public function hasCriterion()
	{
		return ($this->criterion != null);
	}

	public function getCriterion()
	{
		if($this->criterion == null) {
			// This criteria may just consist of sub criteria.
			// We want to be able to call getCriterion() at any time,
			// so we should not merge the sub criteria criterion with $this->criterion

			$criterion = null;
			foreach($this->andCriteria as $criteria) {
				$criterion = ($criterion) ? $criterion->andCriterion($criteria->getCriterion()) : $criteria->getCriterion();
			}
			foreach($this->orCriteria as $criteria) {
				$criterion = ($criterion) ? $criterion->orCriterion($criteria->getCriterion()) : $criteria->getCriterion();
			}

			return $criterion;
		}
		
		// Ensure the current criterion is closed first.
		$this->closeCriterion();

		// Return a defensive copy. 
		// We should only edit the underlying criterion by calling modifier methods on this object.
		return clone $this->criterion;
	}

	public function where($field)
	{
		if($this->criterion != null) {
			//throw new CriteriaException('Criteria build error, criterion already started.');
			return $this->andWhere($field);
		}

		$this->criterion = new Criterion($field);

		return $this;
	}

	public function andWhere($field)
	{
		if($this->criterion == null) {
			return $this->where($field);
		}

		$this->closeCriterion();
		$this->currentAndCriterion = new Criterion($field);

		return $this;
	}

	public function orWhere($field)
	{
		if($this->criterion == null) {
			return $this->where($field);
		}

		$this->closeCriterion();
		$this->currentOrCriterion = new Criterion($field);

		return $this;
	}

	protected function closeCriterion()
	{
		if($this->currentAndCriterion != null) {
			if(!$this->currentAndCriterion->hasValue()) {
				throw new CriteriaException('Criteria build error, no value set for current AND criterion');
			}
			$this->criterion->andCriterion($this->currentAndCriterion);
			$this->currentAndCriterion = null;
		} else if($this->currentOrCriterion != null) {
			if(!$this->currentOrCriterion->hasValue()) {
				throw new CriteriaException('Criteria build error, no value set for current OR criterion');
			}
			$this->criterion->orCriterion($this->currentOrCriterion);
			$this->currentOrCriterion = null;
		} else if((sizeof($this->andCriteria)) && ($this->hasCriterion())) {
			foreach($this->andCriteria as $criteria) {
				$this->criterion->andCriterion($criteria->getCriterion());
			}
			$this->andCriteria = array();
		} else if((sizeof($this->orCriteria)) && ($this->hasCriterion())) {
			foreach($this->orCriteria as $criteria) {
				$this->criterion->orCriterion($criteria->getCriterion());
			}
			$this->orCriteria = array();
		}
	}

	private function getCurrentCriterion()
	{
		if($this->currentAndCriterion != null) {
			return $this->currentAndCriterion;
		} else if($this->currentOrCriterion != null) {
			return $this->currentOrCriterion;
		} else if($this->criterion != null){
			return $this->criterion;
		} else {
			throw new CriteriaException('Criteria build error, no current criterion to append to.');
		}
	}

	public function andCriteria()
	{
		if(($this->currentAndCriteria != null) || ($this->currentOrCriteria != null)) {
			throw new CriteriaException('Criteria build error, can not start new criteria.');
		}

		$this->closeCriterion();
		$this->currentAndCriteria = new Criteria($this);

		return $this->currentAndCriteria;
	}

	public function orCriteria()
	{
		if(($this->currentAndCriteria != null) || ($this->currentOrCriteria != null)) {
			throw new CriteriaException('Criteria build error, can not start new criteria.');
		}

		$this->closeCriterion();
		$this->currentOrCriteria = new Criteria($this);

		return $this->currentOrCriteria;
	}

	public function close()
	{
		if($this->parent == null) {
			throw new CriteriaException('Criteria build error, can not close criteria without a parent.');
		}

		$this->parent->closeCriteria();
		return $this->parent;
	}

	public function closeAll()
	{
		if($this->parent == null) {
			return $this;
		}
		
		$this->parent->closeCriteria();
		return $this->parent->closeAll();
	}

	public function closeCriteria()
	{
		if($this->currentAndCriteria != null) {
			$this->andCriteria[] = $this->currentAndCriteria;
			$this->currentAndCriteria = null;
		} else if($this->currentOrCriteria != null) {
			$this->orCriteria[] = $this->currentOrCriteria;
			$this->currentOrCriteria = null;
		} else {
			throw new CriteriaException('Criteria build error, no child criteria to close.');
		}

		$this->closeCriterion();
	}

	private function setCurrentCriterionValue($value, $type)
	{
		$currentCriterion = $this->getCurrentCriterion();

		if($currentCriterion->hasValue()) {
			throw new CriteriaException('Criteria build error, value already set for current criterion');
		}

		$currentCriterion->setValue($value, $type);
	}

	private function addCriterion($method, $value, $type)
	{
		if(!preg_match('#^(and|or)[A-Z]#', $method, $matches)) {
			throw new CriteriaException('Unknown criteria build method for new criterion.');
		}

		$currentCriterion = $this->getCurrentCriterion();

		if(!$currentCriterion->hasValue()) {
			throw new CriteriaException('Criteria build error, no value set for current criterion');
		}

		$newCriterion = new Criterion($currentCriterion->getField());
		$newCriterion->setValue($value, $type);

		$criterionMethod = $matches[1].'Criterion';
		$currentCriterion->$criterionMethod($newCriterion);
	}

	public function __call($method, $args)
	{
		if(sizeof($args) != 1) {
			throw new CriteriaException('Criteria build error, no value specified for method '.$method);
		}

		$value = $args[0];

		if(in_array($method, array_keys(self::$currentCriterionMethodMap))) {
			$this->setCurrentCriterionValue($value, self::$currentCriterionMethodMap[$method]);
		} else if(in_array($method, array_keys(self::$newCriterionMethodMap))) {
			$this->addCriterion($method, $value, self::$newCriterionMethodMap[$method]);
		} else {
			throw new CriteriaException('Unknown criteria build method.');
		}

		return $this;
	}
}
