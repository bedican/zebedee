<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\criteria;

use Exception;

class CriteriaException extends Exception
{
}