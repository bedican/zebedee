<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\criteria\builder;

use framework\db\dao\Dao;
use framework\exception\IllegalArgumentException;

class UpdateCriteriaBuilder
{
	private $dao;
	private $model;
	private $criteria;

	public function __construct($dao, $model)
	{
		if(! $dao instanceof Dao) {
			throw new IllegalArgumentException('$dao is not a Dao');
		}
		
		$this->dao = $dao;
		$this->model = $model;

		$this->criteria = $this->dao->getDriver()->newUpdateCriteria()
			->update($this->dao->getCollection());
	}
	
	public function execute()
	{
		$driver = $this->dao->getDriver();
		$mapper = $this->dao->getMapper();
		
		$document = $mapper->toArray($this->model);
		
		if(sizeof($fields = $this->criteria->getUpdateFields())) {
			$document = array_intersect_key($document, array_flip($fields));		
		}

		return $driver->update($document, $this->criteria);
	}
	
	public function upsert()
	{
		$driver = $this->dao->getDriver();
		$mapper = $this->dao->getMapper();

		$document = $mapper->toArray($this->model);

		if(sizeof($fields = $this->criteria->getUpdateFields())) {
			$document = array_intersect_key($document, array_flip($fields));		
		}

		return $driver->upsert($document, $this->criteria);
	}
	
	public function __call($method, $args)
	{
		$return = call_user_func_array(array($this->criteria, $method), $args);
		
		if($return != $this->criteria) {
			return $return;
		}
		
		return $this;
	}
}