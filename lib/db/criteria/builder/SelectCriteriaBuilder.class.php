<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\criteria\builder;

use framework\db\dao\Dao;
use framework\db\criteria\Criteria;
use framework\db\criteria\SelectCriteria;
use framework\db\result\AggregatedResultRow;
use framework\exception\IllegalStateException;
use framework\exception\IllegalArgumentException;

class SelectCriteriaBuilder
{
	private $dao;
	private $criteria;
	private $rawResult;

	public function __construct($dao)
	{
		if(! $dao instanceof Dao) {
			throw new IllegalArgumentException('$dao is not a Dao');
		}
		
		$this->dao = $dao;
		$this->rawResult = false;

		$this->criteria = $this->dao->getDriver()->newSelectCriteria()
			->from($this->dao->getCollection());
	}
	
	private function throwIfNotSelectCriteria()
	{
		if(! $this->criteria instanceof SelectCriteria) {
			throw new IllegalStateException('Builder does not currently hold an instance of SelectCriteria');
		}
	}
	
	public function fields($fields)
	{
		$this->throwIfNotSelectCriteria();
		$this->criteria->select($fields);
		return $this;
	}
	
	public function asRawResult()
	{
		$this->rawResult = true;
		return $this;
	}
	
	public function execute()
	{
		$this->throwIfNotSelectCriteria();

		$mapper = $this->dao->getMapper();
		$driver = $this->dao->getDriver();

		$isAggregated = $this->criteria->isAggregated();
		$result = $driver->retrieve($this->criteria);
		
		// If we only requested one, then return a single object

		if($this->criteria->getLength() == 1) {
			if($row = $result->getNextRow()) {
				return (($row instanceof AggregatedResultRow) || ($this->rawResult)) ? $row : $mapper->toModel($row);
			}
			return null;
		}
		
		// Otherwise, return an array
		
		$objects = array();
		while($row = $result->getNextRow()) {
			$objects[] = (($row instanceof AggregatedResultRow) || ($this->rawResult)) ? $row : $mapper->toModel($row);
		}
		
		return $objects;
	}
	
	public function count()
	{
		$this->throwIfNotSelectCriteria();
		return $this->dao->getDriver()->count($this->criteria);
	}
	
	public function __call($method, $args)
	{
		$return = call_user_func_array(array($this->criteria, $method), $args);
		
		if(! $return instanceof Criteria) {
			return $return;
		}
		
		$this->criteria = $return;
		return $this;
	}
}