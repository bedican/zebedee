<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\criteria\builder;

use framework\db\dao\Dao;
use framework\exception\IllegalArgumentException;

class DeleteCriteriaBuilder
{
	private $dao;
	private $criteria;

	public function __construct($dao)
	{
		if(! $dao instanceof Dao) {
			throw new IllegalArgumentException('$dao is not a Dao');
		}
		
		$this->dao = $dao;

		$this->criteria = $this->dao->getDriver()->newDeleteCriteria()
			->from($this->dao->getCollection());
	}
	
	public function execute()
	{
		return $this->dao->getDriver()->delete($this->criteria);
	}
	
	public function __call($method, $args)
	{
		$return = call_user_func_array(array($this->criteria, $method), $args);
		
		if($return != $this->criteria) {
			return $return;
		}
		
		return $this;
	}
}