<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\criteria;

class UpdateCriteria extends Criteria
{
	private $collection = null;
	private $updateFields = array();

	public function __construct()
	{
	}

	public function getCollection()
	{
		if($this->collection == null) {
			throw new CriteriaException('Criteria build error, collection not specified');
		}
		
		return $this->collection;
	}

	public function getUpdateFields()
	{
		return $this->updateFields;
	}
	
	public function update($collection)
	{
		if($this->collection != null) {
			throw new CriteriaException('Criteria build error, collection already specified');
		}

		$this->collection = $collection;
		return $this;
	}
	
	public function with($fields)
	{
		if(!is_array($fields)) {
			throw new CriteriaException('Criteria build error, invalid update fields list');
		}
		
		$this->updateFields = $fields;
		return $this;
	}
	
	public function toString()
	{
		if($this->collection == null) {
			throw new CriteriaException('No collection selected');
		}

		$output = 'update '.$this->collection.' fields '.implode(', ', $this->updateFields);
		
		if($criterion = $this->getCriterion()) {
			$output .= ' with criteria '.$criterion->toString();
		}

		return $output;
	}
}
