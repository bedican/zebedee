<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\criteria;

use framework\exception\IllegalArgumentException;

class HavingCriteria extends Criteria
{
	private $selectCriteria;

	public function __construct($selectCriteria)
	{
		if(! $selectCriteria instanceof SelectCriteria) {
			throw new IllegalArgumentException('$selectCriteria is not of type SelectCriteria');
		}
		
		$this->selectCriteria = $selectCriteria;
	}

	public function close()
	{
		$this->closeCriterion();
		return $this->selectCriteria;
	}
	
	public function closeAll()
	{
		parent::closeAll();
		return $this->selectCriteria;
	}
	
	public function toString()
	{
		$output = 'having';
		
		if($criterion = $this->getCriterion()) {
			$output .= ' '.$criterion->toString();
		}

		return $output;
	}
}
