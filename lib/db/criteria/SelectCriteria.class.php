<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\criteria;

use framework\exception\IllegalArgumentException;

class SelectCriteria extends Criteria
{
	const ORDER_ASC = 'asc';
	const ORDER_DESC = 'desc';
	
	private $collection = null;
	private $selectedFields = array();

	private $order = array();
	
	private $group = array();
	private $havingCriteria = null;
	
	private $sumFields = array();
	private $maxFields = array();
	private $minFields = array();
	private $avgFields = array();
	
	private $offset = null;
	private $length = null;

	public function __construct()
	{
	}
	
	public function getSelectedFields()
	{
		return $this->selectedFields;
	}

	public function getCollection()
	{
		if($this->collection == null) {
			throw new CriteriaException('Criteria build error, collection not specified');
		}
		
		return $this->collection;
	}
	
	public function getSumFields()
	{
		return $this->sumFields;
	}
	
	public function getMaxFields()
	{
		return $this->maxFields;
	}
	
	public function getMinFields()
	{
		return $this->minFields;
	}
	
	public function getAvgFields()
	{
		return $this->avgFields;
	}
	
	public function hasSumFields()
	{
		return (sizeof($this->sumFields) != 0);
	}
	
	public function hasMaxFields()
	{
		return (sizeof($this->maxFields) != 0);
	}
	
	public function hasMinFields()
	{
		return (sizeof($this->minFields) != 0);
	}
	
	public function hasAvgFields()
	{
		return (sizeof($this->avgFields) != 0);
	}

	public function hasOffset()
	{
		return ($this->offset !== null);
	}

	public function hasLength()
	{
		return ($this->length !== null);
	}

	public function getOffset()
	{
		return $this->offset;
	}

	public function getLength()
	{
		return $this->length;
	}

	public function hasOrder()
	{
		return (sizeof($this->order) != 0);
	}

	public function getOrder()
	{
		return $this->order;
	}
	
	public function hasGroup()
	{
		return (sizeof($this->group) != 0);
	}
	
	public function getGroup()
	{
		return $this->group;
	}
	
	public function hasHaving()
	{
		return ($this->havingCriteria != null);
	}
	
	public function getHaving()
	{
		return clone $this->havingCriteria;
	}
	
	public function isAggregated()
	{
		return (
			($this->hasSumFields()) ||
			($this->hasMaxFields()) ||
			($this->hasMinFields()) ||
			($this->hasAvgFields())
		);
	}
	
	public function select($fields)
	{
		if(!is_array($fields)) {
			throw new CriteriaException('Criteria build error, invalid select fields list');
		}
		
		$this->selectedFields = $fields;
		return $this;
	}
	
	public function from($collection)
	{
		if($this->collection != null) {
			throw new CriteriaException('Criteria build error, collection already specified');
		}

		$this->collection = $collection;
		return $this;
	}
	
	public function sum($field, $alias = null)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if((!is_string($alias)) && ($alias != null)) {
			throw new IllegalArgumentException('$alias is not of type string or null');
		}

		$alias = ($alias != null) ? $alias : 'sum_'.$field;
		
		if(in_array($alias, $this->sumFields)) {
			return $this;
		}

		$this->sumFields[$alias] = $field;
		return $this;
	}
	
	public function max($field, $alias = null)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if((!is_string($alias)) && ($alias != null)) {
			throw new IllegalArgumentException('$alias is not of type string or null');
		}

		$alias = ($alias != null) ? $alias : 'max_'.$field;
		
		if(in_array($alias, $this->maxFields)) {
			return $this;
		}

		$this->maxFields[$alias] = $field;
		return $this;
	}
	
	public function min($field, $alias = null)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if((!is_string($alias)) && ($alias != null)) {
			throw new IllegalArgumentException('$alias is not of type string or null');
		}

		$alias = ($alias != null) ? $alias : 'min_'.$field;
		
		if(in_array($alias, $this->minFields)) {
			return $this;
		}

		$this->minFields[$alias] = $field;
		return $this;
	}
	
	public function avg($field, $alias = null)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if((!is_string($alias)) && ($alias != null)) {
			throw new IllegalArgumentException('$alias is not of type string or null');
		}

		$alias = ($alias != null) ? $alias : 'avg_'.$field;
		
		if(in_array($alias, $this->avgFields)) {
			return $this;
		}

		$this->avgFields[$alias] = $field;
		return $this;
	}

	public function limit($arg1, $arg2 = null)
	{
		$this->closeCriterion();
		$this->offset = ($arg2 == null) ? 0 : $arg1;
		$this->length = ($arg2 == null) ? $arg1 : $arg2;
		return $this;
	}

	public function orderBy($field)
	{
		$this->closeCriterion();

		$fields = is_array($field) ? $field : array($field);

		foreach($fields as $field) {
			$this->order[] = array(SelectCriteria::ORDER_ASC, $field);			
		}

		return $this;
	}

	public function orderByDesc($field)
	{
		$this->closeCriterion();
		
		$fields = is_array($field) ? $field : array($field);

		foreach($fields as $field) {
			$this->order[] = array(SelectCriteria::ORDER_DESC, $field);
		}

		return $this;
	}
	
	public function groupBy($field)
	{
		$this->closeCriterion();
		
		$fields = is_array($field) ? $field : array($field);

		foreach($fields as $field) {
			$this->group[] = $field;
		}
		
		return $this;
	}
	
	public function having($field)
	{
		if(!$this->hasGroup()) {
			throw new CriteriaException('Can not set having criteria without group by');
		}
		if($this->havingCriteria != null) {
			throw new CriteriaException('Having criteria already exists');
		}
		
		$this->havingCriteria = new HavingCriteria($this);
		$this->havingCriteria->where($field);
		
		return $this->havingCriteria;
	}
	
	public function toString()
	{
		if($this->collection == null) {
			throw new CriteriaException('No collection selected');
		}

		$output = 'filter '.$this->collection;
		
		if($criterion = $this->getCriterion()) {
			$output .= ' with criteria '.$criterion->toString();
		}
		
		if(sizeof($this->group)) {
			$output .= ' group by '.implode(', ', $this->group);
		}
		
		if($this->havingCriteria) {
			if($criterion = $this->havingCriteria->getCriterion()) {
				$output .= ' having '.$criterion->toString();
			}
		}

		if(sizeof($this->order))
		{
			$output .= ' order by ';
			$orderby = array();

			foreach($this->order as $order)
			{
				list($direction, $field) = $order;

				if($direction == SelectCriteria::ORDER_ASC) {
					$orderby[] = $field.' asc';
				} else if($direction == SelectCriteria::ORDER_DESC) {
					$orderby[] = $field.' desc';
				}
			}

			$output .= implode(', ', $orderby);
		}

		if(($this->offset !== null) && ($this->length !== null)) {
			$output .= ' limit '.$this->offset.', '.$this->length;
		}

		return $output;
	}
}
