<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\criteria;

class DeleteCriteria extends Criteria
{
	private $collection = null;

	public function __construct()
	{
	}

	public function getCollection()
	{
		if($this->collection == null) {
			throw new CriteriaException('Criteria build error, collection not specified');
		}
		
		return $this->collection;
	}
	
	public function from($collection)
	{
		if($this->collection != null) {
			throw new CriteriaException('Criteria build error, collection already specified');
		}

		$this->collection = $collection;
		return $this;
	}
	
	public function toString()
	{
		if($this->collection == null) {
			throw new CriteriaException('No collection selected');
		}

		$output = 'delete from '.$this->collection;
		
		if($criterion = $this->getCriterion()) {
			$output .= ' with criteria '.$criterion->toString();
		}

		return $output;
	}
}
