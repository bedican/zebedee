<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\criteria;

class Criterion
{
	const EQUALS = 'eq';
	const NOT_EQUALS = 'ne';
	const IN = 'in';
	const NOT_IN = 'nin';
	const GREATER_THAN = 'gt';
	const LESS_THAN = 'lt';
	const GREATER_EQUALS = 'ge';
	const LESS_EQUALS = 'le';
	const LIKE = 'like';
	const NOT_LIKE = 'nlike';
	const REGEX = 'regex';
	const NOT_REGEX = 'nregex';

	const LOGIC_AND = 'and';
	const LOGIC_OR = 'or';

	private $field;
	private $value;
	private $type;

	private $criterion;

	public function __construct($field)
	{
		$this->field = $field;
		$this->value = null;
		$this->type = self::EQUALS;

		$this->criterion = array();
	}

	public function __clone()
	{
		for($x = 0; $x < sizeof($this->criterion); $x++) {
			$this->criterion[$x][1] = clone $this->criterion[$x][1];
		}
	}

	public function getField()
	{
		return $this->field;
	}

	public function getValue()
	{
		return $this->value;
	}

	public function hasValue()
	{
		return ($this->value !== null);
	}

	public function getType()
	{
		return $this->type;
	}

	public function getCriterion()
	{
		// Return a defensive copy.
		// Accessing nested criterion should be left to and/orCriterion methods

		$criterionList = array();

		foreach($this->criterion as $iterator) {
			list($logic, $criterion) = $iterator;
			$criterionList[] = array($logic, clone $criterion);
		}

		return $criterionList;
	}

	public function setValue($value, $type = null)
	{
		if((($type == self::IN) || ($type == self::NOT_IN)) && (!is_array($value))) {
			$value = array($value);
		}

		$this->value = $value;

		if($type != null) {
			$this->type = $type;
		}
	}

	public function andCriterion($criterion)
	{
		// We ensure encapsulation by cloning the added criterion
		$this->criterion[] = array(self::LOGIC_AND, clone $criterion);
	}

	public function orCriterion($criterion)
	{
		// We ensure encapsulation by cloning the added criterion
		$this->criterion[] = array(self::LOGIC_OR, clone $criterion);
	}

	public function toString()
	{
		$value = is_array($this->value) ? ('('.implode(', ', $this->value).')') : $this->value;

		$output = '('.$this->field.' '.$this->type.' '.$value.')';

		if(sizeof($this->criterion)) {
			foreach($this->criterion as $criterion) {
				$output .= ' '.$criterion[0].' '.$criterion[1]->toString();
			}
			$output = '('.$output.')';
		}

		return $output;
	}
}