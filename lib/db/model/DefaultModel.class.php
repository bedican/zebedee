<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\model;

use framework\exception\IllegalArgumentException;

class DefaultModel
{
	private $schema;
	private $values;
	
	public function __construct($schema)
	{
		if(! $schema instanceof Schema) {
			throw new IllegalArgumentException('$schema is not of type Schema');
		}
		
		$this->schema = $schema;
		$this->values = array();
	}
	
	protected function getSchema()
	{
		return $this->schema;
	}
	
	public function get($name, $default = null)
	{
		if(!$this->schema->hasField($name)) {
			throw new IllegalArgumentException('Unknown field name');
		}
		
		if(!array_key_exists($name, $this->values)) {
			return $default;
		}
		
		return $this->values[$name];
	}
	
	public function set($name, $value)
	{
		if(!$this->schema->hasField($name)) {
			throw new IllegalArgumentException('Unknown field name');
		}
		
		$this->values[$name] = $value;
	}
}