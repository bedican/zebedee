<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\dao;

interface Dao
{
	public function getDriver();
	public function getMapper();
	public function getCollection();

	// Obtain Criteria builders
	public function retrieve();
	public function retrieveOne();
	public function update($model);
	public function delete();

	// Retrieve//updateDelete by Primary Key
	public function getByPk($pk);
	public function deleteByPk($pk);
	public function updateByPk($model);
	
	// Insert a new model
	public function insert($model);
}