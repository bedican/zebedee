<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\dao;

use framework\application\Application;
use framework\db\DatabaseManager;
use framework\db\config\DaoConfig;
use framework\db\driver\Driver;
use framework\db\criteria\builder\DeleteCriteriaBuilder;
use framework\db\criteria\builder\UpdateCriteriaBuilder;
use framework\db\criteria\builder\SelectCriteriaBuilder;
use framework\exception\IllegalArgumentException;
use framework\exception\IllegalStateException;

abstract class AbstractDao implements Dao
{
	private $databaseManager;
	private $application;
	private $driver;
	private $collection;
	private $mapper;

	public final function __construct($databaseManager, $application, $driver, $config)
	{
		if(! $databaseManager instanceof DatabaseManager) {
			throw new IllegalArgumentException('$databaseManager is not of type DatabaseManager');
		}
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(! $driver instanceof Driver) {
			throw new IllegalArgumentException('$driver is not of type Driver');
		}
		if(! $config instanceof DaoConfig) {
			throw new IllegalArgumentException('$config is not of type DaoConfig');
		}
		
		// It is preferred that the dao can use any db engine, and thus any driver implementing the Driver interface.
		// However in some instances there may be a need to use specific functionality of a particular Driver,
		// in which case, the dao should specify the driver(s) is it dependent on.
		$dependencies = $this->getDriverDependencies();
		if((is_array($dependencies)) && (!in_array(get_class($driver), $dependencies))) {
			throw new IllegalArgumentException('Dao '.$config->getName().' can only access databases using the following drivers: '.implode(', ', $dependencies));
		}
		
		$this->databaseManager = $databaseManager;
		$this->application = $application;
		$this->driver = $driver;
		$this->collection = $config->getCollection();
		
		$mapperClassName = $config->getMapperClass();
		$this->mapper = new $mapperClassName($this->application, $config->getMapperConfig());

		$this->init();
	}

	// Default empty implementation
	protected function init() {}

	protected function getDriverDependencies()
	{
		return true;
	}
	
	protected function getDatabaseManager()
	{
		return $this->databaseManager;
	}
	protected function getApplication()
	{
		return $this->application;
	}

	public function getDriver()
	{
		return $this->driver;
	}
	public function getMapper()
	{
		return $this->mapper;
	}
	public function getCollection()
	{
		return $this->collection;
	}
	
	public function retrieve()
	{
		return new SelectCriteriaBuilder($this);
	}
	
	public function retrieveOne()
	{
		return $this->retrieve()->limit(1);
	}
	
	public function update($model)
	{
		if(!$this->mapper->isMappable($model)) {
			throw new IllegalArgumentException('Can not map $model, incorrect type '.get_class($model));
		}
		
		return new UpdateCriteriaBuilder($this, $model);
	}
	
	public function insert($model)
	{
		if(!$this->mapper->isMappable($model)) {
			throw new IllegalArgumentException('Can not map $model, incorrect type '.get_class($model));
		}
		
		$id = $this->driver->insert($this->getCollection(), $this->mapper->toArray($model));
		
		if($id === false) {
			return false;
		} else if($id === true) {
			if($this->mapper->hasAutoPrimaryKey()) {
				throw new DaoException('Expected an auto generated primary key, however was not set.');
			}

			return true;
		}
		
		// An auto generated id has been set and returned.
		
		if(!$this->mapper->hasAutoPrimaryKey()) {
			throw new DaoException('Did not expect an auto generated primary key, however one was set.');
		}
		
		$this->mapper->setAutoPrimaryKey($model, $id);

		return true;
	}
	
	public function delete()
	{
		return new DeleteCriteriaBuilder($this);
	}
	
	public function getByPk($pk)
	{
		$pk = func_get_args();		
		$pkFields = $this->mapper->getPrimaryKeyFieldNames();

		if(!sizeof($pkFields)) {
			throw new IllegalStateException('No primary keys specified within the field list');
		}
		if((!sizeof($pk)) || ((sizeof($pk)) != (sizeof($pkFields)))) {
			throw new IllegalArgumentException('Invalid arguments list');
		}
		
		$criteria = $this->retrieveOne()->where(array_shift($pkFields))->equals(array_shift($pk));

		while($field = array_shift($pkFields)) {
			$criteria->andWhere($field)->equals(array_shift($pk));
		}
		
		return $criteria->execute();
	}
	
	public function deleteByPk($pk)
	{
		$pk = func_get_args();		
		$pkFields = $this->mapper->getPrimaryKeyFieldNames();

		if(!sizeof($pkFields)) {
			throw new IllegalStateException('No primary keys specified within the field list');
		}
		if((!sizeof($pk)) || ((sizeof($pk)) != (sizeof($pkFields)))) {
			throw new IllegalArgumentException('Invalid arguments list');
		}
		
		$criteria = $this->delete()->where(array_shift($pkFields))->equals(array_shift($pk));
		
		while($field = array_shift($pkFields)) {
			$criteria->andWhere($field)->equals(array_shift($pk));
		}
		
		return $criteria->execute();
	}
	
	public function updateByPk($model)
	{
		$pkFields = $this->mapper->getPrimaryKeyFieldNames();
		
		if(!sizeof($pkFields)) {
			throw new IllegalStateException('No primary keys specified within the field list');
		}

		$pk = $this->mapper->getPrimaryKeyValues($model);
		$criteria = $this->update($model)->where(array_shift($pkFields))->equals(array_shift($pk));
		
		while($field = array_shift($pkFields)) {
			$criteria->andWhere($field)->equals(array_shift($pk));
		}
		
		return $criteria->execute();
	}
}