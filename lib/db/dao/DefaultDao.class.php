<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\db\dao;

use framework\db\filter\Filter;
use framework\exception\IllegalArgumentException;

class DefaultDao extends AbstractDao
{	
	public function getByFilter($filter)
	{
		if(! $filter instanceof Filter) {
			throw new IllegalArgumentException('$filter is not of type Filter');
		}
		
		$criteria = $this->retrieve();
		
		$filter->filter($criteria);
		$filter->limit($criteria);
		$filter->orderBy($criteria);
		
		return $criteria->execute();
	}
	public function getCountByFilter($filter)
	{
		if(! $filter instanceof Filter) {
			throw new IllegalArgumentException('$filter is not of type Filter');
		}
		
		$criteria = $this->retrieve();
		$filter->filter($criteria);
		
		return $criteria->count();
	}
}