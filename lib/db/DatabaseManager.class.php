<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db;

use framework\application\Application;
use framework\db\driver\Dsn;
use framework\db\config\DatabaseConfigBuilder;
use framework\db\config\DatabaseConfig;
use framework\cache\ConfigCache;

class DatabaseManager
{
	private static $dao = array();
	private static $drivers = array();

	private $application;
	private $config;

	public final function __construct($application)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		
		$this->application = $application;

		$this->initConfig();
	}
	
	private function initConfig()
	{
		$cache = new ConfigCache($this->application);
		$this->config = $cache->get('db.xml');

		if($this->config instanceof DatabaseConfig) {
			return;
		}
		
		$builder = new DatabaseConfigBuilder();
		
		// Framework db.xml should always be present
		$this->config = $builder->build(dirname(__FILE__).'/../../config/db.xml');

		// Merge with each enabled plugin db.xml if present
		$plugins = $this->application->getPlugins();
		foreach($plugins as $plugin) {
			$filename = $plugin->getPluginDir().'/config/db.xml';
			if(is_file($filename)) {
				$this->config->merge($builder->build($filename), true);
			}
		}

		// Override with application db.xml if present
		$filename = $this->application->getApplicationDir().'/config/db.xml';
		if(is_file($filename)) {
			$this->config->merge($builder->build($filename), true);
		}

		// Merge environment configuration
		if(is_string($environment = $this->application->getEnvironment())) {
			foreach($plugins as $plugin) {
				$filename = $plugin->getPluginDir().'/config/db.'.$environment.'.xml';
				if(is_file($filename)) {
					$this->config->merge($builder->build($filename), true);
				}
			}
			if(is_file($filename = $this->application->getApplicationDir().'/config/db.'.$environment.'.xml')) {
				$this->config->merge($builder->build($filename), true);
			}
		}
		
		$cache->put('db.xml', $this->config);
	}
	
	private function getDriver($dsn)
	{
		if(array_key_exists($dsn, self::$drivers)) {
			return self::$drivers[$dsn];
		}
		
		$dsn = new Dsn($dsn);
		$driverConfig = $this->config->getDriverConfig($dsn->getDriver());

		if(!$driverConfig) {
			throw new DatabaseManagerException('Unknown driver '.$dsn->getDriver());
		}
		
		$driverClassName = $driverConfig->getDriverClass();

		$driver = new $driverClassName($dsn, $driverConfig);
		self::$drivers[$dsn->toString()] = $driver;
		
		return $driver;
	}
	
	public function getDao($name)
	{
		if(array_key_exists($name, self::$dao)) {
			return self::$dao[$name];
		}
		
		$daoConfig = $this->config->getDaoConfig($name);
		
		if(!$daoConfig) {
			throw new DatabaseManagerException('Unknown dao '.$name);
		}
		
		// The data source the dao is configured to can be declared in either the dao config itself
		// or a separate dao data source config. This allows better configuration management across 
		// the configuration file structure (i.e. app/plugin db.xml).
		
		if($dataSource = $daoConfig->getDataSource()) {
			$dataSourceConfig = $this->config->getDataSourceConfig($dataSource);
		} else {
			$dataSourceConfig = $this->config->getDaoDataSourceConfig($name);
		}

		if(!$dataSourceConfig) {
			throw new DatabaseManagerException('Dao has unknown data source '.$name);
		}
		
		$driver = $this->getDriver($dataSourceConfig->getDsn());
		$daoClassName = $daoConfig->getDaoClass();

		$dao = new $daoClassName($this, $this->application, $driver, $daoConfig);
		self::$dao[$name] = $dao;
		
		return $dao;
	}
}