<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\result;

interface Result
{
	public function getNextRow();
}