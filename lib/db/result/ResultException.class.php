<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\db\result;

use Exception;

class ResultException extends Exception
{
}