<?php // (c) Copyright 2012 Bedican Solutions

namespace framework\db\result;

use framework\exception\IllegalArgumentException;

class AggregatedResultRow
{
	private $group = array();
	private $sum = array();
	private $min = array();
	private $max = array();
	private $avg = array();
	
	public function __construct()
	{
	}

	public function isGrouped()
	{
		return (sizeof($this->group) > 0);
	}
	
	public function getGroupByFields()
	{
		return array_keys($this->group);
	}
	
	public function getGroupBy($field)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if(!array_key_exists($field, $this->group)) {
			throw new IllegalArgumentException('Aggregated result is not grouped by '.$field);
		}
		
		return $this->group[$field];
	}
	
	public function getSum($field)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if(!array_key_exists($field, $this->sum)) {
			throw new IllegalArgumentException('Aggregated result does not hold sum of '.$field);
		}
		
		return $this->sum[$field];
	}
	
	public function getMax($field)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if(!array_key_exists($field, $this->max)) {
			throw new IllegalArgumentException('Aggregated result does not hold max of '.$field);
		}
		
		return $this->max[$field];
	}
	
	public function getMin($field)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if(!array_key_exists($field, $this->min)) {
			throw new IllegalArgumentException('Aggregated result does not hold min of '.$field);
		}
		
		return $this->min[$field];
	}
	
	public function getAvg($field)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if(!array_key_exists($field, $this->avg)) {
			throw new IllegalArgumentException('Aggregated result does not hold avg of '.$field);
		}
		
		return $this->avg[$field];
	}
	
	public function addGroupBy($field, $value)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if(!is_string($value)) {
			throw new IllegalArgumentException('$value is not of type string');
		}

		$this->group[$field] = $value;	
	}
	
	public function setSum($field, $value)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if(!is_numeric($value)) {
			throw new IllegalArgumentException('$value is not of type string');
		}
		
		$this->sum[$field] = $value;
	}
	
	public function setMin($field, $value)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if(!is_numeric($value)) {
			throw new IllegalArgumentException('$value is not of type string');
		}
		
		$this->min[$field] = $value;
	}
	
	public function setMax($field, $value)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if(!is_numeric($value)) {
			throw new IllegalArgumentException('$value is not of type string');
		}
		
		$this->max[$field] = $value;
	}
	
	public function setAvg($field, $value)
	{
		if(!is_string($field)) {
			throw new IllegalArgumentException('$field is not of type string');
		}
		if(!is_numeric($value)) {
			throw new IllegalArgumentException('$value is not of type string');
		}
		
		$this->avg[$field] = $value;
	}
}