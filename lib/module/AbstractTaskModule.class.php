<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\module;

/**
 * Helper class for Task modules to obtain task attributes from the context.
 */
abstract class AbstractTaskModule extends AbstractModule implements TaskModule
{
	protected function getConsole()
	{
		return $this->getContext()->getConsole();
	}
}