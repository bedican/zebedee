<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\module;

/**
 * Helper class for Web modules to obtain web attributes from the context.
 */
abstract class AbstractWebModule extends AbstractModule implements WebModule
{
	protected function getConfig()
	{
		return $this->getContext()->getConfig();
	}
	protected function getRequest()
	{
		return $this->getContext()->getRequest();
	}
	protected function getResponse()
	{
		return $this->getContext()->getResponse();
	}
	protected function getSession($name)
	{
		return $this->getContext()->getSession($name);
	}
	protected function getRouting()
	{
		return $this->getContext()->getRouting();
	}
	protected function getCurrentRoute()
	{
		return $this->getContext()->getCurrentRoute();
	}
	protected function send404()
	{
		throw new WebPageNotFoundException('Page not found');
	}
}