<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\module;

use framework\view\View;
use framework\context\Context;
use framework\application\Application;
use framework\exception\IllegalArgumentException;
use framework\db\DatabaseManager;
use Exception;

abstract class AbstractModule implements Module
{
	private $application;
	private $context;
	private $view;
	private $databaseManager;
	
	public final function __construct($application, $context)
	{
		if(! $application instanceof Application) {
			throw new IllegalArgumentException('$application is not of type Application');
		}
		if(! $context instanceof Context) {
			throw new IllegalArgumentException('$context is not of type Context');
		}
		
		$this->application = $application;
		$this->context = $context;
		$this->view = false;

		$this->databaseManager = new DatabaseManager($this->application);

		$this->init();
	}
	
	// Default empty implementation
	protected function init() {}
	protected function preAction() {}
	protected function postAction($result) {}
	
	public function getApplication()
	{
		return $this->application;
	}
	public function getContext()
	{
		return $this->context;
	}
	public function getDatabaseManager()
	{
		return $this->databaseManager;
	}
	
	public function setView($view)
	{
		if(($view !== false) && (! $view instanceof View)) {
			throw new IllegalArgumentException('$view is not of type View');
		}
		
		$this->view = $view;
	}
	
	public function getView()
	{
		return $this->view;
	}
	
	private function runActionMethod($actionMethod)
	{
		if(!method_exists($this, $actionMethod)) {
			throw new ActionNotFoundException('Action '.$this->context->getAction().' not found');
		}

		$this->preAction();
		$result = $this->$actionMethod();
		
		if(!isset($result)) {
			$result = true;
		}

		$this->postAction($result);
		
		return $result;
	}
	
	public function runAction()
	{
		$action = $this->context->getAction();
		$actionMethod = 'do'.ucfirst($action);

		if((!$this->view) || (!$this->view->isCached())) {
			$result = $this->runActionMethod($actionMethod);
		} else {
			$result = true;
		}
		
		if(($result) && ($this->view)) {
			$this->view->render();
		}
	}
}