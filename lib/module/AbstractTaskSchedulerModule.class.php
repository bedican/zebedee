<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\module;

/**
 * Helper class for Task scheduler modules to obtain task scheduler attributes from the context.
 */
abstract class AbstractTaskSchedulerModule extends AbstractModule implements TaskSchedulerModule
{
	protected function getConsole()
	{
		return $this->getContext()->getConsole();
	}
}