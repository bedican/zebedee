<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\module;

use Exception;

class WebPageNotFoundException extends ActionNotFoundException
{
}