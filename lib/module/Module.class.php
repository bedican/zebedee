<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\module;

interface Module
{
	public function runAction();
}
