<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\config;

use framework\exception\FileNotFoundException;
use DOMDocument, DOMXPath, DOMComment, DOMCharacterData;
use Exception;

abstract class AbstractConfigBuilder
{
	public function __construct()
	{
	}
	
	abstract protected function doBuild($xpath);
	
	public function build($filename)
	{
		if(!is_file($filename)) {
			throw new FileNotFoundException('File not found "'.$filename.'"');
		}

		$doc = new DOMDocument();
		if(!$doc->load($filename, LIBXML_NOBLANKS)) {
			throw new ConfigBuilderException('Failed to load configuration file "'.$filename.'"');
		}
		
		$xpath = new DOMXPath($doc);

		try {
			return $this->doBuild($xpath);
		} catch(Exception $e) {
			throw new ConfigBuilderException($e->getMessage(), 1, $e);
		}
	}
	
	protected function getConfigFromNode($node, $attributes = false)
	{	
		$config = array();
		
		foreach($node->childNodes as $childNode) {
			
			// We do not support character data (i.e. DOMText nodes) at the top level
			if($childNode instanceof DOMCharacterData) {
				continue;
			}
			
			$nodeName = strlen($childNode->getAttribute('name')) ? $childNode->getAttribute('name') : $childNode->nodeName;
			
			if(! $childNode->firstChild instanceof DOMCharacterData) {
				$nodeValue = $this->getConfigFromNode($childNode, $attributes);
			} else {
				$nodeValue = trim($childNode->nodeValue);
				if($attributes) {	
					$nodeAttributes = array();
					if($childNode->attributes->length) {
						foreach($childNode->attributes as $attributeName => $attributeNode) {
							$nodeAttributes[$attributeName] = strval($attributeNode->nodeValue);
						}
					}
					$nodeValue = array(
						'value' => $nodeValue,
						'attributes' => $nodeAttributes
					);
				}			
			}

			if(array_key_exists($nodeName, $config)) {
				if(!is_array($config[$nodeName])) {
					$config[$nodeName] = array($config[$nodeName]);
				}
				$config[$nodeName][] = $nodeValue;
			} else {				
				$config[$nodeName] = $nodeValue;
			}
		}

		if($attributes) {
			$nodeAttributes = array();
			if($node->attributes->length) {
				foreach($node->attributes as $attributeName => $attributeNode) {
					$nodeAttributes[$attributeName] = strval($attributeNode->nodeValue);
				}
			}
			$config = array(
				'value' => $config,
				'attributes' => $nodeAttributes
			);
		}

		return $config;
	}
}