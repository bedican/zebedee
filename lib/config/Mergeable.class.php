<?php // (c) Copyright 2013 Bedican Solutions

namespace framework\config;

interface Mergeable
{
	public function merge($config, $overwrite = false);
}