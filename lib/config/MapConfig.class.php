<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\config;

use framework\exception\IllegalArgumentException;

class MapConfig implements Mergeable
{
	const DELIMITER = '.';
	
	private $values = array();
	
	public function getValues()
	{
		return $this->values;
	}
	
	public function __construct($values = false)
	{
		if(is_array($values)) {
			$this->values = $values;
		}
	}

	public function getValue($path, $default = null)
	{
		if(!strlen($path)) {
			throw new IllegalArgumentException('No path specified');
		}
		
		$path = explode(self::DELIMITER, $path);

		$currentNode = $this->values;
		foreach($path as $currentPath)
		{
			if((!is_array($currentNode)) || (!array_key_exists($currentPath, $currentNode))) {
				return $default;
			}

			$currentNode = $currentNode[$currentPath];
		}

		return $currentNode;
	}

	public function setValue($path, $value)
	{
		if(!strlen($path)) {
			throw new IllegalArgumentException('No path specified');
		}
		if($value === null) {
			$this->removeValue($path);
		}
		
		$currentNode = & $this->values;

		$path = explode(self::DELIMITER, $path);
		$currentPath = array_shift($path);
		
		// Traverse down values as far as we can go with our path
		while(($currentPath) && (array_key_exists($currentPath, $currentNode)) && (is_array($currentNode[$currentPath]))) {
			$currentNode = & $currentNode[$currentPath];
			$currentPath = array_shift($path);
		}
		
		// If there is still a path to traverse, build up values with new data.
		while($currentPath) {
			$currentNode[$currentPath] = array();
			$currentNode = & $currentNode[$currentPath];
			$currentPath = array_shift($path);
		}
		
		$currentNode = $value;
	}

	public function removeValue($path)
	{
		if(!strlen($path)) {
			throw new IllegalArgumentException('No path specified');
		}
		
		$currentNode = & $this->values;
		$path = explode(self::DELIMITER, $path);

		while($currentPath = array_shift($path)) {
			
			if(!array_key_exists($currentPath, $currentNode)) {
				return; // cannot traverse to this path, nothing to remove.
			}
			if(!sizeof($path)) {
				break; // Reached the end of our path
			}

			$currentNode = & $currentNode[$currentPath];
		}

		unset($currentNode[$currentPath]);
	}

	public function hasValue($path)
	{
		if(!strlen($path)) {
			throw new IllegalArgumentException('No path specified');
		}

		$path = explode(self::DELIMITER, $path);
		$currentNode = $this->values;

		foreach($path as $currentPath)
		{
			if((!is_array($currentNode)) || (!array_key_exists($currentPath, $currentNode))) {
				return false;
			}

			$currentNode = $currentNode[$currentPath];
		}

		return true;
	}
	
	private function getPathsRecursive($prefix = false)
	{
		$paths = array();
		
		if($prefix) {
			$values = $this->getValue($prefix, array());
		} else {
			$values = $this->values;
		}
		
		foreach($values as $key => $value) {
			
			if($prefix) {
				$path = $prefix.self::DELIMITER.$key;	
			} else {
				$path = $key;
			}
			
			if(is_array($value)) {
				$paths = array_merge($paths, $this->getPathsRecursive($path));
			} else {
				$paths[] = $path;
			}
		}

		return $paths;
	}
	
	public function getPaths()
	{
		return $this->getPathsRecursive();
	}
	
	public function merge($config, $overwrite = false)
	{
		if(! $config instanceof MapConfig) {
			throw new IllegalArgumentException('$config is not of type MapConfig');
		}
		
		$paths = $config->getPaths();
		
		foreach($paths as $path) {
			if((!$this->hasValue($path)) || ($overwrite)) {
				$this->setValue($path, $config->getValue($path));
			}
		}
	}
}
