<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\console;

use framework\exception\IllegalArgumentException;
use framework\exception\IOException;

class Console
{
	const STDERR = 'php://stderr';
	const STDOUT = 'php://stdout';
	const STDIN = 'php://stdin';

	private $args;
	private $options;
	private $handles;

	public function __construct($args = null)
	{
		if(!is_array($args)) {
			if((array_key_exists('argv', $GLOBALS)) && (is_array($GLOBALS['argv']))) {
				$args = $GLOBALS['argv'];
			} else {
				$args = array();
			}
		}

		$this->options = array();
		$this->handles = array();
		
		$this->parse($args);
	}
	
	private function parse($args)
	{
		array_shift($args);
		
		foreach($args as $arg) {
			// Parse long opts with value, e.g. --option-name=value
			if(preg_match('#^--([a-z0-9-]+)=([a-z0-9_.-]+)$#i', $arg, $matches)) {
				$this->setOption($matches[1], $matches[2]);
			}
			// Parse long opts without value, e.g. --option-name
			else if(preg_match('#^--([a-z0-9-]+)$#i', $arg, $matches)) {
				$this->setOption($matches[1], true);
			}
			// Parse single opts, e.g. -x
			else if(preg_match('#^-([a-z0-9])$#i', $arg, $matches)) {
				$this->setOption($matches[1], true);
			}
			// Parse args, e.g. xyz
			else if(preg_match('#^([^\s]+)$#', $arg, $matches)) {
				$this->addArg($matches[1]);
			}
		}
	}
	
	private function addArg($arg)
	{
		$this->args[] = $arg;
	}
	
	private function setOption($name, $value)
	{
		$this->options[$name] = $value;
	}
	
	public function getArgs()
	{
		return $this->args;
	}
	
	public function getArg($index, $default = null)
	{
		if(!array_key_exists($index, $this->args)) {
			return $default;
		}
		
		return $this->args[$index];
	}
	
	public function shiftArg($default = null)
	{
		if(!sizeof($this->args)) {
			return $default;
		}
		
		return array_shift($this->args);
	}
	
	public function getOption($name, $default = null)
	{
		if(!array_key_exists($name, $this->options)) {
			return $default;
		}

		return $this->options[$name];
	}
	
	public function hasOption($name)
	{
		return array_key_exists($name, $this->options);
	}
	
	public function getIntOption($name, $default = null)
	{
		$value = $this->getOption($name);
		
		if((!is_int($value)) && (!ctype_digit($value))) {
			return $default;
		}

		return intval($value);
	}
	
	public function getHyphenOption($name, $default = null)
	{
		if(!$this->hasOption($name)) {
			return $default;
		}
		
		return implode('', array_map('ucfirst', explode('-', $this->getOption($name))));
	}
	
	private function getStreamHandle($stream)
	{
		if(array_key_exists($stream, $this->handles)) {
			return $this->handles[$stream];
		}
		
		$handle = fopen($stream, 'r+');
		
		if(!$handle) {
			throw new IOException('Could not open stream '.$stream);
		}
		
		$this->handles[$stream] = $handle;
		return $handle;
	}

	public function write($text, $stream = Console::STDOUT)
	{
		if(!in_array($stream, array(Console::STDOUT, Console::STDERR))) {
			throw new IllegalArgumentException('Invalid stream '.$stream);
		}
		
		fwrite($this->getStreamHandle($stream), $text);
	}
	
	public function writeln($text)
	{
		$this->write($text."\n");
	}
	
	public function close()
	{
		foreach($this->handles as $handle) {
			fclose($handle);
		}

		exit();
	}
}