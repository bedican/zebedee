<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\net;

use framework\exception\IllegalArgumentException;
use framework\exception\IllegalStateException;

class Response
{
	private $httpStatus = false;
	private $cookies = array();
	private $headers = array();
	private $redirect = false;
	private $content = '';
	
	private $flushed = false;
	
	public function __construct()
	{
		$this->initHeaders();
	}
	
	private function initHeaders()
	{
		$this->addHeader('Expires', date('D, d M Y H:i:s T', 0));
		$this->addHeader('Cache-Control', 'private, max-age=0, must-revalidate');
	}
	
	public function redirect($location, $perm = false)
	{
		$this->redirect = array($location, $perm);

		$this->clearHeaders();
		$this->httpStatus = false;
		$this->content = '';
		
		$this->flush();
	}
	
	public function clearCookies()
	{
		$this->cookies = array();
	}
	
	public function clearHeaders()
	{
		$this->headers = array();
		$this->initHeaders();
	}
	
	public function setStatus($code)
	{
		$status = HttpStatus::getHttpStatus($code);
		if($status == null) {
			throw new IllegalArgumentException('Unknown status code: '.$code);
		}
		
		$this->httpStatus = $status;
	}
	
	public function getStatus()
	{
		return ($this->httpStatus !== false) ? $this->httpStatus->getCode() : HttpStatus::OK()->getCode();
	}
	
	public function setCookie($name, $value, $expire = 0, $path = '/', $domain = '', $secure = false, $httponly = false)
	{
		$this->cookies[$name] = array(
			'value' => $value,
			'expire' => $expire,
			'path' => $path,
			'domain' => $domain,
			'secure' => $secure,
			'httponly' => $httponly
		);
	}
	
	public function clearCookie($name)
	{
		$this->setCookie($name, '', 0);
	}
	
	public function addHeader($name, $value)
	{
		if(!array_key_exists($name, $this->headers)) {
			$this->headers[$name] = array();
		}
		
		$this->headers[$name][] = $value;
	}
	
	public function setHeader($name, $value)
	{
		$this->headers[$name] = array($value);
	}
	
	public function setCacheLifetime($lifetime)
	{
		if(!is_int($lifetime)) {
			throw new IllegalArgumentException('$lifetime is not an integer');
		}
		if($this->getStatus() != 200) {
			throw new IllegalStateException('Http status is not 200');
		}

		$this->addHeader('Expires', date('D, d M Y H:i:s T', time() + $lifetime));
		$this->addHeader('Cache-Control', 'private, max-age='.$lifetime.', must-revalidate');
	}
	
	public function setContent($content)
	{
		if(!is_string($content)) {
			throw new IllegalArgumentException('$content is not a string');
		}
		
		$this->content = $content;
	}
	
	private function sendCookies()
	{
		foreach($this->cookies as $name => $cookie) {
			setcookie($name, $cookie['value'], $cookie['expire'], $cookie['path'], $cookie['domain'], $cookie['secure'], $cookie['httponly']);
		}
	}
	
	private function sendHeaders()
	{
		if($this->httpStatus !== false) {
			header('HTTP/1.0 '.$this->httpStatus);
			header('Status: '.$this->httpStatus);
		}
				
		foreach($this->headers as $name => $values) {
			foreach($values as $value) {
				header($name.': '.$value);
			}
		}
	}
	
	private function sendRedirect()
	{
		if(!is_array($this->redirect)) {
			throw new IllegalStateException('Can not perform redirect, no location set');
		}
		
		list($location, $perm) = $this->redirect;
		header('Location: '.$location, true, ($perm ? 301 : 307));
	}
	
	public function clear()
	{
		if($this->flushed) {
			throw new IllegalStateException('Can not clear response state, content already sent');
		}
		
		$this->clearCookies();
		$this->clearHeaders();
		$this->redirect = false;
		$this->httpStatus = false;
		$this->content = '';
	}
	
	public function flush()
	{
		if($this->flushed) {
			return;
		}
		
		$this->sendCookies();
		
		if($this->redirect) {
			$this->sendRedirect();
		} else {
			$this->sendHeaders();
			print($this->content);
		}
		
		$this->flushed = true;
	}
}