<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\net;

class Request
{
	private $get;
	private $post;
	private $request;
	private $cookie;
	private $server;
	private $files;
	
	public function __construct()
	{
		$this->get = $_GET;
		$this->post = $_POST;
		$this->request = $_REQUEST;
		$this->cookie = $_COOKIE;
		$this->server = $_SERVER;
		$this->files = $_FILES;
	}
	
	public function getUri()
	{
		return trim($this->server['REQUEST_URI']);
	}
	
	public function getAbsoluteUri()
	{
		return $this->getBaseUri() . $this->getUri();
	}
	
	public function getBaseUri()
	{
		return $this->getScheme() . $this->getHost() . (($this->getPort() == 80) ? '' : (':'.$this->getPort()));
	}
	
	public function getHost()
	{
		return $this->server['SERVER_NAME'];
	}
	
	public function getPort()
	{
		return $this->server['SERVER_PORT'];
	}
	
	public function isSecure()
	{
		return (array_key_exists('HTTPS', $this->server) && ($this->server['HTTPS']));
	}
	
	public function isXmlHttpRequest()
	{
		return ((array_key_exists('HTTP_X_REQUESTED_WITH', $this->server)) && ($this->server['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
	}
	
	public function getScheme()
	{
		return ($this->isSecure() ? 'https://' : 'http://');
	}
	
	public function getMethod()
	{
		return strtolower($this->server['REQUEST_METHOD']);
	}
	
	public function isPost()
	{
		return ($this->getMethod() == 'post');
	}
	
	public function getUserAgent()
	{
		return $this->server['HTTP_USER_AGENT'];
	}
	
	public function getHeader($name, $default = null)
	{
		$name = 'HTTP_'.strtoupper(str_replace('-', '_', $name));
		
		if(!array_key_exists($name, $this->server)) {
			return $default;
		}
		
		return $this->server[$name];
	}
	
	public function getClientIp()
	{
		if(array_key_exists('HTTP_X_FORWARDED_FOR', $this->server)) {
			return $this->server['HTTP_X_FORWARDED_FOR'];
		} else if(array_key_exists('HTTP_CLIENT_IP', $this->server)) {
			return $this->server['HTTP_CLIENT_IP'];
		}
		
		return $this->server['REMOTE_ADDR'];
	}
	
	public function getClientProxy()
	{
		if(array_key_exists('HTTP_X_FORWARDED_FOR', $this->server)) {
			if(array_key_exists('HTTP_CLIENT_IP', $this->server)) {
				return $this->server['HTTP_CLIENT_IP'];
			} else {
				return $this->server['REMOTE_ADDR'];
			}
		}
		
		return $this->getClientIp();
	}
	
	public function getGetNames()
	{
		return array_keys($this->get);
	}
	
	public function hasGetValue($name)
	{
		return array_key_exists($name, $this->get);
	}
	
	public function isGetValue($name, $value)
	{
		if(!$this->hasGetValue($name)) {
			return false;
		}
		
		return ($this->get[$name] == $value);
	}
	
	public function getGetValue($name, $default = null)
	{
		if(!$this->hasGetValue($name)) {
			return $default;
		}
		
		return $this->get[$name];
	}
	
	public function getGetValues()
	{
		return $this->get;
	}
	
	public function getPostNames()
	{
		return array_keys($this->post);
	}
	
	public function hasPostValue($name)
	{
		return array_key_exists($name, $this->post);
	}
	
	public function isPostValue($name, $value)
	{
		if(!$this->hasPostValue($name)) {
			return false;
		}
		
		return ($this->post[$name] == $value);
	}
	
	public function getPostValue($name, $default = null)
	{
		if(!$this->hasPostValue($name)) {
			return $default;
		}
		
		return $this->post[$name];
	}
	
	public function getPostValues()
	{
		return $this->post;
	}
	
	public function getRequestNames()
	{
		return array_keys($this->request);
	}
	
	public function hasRequestValue($name)
	{
		return array_key_exists($name, $this->request);
	}
	
	public function isRequestValue($name, $value)
	{
		if(!$this->hasRequestValue($name)) {
			return false;
		}
		
		return ($this->request[$name] == $value);
	}
	
	public function getRequestValue($name, $default = null)
	{
		if(!$this->hasRequestValue($name)) {
			return $default;
		}
		
		return $this->request[$name];
	}
	
	public function getRequestValues()
	{
		return $this->request;
	}

	public function getCookieNames()
	{
		return array_keys($this->cookie);
	}
	
	public function hasCookieValue($name)
	{
		return array_key_exists($name, $this->cookie);
	}
	
	public function isCookieValue($name, $value)
	{
		if(!$this->hasCookieValue($name)) {
			return false;
		}
		
		return ($this->cookie[$name] == $value);
	}
	
	public function getCookieValue($name, $default = null)
	{
		if(!$this->hasCookieValue($name)) {
			return $default;
		}
		
		return $this->cookie[$name];
	}
	
	public function hasFiles()
	{
		return (sizeof($this->files) > 0);
	}
	
	public function getFileFieldNames()
	{
		return array_keys($this->files);
	}
	
	public function hasFile($name)
	{
		return array_key_exists($name, $this->files);
	}
	
	public function getFile($name)
	{
		if(!$this->hasFile($name)) {
			return null;
		}
		
		return $this->files[$name];
	}
}