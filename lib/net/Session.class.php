<?php // (c) Copyright 2011 Bedican Solutions

namespace framework\net;

use framework\exception\IllegalArgumentException;

class Session
{
	private static $started = false;
	private static $sessions = array();

	private $isnew;
	private $invalidated;

	private $name;
	private $created;
	private $lastAccessed;
	private $maxInactivity;

	private $values;

	/**
	 * Either returns the session with the given name from the session storage mechanism if it exists or creates a new session.
	 *
	 * @param String $name The session name.
	 * @return Session The session of the given name.
	 * @see Session::isNew()
	 */
	public static function getInstance($name = 'default')
	{
		if(!self::$started) {
			self::$started = session_start();
		}

		if(isset(self::$sessions[$name])) {
			return self::$sessions[$name];
		}

		if(isset($_SESSION[$name])) {
			$session = unserialize($_SESSION[$name]);
		} else {
			$session = new Session($name);
		}

		self::$sessions[$name] = $session;
		return $session;
	}

	private function __construct($name)
	{
		$this->isnew = true;
		$this->invalidated = false;
		$this->name = $name;
		$this->created = time();
		$this->lastAccessed = time();
		$this->maxInactivity = 1800;
		$this->values = array();
	}

// NOTE: Saving the session on destruct or on script shutdown may cause problems
// when serving a http redirect (301/307). The data needs to be written to the $_SESSION
// varible as soon as possible and before sending the header, which may not happen if we do it here.
//
//	/**
//	 * Destructor. Saves the session if eligible.
//	 */
//	public function __destruct()
//	{
//		$this->save();
//	}

	/**
	 * PHP sleep magic method.
	 */
	public function __sleep()
	{
		return array('name', 'created', 'lastAccessed', 'maxInactivity', 'values');
	}

	/**
	 * PHP wakeup magic method.
	 */
	public function __wakeup()
	{
		$this->isnew = false;
		$this->invalidated = false;
	}

	/**
	 * Returns true if the session is new and was not retrieved from the session storage mechanism.
	 *
	 * @return Boolean true if the session is new.
 	 */
	public function isNew()
	{
		return $this->isnew;
	}

	/**
	 * Returns true if the session has been invalidated.
	 * A session is invaldated by calling its destroy() method.
	 *
	 * @return Boolean true if the session has been invalidated.
	 * @see Session::destroy()
 	 */
	public function isInvalidated()
	{
		return $this->invalidated;
	}

	/**
	 * Returns true if the session does not contain and data.
	 *
	 * @return Boolean true if the session does not contain data.
 	 */
	public function isEmpty()
	{
		return (sizeof($this->values) == 0);
	}

	/**
	 * Saves the session to the storage mechanism if eligible.
	 * A session is eligible for storage if it:
	 * 	- Has not been invalidated.
	 * 	- Has not expired.
	 * 	- Is not new and empty.
	 */
	public function save()
	{
		if($this->isInvalidated()) {
			return;
		}
		if($this->isExpired()) {
			return;
		}
		if(($this->isNew()) && ($this->isEmpty())) {
			return;
		}

		$_SESSION[$this->name] = serialize($this);
	}

	/**
	 * Invalidates the session.
	 */
	public function destroy()
	{
		if($this->isInvalidated()) {
			return;
		}

		unset($_SESSION[$this->name]);
		$this->invalidated = true;
	}

	/**
	 * Returns true if the session has expired.
	 * A session has expired if it has not been accessed within its max inactivity period.
	 * If the session has expired it will be invalidated.
	 *
	 * @return Boolean true if the session has expired. 
	 */
	public function isExpired()
	{
		if(($this->lastAccessed + $this->maxInactivity) > time()) {
			return false;
		}

		$this->destroy();
		return true;
	}

	/**
	 * Return the timestamp of when this session was created.
	 *
	 * @return Integer when this session was created.
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * Return the timestamp of when this session was last accessed.
	 *
	 * @return Integer when this session was last accessed.
	 */
	public function getLastAccessed()
	{
		return $this->lastAccessed;
	}

	/**
	 * Return the max inactivity period in seconds for this session.
	 *
	 * @return Integer the max inactivity period for this session.
	 */
	public function getMaxInactivity()
	{
		return $this->maxInactivity;
	}

	/**
	 * Sets the max inactivity period in seconds for this session.
	 *
	 * @param Integer $maxInactivity The period in seconds.
	 */
	public function setMaxInactivity($maxInactivity)
	{
		if(!is_int($maxInactivity)) {
			throw new IllegalArgumentException('$maxInactivity is not of type integer');
		}
		if($this->isInvalidated()) {
			return;
		}

		$this->maxInactivity = $maxInactivity;
		$this->save();
	}

	/**
	 * Returns true if a value for the given key exists.
	 *
	 * @param String $key The key.
	 * @return Boolean true if a value for $key exists.
	 */
	public function isValue($key)
	{
		$this->access();
		return array_key_exists($key, $this->values);
	}

	/**
	 * Returns a value for a given key.
	 *
	 * @param String $key The key.
	 * @param String $default A default value if a value for $key does not exist.
	 * @return String The value for $key or $default if it does not exist.
	 */
	public function getValue($key, $default = null)
	{
		$this->access();

		if(($this->isExpired()) || (!$this->isValue($key))) {
			return $default;
		}

		return $this->values[$key];
	}

	/**
	 * Sets a value for a given key. 
	 *
	 * @param String $key The key.
	 * @param mixed $value The value to store, if null the value is removed.
	 */
	public function setValue($key, $value)
	{
		$this->access();

		if($value != null) {
			$this->values[$key] = $value;
		} else {
			unset($this->values[$key]);
		}
		
		$this->save();
	}
	
	private function access()
	{
		$this->lastAccessed = time();
	}
}