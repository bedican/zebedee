# Zebedee

[Home](../README.md) - Application

## Application

- [Directory structure](#markdown-header-directory-structure)
- [Class naming convention](#markdown-header-class-naming-convention)
- [The app namespace](#markdown-header-the-app-namespace)
- [The application class](#markdown-header-the-application-class)
- [Configuring the application](#markdown-header-configuring-the-application)

### Directory structure
The application directory structure is detailed below.

| Directory | Description                                                               |
|-----------|---------------------------------------------------------------------------|
| ./bin     | Holds application cli entry points (task and task scheduling).            |
| ./cache   | Generated cache files (requires write permissions).                       |
| ./config  | Application level configuration and bootstrap for zebedee.                |
| ./layouts | Holds common template layouts.                                            |
| ./lib     | All the application class files under the "app" namespace.                |
| ./log     | Generated log files (requires write permissions).                         |
| ./tmp     | Temporary generated files (requires write permissions).                   |
| ./modules | Module specific files, see the modules section for more information       |
| ./web     | This is the public web directory, and holds application web entry points. |
| ./plugins | Holds plugins.                                                            |

### Class naming convention
All classes should be defined within a file of the name <ClassName>.class.php

### The app namespace
All class files under the app namespace live within the application lib directory. Subdirectories within are translated to namespaces. For example, classes within the namespace app\models should be placed within the directory ./lib/models.

### The application class
Each application has its own Application subclass with the name <ApplicationName>Application in the app namespace. The subclass is defined within the file ./lib/<ApplicationName>Application.class.php and can be modified to provide further application level functionality.

The application class provides an interface to application level configuration and logging, and also access to each enabled plugin, (see the plugins section for more information).

### Configuring the application
Application configuration is held within the file app.xml. See the configuration section for more information.
