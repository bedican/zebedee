# Zebedee

[Home](../README.md) - Modules

## Modules

- [Directory structure](#markdown-header-directory-structure)
- [The module namespace](#markdown-header-the-module-namespace)
- [The module class](#markdown-header-the-module-class)
- [Context](#markdown-header-context)
- [Web modules](#markdown-header-web-modules)
- [Task modules](#markdown-header-task-modules)
- [Task scheduler modules](#markdown-header-task-scheduler-modules)

### Directory structure
A modules directory structure is detailed below.

| Directory | Description                                                                                     |
|-----------|-------------------------------------------------------------------------------------------------|
| ./config  | Module level configuration.                                                                     |
| ./lib     | All the module class files, under the "module\<Name>" namespace.                                |
| ./views   | View template files.                                                                            |
| ./layouts | Module template layouts, these will override layouts on the application level of the same name. |

### The module namespace
All class files under the module namespace live within the specific module lib directory. Subdirectories within are translated to namespaces. For example, classes within the namespace module\\foo\\bar should be placed within the ./lib/bar directory of the foo module.

Plugins can also hold modules (see the plugins section for more information), which are also within the same namespace as modules within the application. The class loader will search for a module within the application first, then within the enabled plugin modules of the application, and lastly within the enabled plugin modules of zebedee.

### The module class
A module class definition should be created within the file ./lib/<Name>Module.class.php, with the class name of <Name>Module and the namespace of module\\<Name>, (all modules are under the module namespace).

A module class contains all of the actions for that module. Before an action is run, the optional method preAction is called, and after, the optional method postAction is called with the result of the action. The result of the action is the return value of that action, and if not true then the modules view will not be rendered. If no value is returned by the action, it is considered true. For example,

```
<?php

namespace module\foo;

use framework\module\AbstractWebModule;

class FooModule extends AbstractWebModule
{
	protected function preAction()
	{
		// I get called first
	}
	protected function doMyAction()
	{
		return false; // Do not render the view.
	}
	protected function postAction($result)
	{
		// I get called last, with $result being false.
	}
}
```

### Context
Each action is run from its modules respective controller with a context object detailing the context under which the module was invoked. Three abstract classes, for each type of module, provide helper methods to access the specifics of that context.

While the normal process would be to create concrete classes of the relevant abstract class, implementing the relevant module interface, it is perfectly possible to directly implement one or more of the interfaces to enable the module actions to be run from more than one controller. The caveat of doing this would be that the context may be one of multiple types, and the context would need to be type checked to determine under which context, and which controller, the action was being run.

### Web modules
In order for a module to be run from the web controller it must implement the WebModule interface, and be configured within the routing configuration, routing.xml. See the configuration section for further information.

To create a web module, the abstract class AbstractWebModule should be extended, which implements the WebModule interface and provides helper methods to access the properties of the context passed in by the controller.

An example web module is shown below,

```
<?php

namespace module\foo;

use framework\module\AbstractWebModule;

class FooModule extends AbstractWebModule
{
	protected function doMyAction()
	{
		// Business logic for myAction action.
	}
}
```

### Task modules
In order for a module to be run from the task controller it must implement the TaskModule interface.

To create a task module, the abstract class AbstractTaskModule should be extended, which implements the TaskModule interface and provides helper methods to access the properties of the context passed in by the controller.

An example task module is shown below,

```
<?php

namespace module\foo;

use framework\module\AbstractTaskModule;

class FooModule extends AbstractTaskModule
{
	protected function doMyAction()
	{
		// Business logic for myAction action.
	}
}
```

### Task scheduler modules
In order for a module to be run from the task scheduler controller it must implement the TaskSchedulerModule interface and be added to the TaskSchedule of the controller.

To create a task module, the abstract class AbstractTaskSchedulerModule should be extended, which implements the TaskSchedulerModule interface and provides helper methods to access the properties of the context passed in by the controller.

An example task scheduler module is shown below,

```
<?php

namespace module\foo;

use framework\module\AbstractTaskSchedulerModule;

class FooModule extends AbstractTaskSchedulerModule
{
	protected function doMyAction()
	{
		// Business logic for myAction action.
	}
}
```

The TaskSchedule specifying the schedule for the module and action,

```
<?php

namespace app;

use framework\task\TaskSchedule;
use framework\task\ScheduledTask;

abstract class MyTaskSchedule extends TaskSchedule
{
	protected function init()
	{
		// Every Wed 13:00
		$this->addTask(new ScheduledTask('foo', 'myAction'), 'DW3 H13 M0');
	}
}
```

Configuring the TaskSchedule for the controller within app.xml,

```xml
<application>
	<task-schedule-class>\app\MyTaskSchedule</task-schedule-class>
</application>
```
