# Zebedee

[Home](../README.md) - Databases

## Databases

- [Drivers](#markdown-header-drivers)
- [Data sources](#markdown-header-data-sources)
- [Data access objects](#markdown-header-data-access-objects)
- [Mappers](#markdown-header-mappers)
- [Schemas](#markdown-header-schemas)
- [Database mananger](#markdown-header-database-mananger)

### Drivers
Two plugins are made available providing driver implementations for accessing mongo and mySql databases, (see the plugins section on how to enable a plugin). Generally, the drivers are not used directly to access the database, that is done with the use of database access objects which hold an instance of the driver, see below.

To create a driver for a database engine, the Driver and Result interfaces must be implemented. To make this process easier the abstract class AbstractDriver is provided. Optionally, criteria builders can also be extended and returned by the Driver to provide further criteria relevant for executing queries against that specific database engine.

To enable a driver, it must be added to the db.xml configuration file, for example,

```xml
<config>
	<drivers>
 		<driver>
			<name>mydriver</name>
			<driver-class>\app\db\driver\MyDriver</driver-class>
		</driver>
	</drivers>
</config>
```

See the mongo and mySql plugins for Driver implementation examples.

### Data sources
A data source is the configuration of an identifier against a data source name (DSN) within the db.xml configuration file. This allows for the same data source to be used for multiple data access objects (see below), and ensures we only need to declare the DSN in one location. The driver included in any DSN must be implemented and enabled. For example,

```xml
<config>
	<data-sources>
		<data-source>
			<name>my-data-source</name>
			<dsn>mydriver://user@localhost/database</dsn>
		</data-source>
	</data-sources>
</config>
```

### Data access objects
Accessing a collection (or table) within the data source is carried out through a database access object (DAO), which provides an interface to retrieve and update documents (or records) within that collection with the use of criteria objects.

Document fields within the collection are mapped to model objects and returned by the DAO. By default, documents are mapped using a DefaultMapper which maps against DefaultModel objects that provide a generic interface to the fields of the document. It is possible, however, to configure the DAO to use an alternative mapper to map against a more custom model, see below.

Unless a DAO is configured, the DefaultDao will be used. To create a DAO, the DAO interface must be implemented. To make this process easier the abstract class AbstractDao is provided, however it is recommended to extend the DefaultDao class.

For common queries and operations, it is recommended to create methods on the DAO class so query building is encapsulated within one location. For example,

```
<?php

namespace app\db;

use framework\db\dao\DefaultDao;

class MyDao extends DefaultDao
{
	public function getByUsername($username)
	{
		// Example method to abstract query building.
		return $this->retrieveOne()->where('username')->equals($username)->execute();
	}
}
```

For the database manager (see below), to be aware of a custom DAO it must be configured within the db.xml configuration file,

```xml
<config>

	<daos>
		<dao>
			<name>my-dao</name>
			<collection>mycollection</collection>
			<dao-class>\app\db\MyDao</dao-class>
		</dao>
	</daos>

	<dao-data-sources>
		<dao-data-source>
			<dao>my-dao</dao>
			<data-source>my-data-source</data-source>
		</dao-data-source>
	</dao-data-sources>

</config>
```

### Mappers
By default, a DAO is configured with an instance of DefaultMapper which maps to DefaultModel objects. In most cases this is sufficient, however, the way data is represented in a database may not always be the way we wish to represent the same data within the application.

An alternative option to the DefaultMapper is to use the GenericMapper which makes use of a defined schema held within the schema.xml configuration file, see below. The GenericMapper calls a setter method on the model object for each field in the schema. The GenericMapper can also map to embedded schemas defined within the schema. For example,

```xml
<config>

	<daos>
		<dao>
			<name>my-dao</name>
			<collection>mycollection</collection>
			<dao-class>\app\db\MyDao</dao-class>
			<mapper-class>\framework\db\mapper\GenericMapper</mapper-class>
			<mapper-config>
				<schemas>
					<schema name="my-schema">\app\db\MySchema</schema>
					<embedded>
						<schema name="my-embedded-schema">\app\db\MyEmbedded</schema>
					</embedded>
				</schemas>
			</mapper-config>
		</dao>
	</daos>

</config>
```

Another option similar to using GenericMapper is to use the MultiSchemaMapper which makes use of multiple defined schemas held within the schema.xml configuration file, see below. A field within the document is used to denote which schema will be used when mapping to a model object. The MultiSchemaMapper can also map to embedded schemas defined within the schema. For example,

```xml
<config>

	<daos>
		<dao>
			<name>my-dao</name>
			<collection>mycollection</collection>
			<dao-class>\app\db\MyDao</dao-class>
			<mapper-class>\framework\db\mapper\MultiSchemaMapper</mapper-class>
			<mapper-config>
				<type-field>_id</type-field>
				<types>
					<type name="mytype">
						<schema name="my-schema">\app\db\MySchema</schema>
						<embedded>
							<schema name="my-embedded-schema">\app\db\MyEmbedded</schema>
						</embedded>
					</type>
					<type name="wildcard-type-*">
						<schema name="my-second-schema">\app\db\MySecondSchema</schema>
					</type>
				</types>
			</mapper-config>
		</dao>
	</daos>

</config>
```

A custom mapper can also be created. To create a mapper the Mapper interface must be implemented. To make this process easier the abstract class AbstractMapper is provided, however DefaultMapper, GenericMapper or MultiSchemaMapper could also be extended.

```
<?php

namespace app\db;

use framework\db\mapper\AbstractMapper;

class MyMapper extends AbstractMapper
{
	protected function canMap($model)
	{
		return ($model instanceof MyModel);
	}

	protected function hasAutoField()
	{
		return true;
	}

	protected function setAutoField($model, $value)
	{
		$model->setId($value);
	}

	protected function getPkValues($model)
	{
		return array($model->getId());
	}

	protected function getPkFieldNames()
	{
		return array('id');
	}

	protected function arrayToModel($arr)
	{
		$model = new MyModel();
		$model->setUsername($arr['username']);
		// ...

		return $model;
	}

	protected function modelToArray($model)
	{
		$arr = array();
		$arr['username'] = $model->getUsername();
		// ...

		return $arr;
	}
}
```

The DAO can then be configured with the custom mapper, for example,

```xml
<config>

	<daos>
		<dao>
			<name>my-dao</name>
			<collection>mycollection</collection>
			<dao-class>\app\db\MyDao</dao-class>
			<mapper-class>\app\db\MyMapper</mapper-class>
		</dao>
	</daos>

</config>
```

### Schemas
Schemas are used by mappers that extend AbstractSchemaMapper, (GenericMapper and MultiSchemaMapper). The mapping of schema to model is maintained within the db.xml configuration file within the mapper-config block, see above. The schema definitions are maintained within the schema.xml configuration file. For example,

```xml
<schemas>

	<schema name="event">
		<field>time</field>
		<field>type</field>
		<field>message</field>
	</schema>
	<schema name="person">
		<field auto="true" primary="true">_id</field>
		<field>firstname</field>
		<field>lastname</field>
	</schema>
	<schema name="customer" extend="person">
		<field auto="true" primary="true">_id</field>
		<field embed="item-event" collection="true">events</field>
		<field>createdDate</field>
		<field>loyaltyLevel</field>
	</schema>

</schemas>
```

### Database mananger
The database manager is the main entry point to the database abstraction layer and is used to obtain DAO objects. For example,

```
<?php

namespace module\foo;

use framework\module\AbstractWebModule;

class FooModule extends AbstractWebModule
{
	protected function doMyAction()
	{
		$dbm = $this->getDatabaseManager();
		$myDao = $dbm->getDao('my-dao');

		$user = $myDao->getByUsername('JoeBloggs');

		// my-dao is configured to return DefaultModel objects
		$name = $user->get('name');
	}
}
```
