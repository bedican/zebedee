# Zebedee

[Home](../README.md) - Plugins

## Plugins

- [Directory structure](#markdown-header-directory-structure)
- [The plugin namespace](#markdown-header-the-plugin-namespace)
- [The plugin class](#markdown-header-the-plugin-class)
- [Standard plugins](#markdown-header-standard-plugins)
- [Enabling a plugin](#markdown-header-enabling-a-plugin)
- [Plugin search order](#markdown-header-plugin-search-order)
- [Plugin module search order](#markdown-header-plugin-module-search-order)

### Directory structure
A plugin directory structure is detailed below.

| Directory  | Description                                                                   |
|------------|-------------------------------------------------------------------------------|
| ./config   | Plugin level configuration.                                                   |
| ./lib      | All the plugin class files, under the "plugin\<Name>" namespace.              |
| ./modules  | Modules held within the plugin, see the modules section for more information. |

### The plugin namespace
All class files under the plugin namespace live within the specific plugin lib directory. Subdirectories within are translated to namespaces. For example, classes within the namespace plugin\\foo\\bar should be placed within the ./lib/bar directory of the foo plugin.

### The plugin class
A plugin class definition should be created within the file ./lib/<Name>Plugin.class.php, with the class name of <Name>Plugin and the namespace of plugin\\<Name>. For example,

```
<?php

namespace plugin\myPlugin;

use framework\plugin\Plugin;

class MyPluginPlugin extends Plugin
{
}
```

The plugin class provides an interface to plugin level configuration, held within plugin.xml. See the configuration section for more information.

The method getPlugin can be used on the application object to obtain the plugin instance, however, care should be taken creating dependancies between plugins using this method (simply for good design), and dependancies should be declared within the plugin itself. For example,

```
<?php

namespace plugin\myPlugin;

use framework\plugin\Plugin;

class MyPluginPlugin extends Plugin
{
	protected function getDependencies()
	{
		// Has a dependancy on zooPlugin, dooPlugin and one of barPlugin or fooPlugin
		return array('zooPlugin', 'dooPlugin', 'barPlugin fooPlugin');
	}
}
```

### Standard plugins
The following plugins are supplied with zebedee.

| Plugin         | Description                                                                                 |
|----------------|---------------------------------------------------------------------------------------------|
| auth           | Plugin to provide core functionality for an authentication module.                          |
| common         | The common plugin, made available to all applications by default. The task and error modules within are enabled by default. This plugin holds standard error handling actions for web controllers and the clear-cache action.                                                        |
| dbTaskSchedule | A plugin to provide a database driven TaskSchedule instance for task scheduler controllers. |
| mongo          | Mongo database access layer.                                                                |
| mySql          | MySql database access layer.                                                                |

### Enabling a plugin
Plugins, and modules within plugins, are enabled from within the app.xml configuration file, see the configuration section for more information. The configuration option within the app.xml files for each plugin is ignored to avoid one plugin being able to enable another, and ensures explicit consent is required at the application level. For example,

```xml
<application>

	<enabled-plugins>
		<plugin>myPlugin</plugin>
	</enabled-plugins>

	<plugin-modules>
		<plugin-module>
			<plugin>myPlugin</plugin>
			<module>myModule</module>
		</plugin-module>
	</plugin-modules>

</application>
```

### Plugin search order
As it is possible to have multiple plugins in different locations with the same name, the class loader will search for a plugin first within the application plugins directory and then within the plugins directory of zebedee. This makes it possible to override and replace a zebedee plugin completely, by creating a plugin with the same name within the application.

### Plugin module search order
As it is possible to have multiple modules in different locations with the same name, the class loader will search for a module in the following order. This makes it possible to override and replace a module completely, by placing it in the right location, or choosing which module is used through configuration.

- The application ./modules directory
- Within the ./modules directory of each enabled plugin in the order the plugin and plugin module is enabled within app.xml, also see plugin search order above.
