# Zebedee

[Home](../README.md) - Cache

## Cache

- [Configuraton cache](#markdown-header-configuration-cache)
- [View cache](#markdown-header-view-cache)
- [Snippet cache](#markdown-header-snippet-cache)
- [Clearing the cache](#markdown-header-clearing-the-cache)

### Configuration cache
Configuration files are loaded from multiple locations and merged to produce a compiled configuration, see the configuration section for more information. The compiled result is cached within the ./cache/config directory of the application. The default behaviour is to cache indefinately, however this can be disabled by setting the option use-config-cache to false within
the app.xml configuration file. For this setting to take effect, the configuration cache needs to be cleared.

```xml
<application>
	<use-config-cache>false</use-config-cache>
</application>
```

### View cache
Every view (and thus the result every action that renders a view) is optionally cachable and cached within the ./cache/view directory of the application.

By default the view cache is disabled, and can be enabled with a given cache lifetime for each action with the configuration file cache.xml. See the configuration section for more information.

For a WebPageView, the result is cached if the view cache is enabled, a 200 response status code has been set and the request method was not of type POST. Additionally, Expires and Cache-Control headers are also set on the http response object with the view cache lifetime.

### Snippet cache
The PhpTemplateView class (and the WebPageView that extends it) implements a snippet cache, stored within the ./cache/snippet directory of the application. The snippet cache is a mechanism of caching snippets of view content as an alternative to caching the full view result.

Due to the use of the snippet cache being explicit it is enabled by default, however can be disabled for each action by setting the option use-snippet-cache to false within the configuration file cache.xml. See the configuration section for more information.

For a WebPageView, the snippet cache is disabled if the response status code is not 200 or the request method is of type POST. Example usage,

```
<body>
<?php $this->cache('menu', 3600); ?>
<div id="menu">Imagine a cool menu here!</div>
<?php $this->save(); ?>
...
</body>
```

In this example, the first parameter is a key for the cached content, unique to the current action and the second parameter specifies a lifetime in seconds. If the second parameter is not provided the configured lifetime of the current action set within cache.xml will be used. See the configuration section for more information.

### Clearing the cache
A default "task" module is provided within the common plugin, enabled by default, which provides a "clear-cache" action used for clearing the cache. This can be run from within any application with the following,

```bash
php ./bin/task.php clear-cache
```

The cache can also be cleared on a more granular level. An optional type parameter can be specified as either config or view (view also includes snippet cache) and in the case of clearing the view cache, an optional module parameter can be specified to clear the cache of just that module. For example,

```bash
php ./bin/task.php clear-cache --type=config
php ./bin/task.php clear-cache --type=view
php ./bin/task.php clear-cache --type=view --module=myModule
```

It is also safe to manually delete the subdirectories within the ./cache directory.
