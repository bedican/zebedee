# Zebedee

[Home](../README.md) - Configuration

## Configuration

- [Application (app.xml)](#markdown-header-application)
- [Web controller (web.xml)](#markdown-header-web-controller)
- [Routing (routing.xml)](#markdown-header-routing)
- [View cache (cache.xml)](#markdown-header-view-cache)
- [Web page view assets (assets.xml)](#markdown-header-web-page-view-assets)
- [Plugin (plugin.xml)](#markdown-header-plugin)
- [Database (db.xml)](#markdown-header-database)
- [Schema (schema.xml)](#markdown-header-schema)

### Application
Application configuration is held within the file app.xml, and is loaded and merged from multiple locations in the following order,

- ./config directory within zebedee
- ./config directory within the application (values overwritten)
- ./config directory within each plugin in the order the plugins were enabled (values not overwritten)

The application configuration has the following options,

| Option                                    | Description                                                       |
|-------------------------------------------|-------------------------------------------------------------------|
| /application/enabled-plugins/plugin       | A list of enabled plugins, (see plugins section).                 |
| /application/plugin-modules/plugin-module | A list of enabled plugin modules, (see plugins section).          |
| /application/logging/level                | A list of log levels to log, either: all, debug, notice or error. |
| /application/use-config-cache             | Whether to cache compiled configuration, (see cache section).     |

The list of enabled plugins and plugin modules are ignored for configuration files within any plugin. This avoids one plugin being able to enable another, and ensures explicit consent is required at the application level.

Additional configuration can also be specified and accessed from the application config instance. For example,

```xml
<application>

	<single-value>foo</single-value>

	<nested-value>
		<value>bar</value>
	</nested-value>

	<list-values>
		<item>one</item>
		<item>two</item>
		<item>three</item>
	</list-values>

</application>
```
Can be accessed with:

```
<?php

namespace module\foo;

use framework\module\AbstractWebModule;

class FooModule extends AbstractWebModule
{
	protected function doMyAction()
	{
		$config = $this->getApplication()->getConfig();

		$single = $config->getValue('single-value');
		$nested = $config->getValue('nested-value.value');
		$list = $config->getValue('list-values.item');
	}
}
```

### Web controller
Web controller configuration is held within the file web.xml, and is loaded and merged from multiple locations in the following order,

- ./config directory within zebedee
- ./config directory within each plugin in the order the plugins were enabled (values overwritten)
- ./config directory within the application (values overwritten)

The web controller configuration has the following options,

| Option            | Description                                         |
|-------------------|-----------------------------------------------------|
| /web/error-module | The module containing the exception handler action. |
| /web/error-action | The exception handler action.                       |
| /web/upload-dir   | The upload directory used by the form framework.    |

As like app.xml above, additional configuration can also be specified and accessed from the web controller config instance. For example,

```
<?php

namespace module\foo;

use framework\module\AbstractWebModule;

class FooModule extends AbstractWebModule
{
	protected function doMyAction()
	{
		$foo = $this->getConfig()->getValue('foo');
	}
}
```

### Routing
Routing configuration is held within the file routing.xml, and is loaded and merged from multiple locations in the following order,

- /config directory within zebedee
- /config directory within each plugin in the order the plugins were enabled (values overwritten)
- /config directory within the application (values overwritten)

The routing configuration has the following options,

| Option                                | Description                                                         |
|---------------------------------------|---------------------------------------------------------------------|
| /routing/config/default-action        | The action substituted for the "default" action within routing.xml. |
| /routing/config/default-action-suffix | The default action suffix used when matching route paths.           |
| /routing/config/error404-module       | The module holding the error 404 action.                            |
| /routing/config/error404-action       | The error 404 action.                                               |
| /routing/routes                       | The defined routes, see below.                                      |

Routes are specified with a path mapping to a module, action and layout, (and other optional parameters). A route is searched for against each route path in the order defined until a match is found. For example,

```xml
<routing>
	<routes>

		<route name="my-module">
			<path>/path/to/index.html</path>
			<params>
				<param name="module">myModule</param>
				<param name="action">myAction</param>
			</params>
		</route>

		<route name="my-module-page">
			<path>/path/to/{page}.html</path>
			<params>
				<param name="module">myModule</param>
				<param name="action">myPage</param>
				<param name="layout">false</param>
			</params>
		</route>

		<route name="module-action">
			<path>/{module}/{action}/index.html</path>
			<params>
				<param name="layout">common</param>
			</params>
		</route>

		<route name="action">
			<path>/{action}.html</path>
		</route>

	</routes>
</routing>
```

The example configuration above will translate the routes as follows,

| Route          | Description |
|----------------|-------------|
| my-module      | Matches the uri /path/to/index.html and runs the myAction action within the myModule module. The "default" layout will be used. |
| my-module-page | Matches the uri /path/to/{page}.html and runs the myPage action within the myModule module with the page parameter set to the matching text of the {page} placeholder within the uri. A layout will not be used as it is set to false. |
| module-action  | Matches the uri /{module}/{action}/index.html and runs the action and module specified with the {action} and {module} placeholders. For the action, the value specified within /routing/config/default-action will be translated to the "default" action. The common layout will be used. |
| action         | Matches the uri /{action}.html and runs the action specified with the {action} placeholder within the default web module "web". The value specified within /routing/config/default-action will be translated to the "default" action, for example /index.html will run the "default" action. The "default" layout will be used. |

The current route instance provides an interface to obtain the parameters of the currently matched route.

```
<?php

namespace module\myModule;

use framework\module\AbstractWebModule;

class MyModuleModule extends AbstractWebModule
{
	protected function doMyPage()
	{
		$page = $this->getCurrentRoute()->getParameter('page');
	}
}
```

### View cache
View cache configuration is held within the file cache.xml, and is loaded and merged from multiple locations in the following order,

- ./config directory within zebedee
- ./config directory within the application (values overwritten)
- ./config directory within the current module (values overwritten)

The view cache configuration specifies which actions have the view cache enabled and the cache lifetime. An action name can also be specified as "\*" for all actions or a comma separated list. For example, (the action myAction is cached for 3600 seconds),

```xml
<cache>

	<action name="myAction">
		<enabled>true</enabled>
		<lifetime>3600</lifetime>
	</action>

</cache>
```

When creating views, additional cache configuration can also be specified and accessed from within the view instance. As in the case of the snippet cache and PhpTemplateView, see the cache section for more information. Configuration is accessed with the protected getCacheConfigValue method on AbstractView.

```xml
<cache>

	<action name="myAction">
		<enabled>true</enabled>
		<lifetime>3600</lifetime>
		<config>
			<use-snippet-cache>false</use-snippet-cache>
		</config>
	</action>

</cache>
```

### Web page view assets
Web page asset configuration is held within the file assets.xml, and is loaded and merged from multiple locations in the following order,

- ./config directory within zebedee
- ./config directory within the application (values overwritten)
- ./config directory within the current module (values overwritten)

The web page view asset configuration specifies which actions have which javascript and stylesheet files. Paths can either be absolute or relative to the js and css subdirectories within the applications public web directory. For example,

```xml
<assets>

	<action name="myAction">
		<javascripts>
			<javascript>http://www.example.com/jquery-min.js</javascript>
		</javascripts>
		<stylesheets>
			<stylesheet>common.css</stylesheet>
			<stylesheet>site-v1.css</stylesheet>
		</stylesheets>
	</action>

</assets>
```

### Plugin
Plugin configuration is held within the file plugin.xml in the plugin config directory and configuration can be accessed from the plugin config instance. For example,

```xml
<plugin>

	<foo>bar</foo>

</plugin>
```

Can be accessed with,

```
<?php

namespace module\foo;

use framework\module\AbstractWebModule;

class FooModule extends AbstractWebModule
{
	protected function doMyAction()
	{
		$plugin = $this->getApplication()->getPlugin('myPlugin');
		$foo = $plugin->getConfig()->getValue('foo');
	}
}
```

### Database
Database configuration is held within the file db.xml, and is loaded and merged from multiple locations in the following order,

- ./config directory within zebedee
- ./config directory within each plugin in the order the plugins were enabled (values overwritten)
- ./config directory within the application (values overwritten)

See the databases section for more information on configuration options.

### Schema
Schema configuration is held within the file schema.xml, and is loaded and merged from multiple locations in the following order,

- ./config directory within zebedee
- ./config directory within each plugin in the order the plugins were enabled (values overwritten)
- ./config directory within the application (values overwritten)

See the databases section for more information on configuration options.
