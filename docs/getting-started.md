# Zebedee

[Home](../README.md) - Getting started

## Getting Started

- [Installing](#markdown-header-installing)
- [Creating an application](#markdown-header-creating-an-application)

### Installing
To install zebedee, clone the repository to any location with the desired release tag.

Zebedee can be installed in a common location for all applications, or in a location under each application. The connection between zebedee and the application is via a PHP include statement.

The recommended approach is to install each zebedee release in a common location, and link each application to the
desired release. For example,

```
/home/www/zebedee/zebedee-0.1
/home/www/zebedee/zebedee-0.2
```

### Creating an application
Once zebedee is installed you can create an application. This is done by changing your working directory to where you wish to install your new application and running the create-application task.

When running the create-application task, it should be run from the zebedee release you wish to base your application on. For example,

```bash
cd /home/www
php <zebedee-path>/apps/default/bin/task.php create-application --name=myapp
```

This will create a myapp directory containing your new application. The application is linked to zebedee from an include statement within the file ./myapp/config/config.inc.php.

If you are creating a web application, you will also need to create a virtual host within your webserver. A .htaccess file exists within the public web directory of your new application to rewrite all urls to the file index.php. You may wish to move the configuration to the virtual host instead.

An example virtual host for apache is shown below.

```
<VirtualHost *:80>
        ServerName www.myapp.com
        DocumentRoot /home/www/myapp/web
        <Directory "/home/www/myapp/web">
                Options -Indexes FollowSymLinks
                AllowOverride All
                Order allow,deny
                Allow from all
        </Directory>
</VirtualHost>
```

If you are creating an application requiring scheduling of tasks, you will also need to configure the scheduling mechanism to point to the task scheduler entry point ./bin/task-scheduler.sh and invoke the entry point once per minute.

An example configuration for cron is shown below,

```
* * * * * /home/www/myapp/bin/task-scheduler.sh
```
