# Zebedee

[Home](../README.md) - Views

## Views

- [Views and writers](#markdown-header-views-and-writers)
- [Default views](#markdown-header-default-views)
- [Web page view](#markdown-header-web-page-view)
- [Changing views](#markdown-header-changing-views)
- [Creating a view](#markdown-header-creating-a-view)

### Views and writers
Views are used by modules to produce a formatted output of data, and writers are used by the view to write the formatted output to a device or stream. This enables a view to be written to a different medium under different conditions.

### Default views
By default, the web controller configures all modules with a WebPageView, which is a PhpTemplateView with additional functionality suited to web pages; making use of a ResponseWriter to write the result to a http response object, and handling stylesheet and javascript assets via the assets.xml configuration file. See the configuration section for more information.

For the task and task scheduler controllers, by default, modules are configured without a view.

### Web page view
The web page view (WebPageView) is a PhpTemplateView and uses php templates located within the views directory of the module with the filename of the same name as the action. If a php template does not exist for the action, the web controller will not set a view on the module.

Template variables can be set on a PhpTemplateView using the setVar method making them available from within the template. For example,

```
<?php

namespace module\foo;

use framework\module\AbstractWebModule;

class FooModule extends AbstractWebModule
{
	protected function doMyAction()
	{
		$this->getView()->setVar('myVar', 'Helo World !');
	}
}
```

Makes the variable available within the template ./modules/foo/views/myAction.php

```
<?php echo $myVar; ?>
```

The WebPageView makes available the following variables in all templates,

| Variable     | Description                                                                              |
|--------------|------------------------------------------------------------------------------------------|
| $routePath   | A string noting the current module and action, with the format: <module>.<action>        |
| $route       | The current route object.                                                                |
| $routing     | Routing object, providing an interface to the routing configuration.                     |
| $javascripts | An array of javascript include uris. See the configuration section for more information. |
| $stylesheets | An array of stylesheet include uris. See the configuration section for more information. |

The routing object provides an interface to the routing configuration to generate urls from a given action and module. This allows route paths to be modified within the routing.xml configuration file without the need to update all templates. See the configuration section for more information. For example,

```
<?php echo $routing->getUrl('myModule.myAction'); ?>
```

### Changing views
A modules view can be changed within the action using the setView method on the module object. Alternatively, to change the default view at the controller level for all modules, the controller can be extended and the method getView can be overriden.

### Creating a view
To create a view, the View interface should be implemented. The abstract class AbstractView is available to make this process easier, which implements the built in caching functionality of views and holds a PrintWriter by default. When extending the AbstractView class, the method getContent needs to be implemented.
