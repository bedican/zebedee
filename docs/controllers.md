# Zebedee

[Home](../README.md) - Controllers

## Controllers

- [The web controller](#markdown-header-the-web-controller)
- [The task controller](#markdown-header-the-task-controller)
- [The task scheduler controller](#markdown-header-the-task-scheduler-controller)

### The web controller
The web controller is the web entry point to the application and is used to invoke modules implementing the WebModule interface.

Access to the web controller is exposed by the publicly accessible file ./web/index.php. The controller determines which module and action to run from the request uri and configured routing, held within the file routing.xml. See the configuration section for more information.

By default, the web controller configures the module with an instance of WebPageView. A different view can be used, either by setting it from within the module itself, or by extending the web controller and overriding the getView method. If an extended web controller is used, index.php from within the public web directory should be updated to use the new web controller.

Web controller configuration is held within the file web.xml. See the configuration section for more information.

### The task controller
The task controller is the cli entry point to the application and is used to invoke modules implementing the TaskModule interface.

Access to the task controller is exposed by the file ./bin/task.php. The controller determines which module and action to run from the first and second parameters. If only one parameter is given, that action is run on the "task" module, if no parameters are given, the action "default" is run on the "task" module. For example,

```bash
php ./bin/task.php <module> <action> [options]
php ./bin/task.php <action> [options]
php ./bin/task.php [options]
```

A default "task" module is provided within the common plugin, enabled by default, which provides a "clear-cache" action used for clearing the cache. See the plugins section for more information.

For all modules run from the task controller, console logging can be enabled with the --log-console option. This displays notice and debug log level messages to stdout, and error log level messsages to stderr.

By default, the task controller configures the module without a view. See the views section for more information.

### The task scheduler controller
The task scheduler controller is the cron entry point to the application and is used to invoke modules implementing the TaskSchedulerModule interface.

Access to the task scheduler controller is exposed by the file ./bin/task-scheduler.sh which should be invoked once per minute. The controller determines which module and action to run from a TaskSchedule instance which specifies which modules and actions to run at which times.

The TaskSchedule instance used by the controller is specified within the configuration file app.xml with the value specified at /application/task-schedule-class.

By default, the task scheduler controller configures the module without a view. See the views section for more information.
