<?php // (c) Copyright 2011 Bedican Solutions

use framework\lang\ClassLoader;

date_default_timezone_set('Europe/London');

include_once(dirname(__FILE__).'/../lib/lang/ClassLoader.class.php');
ClassLoader::getInstance()->bootstrap();
